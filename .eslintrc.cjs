module.exports = {
    "env": {
        "browser": true,
        "es2022": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "rules": {
    },

    // Note: the following s required to fix a linter error process not defined when parsing
    // rollup.xxx/config.js file
    // See SO: https://stackoverflow.com/questions/50894000/eslint-process-is-not-defined
    "globals": {
        "process": true
    }
};
