console.debug( "Executing script in ilktest-tools-subs.js module...");

// ILKTEST Substitutions
export function load_ilktest_database_tool_template(template )
{
    return template.replaceAll('__FACILITY__', 'ILKTEST' )
        .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
        .replaceAll('__JS_SUPPORT_FILE__', './ilktest-tools-support.js' )
}

export function load_ilktest_command_tool_template(template )
{
    return template.replaceAll('__FACILITY__', 'ILKTEST' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__JS_SUPPORT_FILE__', './ilktest-tools-support.js' )
}

export function load_ilktest_facility_overview_tool_template( template )
{
    return template.replaceAll('__FACILITY__', 'ILKTEST' )
        .replaceAll('__INTERLOCK_VIEW__', 'ilktest-main-view.html' )
        .replaceAll('__ILKCS_COMMAND_TOOL__', 'ilktest-ilkcs-command-tool.html' )
        .replaceAll('__ILKDB_EXPLORER_TOOL__', 'ilktest-ilkdb-explorer-tool.html' )
        .replaceAll('__SOFTWARE_ARCH__', '../ui/ilktest-software-arch.html' )
        .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
        .replaceAll('__CSS_SUPPORT_FILE__', './wc-register-view.css' )
        .replaceAll('__JS_SUPPORT_FILE__', './ilktest-tools-support.js' )
        .replaceAll('__VERSION__', '7.1.0' );
}

// PROSCAN Substitutions
export function load_proscan_database_tool_template(template )
{
    return template.replaceAll('__FACILITY__', 'PROSCAN' )
        .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
        .replaceAll('__JS_SUPPORT_FILE__', './proscan-tools-support.js' )
}

export function load_proscan_command_tool_template(template )
{
    return template.replaceAll('__FACILITY__', 'PROSCAN' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__JS_SUPPORT_FILE__', './proscan-tools-support.js' )
}

export function load_proscan_facility_overview_tool_template( template )
{
    return template.replaceAll('__FACILITY__', 'PROSCAN' )
        .replaceAll('__INTERLOCK_VIEW__', 'proscan-main-view.html' )
        .replaceAll('__ILKCS_COMMAND_TOOL__', 'proscan-ilkcs-command-tool.html' )
        .replaceAll('__ILKDB_EXPLORER_TOOL__', 'proscan-ilkdb-explorer-tool.html' )
        .replaceAll('__SOFTWARE_ARCH__', '../ui/proscan-software-arch.html' )
        .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
        .replaceAll('__CSS_SUPPORT_FILE__', './wc-register-view.css' )
        .replaceAll('__JS_SUPPORT_FILE__', './proscan-tools-support.js' )
        .replaceAll('__VERSION__', '7.1.0' );
}

// HIPA Substitutions
export function load_hipa_database_tool_template(template )
{
    return template.replaceAll('__FACILITY__', 'HIPA' )
        .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
        .replaceAll('__JS_SUPPORT_FILE__', './hipa-tools-support.js' )
}

export function load_hipa_command_tool_template(template )
{
    return template.replaceAll('__FACILITY__', 'HIPA' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__JS_SUPPORT_FILE__', './hipa-tools-support.js' )
}

export function load_hipa_facility_overview_tool_template( template )
{
    return template.replaceAll('__FACILITY__', 'HIPA' )
        .replaceAll('__INTERLOCK_VIEW__', 'hipa-main-view.html' )
        .replaceAll('__ILKCS_COMMAND_TOOL__', 'hipa-ilkcs-command-tool.html' )
        .replaceAll('__ILKDB_EXPLORER_TOOL__', 'hipa-ilkdb-explorer-tool.html' )
        .replaceAll('__SOFTWARE_ARCH__', '../ui/hipa-software-arch.html' )
        .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
        .replaceAll('__CSS_SUPPORT_FILE__', './wc-register-view.css' )
        .replaceAll('__JS_SUPPORT_FILE__', './hipa-tools-support.js' )
        .replaceAll('__VERSION__', '7.1.0' );
}

export function load_ists_database_tool_template(template )
{
    return template.replaceAll('__FACILITY__', 'ISTS' )
        .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
        .replaceAll('__JS_SUPPORT_FILE__', './ists-tools-support.js' )
}

export function load_ists_command_tool_template(template )
{
    return template.replaceAll('__FACILITY__', 'ISTS' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__JS_SUPPORT_FILE__', './ists-tools-support.js' )
}

export function load_ists_facility_overview_tool_template( template )
{
    return template.replaceAll('__FACILITY__', 'ISTS' )
        .replaceAll('__INTERLOCK_VIEW__', 'ists-main-view.html' )
        .replaceAll('__ILKCS_COMMAND_TOOL__', 'ists-ilkcs-command-tool.html' )
        .replaceAll('__ILKDB_EXPLORER_TOOL__', 'ists-ilkdb-explorer-tool.html' )
        .replaceAll('__SOFTWARE_ARCH__', '../ui/ists-software-arch.html' )
        .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
        .replaceAll('__CSS_SUPPORT_FILE__', './wc-register-view.css' )
        .replaceAll('__JS_SUPPORT_FILE__', './ists-tools-support.js' )
        .replaceAll('__VERSION__', '7.1.0' );
}

