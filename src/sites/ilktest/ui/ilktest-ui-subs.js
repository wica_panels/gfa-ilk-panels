console.debug( "Executing script in ilktest-ui-subs.js module...");

export function load_ilktest_main_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'ILKTEST' )
        .replaceAll( '__FACILITY_LC__', 'ilktest' )
        .replaceAll( '__JS_SUPPORT_FILE__', './ilktest-ui-support.js' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__ILKCS_PROBE_URL_PATH__','/v3/facilities/ilktest?detail=applicationVersionNumber' )
        .replaceAll( '__ILKCS_STREAM_URL_PATH__','/v3/facilities/ilktest/streams?detail=patchStream' )
        .replaceAll( '__ILKCS_ACK_URL_PATH__','/v3/facilities/ilktest?detail=applicationVersionNumber' )
        .replaceAll( '__VERSION__', '7.1.0' )
}

export function load_ilktest_configuration_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'ILKTEST' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__FACILITY_SVG_FILE__', 'ilktest.svg' )
        .replaceAll('__JS_SUPPORT_FILE__', './ilktest-ui-support.js' )
        .replaceAll('__FACILITY_CONFIG_PRIMARY_MODE__', 'Main' )
        .replaceAll('__FACILITY_CONFIG_SECONDARY_MODE__', 'AHK1' )
        .replaceAll('__FACILITY_CONFIG_TERTIARY_MODE__', 'VAR' )
        .replaceAll('__FACILITY_CONFIG_MODE_OPTION__', 'Options' )
}

export function load_ilktest_interlock_view_template( template ) {
    return template.replaceAll('__FACILITY__', 'ILKTEST')
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs')
        .replaceAll('__FACILITY_SVG_FILE__', 'ilktest.svg')
        .replaceAll('__JS_SUPPORT_FILE__', './ilktest-ui-support.js')
        .replaceAll('__FACILITY_CONFIG_PRIMARY_MODE__', 'Main')
        .replaceAll('__FACILITY_CONFIG_SECONDARY_MODE__', 'AHK1')
        .replaceAll('__FACILITY_CONFIG_TERTIARY_MODE__', 'VAR')
        .replaceAll('__FACILITY_CONFIG_MODE_OPTION__', 'Options')
}

export function load_ilktest_statistics_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'ILKTEST' )
}

export function load_ilktest_module_expert_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'ILKTEST' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__JS_SUPPORT_FILE__', './ilktest-ui-support.js' )
}

export function load_ilktest_quick_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'ILKTEST' )
        .replaceAll( '__JS_SUPPORT_FILE__', './ilktest-ui-support.js' )
}

export function load_ilktest_software_arch_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'ILKTEST' )
        .replaceAll('__SOFTWARE_ARCH_PNG_FILE__', 'proscan-software-arch.png' )
}

export function load_ilktest_user_guide_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'ILKTEST' )
        .replaceAll('__USER_GUIDE_PDF_FILE__', '../docs/hipa_software_user_guide_de.pdf' )
}

export function load_proscan_main_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'PROSCAN' )
        .replaceAll( '__FACILITY_LC__', 'proscan' )
        .replaceAll( '__JS_SUPPORT_FILE__', './proscan-ui-support.js' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__ILKCS_PROBE_URL_PATH__','/v3/facilities/proscan?detail=applicationVersionNumber' )
        .replaceAll( '__ILKCS_STREAM_URL_PATH__','/v3/facilities/proscan/streams?detail=patchStream' )
        .replaceAll( '__ILKCS_ACK_URL_PATH__','/v3/facilities/proscan?detail=applicationVersionNumber' )
        .replaceAll( '__VERSION__', '7.1.0' )
}

export function load_proscan_configuration_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'PROSCAN' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__FACILITY_SVG_FILE__', 'proscan.svg' )
        .replaceAll('__JS_SUPPORT_FILE__', './proscan-ui-support.js' )
        .replaceAll('__FACILITY_CONFIG_PRIMARY_MODE__', 'Main' )
        .replaceAll('__FACILITY_CONFIG_SECONDARY_MODE__', 'User' )
        .replaceAll('__FACILITY_CONFIG_TERTIARY_MODE__', 'Mode' )
        .replaceAll('__FACILITY_CONFIG_MODE_OPTION__', 'Options' )
}

export function load_proscan_interlock_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'PROSCAN')
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs')
        .replaceAll('__FACILITY_SVG_FILE__', 'proscan.svg')
        .replaceAll('__JS_SUPPORT_FILE__', './proscan-ui-support.js')
        .replaceAll('__FACILITY_CONFIG_PRIMARY_MODE__', 'Main' )
        .replaceAll('__FACILITY_CONFIG_SECONDARY_MODE__', 'User' )
        .replaceAll('__FACILITY_CONFIG_TERTIARY_MODE__', 'Mode' )
        .replaceAll('__FACILITY_CONFIG_MODE_OPTION__', 'Options' )
}

export function load_proscan_statistics_view_template( template ) {
    return template.replaceAll('__FACILITY__', 'PROSCAN')
        .replaceAll('__JS_SUPPORT_FILE__', './proscan-ui-support.js')
}

export function load_proscan_module_expert_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'PROSCAN' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__JS_SUPPORT_FILE__', './proscan-ui-support.js' )
}

export function load_proscan_quick_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'PROSCAN' )
        .replaceAll( '__JS_SUPPORT_FILE__', './proscan-ui-support.js' )
}

export function load_proscan_software_arch_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'PROSCAN' )
        .replaceAll('__SOFTWARE_ARCH_PNG_FILE__', 'proscan-software-arch.png' )
}

export function load_proscan_user_guide_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'ILKTEST' )
        .replaceAll('__USER_GUIDE_PDF_FILE__', '../docs/hipa_software_user_guide_de.pdf' )
}

export function load_hipa_main_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'HIPA' )
        .replaceAll( '__FACILITY_LC__', 'hipa' )
        .replaceAll( '__JS_SUPPORT_FILE__', './hipa-ui-support.js' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__ILKCS_PROBE_URL_PATH__','/v3/facilities/hipa?detail=applicationVersionNumber' )
        .replaceAll( '__ILKCS_STREAM_URL_PATH__','/v3/facilities/hipa/streams?detail=patchStream' )
        .replaceAll( '__ILKCS_ACK_URL_PATH__','/v3/facilities/hipa?detail=applicationVersionNumber' )
        .replaceAll( '__VERSION__', '7.1.0' )
}

export function load_hipa_configuration_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'HIPA' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__FACILITY_SVG_FILE__', 'hipa.svg' )
        .replaceAll('__JS_SUPPORT_FILE__', './hipa-ui-support.js' )
        .replaceAll('__FACILITY_CONFIG_PRIMARY_MODE__', 'Main' )
        .replaceAll('__FACILITY_CONFIG_SECONDARY_MODE__', 'IP' )
        .replaceAll('__FACILITY_CONFIG_TERTIARY_MODE__', 'UCN' )
        .replaceAll('__FACILITY_CONFIG_MODE_OPTION__', 'Intensity' )
}

export function load_hipa_interlock_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'HIPA' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__FACILITY_SVG_FILE__', 'hipa.svg' )
        .replaceAll('__JS_SUPPORT_FILE__', './hipa-ui-support.js' )
        .replaceAll('__FACILITY_CONFIG_PRIMARY_MODE__', 'Main' )
        .replaceAll('__FACILITY_CONFIG_SECONDARY_MODE__', 'IP' )
        .replaceAll('__FACILITY_CONFIG_TERTIARY_MODE__', 'UCN' )
        .replaceAll('__FACILITY_CONFIG_MODE_OPTION__', 'Intensity' )
}

export function load_hipa_statistics_view_template(template )
{
    return template.replaceAll('__FACILITY__', 'HIPA' )
        .replaceAll('__JS_SUPPORT_FILE__', './hipa-ui-support.js' )
}

export function load_hipa_quick_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'HIPA' )
        .replaceAll( '__JS_SUPPORT_FILE__', './hipa-ui-support.js' )
}

export function load_hipa_module_expert_view_template(template )
{
    return template.replaceAll( '__FACILITY__', 'HIPA' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__JS_SUPPORT_FILE__', './hipa-ui-support.js' )
}

export function load_hipa_software_arch_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'HIPA' )
        .replaceAll('__SOFTWARE_ARCH_PNG_FILE__', 'hipa-software-arch.png' )
}

export function load_hipa_user_guide_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'HIPA' )
        .replaceAll('__USER_GUIDE_PDF_FILE__', '../docs/hipa_software_user_guide_de.pdf' )
}

export function load_ists_main_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'ISTS' )
        .replaceAll( '__FACILITY_LC__', 'ists' )
        .replaceAll( '__JS_SUPPORT_FILE__', './ists-ui-support.js' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__ILKCS_PROBE_URL_PATH__','/v3/facilities/ists?detail=applicationVersionNumber' )
        .replaceAll( '__ILKCS_STREAM_URL_PATH__','/v3/facilities/ists/streams?detail=patchStream' )
        .replaceAll( '__ILKCS_ACK_URL_PATH__','/v3/facilities/ists?detail=applicationVersionNumber' )
        .replaceAll( '__VERSION__', '7.1.0' )
}

export function load_ists_configuration_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'ISTS' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__FACILITY_SVG_FILE__', 'ists.svg' )
        .replaceAll('__JS_SUPPORT_FILE__', './ists-ui-support.js' )
        .replaceAll('__FACILITY_CONFIG_PRIMARY_MODE__', 'Main' )
        .replaceAll('__FACILITY_CONFIG_SECONDARY_MODE__', 'IP' )
        .replaceAll('__FACILITY_CONFIG_TERTIARY_MODE__', 'UCN' )
        .replaceAll('__FACILITY_CONFIG_MODE_OPTION__', 'Intensity' )
}

export function load_ists_interlock_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'ISTS' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__FACILITY_SVG_FILE__', 'ists.svg' )
        .replaceAll('__JS_SUPPORT_FILE__', './ists-ui-support.js' )
        .replaceAll('__FACILITY_CONFIG_PRIMARY_MODE__', 'Main' )
        .replaceAll('__FACILITY_CONFIG_SECONDARY_MODE__', 'IP' )
        .replaceAll('__FACILITY_CONFIG_TERTIARY_MODE__', 'UCN' )
        .replaceAll('__FACILITY_CONFIG_MODE_OPTION__', 'Intensity' )
}

export function load_ists_statistics_view_template(template )
{
    return template.replaceAll('__FACILITY__', 'ISTS' )
        .replaceAll('__JS_SUPPORT_FILE__', './ists-ui-support.js' )
}

export function load_ists_quick_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'ISTS' )
        .replaceAll( '__JS_SUPPORT_FILE__', './ists-ui-support.js' )
}

export function load_ists_module_expert_view_template(template )
{
    return template.replaceAll( '__FACILITY__', 'ISTS' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__JS_SUPPORT_FILE__', './ists-ui-support.js' )
}

export function load_ists_software_arch_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'ISTS' )
        .replaceAll('__SOFTWARE_ARCH_PNG_FILE__', 'ists-software-arch.png' )
}

export function load_ists_user_guide_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'ISTS' )
        .replaceAll('__USER_GUIDE_PDF_FILE__', '../docs/hipa_software_user_guide_de.pdf' )
}