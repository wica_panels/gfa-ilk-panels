console.debug( "Executing script in ilktest-test-rpmip-subs.js module...");

export function load_rpmip_module_register_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-register-view.css' )
        .replaceAll('__RPMIP_MODULE_ID__', 'J124088' );
}

export function load_rpmip_module_state_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-event-view.css' )
        .replaceAll('__RPMIP_MODULE_ID__', 'J124088' );
}

export function load_rpmip_module_event_view_test_template(template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-event-view.css' )
        .replaceAll('__RPMIP_MODULE_ID__', 'J124088' );
}
