console.debug( "Executing script in ilktest-test-gen-ilk-signal-view-subs.js module...");

export function load_gen_ilk_signal_view_test_template( template )
{
    return template.replaceAll( '__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-signal-view.css' );
}
