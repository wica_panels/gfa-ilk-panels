console.debug( "Executing script in ilktest-test-usi2-subs.js module...");

export function load_usi2_board_event_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__USI2_SYSTEM_ID__', 'J3001000' )
        .replaceAll('__USI2_COMMON_ID__', 'J3001000' )
        .replaceAll('__USI2_MODULE_ID_2__', 'J3001001' )
        .replaceAll('__USI2_MODULE_ID_2__', 'J3001002' )
        .replaceAll('__USI2_MODULE_ID_3__', 'J3001003' );
}

export function load_usi2_board_register_view_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__USI2_SYSTEM_ID__', 'J3001000' )
        .replaceAll('__USI2_COMMON_ID__', 'J3001000' )
        .replaceAll('__USI2_MODULE_ID_2__', 'J3001001' )
        .replaceAll('__USI2_MODULE_ID_2__', 'J3001002' )
        .replaceAll('__USI2_MODULE_ID_3__', 'J3001003' );
}

export function load_usi2_common_event_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-event-view.css' )
        .replaceAll('__USI2_COMMON_ID__', 'J3001000' );
}

export function load_usi2_common_register_view_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-register-view.css' )
        .replaceAll('__USI2_COMMON_ID_1__', 'J3001000' );
}

export function load_usi2_module_event_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-event-view.css' )
        .replaceAll('__USI2_MODULE_ID__', 'J3001001' );
}

export function load_usi2_module_register_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-register-view.css' )
        .replaceAll('__USI2_MODULE_ID__', 'J3001001' );
}

export function load_usi2_module_state_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-event-view.css' )
        .replaceAll('__USI2_MODULE_ID__', 'J3001001' );
}

export function load_usi2_system_event_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-event-view.css' )
        .replaceAll('__USI2_SYSTEM_ID__', 'J3001000' );
}

export function load_usi2_system_register_view_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-register-view.css' )
        .replaceAll('__USI2_SYSTEM_ID_1__', 'J3001000' )
        .replaceAll('__USI2_SYSTEM_ID_2__', 'J3002000' )
        .replaceAll('__USI2_SYSTEM_ID_3__', 'J3003000' );
}