console.debug( "Executing script in ilktest-test-siio-subs.js module...");

export function load_siio_module_register_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-register-view.css' )
        .replaceAll('__SIIO_MODULE_ID_1__', 'J050036' )
        .replaceAll('__SIIO_MODULE_ID_2__', 'J050148' );
}

export function load_siio_module_state_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-event-view.css' )
        .replaceAll('__SIIO_MODULE_ID__', 'J050131' );
}

export function load_siio_module_event_view_test_template (template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-event-view.css' )
        .replaceAll('__SIIO_MODULE_ID__', 'J050036' );
}
