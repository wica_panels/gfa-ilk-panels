console.debug( "Executing script in ilktest-test-subs.js module...");

export function load_ilktest_board_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', './ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', './wc-event-view.css' )
        .replaceAll('__USI2_SYSTEM_ID_1__', 'J3001000'  )
        .replaceAll('__USI2_COMMON_ID_1__', 'J3001000'  )
        .replaceAll('__USI2_MODULE_ID_11__', 'J3001001' )
        .replaceAll('__USI2_MODULE_ID_12__', 'J3001002' )
        .replaceAll('__USI2_MODULE_ID_13__', 'J3001003' )

        .replaceAll('__USI2_SYSTEM_ID_2__', 'J3002000'  )
        .replaceAll('__USI2_COMMON_ID_2__', 'J3002000'  )
        .replaceAll('__USI2_MODULE_ID_21__', 'J3002001' )
        .replaceAll('__USI2_MODULE_ID_22__', 'J3002002' )
        .replaceAll('__USI2_MODULE_ID_23__', 'J3002003' )

        .replaceAll('__USI2_SYSTEM_ID_3__', 'J3003000'  )
        .replaceAll('__USI2_COMMON_ID_3__', 'J3003000'  )
        .replaceAll('__USI2_MODULE_ID_31__', 'J3003001' )
        .replaceAll('__USI2_MODULE_ID_32__', 'J3003002' )
        .replaceAll('__USI2_MODULE_ID_33__', 'J3003003' )

        .replaceAll('__RPMIP_SYSTEM_ID__', 'J3003000'  )
        .replaceAll('__RPMIP_MODULE_ID_1__', 'J002004' )
        .replaceAll('__RPMIP_MODULE_ID_2__', 'J002006' )
        .replaceAll('__RPMIP_MODULE_ID_3__', 'J002037' )
        .replaceAll('__RPMIP_MODULE_ID_4__', 'J002097' );
}