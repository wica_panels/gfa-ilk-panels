console.debug( "Executing script in ilktest-test-wica-subs.js module...");

export function load_wica_data_element_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-event-view.css' )
        .replaceAll('__RPMIP_MODULE_ID__', 'J002004' );
}

export function load_wica_data_group_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', '../ilktest-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', '../wc-event-view.css' )
        .replaceAll('__RPMIP_MODULE_ID__', 'J002004' );
}
