import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import copy from "rollup-plugin-copy";

import * as ILKTEST_TOOLS_SUBS from "./tools/ilktest-tools-subs.js";
import * as ILKTEST_UI_SUBS from "./ui/ilktest-ui-subs.js";
import * as ILKTEST_TEST_SUBS from "./test/ilktest-test-subs.js";
import * as ILKTEST_TEST_WICA_SUBS from "./test/ilktest-test-wica-subs.js";
import * as ILKTEST_TEST_SIIO_SUBS from "./test/ilktest-test-siio-subs.js";
import * as ILKTEST_TEST_RPMIP_SUBS from "./test/ilktest-test-rpmip-subs.js";
import * as ILKTEST_TEST_USI2_SUBS from "./test/ilktest-test-usi2-subs.js";
import * as ILKTEST_TEST_GEN_SUBS from "./test/ilktest-test-gen-subs.js";


// noinspection JSUnusedGlobalSymbols
export default () => {

    // eslint-disable-next-line no-undef
    const buildTarget = process.env.BUILD_PROD ? "build/ilktest_prod" : "build/ilktest_dev";
    const indexFile = process.env.BUILD_PROD ? "ilktest-prod-index.html" : "ilktest-dev-index.html";

    // Install ISTS Tools
    return [
    {
        input: 'src/sites/ists/tools/ists-tools-support.js',
        output: {
            dir: buildTarget + '/tools',
            format: 'es'
        },
        plugins: [
            commonjs(),
            resolve(),
            copy({
                targets: [
                    // Install ISTS Command Tool
                    { src: "src/common/tools/templates/command-tool/command-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/command-tool/command-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_ists_command_tool_template( contents.toString() ),
                        rename: "ists-command-tool.html"
                    },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.js", dest:buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_ists_command_tool_template( contents.toString() ),
                        rename: "ists-command-tool.js",
                    },

                    // Install ISTS Database Tool
                    { src: "src/common/tools/templates/database-tool/database-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/database-tool/database-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.html", dest: buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_ists_database_tool_template( contents.toString() ),
                        rename: "ists-database-tool.html",
                    },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.js", dest: buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_ists_database_tool_template( contents.toString() ),
                        rename: "ists-database-tool.js",
                    },

                    // Install ISTS Facility Overview Tool
                    {
                        src: "src/common/tools/templates/facility-overview-tool.template.html", dest: buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_ists_facility_overview_tool_template( contents.toString() ),
                        rename: "ists-facility-overview-tool.html",
                    }
                ]
            } )
        ]
    },
        
    // Install HIPA Tools
    {
        input: 'src/sites/hipa/tools/hipa-tools-support.js',
        output: {
            dir: buildTarget + '/tools',
            format: 'es'
        },
        plugins: [
            commonjs(),
            resolve(),
            copy({
                targets: [
                    // Install HIPA Command Tool
                    { src: "src/common/tools/templates/command-tool/command-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/command-tool/command-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_hipa_command_tool_template( contents.toString() ),
                        rename: "hipa-command-tool.html"
                    },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.js", dest:buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_hipa_command_tool_template( contents.toString() ),
                        rename: "hipa-command-tool.js",
                    },

                    // Install HIPA Database Tool
                    { src: "src/common/tools/templates/database-tool/database-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/database-tool/database-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.html", dest: buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_hipa_database_tool_template( contents.toString() ),
                        rename: "hipa-database-tool.html",
                    },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.js", dest: buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_hipa_database_tool_template( contents.toString() ),
                        rename: "hipa-database-tool.js",
                    },

                    // Install HIPA Facility Overview Tool
                    {
                        src: "src/common/tools/templates/facility-overview-tool.template.html", dest: buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_hipa_facility_overview_tool_template( contents.toString() ),
                        rename: "hipa-facility-overview-tool.html",
                    }
                ]
            } )
        ]
    },

    // Install PROSCAN Tools
    {
        input: 'src/sites/proscan/tools/proscan-tools-support.js',
        output: {
            dir: buildTarget + '/tools',
            format: 'es'
        },
        plugins: [
            commonjs(),
            resolve(),
            copy({
                targets: [
                    // Install PROSCAN Command Tool
                    { src: "src/common/tools/templates/command-tool/command-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/command-tool/command-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_proscan_command_tool_template( contents.toString() ),
                        rename: "proscan-command-tool.html",
                    },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.js", dest:buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_proscan_command_tool_template( contents.toString() ),
                        rename: "proscan-command-tool.js",
                    },

                    // Install PROSCAN Database Tool
                    { src: "src/common/tools/templates/database-tool/database-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/database-tool/database-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.html", dest: buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_proscan_database_tool_template( contents.toString() ),
                        rename: "proscan-database-tool.html",
                    },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.js", dest: buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_proscan_database_tool_template( contents.toString() ),
                        rename: "proscan-database-tool.js",
                    },

                    // Install PROSCAN Facility Overview Tool
                    {
                        src: "src/common/tools/templates/facility-overview-tool.template.html", dest: buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_proscan_facility_overview_tool_template( contents.toString() ),
                        rename: "proscan-facility-overview-tool.html",
                    },
                ]
            })
        ]
    },

    // Install ILKTEST Tools
    {
        input: 'src/sites/ilktest/tools/ilktest-tools-support.js',
        output: {
            dir: buildTarget + '/tools',
            format: 'es'
        },
        plugins: [
            resolve(),
            commonjs(),
            copy({
                targets: [
                    // Install ILKTEST Index
                    { src: "src/sites/ilktest/" + indexFile, dest: buildTarget, rename: "index.html" },
                    { src: "src/common/assets/background.jpg", dest: buildTarget },
                    { src: "src/common/tools/assets/facility-overview-vars.css", dest: buildTarget + "/tools" },
                    { src: "src/common/assets/favicon.ico", dest: buildTarget },

                    // Install ILKTEST Tool Component Support Assets
                    { src: "src/common/assets/wc-register-view.css", dest: buildTarget + "/tools" },
                    { src: "src/common/assets/wc-event-view.css", dest: buildTarget + "/tools" },
                    { src: "src/common/assets/bolt.svg", dest: buildTarget + "/tools" },

                    // Install ILKTEST Command Tool
                    { src: "src/common/tools/templates/command-tool/command-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/command-tool/command-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_ilktest_command_tool_template( contents.toString() ),
                        rename: "ilktest-command-tool.html",
                    },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.js", dest:buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_ilktest_command_tool_template( contents.toString() ),
                        rename: "ilktest-command-tool.js",
                    },

                    // Install ILKTEST Database Tool
                    { src: "src/common/tools/templates/database-tool/database-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/database-tool/database-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_ilktest_database_tool_template( contents.toString() ),
                        rename: "ilktest-database-tool.html",
                    },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.js", dest:buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_ilktest_database_tool_template( contents.toString() ),
                        rename: "ilktest-database-tool.js",
                    },

                    // Install ILKTEST Facility Overview Tool
                    {
                        src: "src/common/tools/templates/facility-overview-tool.template.html", dest: buildTarget + "/tools" ,
                        transform: (contents) => ILKTEST_TOOLS_SUBS.load_ilktest_facility_overview_tool_template( contents.toString() ),
                        rename: "ilktest-facility-overview-tool.html",
                    },
                ],
            } ),
        ] },

        // Install ShoeLace Style
        {
            input: 'node_modules/@shoelace-style/shoelace/dist/shoelace.js',
            output: {
                dir: buildTarget + '/ui/shoelace',
                format: 'es',
            },
            plugins: [
                resolve(),
                copy({
                    targets: [
                        { src: "node_modules/@shoelace-style/shoelace/dist/themes/light.css", dest: buildTarget + '/ui/shoelace', rename: "shoelace-theme-light.css" },
                        { src: "node_modules/@shoelace-style/shoelace/dist/assets", dest: buildTarget + '/ui/shoelace',  },
                    ],
                }),
            ]
        },
        // Install ISTS UI
        {
            input:  'src/sites/ists/ui/ists-ui-support.js',
            output: {
                dir: buildTarget + '/ui',
                format: 'es'
            },
            plugins: [
                commonjs(),
                resolve(),
                copy({
                    targets: [
    
                        // Install ISTS Main View Components
                        { src: "src/common/ui/templates/main-view/main-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/main-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/main-view-legend.svg", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/view-manager.js", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/main-view/main-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_main_view_template( contents.toString() ),
                            rename: "ists-main-view.html",
                        },
                        {
                            src: "src/common/ui/templates/main-view/main-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_main_view_template( contents.toString() ),
                            rename: "ists-main-view.js",
                        },
                        // Install ISTS Configuration View Components
                        { src: "src/common/ui/templates/configuration-view/configuration-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/configuration-view/configuration-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/configuration-view/configuration-view-legend.svg", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/configuration-view/configuration-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_configuration_view_template( contents.toString() ),
                            rename: "ists-configuration-view.html",
                        },
                        {
                            src: "src/common/ui/templates/configuration-view/configuration-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_configuration_view_template( contents.toString() ),
                            rename: "ists-configuration-view.js",
                        },
    
                        // Install ISTS Interlock View Components
                        { src: "src/common/ui/templates/interlock-view/interlock-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/interlock-view/interlock-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/interlock-view/interlock-view-legend.svg", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/interlock-view/interlock-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_interlock_view_template( contents.toString() ),
                            rename: "ists-interlock-view.html",
                        },
                        {
                            src: "src/common/ui/templates/interlock-view/interlock-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_interlock_view_template( contents.toString() ),
                            rename: "ists-interlock-view.js",
                        },
    
                        // Install ISTS Statistics View Components
                        { src: "src/common/ui/templates/statistics-view/statistics-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/statistics-view/statistics-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/statistics-view/statistics-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_statistics_view_template( contents.toString() ),
                            rename: "ists-statistics-view.html",
                        },
                        {
                            src: "src/common/ui/templates/statistics-view/statistics-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_statistics_view_template( contents.toString() ),
                            rename: "ists-statistics-view.js",
                        },
    
                        // Install ISTS Quick View Components
                        { src: "src/common/ui/templates/quick-view/quick-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/quick-view/quick-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/quick-view/quick-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_quick_view_template( contents.toString() ),
                            rename: "ists-quick-view.html",
                        },
                        {
                            src: "src/common/ui/templates/quick-view/quick-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_quick_view_template( contents.toString() ),
                            rename: "ists-quick-view.js",
                        },
    
                        // Install ISTS Module Expert View Components
                        { src: "src/common/ui/templates/module-expert-view/module-expert-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/module-expert-view/module-expert-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/module-expert-view/module-expert-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_module_expert_view_template( contents.toString() ),
                            rename: "ists-module-expert-view.html",
                        },
                        {
                            src: "src/common/ui/templates/module-expert-view/module-expert-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_module_expert_view_template( contents.toString() ),
                            rename: "ists-module-expert-view.js",
                        },
    
                        // Install ISTS Software Arch View Components
                        { src: "src/common/ui/templates/software-arch-view/software-arch-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/software-arch-view/software-arch-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/software-arch-view/software-arch-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_software_arch_view_template( contents.toString() ),
                            rename: "ists-software-arch-view.html",
                        },
    
                        // Install ISTS User Guide View Components
                        { src: "src/common/ui/templates/user-guide-view/user-guide-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/user-guide-view/user-guide-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/user-guide-view/user-guide-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ists_user_guide_view_template( contents.toString() ),
                            rename: "ists-user-guide-view.html",
                        },
                    ],
                } )
            ]
        },
        
        // Install HIPA UI
        {
            input:  'src/sites/hipa/ui/hipa-ui-support.js',
            output: {
                dir: buildTarget + '/ui',
                format: 'es'
            },
            plugins: [
                commonjs(),
                resolve(),
                copy({
                    targets: [

                        // Install HIPA Main View Components
                        { src: "src/common/ui/templates/main-view/main-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/main-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/main-view-legend.svg", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/view-manager.js", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/main-view/main-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_main_view_template( contents.toString() ),
                            rename: "hipa-main-view.html",
                        },
                        {
                            src: "src/common/ui/templates/main-view/main-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_main_view_template( contents.toString() ),
                            rename: "hipa-main-view.js",
                        },
                        // Install HIPA Configuration View Components
                        { src: "src/common/ui/templates/configuration-view/configuration-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/configuration-view/configuration-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/configuration-view/configuration-view-legend.svg", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/configuration-view/configuration-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_configuration_view_template( contents.toString() ),
                            rename: "hipa-configuration-view.html",
                        },
                        {
                            src: "src/common/ui/templates/configuration-view/configuration-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_configuration_view_template( contents.toString() ),
                            rename: "hipa-configuration-view.js",
                        },

                        // Install HIPA Interlock View Components
                        { src: "src/common/ui/templates/interlock-view/interlock-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/interlock-view/interlock-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/interlock-view/interlock-view-legend.svg", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/interlock-view/interlock-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_interlock_view_template( contents.toString() ),
                            rename: "hipa-interlock-view.html",
                        },
                        {
                            src: "src/common/ui/templates/interlock-view/interlock-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_interlock_view_template( contents.toString() ),
                            rename: "hipa-interlock-view.js",
                        },

                        // Install HIPA Statistics View Components
                        { src: "src/common/ui/templates/statistics-view/statistics-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/statistics-view/statistics-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/statistics-view/statistics-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_statistics_view_template( contents.toString() ),
                            rename: "hipa-statistics-view.html",
                        },
                        {
                            src: "src/common/ui/templates/statistics-view/statistics-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_statistics_view_template( contents.toString() ),
                            rename: "hipa-statistics-view.js",
                        },

                        // Install HIPA Quick View Components
                        { src: "src/common/ui/templates/quick-view/quick-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/quick-view/quick-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/quick-view/quick-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_quick_view_template( contents.toString() ),
                            rename: "hipa-quick-view.html",
                        },
                        {
                            src: "src/common/ui/templates/quick-view/quick-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_quick_view_template( contents.toString() ),
                            rename: "hipa-quick-view.js",
                        },

                        // Install HIPA Module Expert View Components
                        { src: "src/common/ui/templates/module-expert-view/module-expert-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/module-expert-view/module-expert-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/module-expert-view/module-expert-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_module_expert_view_template( contents.toString() ),
                            rename: "hipa-module-expert-view.html",
                        },
                        {
                            src: "src/common/ui/templates/module-expert-view/module-expert-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_module_expert_view_template( contents.toString() ),
                            rename: "hipa-module-expert-view.js",
                        },

                        // Install HIPA Software Arch View Components
                        { src: "src/common/ui/templates/software-arch-view/software-arch-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/software-arch-view/software-arch-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/software-arch-view/software-arch-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_software_arch_view_template( contents.toString() ),
                            rename: "hipa-software-arch-view.html",
                        },

                        // Install HIPA User Guide View Components
                        { src: "src/common/ui/templates/user-guide-view/user-guide-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/user-guide-view/user-guide-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/user-guide-view/user-guide-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_hipa_user_guide_view_template( contents.toString() ),
                            rename: "hipa-user-guide-view.html",
                        },
                     ],
                } )
            ]
        },

        // Install PROSCAN UI
        {
            input: 'src/sites/proscan/ui/proscan-ui-support.js',
            output: {
                dir: buildTarget + '/ui',
                format: 'es'
            },
            plugins: [
                commonjs(),
                resolve(),
                copy({
                    targets: [
                        // Install PROSCAN Main View Components
                        { src: "src/common/ui/templates/main-view/main-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/main-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/main-view-legend.svg", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/view-manager.js", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/main-view/main-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_main_view_template( contents.toString() ),
                            rename: "proscan-main-view.html",
                        },
                        {
                            src: "src/common/ui/templates/main-view/main-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_main_view_template( contents.toString() ),
                            rename: "proscan-main-view.js",
                        },

                        // Install PROSCAN Configuration View Components
                        { src: "src/common/ui/templates/configuration-view/configuration-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/configuration-view/configuration-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/configuration-view/configuration-view-legend.svg", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/configuration-view/configuration-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_configuration_view_template( contents.toString() ),
                            rename: "proscan-configuration-view.html",
                        },
                        {
                            src: "src/common/ui/templates/configuration-view/configuration-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_configuration_view_template( contents.toString() ),
                            rename: "proscan-configuration-view.js",
                        },

                        // Install PROSCAN Interlock View Components
                        { src: "src/common/ui/templates/interlock-view/interlock-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/interlock-view/interlock-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/interlock-view/interlock-view-legend.svg", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/interlock-view/interlock-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_interlock_view_template( contents.toString() ),
                            rename: "proscan-interlock-view.html",
                        },
                        {
                            src: "src/common/ui/templates/interlock-view/interlock-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_interlock_view_template( contents.toString() ),
                            rename: "proscan-interlock-view.js",
                        },

                        // Install PROSCAN Statistics View Components
                        { src: "src/common/ui/templates/statistics-view/statistics-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/statistics-view/statistics-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/statistics-view/statistics-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_statistics_view_template( contents.toString() ),
                            rename: "proscan-statistics-view.html",
                        },
                        {
                            src: "src/common/ui/templates/statistics-view/statistics-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_statistics_view_template( contents.toString() ),
                            rename: "proscan-statistics-view.js",
                        },

                        // Install PROSCAN Quick View Components
                        { src: "src/common/ui/templates/quick-view/quick-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/quick-view/quick-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/quick-view/quick-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_quick_view_template( contents.toString() ),
                            rename: "proscan-quick-view.html",
                        },
                        {
                            src: "src/common/ui/templates/quick-view/quick-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_quick_view_template( contents.toString() ),
                            rename: "proscan-quick-view.js",
                        },

                        // Install PROSCAN Module Expert View Components
                        { src: "src/common/ui/templates/module-expert-view/module-expert-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/module-expert-view/module-expert-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/module-expert-view/module-expert-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_module_expert_view_template( contents.toString() ),
                            rename: "proscan-module-expert-view.html",
                        },
                        {
                            src: "src/common/ui/templates/module-expert-view/module-expert-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_module_expert_view_template( contents.toString() ),
                            rename: "proscan-module-expert-view.js",
                        },

                        // Install PROSCAN Software Arch View Components
                        { src: "src/common/ui/templates/software-arch-view/software-arch-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/software-arch-view/software-arch-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/software-arch-view/software-arch-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_software_arch_view_template( contents.toString() ),
                            rename: "proscan-software-arch-view.html",
                        },

                        // Install PROSCAN User Guide View Components
                        { src: "src/common/ui/templates/user-guide-view/user-guide-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/user-guide-view/user-guide-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/user-guide-view/user-guide-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_proscan_user_guide_view_template( contents.toString() ),
                            rename: "proscan-user-guide-view.html",
                        },
                    ],
                }),
            ]
        },

        // Install ILKTEST UI
        {
            input: 'src/sites/ilktest/ui/ilktest-ui-support.js',
            output: {
                dir: buildTarget + '/ui',
                format: 'es'
            },
            plugins: [
                resolve(),
                commonjs(),
                copy({
                   targets: [
                       // Install ILKTEST UI Component Support Assets
                        { src: "src/common/assets/bolt.svg", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/ilktest.svg", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/proscan.svg", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/hipa.svg", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/ists.svg", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/sound_enabled.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/sound_disabled.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/voice_enabled.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/voice_disabled.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/ion_source_interlock.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/avki_interlock.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/ip_branch_interlock.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/new_interlock.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/new_hipa_interlock.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/new_proscan_interlock.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/interlock_cleared.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/up4note.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/yeow.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/alert.mp3", dest: buildTarget + "/ui" },

                        // Install ILKTEST Main View Components
                        { src: "src/common/ui/templates/main-view/main-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/main-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/main-view-legend.svg", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/view-manager.js", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/main-view/main-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_main_view_template( contents.toString() ),
                            rename: "ilktest-main-view.html",
                        },
                        {
                            src: "src/common/ui/templates/main-view/main-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_main_view_template( contents.toString() ),
                            rename: "ilktest-main-view.js",
                        },

                        // Install ILKTEST Configuration View Components
                        { src: "src/common/ui/templates/configuration-view/configuration-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/configuration-view/configuration-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/configuration-view/configuration-view-legend.svg", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/configuration-view/configuration-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_configuration_view_template( contents.toString() ),
                            rename: "ilktest-configuration-view.html",
                        },
                        {
                            src: "src/common/ui/templates/configuration-view/configuration-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_configuration_view_template( contents.toString() ),
                            rename: "ilktest-configuration-view.js",
                        },

                        // Install ILKTEST Interlock View Components
                        { src: "src/common/ui/templates/interlock-view/interlock-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/interlock-view/interlock-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/interlock-view/interlock-view-legend.svg", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/interlock-view/interlock-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_interlock_view_template( contents.toString() ),
                            rename: "ilktest-interlock-view.html",
                        },
                        {
                            src: "src/common/ui/templates/interlock-view/interlock-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_interlock_view_template( contents.toString() ),
                            rename: "ilktest-interlock-view.js",
                        },

                        // Install ILKTEST Statistics View Components
                        { src: "src/common/ui/templates/statistics-view/statistics-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/statistics-view/statistics-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/statistics-view/statistics-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_statistics_view_template( contents.toString() ),
                            rename: "ilktest-statistics-view.html",
                        },
                        {
                            src: "src/common/ui/templates/statistics-view/statistics-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_statistics_view_template( contents.toString() ),
                            rename: "ilktest-statistics-view.js",
                        },

                        // Install ILKTEST Module Expert View Components
                        { src: "src/common/ui/templates/module-expert-view/module-expert-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/module-expert-view/module-expert-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/module-expert-view/module-expert-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_module_expert_view_template( contents.toString() ),
                            rename: "ilktest-module-expert-view.html",
                        },
                        {
                            src: "src/common/ui/templates/module-expert-view/module-expert-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_module_expert_view_template( contents.toString() ),
                            rename: "ilktest-module-expert-view.js",
                        },

                        // Install ILKTEST Quick View Components
                        { src: "src/common/ui/templates/quick-view/quick-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/quick-view/quick-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/quick-view/quick-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_quick_view_template( contents.toString() ),
                            rename: "ilktest-quick-view.html",
                        },
                        {
                            src: "src/common/ui/templates/quick-view/quick-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_quick_view_template( contents.toString() ),
                            rename: "ilktest-quick-view.js",
                        },

                        // Install ILKTEST Software Arch View Components
                        { src: "src/common/ui/templates/software-arch-view/software-arch-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/software-arch-view/software-arch-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/software-arch-view/software-arch-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_software_arch_view_template( contents.toString() ),
                            rename: "ilktest-software-arch-view.html",
                        },

                        // Install ILKTEST User Guide View Components
                        { src: "src/common/ui/templates/user-guide-view/user-guide-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/user-guide-view/user-guide-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/user-guide-view/user-guide-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => ILKTEST_UI_SUBS.load_ilktest_user_guide_view_template( contents.toString() ),
                            rename: "ilktest-user-guide-view.html",
                        },
                    ],
                }),
            ]
        },

        // Install ILKTEST TEST
        {
            input: 'src/sites/ilktest/test/ilktest-test-support.js',
            output: {
            dir: buildTarget + "/test",
                format: 'es',
                sourcemap: true,
            },
            plugins: [
                resolve(),
                commonjs(),
                copy({
                    targets: [
                        // Install ILKTEST Test Component Support Assets
                        {
                            src: "src/common/assets/wc-register-view.css",
                            dest: buildTarget + "/test"
                        },
                        {
                            src: "src/common/assets/wc-event-view.css",
                            dest: buildTarget + "/test"
                        },
                        {
                            src: "src/common/assets/wc-signal-view.css",
                            dest: buildTarget + "/test"
                        },

                        // Install ILKTEST WICA Test Components
                        {
                            src: "src/common/test/templates/wica/wica-data-element-test.template.html",
                            dest: buildTarget + "/test/wica",
                            transform: (contents) => ILKTEST_TEST_WICA_SUBS.load_wica_data_element_test_template( contents.toString() ),
                            rename: "ilktest-wica-data-element-test.html"
                        },
                        {
                            src: "src/common/test/templates/wica/wica-data-group-test.template.html",
                            dest: buildTarget + "/test/wica",
                            transform: (contents) => ILKTEST_TEST_WICA_SUBS.load_wica_data_group_test_template( contents.toString() ),
                            rename: "ilktest-wica-data-group-test.html"
                        },

                        // Install ILKTEST RPMIP Test Components
                        {
                            src: "src/common/test/templates/rpmip/rpmip-module-register-view-test.template.html",
                            dest: buildTarget + "/test/rpmip",
                            transform: (contents) => ILKTEST_TEST_RPMIP_SUBS.load_rpmip_module_register_view_test_template( contents.toString() ),
                            rename: "ilktest-rpmip-module-register-view-test.html"
                        },
                        {
                            src: "src/common/test/templates/rpmip/rpmip-module-state-view-test.template.html",
                            dest: buildTarget + "/test/rpmip",
                            transform: (contents) => ILKTEST_TEST_RPMIP_SUBS.load_rpmip_module_state_view_test_template( contents.toString() ),
                            rename: "ilktest-rpmip-module-state-view-test.html"
                        },
                        {
                            src: "src/common/test/templates/rpmip/rpmip-module-event-view-test.template.html",
                            dest: buildTarget + "/test/rpmip",
                            transform: (contents) => ILKTEST_TEST_RPMIP_SUBS.load_rpmip_module_event_view_test_template( contents.toString() ),
                            rename: "ilktest-rpmip-module-event-view-test.html"
                        },

                        // Install ILKTEST SIIO Test Components
                        {
                            src: "src/common/test/templates/siio/siio-module-register-view-test.template.html",
                            dest: buildTarget + "/test/siio",
                            transform: (contents) => ILKTEST_TEST_SIIO_SUBS.load_siio_module_register_view_test_template( contents.toString() ),
                            rename: "ilktest-siio-module-register-view-test.html"
                        },
                        {
                            src: "src/common/test/templates/siio/siio-module-state-view-test.template.html",
                            dest: buildTarget + "/test/siio",
                            transform: (contents) => ILKTEST_TEST_SIIO_SUBS.load_siio_module_state_view_test_template( contents.toString() ),
                            rename: "ilktest-siio-module-state-view-test.html"
                        },
                        {
                            src: "src/common/test/templates/siio/siio-module-event-view-test.template.html",
                            dest: buildTarget + "/test/siio",
                            transform: (contents) => ILKTEST_TEST_SIIO_SUBS.load_siio_module_event_view_test_template(contents.toString()),
                            rename: "ilktest-siio-module-event-view-test.html"
                        },

                        // Install ILKTEST USI2 Test Components
                        {
                            src: "src/common/test/templates/usi2/usi2-module-register-view-test.template.html",
                            dest: buildTarget + "/test/usi2",
                            transform: (contents) => ILKTEST_TEST_USI2_SUBS.load_usi2_module_register_view_test_template( contents.toString() ),
                            rename: "ilktest-usi2-module-register-view-test.html"
                        },

                        // Install ILKTEST GEN Test Components
                        {
                            src: "src/common/test/templates/gen/gen-ilk-signal-test.template.html",
                            dest: buildTarget + "/test/gen",
                            transform: (contents) => ILKTEST_TEST_GEN_SUBS.load_gen_ilk_signal_view_test_template( contents.toString() ),
                            rename: "ilktest-gen-ilk-signal-test.html"
                        },

                        // Install ILKTEST Test Components
                        {
                            src: "src/common/test/templates/ilktest-board-view-test.template.html",
                            dest: buildTarget + "/test",
                            transform: (contents) => ILKTEST_TEST_SUBS.load_ilktest_board_view_test_template( contents.toString() ),
                            rename: "ilktest-board-view-test22.html"
                        }
                    ],
               }),
            ]
        },
    ];
}