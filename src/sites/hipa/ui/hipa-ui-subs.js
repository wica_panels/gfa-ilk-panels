console.debug( "Executing script in hipa-ui-subs.js module...");

export function load_hipa_main_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'HIPA' )
        .replaceAll( '__FACILITY_LC__', 'hipa' )
        .replaceAll( '__JS_SUPPORT_FILE__', './hipa-ui-support.js' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__ILKCS_PROBE_URL_PATH__','/v3/facilities/hipa?detail=applicationVersionNumber' )
        .replaceAll( '__ILKCS_STREAM_URL_PATH__','/v3/facilities/hipa/streams?detail=patchStream' )
        .replaceAll( '__ILKCS_ACK_URL_PATH__','/v3/facilities/hipa?detail=applicationVersionNumber' )
        .replaceAll( '__VERSION__', '7.1.0' )
}

export function load_hipa_configuration_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'HIPA' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__FACILITY_SVG_FILE__', 'hipa.svg' )
        .replaceAll( '__JS_SUPPORT_FILE__', './hipa-ui-support.js' )
        .replaceAll( '__FACILITY_CONFIG_PRIMARY_MODE__', 'Main' )
        .replaceAll( '__FACILITY_CONFIG_SECONDARY_MODE__', 'IP' )
        .replaceAll( '__FACILITY_CONFIG_TERTIARY_MODE__', 'UCN' )
        .replaceAll( '__FACILITY_CONFIG_MODE_OPTION__', 'Intensity' )
}

export function load_hipa_interlock_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'HIPA' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__FACILITY_SVG_FILE__', 'hipa.svg' )
        .replaceAll( '__JS_SUPPORT_FILE__', './hipa-ui-support.js' )
        .replaceAll( '__FACILITY_CONFIG_PRIMARY_MODE__', 'Main' )
        .replaceAll( '__FACILITY_CONFIG_SECONDARY_MODE__', 'IP' )
        .replaceAll( '__FACILITY_CONFIG_TERTIARY_MODE__', 'UCN' )
        .replaceAll( '__FACILITY_CONFIG_MODE_OPTION__', 'Intensity' )
}

export function load_hipa_statistics_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'HIPA' )
        .replaceAll( '__JS_SUPPORT_FILE__', './hipa-ui-support.js' )
}

export function load_hipa_quick_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'HIPA' )
        .replaceAll( '__JS_SUPPORT_FILE__', './hipa-ui-support.js' )
}

export function load_hipa_module_expert_view_template(template )
{
    return template.replaceAll( '__FACILITY__', 'HIPA' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__JS_SUPPORT_FILE__', './hipa-ui-support.js' )
}

export function load_hipa_software_arch_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'HIPA' )
        .replaceAll( '__SOFTWARE_ARCH_PNG_FILE__', 'hipa-software-arch.png' )
}

export function load_hipa_user_guide_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'HIPA' )
        .replaceAll('__USER_GUIDE_PDF_FILE__', '../docs/hipa_software_user_guide_de.pdf' )
}