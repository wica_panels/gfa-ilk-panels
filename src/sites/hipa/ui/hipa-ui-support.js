console.debug( "Executing script in hipa-ui-support.js module...");

import {ShadowDomHelper} from  "../../../common/ui/js/shadow-dom-helper.js";
import {StateManager} from  "../../../common/ui/js/state-manager.js";
import {SvgStateManager} from "../../../common/ui/js/svg-state-manager.js";
import {SvgContentManager} from "../../../common/ui/js/svg-content-manager.js";

import {STREAM_SUPPORT} from '../../../common/ui/js/stream-support.js';
import {SESSION_SUPPORT} from '../../../common/ui/js/session-support.js';
import {MODULE_SUPPORT} from  "../../../common/ui/js/module-support.js";
import {SIGNAL_SUPPORT} from  "../../../common/ui/js/signal-support.js";
import {SOUND_SUPPORT} from  "../../../common/ui/js/sound-suppport.js";
import {HTTP_SUPPORT} from  "../../../common/js/http-support.js";
import {HTML_ELEMENT_SUPPORT} from  "../../../common/js/html-element-support.js";
import {SVG_SUPPORT} from  "../../../common/ui/js/svg-support.js";

import {IlkPopupMessage} from "@psi/ilk-wc-factory/gen/ilk-popup-message.js";
import {IlkPopupHttpError} from "@psi/ilk-wc-factory/gen/ilk-popup-http-error.js";
import {IlkSignal} from "@psi/ilk-wc-factory/gen/ilk-signal.js";
import {IlkSignalGroup} from "@psi/ilk-wc-factory/gen/ilk-signal-group.js";
import {IlkSignalGroupContainer} from "@psi/ilk-wc-factory/gen/ilk-signal-group-container.js";
import {IlkModule} from "@psi/ilk-wc-factory/gen/ilk-module.js";
import {IlkModuleContainer} from "@psi/ilk-wc-factory/gen/ilk-module-container.js";
import {IlkTimestrip} from "@psi/ilk-wc-factory/gen/ilk-timestrip.js";
import {IlkTimestripGroup} from "@psi/ilk-wc-factory/gen/ilk-timestrip-group.js";

import {HipaInterlockViewPropertyManager as InterlockViewPropertyManager} from "../../../common/ui/js/hipa-interlock-view-property-manager.js";
import {HipaConfigurationViewPropertyManager as ConfigurationViewPropertyManager}  from "../../../common/ui/js/hipa-configuration-view-property-manager.js";
import * as FAST_JSON_PATCH from 'fast-json-patch';
import {Formatter} from "fracturedjsonjs";
import Chart from 'chart.js/auto';

export {
    IlkPopupMessage,
    IlkPopupHttpError,
    IlkSignal,
    IlkSignalGroup,
    IlkSignalGroupContainer,
    IlkModule,
    IlkModuleContainer,
    IlkTimestrip,
    IlkTimestripGroup,
    ShadowDomHelper,
    StateManager,
    SvgStateManager,
    SvgContentManager,
    InterlockViewPropertyManager,
    ConfigurationViewPropertyManager,
    Formatter,
    Chart,
    STREAM_SUPPORT,
    SESSION_SUPPORT,
    MODULE_SUPPORT,
    SIGNAL_SUPPORT,
    HTTP_SUPPORT,
    HTML_ELEMENT_SUPPORT,
    FAST_JSON_PATCH,
    SOUND_SUPPORT,
    SVG_SUPPORT
}