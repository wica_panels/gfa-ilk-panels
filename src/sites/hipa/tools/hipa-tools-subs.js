console.debug( "Executing script in hipa-tools-subs.js module...");

export function load_hipa_database_tool_template(template )
{
    return template.replaceAll('__FACILITY__', 'HIPA' )
    .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
    .replaceAll('__JS_SUPPORT_FILE__', './hipa-tools-support.js' )
}

export function load_hipa_command_tool_template( template )
{
    return template.replaceAll('__FACILITY__', 'HIPA' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__JS_SUPPORT_FILE__', './hipa-tools-support.js' )
}

export function load_hipa_facility_overview_tool_template( template )
{
    return template.replaceAll('__FACILITY__', 'HIPA' )
        .replaceAll('__INTERLOCK_VIEW__', 'hipa-main-view.html' )
        .replaceAll('__ILKCS_COMMAND_TOOL__', 'hipa-ilkcs-command-tool.html' )
        .replaceAll('__ILKDB_EXPLORER_TOOL__', 'hipa-ilkdb-explorer-tool.html' )
        .replaceAll('__SOFTWARE_ARCH__', '../ui/hipa-software-arch.html' )
        .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
        .replaceAll('__CSS_SUPPORT_FILE__', './wc-register-view.css' )
        .replaceAll('__JS_SUPPORT_FILE__', './hipa-tools-support.js' )
        .replaceAll('__VERSION__', '7.1.0' );
}