// noinspection JSUnusedGlobalSymbols

import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import copy from "rollup-plugin-copy";

import * as HIPA_TOOLS_SUBS from "./tools/hipa-tools-subs.js";
import * as HIPA_UI_SUBS from "./ui/hipa-ui-subs.js";
import * as HIPA_TEST_SUBS from "./test/hipa-test-subs.js";

export default () => {

    const buildTarget = "build/hipa_prod";
    const indexFile = "hipa-index.html";

    return [
    {
        input: 'src/sites/hipa/tools/hipa-tools-support.js',
        output: {
            dir: buildTarget + '/tools',
            format: 'es'
        },
        plugins: [
            commonjs(),
            resolve(),
            copy({
                targets: [
                    // Install HIPA Index
                    { src: "src/sites/hipa/" + indexFile, dest: buildTarget, rename: "index.html" },
                    { src: "src/common/assets/background.jpg", dest: buildTarget },
                    { src: "src/common/tools/assets/facility-overview-vars.css", dest: buildTarget + "/tools" },
                    { src: "src/common/assets/favicon.ico", dest: buildTarget },

                    // Install HIPA Tool Component Support Assets
                    { src: "src/common/assets/wc-register-view.css", dest: buildTarget + "/tools" },
                    { src: "src/common/assets/wc-event-view.css", dest: buildTarget + "/tools" },
                    { src: "src/common/assets/bolt.svg", dest: buildTarget + "/tools" },

                    // Install HIPA Command Tool
                    { src: "src/common/tools/templates/command-tool/command-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/command-tool/command-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => HIPA_TOOLS_SUBS.load_hipa_command_tool_template( contents.toString() ),
                        rename: "hipa-command-tool.html",
                    },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.js", dest:buildTarget + "/tools" ,
                        transform: (contents) => HIPA_TOOLS_SUBS.load_hipa_command_tool_template( contents.toString() ),
                        rename: "hipa-command-tool.js",
                    },

                    // Install HIPA Database Tool
                    { src: "src/common/tools/templates/database-tool/database-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/database-tool/database-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => HIPA_TOOLS_SUBS.load_hipa_database_tool_template( contents.toString() ),
                        rename: "hipa-database-tool.html",
                    },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.js", dest:buildTarget + "/tools" ,
                        transform: (contents) => HIPA_TOOLS_SUBS.load_hipa_database_tool_template( contents.toString() ),
                        rename: "hipa-database-tool.js",
                    },

                    // Install HIPA Facility Overview Tool
                    {
                        src: "src/common/tools/templates/facility-overview-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => HIPA_TOOLS_SUBS.load_hipa_facility_overview_tool_template( contents.toString() ),
                        rename: "hipa-facility-overview-tool.html",
                    },
                ],
            } ),
        ] },

        // Install ShoeLace Style
        {
            input: 'node_modules/@shoelace-style/shoelace/dist/shoelace.js',
            output: {
                dir: buildTarget + '/ui/shoelace',
                format: 'es',
            },
            plugins: [
                resolve(),
                copy({
                    targets: [
                        { src: "node_modules/@shoelace-style/shoelace/dist/themes/light.css", dest: buildTarget + '/ui/shoelace', rename: "shoelace-theme-light.css" },
                        { src: "node_modules/@shoelace-style/shoelace/dist/assets", dest: buildTarget + '/ui/shoelace',  },
                    ],
                }),
            ]
        },

        // Install HIPA UI
        {
            input:  'src/sites/hipa/ui/hipa-ui-support.js',
            output: {
                dir: buildTarget + '/ui',
                format: 'es'
            },
            plugins: [
                commonjs(),
                resolve(),
                copy({
                    targets: [
                        // Install HIPA UI Component Support Assets
                        { src: "src/common/assets/bolt.svg", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/hipa.svg", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/sound_enabled.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/sound_disabled.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/voice_enabled.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/voice_disabled.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/ion_source_interlock.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/avki_interlock.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/ip_branch_interlock.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/new_interlock.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/new_hipa_interlock.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/new_proscan_interlock.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/interlock_cleared.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/up4note.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/yeow.mp3", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/assets/sounds/alert.mp3", dest: buildTarget + "/ui" },

                        // Install HIPA Main View Components
                        { src: "src/common/ui/templates/main-view/main-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/main-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/main-view-legend.svg", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/main-view/view-manager.js", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/main-view/main-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_main_view_template( contents.toString() ),
                            rename: "hipa-main-view.html",
                        },
                        {
                            src: "src/common/ui/templates/main-view/main-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_main_view_template( contents.toString() ),
                            rename: "hipa-main-view.js",
                        },

                        // Install HIPA Configuration View Components
                        { src: "src/common/ui/templates/configuration-view/configuration-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/configuration-view/configuration-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/configuration-view/configuration-view-legend.svg", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/configuration-view/configuration-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_configuration_view_template( contents.toString() ),
                            rename: "hipa-configuration-view.html",
                        },
                        {
                            src: "src/common/ui/templates/configuration-view/configuration-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_configuration_view_template( contents.toString() ),
                            rename: "hipa-configuration-view.js",
                        },

                        // Install HIPA Interlock View Components
                        { src: "src/common/ui/templates/interlock-view/interlock-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/interlock-view/interlock-view-vars.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/interlock-view/interlock-view-legend.svg", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/interlock-view/interlock-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_interlock_view_template( contents.toString() ),
                            rename: "hipa-interlock-view.html",
                        },
                        {
                            src: "src/common/ui/templates/interlock-view/interlock-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_interlock_view_template( contents.toString() ),
                            rename: "hipa-interlock-view.js",
                        },

                        // Install HIPA Statistics View Components
                        { src: "src/common/ui/templates/statistics-view/statistics-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/statistics-view/statistics-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/statistics-view/statistics-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_statistics_view_template( contents.toString() ),
                            rename: "hipa-statistics-view.html",
                        },
                        {
                            src: "src/common/ui/templates/statistics-view/statistics-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_statistics_view_template( contents.toString() ),
                            rename: "hipa-statistics-view.js",
                        },

                        // Install HIPA Quick View Components
                        { src: "src/common/ui/templates/quick-view/quick-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/quick-view/quick-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/quick-view/quick-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_quick_view_template( contents.toString() ),
                            rename: "hipa-quick-view.html",
                        },
                        {
                            src: "src/common/ui/templates/quick-view/quick-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_quick_view_template( contents.toString() ),
                            rename: "hipa-quick-view.js",
                        },

                        // Install HIPA Module Expert View Components
                        { src: "src/common/ui/templates/module-expert-view/module-expert-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/module-expert-view/module-expert-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/module-expert-view/module-expert-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_module_expert_view_template( contents.toString() ),
                            rename: "hipa-module-expert-view.html",
                        },
                        {
                            src: "src/common/ui/templates/module-expert-view/module-expert-view.template.js", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_module_expert_view_template( contents.toString() ),
                            rename: "hipa-module-expert-view.js",
                        },

                        // Install HIPA Software Arch View Components
                        { src: "src/common/ui/templates/software-arch-view/software-arch-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/software-arch-view/software-arch-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/software-arch-view/software-arch-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_software_arch_view_template( contents.toString() ),
                            rename: "hipa-software-arch-view.html",
                        },

                        // Install HIPA User Guide View Components
                        { src: "src/common/ui/templates/user-guide-view/user-guide-view.css", dest: buildTarget + "/ui" },
                        { src: "src/common/ui/templates/user-guide-view/user-guide-view-vars.css", dest: buildTarget + "/ui" },
                        {
                            src: "src/common/ui/templates/user-guide-view/user-guide-view.template.html", dest: buildTarget + "/ui",
                            transform: (contents) => HIPA_UI_SUBS.load_hipa_user_guide_view_template( contents.toString() ),
                            rename: "hipa-user-guide-view.html",
                        },
                    ],
                } )
            ]
        },

        // Install HIPA TEST
        {
            input: 'src/sites/hipa/test/hipa-test-support.js',
            output: {
                dir: buildTarget + '/test',
                format: 'es',
            },
            plugins: [
                commonjs(),
                resolve(),
                copy({
                    targets: [

                        // Install HIPA Test Component Support Assets
                        { src: "src/common/assets/wc-register-view.css",dest: buildTarget + "/test" },
                        { src: "src/common/assets/wc-event-view.css", dest: buildTarget + "/test" },

                        // Install HIPA Test Components
                        {
                            src: "src/common/test/templates/hipa-board-register-view-test.template.html", dest: "build/hipa_prod/test/",
                            transform: (contents) => HIPA_TEST_SUBS.load_hipa_board_register_view_test_template( contents.toString() ),
                            rename: "hipa-board-register-view-test.html"
                        },
                        {
                            src: "src/common/test/templates/hipa-board-event-view-test.template.html", dest: "build/hipa_prod/test/",
                            transform: (contents) => HIPA_TEST_SUBS.load_hipa_board_event_view_test_template( contents.toString() ),
                            rename: "hipa-board-event-view-test.html"
                        },
                        {
                            src: "src/common/test/templates/hipa-board-view-test.template.html", dest: "build/hipa_prod/test/",
                            transform: (contents) => HIPA_TEST_SUBS.load_hipa_board_view_test_template( contents.toString() ),
                            rename: "hipa-board-view-test.html"
                        },
                    ],
                })
            ]
        },
    ];
}