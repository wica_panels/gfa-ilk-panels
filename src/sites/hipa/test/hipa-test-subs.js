console.debug( "Executing script in hipa-test-subs.js module...");

export function load_hipa_board_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', './hipa-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', './wc-event-view.css' )
        .replaceAll('__USI2_SYSTEM_ID__', 'J0001000'  )
        .replaceAll('__USI2_COMMON_ID__', 'J0001000'  )
        .replaceAll('__USI2_MODULE_ID__', 'J0001001' )
}

export function load_hipa_board_register_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', './hipa-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', './wc-event-view.css' )
        .replaceAll('__USI2_SYSTEM_ID__', 'J0001000'  )
        .replaceAll('__USI2_COMMON_ID__', 'J0001000'  )
        .replaceAll('__USI2_MODULE_ID__', 'J0001001' )
}

export function load_hipa_board_event_view_test_template( template )
{
    return template.replaceAll('__JS_SUPPORT_FILE__', './hipa-test-support.js' )
        .replaceAll('__CSS_SUPPORT_FILE__', './wc-event-view.css' )
        .replaceAll('__USI2_SYSTEM_ID__', 'J0001000'  )
        .replaceAll('__USI2_COMMON_ID__', 'J0001000'  )
        .replaceAll('__USI2_MODULE_ID__', 'J0001001' )
}