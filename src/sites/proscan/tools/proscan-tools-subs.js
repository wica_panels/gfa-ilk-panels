console.debug( "Executing script in proscan-tools-subs.js module...");

export function load_proscan_database_tool_template(template )
{
    return template.replaceAll('__FACILITY__', 'PROSCAN' )
    .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
    .replaceAll('__JS_SUPPORT_FILE__', './proscan-tools-support.js' )
}

export function load_proscan_command_tool_template(template )
{
    return template.replaceAll('__FACILITY__', 'PROSCAN' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__JS_SUPPORT_FILE__', './proscan-tools-support.js' )
}

export function load_proscan_facility_overview_tool_template( template )
{
    return template.replaceAll('__FACILITY__', 'PROSCAN' )
        .replaceAll('__INTERLOCK_VIEW__', 'proscan-main-view.html' )
        .replaceAll('__ILKCS_COMMAND_TOOL__', 'proscan-ilkcs-command-tool.html' )
        .replaceAll('__ILKDB_EXPLORER_TOOL__', 'proscan-ilkdb-explorer-tool.html' )
        .replaceAll('__SOFTWARE_ARCH__', '../ui/proscan-software-arch.html' )
        .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
        .replaceAll('__CSS_SUPPORT_FILE__', './wc-register-view.css' )
        .replaceAll('__JS_SUPPORT_FILE__', './proscan-tools-support.js' )
        .replaceAll('__VERSION__', '7.1.0' );
}