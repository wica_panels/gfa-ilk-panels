// noinspection JSUnusedGlobalSymbols

import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import copy from "rollup-plugin-copy";

import * as PROSCAN_TOOLS_SUBS from "./tools/proscan-tools-subs.js";
import * as PROSCAN_UI_SUBS from "./ui/proscan-ui-subs.js";

export default () => {

    const buildTarget = "build/proscan_prod";
    const indexFile = "proscan-index.html";

    return [ {
        input: 'src/sites/proscan/tools/proscan-tools-support.js',
        output: {
            dir: buildTarget + "/tools",
            format: 'es',
            sourcemap: true,
        },
        plugins: [
            copy({
                targets: [
                    // Install PROSCAN Index
                    { src: "src/sites/proscan/" + indexFile, dest: buildTarget, rename: "index.html" },
                    { src: "src/common/assets/background.jpg", dest: buildTarget },
                    { src: "src/common/tools/assets/facility-overview-vars.css", dest: buildTarget + "/tools" },
                    { src: "src/common/assets/favicon.ico", dest: buildTarget },

                    // Install PROSCAN Tool Component Support Assets
                    { src: "src/common/assets/wc-register-view.css", dest: buildTarget + "/tools" },
                    { src: "src/common/assets/wc-event-view.css", dest: buildTarget + "/tools" },
                    { src: "src/common/assets/bolt.svg", dest: buildTarget + "/tools" },

                    // Install PROSCAN Command Tool
                    { src: "src/common/tools/templates/command-tool/command-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/command-tool/command-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => PROSCAN_TOOLS_SUBS.load_proscan_command_tool_template( contents.toString() ),
                        rename: "proscan-command-tool.html",
                    },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.js", dest:buildTarget + "/tools" ,
                        transform: (contents) => PROSCAN_TOOLS_SUBS.load_proscan_command_tool_template( contents.toString() ),
                        rename: "proscan-command-tool.js",
                    },

                    // Install PROSCAN Database Tool
                    { src: "src/common/tools/templates/database-tool/database-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/database-tool/database-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => PROSCAN_TOOLS_SUBS.load_proscan_database_tool_template( contents.toString() ),
                        rename: "proscan-database-tool.html",
                    },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.js", dest:buildTarget + "/tools" ,
                        transform: (contents) => PROSCAN_TOOLS_SUBS.load_proscan_database_tool_template( contents.toString() ),
                        rename: "proscan-database-tool.js",
                    },

                    // Install PROSCAN Facility Overview Tool
                    {
                        src: "src/common/tools/templates/facility-overview-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => PROSCAN_TOOLS_SUBS.load_proscan_facility_overview_tool_template(contents.toString() ),
                        rename: "proscan-facility-overview-tool.html",
                    },
                ],
            }),
            resolve(),
            commonjs(),
        ] },
        // Install ShoeLace Style
        {
            input: [ 'node_modules/@shoelace-style/shoelace/dist/shoelace.js' ],
            output: {
                dir: buildTarget + '/ui/shoelace',
                format: 'es',
            },
            plugins: [
                resolve(),
                copy({
                    targets: [
                        { src: "node_modules/@shoelace-style/shoelace/dist/themes/light.css", dest: buildTarget + '/ui/shoelace', rename: "shoelace-theme-light.css" },
                        { src: "node_modules/@shoelace-style/shoelace/dist/assets", dest: buildTarget + '/ui/shoelace',  },
                    ],
                }),
            ]
        },
        {
        input: [
            'src/sites/proscan/ui/proscan-ui-support.js',
        ],
        output: {
            dir: buildTarget + '/ui',
            format: 'es',
            sourcemap: true,
        },
        plugins: [
            copy({
                targets: [
                    // Install PROSCAN UI Component Support Assets
                    { src: "src/common/assets/bolt.svg", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/proscan.svg", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/sound_enabled.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/sound_disabled.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/voice_enabled.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/voice_disabled.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/ion_source_interlock.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/avki_interlock.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/ip_branch_interlock.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/new_interlock.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/new_hipa_interlock.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/new_proscan_interlock.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/interlock_cleared.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/up4note.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/yeow.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/alert.mp3", dest: buildTarget + "/ui" },

                    // Install PROSCAN Main View Components
                    { src: "src/common/ui/templates/main-view/main-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/main-view/main-view-vars.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/main-view/main-view-legend.svg", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/main-view/view-manager.js", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/main-view/main-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_main_view_template( contents.toString() ),
                        rename: "proscan-main-view.html",
                    },
                    {
                        src: "src/common/ui/templates/main-view/main-view.template.js", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_main_view_template( contents.toString() ),
                        rename: "proscan-main-view.js",
                    },

                    // Install PROSCAN Configuration View Components
                    { src: "src/common/ui/templates/configuration-view/configuration-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/configuration-view/configuration-view-vars.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/configuration-view/configuration-view-legend.svg", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/configuration-view/configuration-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_configuration_view_template( contents.toString() ),
                        rename: "proscan-configuration-view.html",
                    },
                    {
                        src: "src/common/ui/templates/configuration-view/configuration-view.template.js", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_configuration_view_template( contents.toString() ),
                        rename: "proscan-configuration-view.js",
                    },

                    // Install PROSCAN Interlock View Components
                    { src: "src/common/ui/templates/interlock-view/interlock-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/interlock-view/interlock-view-vars.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/interlock-view/interlock-view-legend.svg", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/interlock-view/interlock-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_interlock_view_template( contents.toString() ),
                        rename: "proscan-interlock-view.html",
                    },
                    {
                        src: "src/common/ui/templates/interlock-view/interlock-view.template.js", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_interlock_view_template( contents.toString() ),
                        rename: "proscan-interlock-view.js",
                    },

                    // Install PROSCAN Statistics View Components
                    { src: "src/common/ui/templates/statistics-view/statistics-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/statistics-view/statistics-view-vars.css", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/statistics-view/statistics-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_statistics_view_template( contents.toString() ),
                        rename: "proscan-statistics-view.html",
                    },
                    {
                        src: "src/common/ui/templates/statistics-view/statistics-view.template.js", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_statistics_view_template( contents.toString() ),
                        rename: "proscan-statistics-view.js",
                    },

                    // Install PROSCAN Quick View Components
                    { src: "src/common/ui/templates/quick-view/quick-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/quick-view/quick-view-vars.css", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/quick-view/quick-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_quick_view_template( contents.toString() ),
                        rename: "proscan-quick-view.html",
                    },
                    {
                        src: "src/common/ui/templates/quick-view/quick-view.template.js", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_quick_view_template( contents.toString() ),
                        rename: "proscan-quick-view.js",
                    },
                    
                    // Install PROSCAN Module Expert View Components
                    { src: "src/common/ui/templates/module-expert-view/module-expert-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/module-expert-view/module-expert-view-vars.css", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/module-expert-view/module-expert-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_module_expert_view_template( contents.toString() ),
                        rename: "proscan-module-expert-view.html",
                    },
                    {
                        src: "src/common/ui/templates/module-expert-view/module-expert-view.template.js", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_module_expert_view_template( contents.toString() ),
                        rename: "proscan-module-expert-view.js",
                    },

                    // Install PROSCAN Software Arch View Components
                    { src: "src/common/ui/templates/software-arch-view/software-arch-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/software-arch-view/software-arch-view-vars.css", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/software-arch-view/software-arch-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_software_arch_view_template( contents.toString() ),
                        rename: "proscan-software-arch-view.html",
                    },

                    // Install PROSCAN User Guide View Components
                    { src: "src/common/ui/templates/user-guide-view/user-guide-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/user-guide-view/user-guide-view-vars.css", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/user-guide-view/user-guide-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => PROSCAN_UI_SUBS.load_proscan_user_guide_view_template( contents.toString() ),
                        rename: "proscan-user-guide-view.html",
                    },
                ],
            } ),
            resolve(),
            commonjs(),
        ] },    
    ];
}