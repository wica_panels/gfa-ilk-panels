console.debug( "Executing script in proscan-ui-subs.js module...");

export function load_proscan_main_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'PROSCAN' )
        .replaceAll( '__FACILITY_LC__', 'proscan' )
        .replaceAll( '__JS_SUPPORT_FILE__', './proscan-ui-support.js' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__ILKCS_PROBE_URL_PATH__','/v3/facilities/proscan?detail=applicationVersionNumber' )
        .replaceAll( '__ILKCS_STREAM_URL_PATH__','/v3/facilities/proscan/streams?detail=patchStream' )
        .replaceAll( '__ILKCS_ACK_URL_PATH__','/v3/facilities/proscan?detail=applicationVersionNumber' )
        .replaceAll( '__VERSION__', '7.1.0' )
}

export function load_proscan_configuration_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'PROSCAN' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__FACILITY_SVG_FILE__', 'proscan.svg' )
        .replaceAll( '__JS_SUPPORT_FILE__', './proscan-ui-support.js' )
        .replaceAll( '__FACILITY_CONFIG_PRIMARY_MODE__', 'Main' )
        .replaceAll( '__FACILITY_CONFIG_SECONDARY_MODE__', 'User' )
        .replaceAll( '__FACILITY_CONFIG_TERTIARY_MODE__', 'Mode' )
        .replaceAll( '__FACILITY_CONFIG_MODE_OPTION__', 'Options' )
}

export function load_proscan_interlock_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'PROSCAN' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__FACILITY_SVG_FILE__', 'proscan.svg' )
        .replaceAll( '__JS_SUPPORT_FILE__', './proscan-ui-support.js' )
        .replaceAll( '__FACILITY_CONFIG_PRIMARY_MODE__', 'Main' )
        .replaceAll( '__FACILITY_CONFIG_SECONDARY_MODE__', 'User' )
        .replaceAll( '__FACILITY_CONFIG_TERTIARY_MODE__', 'Mode' )
        .replaceAll( '__FACILITY_CONFIG_MODE_OPTION__', 'Options' )
}

export function load_proscan_statistics_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'PROSCAN' )
        .replaceAll( '__JS_SUPPORT_FILE__', './proscan-ui-support.js' )
}

export function load_proscan_quick_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'PROSCAN' )
        .replaceAll( '__JS_SUPPORT_FILE__', './proscan-ui-support.js' )
}

export function load_proscan_module_expert_view_template(template )
{
    return template.replaceAll( '__FACILITY__', 'PROSCAN' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__JS_SUPPORT_FILE__', './proscan-ui-support.js' )
}

export function load_proscan_software_arch_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'PROSCAN' )
        .replaceAll( '__SOFTWARE_ARCH_PNG_FILE__', 'proscan-software-arch.png' )
}

export function load_proscan_user_guide_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'PROSCAN' )
        .replaceAll('__USER_GUIDE_PDF_FILE__', '../docs/hipa_software_user_guide_de.pdf' )
}