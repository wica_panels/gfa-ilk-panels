console.debug( "Executing script in ists-ui-subs.js module...");

export function load_ists_main_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'ISTS' )
        .replaceAll( '__FACILITY_LC__', 'ists' )
        .replaceAll( '__JS_SUPPORT_FILE__', './ists-ui-support.js' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__ILKCS_PROBE_URL_PATH__','/v3/facilities/ists?detail=applicationVersionNumber' )
        .replaceAll( '__ILKCS_STREAM_URL_PATH__','/v3/facilities/ists/streams?detail=patchStream' )
        .replaceAll( '__ILKCS_ACK_URL_PATH__','/v3/facilities/ists?detail=applicationVersionNumber' )
        .replaceAll( '__VERSION__', '7.1.0' )
}

export function load_ists_configuration_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'ISTS' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__FACILITY_SVG_FILE__', 'ists.svg' )
        .replaceAll( '__JS_SUPPORT_FILE__', './ists-ui-support.js' )
        .replaceAll( '__FACILITY_CONFIG_PRIMARY_MODE__', 'Main' )
        .replaceAll( '__FACILITY_CONFIG_SECONDARY_MODE__', 'IP' )
        .replaceAll( '__FACILITY_CONFIG_TERTIARY_MODE__', 'UCN' )
        .replaceAll( '__FACILITY_CONFIG_MODE_OPTION__', 'Intensity' )
}

export function load_ists_interlock_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'ISTS' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__FACILITY_SVG_FILE__', 'ists.svg' )
        .replaceAll( '__JS_SUPPORT_FILE__', './ists-ui-support.js' )
        .replaceAll( '__FACILITY_CONFIG_PRIMARY_MODE__', 'Main' )
        .replaceAll( '__FACILITY_CONFIG_SECONDARY_MODE__', 'IP' )
        .replaceAll( '__FACILITY_CONFIG_TERTIARY_MODE__', 'UCN' )
        .replaceAll( '__FACILITY_CONFIG_MODE_OPTION__', 'Intensity' )
}

export function load_ists_statistics_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'ISTS' )
        .replaceAll( '__JS_SUPPORT_FILE__', './ists-ui-support.js' )
}

export function load_ists_quick_view_template( template )
{
    return template.replaceAll( '__FACILITY__', 'ISTS' )
        .replaceAll( '__JS_SUPPORT_FILE__', './ists-ui-support.js' )
}

export function load_ists_module_expert_view_template(template )
{
    return template.replaceAll( '__FACILITY__', 'ISTS' )
        .replaceAll( '__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll( '__JS_SUPPORT_FILE__', './ists-ui-support.js' )
}

export function load_ists_software_arch_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'ISTS' )
        .replaceAll( '__SOFTWARE_ARCH_PNG_FILE__', 'ists-software-arch.png' )
}

export function load_ists_user_guide_view_template( template )
{
    return template.replaceAll('__FACILITY__', 'ISTS' )
        .replaceAll('__USER_GUIDE_PDF_FILE__', '../docs/hipa_software_user_guide_de.pdf' )
}