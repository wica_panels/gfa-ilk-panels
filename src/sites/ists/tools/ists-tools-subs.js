console.debug( "Executing script in ists-tools-subs.js module...");

export function load_ists_database_tool_template( template )
{
    return template.replaceAll('__FACILITY__', 'ISTS' )
    .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
    .replaceAll('__JS_SUPPORT_FILE__', './ists-tools-support.js' )
}

export function load_ists_command_tool_template( template )
{
    return template.replaceAll('__FACILITY__', 'ISTS' )
        .replaceAll('__ILKCS_SERVER_PATH_PREFIX__', '/ilkcs' )
        .replaceAll('__JS_SUPPORT_FILE__', './ists-tools-support.js' )
}

export function load_ists_facility_overview_tool_template( template )
{
    return template.replaceAll('__FACILITY__', 'ISTS' )
        .replaceAll('__INTERLOCK_VIEW__', 'ists-main-view.html' )
        .replaceAll('__ILKCS_COMMAND_TOOL__', 'ists-ilkcs-command-tool.html' )
        .replaceAll('__ILKDB_EXPLORER_TOOL__', 'ists-ilkdb-explorer-tool.html' )
        .replaceAll('__SOFTWARE_ARCH__', '../ui/ists-software-arch.html' )
        .replaceAll('__ILKDB_SERVER_PATH_PREFIX__', '/ilkdb' )
        .replaceAll('__CSS_SUPPORT_FILE__', './wc-register-view.css' )
        .replaceAll('__JS_SUPPORT_FILE__', './ists-tools-support.js' )
        .replaceAll('__VERSION__', '7.1.0' );
}