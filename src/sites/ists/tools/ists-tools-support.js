console.debug( "Executing script in ists-tools-support.js module...");

import {ShadowDomHelper} from  "../../../common/ui/js/shadow-dom-helper.js";
import {WicaDataElement} from "@psi/ilk-wc-factory/wica/wica-data-element.js";
import {WicaDataGroup} from "@psi/ilk-wc-factory/wica/wica-data-group.js";
import {WicaChannelWriter} from "@psi/ilk-wc-factory/wica/wica-channel-writer.js";

import {WebComponentEventViewer} from "@psi/ilk-wc-factory/wc/wc-event-viewer.js";

import {IlkModuleStateViewer} from "@psi/ilk-wc-factory/gen/ilk-module-state-viewer.js";
import {IlkRegisterDecoder} from "@psi/ilk-wc-factory/gen/ilk-register-decoder.js";
import {IlkModuleDecoder} from "@psi/ilk-wc-factory/gen/ilk-module-decoder.js";

import {SiioModule} from "@psi/ilk-wc-factory/siio/siio-module.js";
import {SiioModuleRegisterView} from "@psi/ilk-wc-factory/siio/siio-module-register-view.js";
import {SiioModuleStateView} from "@psi/ilk-wc-factory/siio/siio-module-state-view.js";

import {RpmipModule} from "@psi/ilk-wc-factory/rpmip/rpmip-module.js";
import {RpmipModuleRegisterView} from "@psi/ilk-wc-factory/rpmip/rpmip-module-register-view.js";
import {RpmipModuleStateView} from "@psi/ilk-wc-factory/rpmip/rpmip-module-state-view.js";

import {Usi2Module} from "@psi/ilk-wc-factory/usi2/usi2-module.js";
import {Usi2Common} from "@psi/ilk-wc-factory/usi2/usi2-common.js";
import {Usi2System} from "@psi/ilk-wc-factory/usi2/usi2-system.js";
import {Usi2Board} from "@psi/ilk-wc-factory/usi2/usi2-board.js";
import {Usi2ModuleStateView} from "@psi/ilk-wc-factory/usi2/usi2-module-state-view.js";
import {Usi2SystemRegisterView} from "@psi/ilk-wc-factory/usi2/usi2-system-register-view.js";
import {Usi2CommonRegisterView} from "@psi/ilk-wc-factory/usi2/usi2-common-register-view.js";
import {Usi2ModuleRegisterView} from "@psi/ilk-wc-factory/usi2/usi2-module-register-view.js";
import {DocumentSupportLoader} from "@psi/wica-js/client-api";
import {Formatter} from "fracturedjsonjs";

import {HTTP_SUPPORT} from  "../../../common/js/http-support.js";
import {HTTP_STREAM_SUPPORT} from  "../../../common/tools/js/http-stream-support.js";
import {HTML_ELEMENT_SUPPORT} from  "../../../common/js/html-element-support.js";

export {
    WicaDataElement,
    WicaDataGroup,
    WicaChannelWriter,
    SiioModule,
    RpmipModule,
    Usi2Module,
    Usi2Common,
    Usi2System,
    Usi2Board,
    WebComponentEventViewer,
    ShadowDomHelper,
    IlkModuleStateViewer,
    IlkRegisterDecoder,
    IlkModuleDecoder,
    SiioModuleRegisterView,
    SiioModuleStateView,
    RpmipModuleRegisterView,
    RpmipModuleStateView,
    Usi2SystemRegisterView,
    Usi2CommonRegisterView,
    Usi2ModuleRegisterView,
    Usi2ModuleStateView,
    DocumentSupportLoader,
    HTTP_SUPPORT,
    HTTP_STREAM_SUPPORT,
    HTML_ELEMENT_SUPPORT
}

let documentSupportLoader = null;

// Provide a hook for restarting wica support of the current document
function restartDocumentSupportLoader() {

    if ( documentSupportLoader === null ) {
        console.log( "Wica is creating new DocumentSupportLoader..." );
        const WICA_OWN_HOST = location.origin;
        documentSupportLoader = new DocumentSupportLoader( WICA_OWN_HOST );
        console.log( "Wica DocumentSupportLoader was created OK." );
    }
    else {
        console.log( "Wica is shutting down support for the existing document..." );
        documentSupportLoader.shutdown();
        console.log( "Wica document support was shutdown OK." );
    }

    console.log( "Wica is activating support for a new document..." );

    setTimeout( () => {
        documentSupportLoader.activate( Number.MAX_VALUE, 500 );
        console.log("Wica document support has been activated OK.");
    }, 0 );
}

document.wicaRestartDocumentSupportLoader = restartDocumentSupportLoader;
document.formatter = new Formatter();
document.formatter.maxInlineLength = 160;

window.onload = function() {
    setTimeout( () => {
        // Suppress document test rendering
        document.wicaRestartDocumentSupportLoader();
        console.log("Wica document support has been activated OK.");
    }, 0 );
}