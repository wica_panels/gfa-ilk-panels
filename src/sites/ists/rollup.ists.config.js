// noinspection JSUnusedGlobalSymbols

import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import copy from "rollup-plugin-copy";

import * as ISTS_TOOLS_SUBS from "./tools/ists-tools-subs.js";
import * as ISTS_UI_SUBS from "./ui/ists-ui-subs.js";

export default () => {

    const buildTarget = "build/ists_prod";
    const indexFile = "ists-index.html";

    return [ {
        input: 'src/sites/ists/tools/ists-tools-support.js',
        output: {
            dir: buildTarget + "/tools",
            format: 'es',
            sourcemap: true,
        },
        plugins: [
            copy({
                targets: [
                    // Install ISTS Index
                    { src: "src/sites/ists/" + indexFile, dest: buildTarget, rename: "index.html" },
                    { src: "src/common/assets/background.jpg", dest: buildTarget },
                    { src: "src/common/tools/assets/facility-overview-vars.css", dest: buildTarget + "/tools" },
                    { src: "src/common/assets/favicon.ico", dest: buildTarget },

                    // Install ISTS Tool Component Support Assets
                    { src: "src/common/assets/wc-register-view.css", dest: buildTarget + "/tools" },
                    { src: "src/common/assets/wc-event-view.css", dest: buildTarget + "/tools" },
                    { src: "src/common/assets/bolt.svg", dest: buildTarget + "/tools" },

                    // Install ISTS Command Tool
                    { src: "src/common/tools/templates/command-tool/command-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/command-tool/command-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => ISTS_TOOLS_SUBS.load_ists_command_tool_template( contents.toString() ),
                        rename: "ists-command-tool.html",
                    },
                    {
                        src: "src/common/tools/templates/command-tool/command-tool.template.js", dest:buildTarget + "/tools" ,
                        transform: (contents) => ISTS_TOOLS_SUBS.load_ists_command_tool_template( contents.toString() ),
                        rename: "ists-command-tool.js",
                    },

                    // Install ISTS Database Tool
                    { src: "src/common/tools/templates/database-tool/database-tool.css", dest: buildTarget + "/tools" },
                    { src: "src/common/tools/templates/database-tool/database-tool-vars.css", dest: buildTarget + "/tools" },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => ISTS_TOOLS_SUBS.load_ists_database_tool_template( contents.toString() ),
                        rename: "ists-database-tool.html",
                    },
                    {
                        src: "src/common/tools/templates/database-tool/database-tool.template.js", dest:buildTarget + "/tools" ,
                        transform: (contents) => ISTS_TOOLS_SUBS.load_ists_database_tool_template( contents.toString() ),
                        rename: "ists-database-tool.js",
                    },

                    // Install ISTS Facility Overview Tool
                    {
                        src: "src/common/tools/templates/facility-overview-tool.template.html", dest:buildTarget + "/tools" ,
                        transform: (contents) => ISTS_TOOLS_SUBS.load_ists_facility_overview_tool_template(contents.toString() ),
                        rename: "ists-facility-overview-tool.html",
                    },
                ],
            }),
            resolve(),
            commonjs(),
        ] },
        // Install ShoeLace Style
        {
            input: [ 'node_modules/@shoelace-style/shoelace/dist/shoelace.js' ],
            output: {
                dir: buildTarget + '/ui/shoelace',
                format: 'es',
            },
            plugins: [
                resolve(),
                copy({
                    targets: [
                        { src: "node_modules/@shoelace-style/shoelace/dist/themes/light.css", dest: buildTarget + '/ui/shoelace', rename: "shoelace-theme-light.css" },
                        { src: "node_modules/@shoelace-style/shoelace/dist/assets", dest: buildTarget + '/ui/shoelace',  },
                    ],
                }),
            ]
        },
        {
        input: [
            'src/sites/ists/ui/ists-ui-support.js',
        ],
        output: {
            dir: buildTarget + '/ui',
            format: 'es',
            sourcemap: true,
        },
        plugins: [
            copy({
                targets: [
                    // Install ISTS UI Component Support Assets
                    { src: "src/common/assets/bolt.svg", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/ists.svg", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/sound_enabled.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/sound_disabled.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/voice_enabled.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/voice_disabled.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/ion_source_interlock.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/avki_interlock.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/ip_branch_interlock.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/new_interlock.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/new_hipa_interlock.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/new_proscan_interlock.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/interlock_cleared.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/up4note.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/yeow.mp3", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/assets/sounds/alert.mp3", dest: buildTarget + "/ui" },

                    // Install ISTS Main View Components
                    { src: "src/common/ui/templates/main-view/main-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/main-view/main-view-vars.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/main-view/main-view-legend.svg", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/main-view/view-manager.js", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/main-view/main-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_main_view_template( contents.toString() ),
                        rename: "ists-main-view.html",
                    },
                    {
                        src: "src/common/ui/templates/main-view/main-view.template.js", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_main_view_template( contents.toString() ),
                        rename: "ists-main-view.js",
                    },

                    // Install ISTS Configuration View Components
                    { src: "src/common/ui/templates/configuration-view/configuration-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/configuration-view/configuration-view-vars.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/configuration-view/configuration-view-legend.svg", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/configuration-view/configuration-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_configuration_view_template( contents.toString() ),
                        rename: "ists-configuration-view.html",
                    },
                    {
                        src: "src/common/ui/templates/configuration-view/configuration-view.template.js", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_configuration_view_template( contents.toString() ),
                        rename: "ists-configuration-view.js",
                    },

                    // Install ISTS Interlock View Components
                    { src: "src/common/ui/templates/interlock-view/interlock-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/interlock-view/interlock-view-vars.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/interlock-view/interlock-view-legend.svg", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/interlock-view/interlock-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_interlock_view_template( contents.toString() ),
                        rename: "ists-interlock-view.html",
                    },
                    {
                        src: "src/common/ui/templates/interlock-view/interlock-view.template.js", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_interlock_view_template( contents.toString() ),
                        rename: "ists-interlock-view.js",
                    },

                    // Install ISTS Statistics View Components
                    { src: "src/common/ui/templates/statistics-view/statistics-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/statistics-view/statistics-view-vars.css", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/statistics-view/statistics-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_statistics_view_template( contents.toString() ),
                        rename: "ists-statistics-view.html",
                    },
                    {
                        src: "src/common/ui/templates/statistics-view/statistics-view.template.js", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_statistics_view_template( contents.toString() ),
                        rename: "ists-statistics-view.js",
                    },

                    // Install ISTS Quick View Components
                    { src: "src/common/ui/templates/quick-view/quick-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/quick-view/quick-view-vars.css", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/quick-view/quick-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_quick_view_template( contents.toString() ),
                        rename: "ists-quick-view.html",
                    },
                    {
                        src: "src/common/ui/templates/quick-view/quick-view.template.js", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_quick_view_template( contents.toString() ),
                        rename: "ists-quick-view.js",
                    },
                    
                    // Install ISTS Module Expert View Components
                    { src: "src/common/ui/templates/module-expert-view/module-expert-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/module-expert-view/module-expert-view-vars.css", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/module-expert-view/module-expert-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_module_expert_view_template( contents.toString() ),
                        rename: "ists-module-expert-view.html",
                    },
                    {
                        src: "src/common/ui/templates/module-expert-view/module-expert-view.template.js", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_module_expert_view_template( contents.toString() ),
                        rename: "ists-module-expert-view.js",
                    },

                    // Install ISTS Software Arch View Components
                    { src: "src/common/ui/templates/software-arch-view/software-arch-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/software-arch-view/software-arch-view-vars.css", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/software-arch-view/software-arch-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_software_arch_view_template( contents.toString() ),
                        rename: "ists-software-arch-view.html",
                    },

                    // Install ISTS User Guide View Components
                    { src: "src/common/ui/templates/user-guide-view/user-guide-view.css", dest: buildTarget + "/ui" },
                    { src: "src/common/ui/templates/user-guide-view/user-guide-view-vars.css", dest: buildTarget + "/ui" },
                    {
                        src: "src/common/ui/templates/user-guide-view/user-guide-view.template.html", dest: buildTarget + "/ui",
                        transform: (contents) => ISTS_UI_SUBS.load_ists_user_guide_view_template( contents.toString() ),
                        rename: "ists-user-guide-view.html",
                    },
                ],
            } ),
            resolve(),
            commonjs(),
        ] },    
    ];
}