export class ShadowDomHelper
{
    #shadowHost;
    #shadowRoot;

    constructor( hostElementId )
    {
        const hostEle = document.getElementById( hostElementId );
        if ( hostEle.shadowRoot )
        {
            this.#shadowHost = hostEle;
            this.#shadowRoot = hostEle.shadowRoot;
        }
        else
        {
            throw new Error( "The specified element was not a shadow host" );
        }
    }

    getShadowHost()
    {
        return this.#shadowHost;
    }

    getShadowRoot()
    {
        return this.#shadowRoot;
    }

    getElementByClassName( className )
    {
        return this.#shadowRoot.querySelector( "." + className );
    }

    getElementById( id )
    {
        return this.#shadowRoot.querySelector( "#" + id );
    }

    querySelectorAll( sel )
    {
        return this.#shadowRoot.querySelectorAll( sel );
    }

}