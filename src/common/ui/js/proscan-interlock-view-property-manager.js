import {cfgStates, ilkStates, svcStates, stopperStates} from "./svg-state-constants";

export class ProscanInterlockViewPropertyManager
{
    #hiddenColor;
    #offlineColor;
    #rebootColor;
    #okColor;
    #interlockedColor;
    #bridgedColor;
    #housingColor;
    #invisibleColor;
    #inServiceOpacity;
    #outOfServiceOpacity;

    #openColor;
    #closedColor;
    #inBetweenColor;
    #errorColor;
    #infoColor;

    #inconsistentColor;

    constructor( okColor, interlockedColor, bridgedColor, rebootColor, offlineColor, hiddenColor, housingColor, inServiceOpacity, outOfServiceOpacity )
    {
        this.#okColor = okColor;
        this.#interlockedColor = interlockedColor;
        this.#bridgedColor = bridgedColor;
        this.#rebootColor = rebootColor;
        this.#offlineColor = offlineColor;
        this.#hiddenColor = hiddenColor;
        this.#housingColor = housingColor;
        this.#invisibleColor = "rgba(0,0,0,0)";
        this.#inServiceOpacity = inServiceOpacity;
        this.#outOfServiceOpacity = outOfServiceOpacity;

        this.#openColor = "rgba(0,0,0,0)";
        this.#closedColor = "dimgray";
        this.#inBetweenColor ="cornflowerblue";
        this.#errorColor = "red";
        this.#inconsistentColor = "lightsalmon";

        this.#infoColor ="cornflowerblue";
    }

    /**
     * IONSOURCE Property Mappings.
     *
     * The 'ionsource' svgClass is used only on the IQCOM 'Q' Quelle Section.
     */
    #IONSOURCE_PROPERTY_MAPPINGS()
    {
        return [
            // The ionsource colour is determined by ilkState.
            { svgClass: "ionsource", ilkState: ilkStates.Ok,       property: "--ionsource-color", propertyValue: this.#okColor },
            { svgClass: "ionsource", ilkState: ilkStates.Ilk,      property: "--ionsource-color", propertyValue: this.#interlockedColor },
            { svgClass: "ionsource", ilkState: ilkStates.Bridged,  property: "--ionsource-color", propertyValue: this.#bridgedColor },
            { svgClass: "ionsource", ilkState: ilkStates.Rebooted, property: "--ionsource-color", propertyValue: this.#rebootColor },
            { svgClass: "ionsource", ilkState: ilkStates.Offline,  property: "--ionsource-color", propertyValue: this.#offlineColor },

            // The ionsource opacity is determined by svcState.
            { svgClass: "ionsource", svcState: svcStates.Off, property: "--ionsource-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "ionsource", svcState: svcStates.On,  property: "--ionsource-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * COMET Property Mappings.
     *
     * The 'comet' svgClass is used only on the COMET Section.
     */
    #COMET_PROPERTY_MAPPINGS()
    {
        return [
            // The comet shape colour is determined by ilkState.
            { svgClass: "comet", ilkState: ilkStates.Ok,       property: "--comet-color", propertyValue: this.#okColor },
            { svgClass: "comet", ilkState: ilkStates.Ilk,      property: "--comet-color", propertyValue: this.#interlockedColor },
            { svgClass: "comet", ilkState: ilkStates.Bridged,  property: "--comet-color", propertyValue: this.#rebootColor },
            { svgClass: "comet", ilkState: ilkStates.Rebooted, property: "--comet-color", propertyValue: this.#rebootColor },
            { svgClass: "comet", ilkState: ilkStates.Offline,  property: "--comet-color", propertyValue: this.#offlineColor },

            // The comet opacity is determined by svcState.
            { svgClass: "comet", svcState: svcStates.Off, property: "--comet-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "comet", svcState: svcStates.On,  property: "--comet-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * INFOSECTION Property Mappings.
     *
     * At PROSCAN the 'infosection' svgClass is used in various places eg &$BME1, &#AMA3, &$GNT1, &$GNT2, &$GNT3, &$OPT, &#AMD2.
     */
    #INFOSECTION_PROPERTY_MAPPINGS()
    {
        return [
            // The infosection shape colour is determined by ilkState.
            { svgClass: "infosection", ilkState: ilkStates.Ok,       property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", ilkState: ilkStates.Ilk,      property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", ilkState: ilkStates.Bridged,  property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", ilkState: ilkStates.Rebooted, property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", ilkState: ilkStates.Offline,  property: "--infopoint-shape-color",   propertyValue: this.#infoColor },

            // The infosection opacity is determined by svcState.
            { svgClass: "infosection", svcState: svcStates.Off, property: "--infopoint-opacity",       propertyValue: this.#outOfServiceOpacity },
            { svgClass: "infosection", svcState: svcStates.On,  property: "--infopoint-opacity",       propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * EXTRASECTION Property Mappings.
     *
     * At PROSCAN the 'extrasection' svgClass is used in various places eg &$BMA1, &#AMA1, &$BMD1, &$BMC1, &$BMB1, &$GNT2.
     */
    #EXTRASECTION_PROPERTY_MAPPINGS()
    {
        return [
            // The extrasection shape colour is determined by ilkState.
            { svgClass: "extrasection", ilkState: ilkStates.Ok,       property: "--extrapoint-shape-color",   propertyValue: this.#okColor },
            { svgClass: "extrasection", ilkState: ilkStates.Ilk,      property: "--extrapoint-shape-color",   propertyValue: this.#interlockedColor },
            { svgClass: "extrasection", ilkState: ilkStates.Bridged,  property: "--extrapoint-shape-color",   propertyValue: this.#bridgedColor },
            { svgClass: "extrasection", ilkState: ilkStates.Rebooted, property: "--extrapoint-shape-color",   propertyValue: this.#rebootColor },
            { svgClass: "extrasection", ilkState: ilkStates.Offline,  property: "--extrapoint-shape-color",   propertyValue: this.#offlineColor },

            // The extrasection opacity is determined by svcState.
            { svgClass: "extrasection", svcState: svcStates.Off, property: "--extrapoint-opacity",       propertyValue: this.#outOfServiceOpacity },
            { svgClass: "extrasection", svcState: svcStates.On,  property: "--extrapoint-opacity",       propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * BEAMLINE Property Mappings.
     *
     * The 'beamline' svgClass is used on the  'S', 'E' and 'N' Sections.
     *   eg's for N:  "$BI1", "INJ2", "#AXC", "#EXT", "#AXE2", "AXE", "#RING", "$BR", "AHA", "&$BRN", "#EHT", "#TGM",
     *                "TGM", "#TGE", "TGE", "#AHL", "#SINQ", "#ABK1", "ABK1", "#ABK2.
     *   eg's for S: "$MVP1", "$BV4", "$BW2", "&$BV4N", "&$BW2", "&$BW2N", "$BX1", "&$BN5", "&$BX1", "BY1", "BY2", "$BN5".
     *   eg's for E: "$BX2", "TGW", "$BHE", "$SINQ", "$BC1", "$TGB".
     *
     *   Allowed ilkState Values: [ ilkStates.Ok, ilkStates.Ilk, ilkStates.Bridged, "PON, ilkStates.Offline ].
     *   Allowed svcState Values: [ "OFF", "ON" ].
     */
    #BEAMLINE_PROPERTY_MAPPINGS()
    {
        return [
            // The beamline colour is determined by ilkState.
            { svgClass: "beamline",    ilkState: ilkStates.Ok,       property: "--beamline-color", propertyValue: this.#okColor },
            { svgClass: "beamline",    ilkState: ilkStates.Ilk,      property: "--beamline-color", propertyValue: this.#interlockedColor },
            { svgClass: "beamline",    ilkState: ilkStates.Bridged,  property: "--beamline-color", propertyValue: this.#bridgedColor },
            { svgClass: "beamline",    ilkState: ilkStates.Rebooted, property: "--beamline-color", propertyValue: this.#rebootColor },
            { svgClass: "beamline",    ilkState: ilkStates.Offline,  property: "--beamline-color", propertyValue: this.#offlineColor },

            // The beamline opacity is determined by svcState.
            { svgClass: "beamline",    svcState: svcStates.Off, property: "--beamline-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamline",    svcState: svcStates.On,  property: "--beamline-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * BEAMSTOPPER Property Mappings.
     *
     * The 'beamstopper' svgClass is used on the 'S' Stopper Sections - "Abschnitt mit Strahlfaenger".
     * eg "COMET, $BMA1, $BMB1, $BMC1, $BMD1, $BMD2, $BME1"
     */
    #BEAMSTOPPER_PROPERTY_MAPPINGS()
    {
        return [
            // The beamstopper housing colour is determined by ilkState.
            { svgClass: "beamstopper", ilkState: ilkStates.Ok,       property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Ilk,      property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Bridged,  property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Rebooted, property: "--beamstopper-housing-color", propertyValue: this.#rebootColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Offline,  property: "--beamstopper-housing-color", propertyValue: this.#offlineColor },

            // The beamstopper path colour is determined by ilkState.
            { svgClass: "beamstopper", ilkState: ilkStates.Ok,       property: "--beamstopper-path-color", propertyValue: this.#okColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Ilk,      property: "--beamstopper-path-color", propertyValue: this.#interlockedColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Bridged,  property: "--beamstopper-path-color", propertyValue: this.#bridgedColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Rebooted, property: "--beamstopper-path-color", propertyValue: this.#rebootColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Offline,  property: "--beamstopper-path-color", propertyValue: this.#offlineColor },

            // The beamstopper stopper colour is determined by stopperState.
            { svgClass: "beamstopper", stopperState: stopperStates.Open,      property: "--beamstopper-stopper-color", propertyValue: this.#openColor },
            { svgClass: "beamstopper", stopperState: stopperStates.Closed,    property: "--beamstopper-stopper-color", propertyValue: this.#closedColor },
            { svgClass: "beamstopper", stopperState: stopperStates.InBetween, property: "--beamstopper-stopper-color", propertyValue: this.#inBetweenColor },
            { svgClass: "beamstopper", stopperState: stopperStates.Error,     property: "--beamstopper-stopper-color", propertyValue: this.#errorColor },
            { svgClass: "beamstopper", stopperState: stopperStates.Offline,   property: "--beamstopper-stopper-color", propertyValue: this.#offlineColor },

            // The beamstopper opacity is determined by svcState.
            { svgClass: "beamstopper", svcState: svcStates.Off, property: "--beamstopper-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamstopper", svcState: svcStates.On,  property: "--beamstopper-opacity", propertyValue: this.#inServiceOpacity }
        ];
    }

    /**
     * KICKER Property Mappings
     *
     * At HIPA the 'kicker' svgClass is used on the $BMA1 (S) Section.
     */
    #KICKER_PROPERTY_MAPPINGS()
    {
        return [
            // The kicker housing colour is determined by ilkState.
            { svgClass: "kicker", ilkState: ilkStates.Ok,       property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", ilkState: ilkStates.Ilk,      property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", ilkState: ilkStates.Bridged,  property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", ilkState: ilkStates.Rebooted, property: "--kicker-housing-color", propertyValue: this.#rebootColor },
            { svgClass: "kicker", ilkState: ilkStates.Offline,  property: "--kicker-housing-color", propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not configured.
            { svgClass: "kicker", cfgState: cfgStates.Inconsistent, property: "--kicker-housing-color",  propertyValue: this.#inconsistentColor },

            // The kicker pathKicked colour is determined by ilkState...
            { svgClass: "kicker", ilkState: ilkStates.Ok,       property: "--kicker-pathKicked-color", propertyValue: this.#okColor },
            { svgClass: "kicker", ilkState: ilkStates.Ilk,      property: "--kicker-pathKicked-color", propertyValue: this.#interlockedColor },
            { svgClass: "kicker", ilkState: ilkStates.Bridged,  property: "--kicker-pathKicked-color", propertyValue: this.#bridgedColor },
            { svgClass: "kicker", ilkState: ilkStates.Rebooted, property: "--kicker-pathKicked-color", propertyValue: this.#rebootColor },
            { svgClass: "kicker", ilkState: ilkStates.Offline,  property: "--kicker-pathKicked-color", propertyValue: this.#offlineColor },

            // ...except where the stopperState says the path is not active.
            { svgClass: "kicker", stopperState: stopperStates.Open,    property: "--kicker-pathKicked-color", propertyValue: this.#invisibleColor },
            { svgClass: "kicker", stopperState: stopperStates.Offline, property: "--kicker-pathKicked-color", propertyValue: this.#offlineColor },

            // The kicker pathStraight colour is determined by ilkState...
            { svgClass: "kicker", ilkState: ilkStates.Ok,       property: "--kicker-pathStraight-color", propertyValue: this.#okColor },
            { svgClass: "kicker", ilkState: ilkStates.Ilk,      property: "--kicker-pathStraight-color", propertyValue: this.#interlockedColor },
            { svgClass: "kicker", ilkState: ilkStates.Bridged,  property: "--kicker-pathStraight-color", propertyValue: this.#bridgedColor },
            { svgClass: "kicker", ilkState: ilkStates.Rebooted, property: "--kicker-pathStraight-color", propertyValue: this.#rebootColor },
            { svgClass: "kicker", ilkState: ilkStates.Offline,  property: "--kicker-pathStraight-color", propertyValue: this.#offlineColor },

            // ...except where the stopperState says the path is not active
            { svgClass: "kicker", stopperState: stopperStates.Closed,  property: "--kicker-pathStraight-color", propertyValue: this.#invisibleColor },
            { svgClass: "kicker", stopperState: stopperStates.Offline, property: "--kicker-pathStraight-color", propertyValue: this.#offlineColor },

            // The beamstopper opacity is determined by svcState.
            { svgClass: "kicker", svcState: svcStates.Off, property: "--kicker-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "kicker", svcState: svcStates.On,  property: "--kicker-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * BEAMSPLITTER Property Mappings.
     *
     * The 'beamsplitter' svgClass is used on the 'V' Verteiler Sections - "Verteilmagnet fuer zwei Aeste". eg "AMA1, AMD2"
     */
    #BEAMSPLITTER_PROPERTY_MAPPINGS()
    {
        return [
            // The beamsplitter housing colour is determined by ilkState.
            { svgClass: "beamsplitter", ilkState: ilkStates.Ok,       property: "--beamsplitter-housing-color",   propertyValue: this.#housingColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Ilk,      property: "--beamsplitter-housing-color",   propertyValue: this.#housingColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Bridged,  property: "--beamsplitter-housing-color",   propertyValue: this.#housingColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Rebooted, property: "--beamsplitter-housing-color",   propertyValue: this.#rebootColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Offline,  property: "--beamsplitter-housing-color",   propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not configured.
            { svgClass: "beamsplitter", cfgState: cfgStates.Inconsistent, property: "--beamsplitter-housing-color",  propertyValue: this.#inconsistentColor },

            // The beamsplitter pathLeft colour is determined by ilkState...
            { svgClass: "beamsplitter", ilkState: ilkStates.Ok,       property: "--beamsplitter-pathLeft-color",  propertyValue: this.#okColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Ilk,      property: "--beamsplitter-pathLeft-color",  propertyValue: this.#interlockedColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Bridged,  property: "--beamsplitter-pathLeft-color",  propertyValue: this.#bridgedColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Rebooted, property: "--beamsplitter-pathLeft-color",  propertyValue: this.#rebootColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Offline,  property: "--beamsplitter-pathLeft-color",  propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not active.
            { svgClass: "beamsplitter", cfgState: cfgStates.Right,    property: "--beamsplitter-pathLeft-color",  propertyValue: this.#invisibleColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Disabled, property: "--beamsplitter-pathLeft-color",  propertyValue: this.#invisibleColor },

            // The beamsplitter pathRight colour is determined by ilkState...
            { svgClass: "beamsplitter", ilkState: ilkStates.Ok,       property: "--beamsplitter-pathRight-color", propertyValue: this.#okColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Ilk,      property: "--beamsplitter-pathRight-color", propertyValue: this.#interlockedColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Bridged,  property: "--beamsplitter-pathRight-color", propertyValue: this.#bridgedColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Rebooted, property: "--beamsplitter-pathRight-color", propertyValue: this.#rebootColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Offline,  property: "--beamsplitter-pathRight-color", propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not active.
            { svgClass: "beamsplitter", cfgState: cfgStates.Left,     property: "--beamsplitter-pathRight-color", propertyValue: this.#invisibleColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Disabled, property: "--beamsplitter-pathRight-color", propertyValue: this.#invisibleColor },

            // The beamsplitter opacity is determined by svcState.
            { svgClass: "beamsplitter", svcState: svcStates.Off, property: "--beamsplitter-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamsplitter", svcState: svcStates.On,  property: "--beamsplitter-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * LABEL Property Mappings
     */
    #LABEL_PROPERTY_MAPPINGS()
    {
        return [
            { svgClass: "label", svcState: svcStates.Off, property: "--label-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "label", svcState: svcStates.On,  property: "--label-opacity", propertyValue: this.#inServiceOpacity }
        ];
    }

    /**
     * BEAMSTEERER Property Mappings
     *
     * The 'beamsplitter' svgClass is used on the 'G' Giesskanne Section - "Verteilmagnet fuer zwei Aeste". eg "AMA3"
     */
    #BEAMSTEERER_PROPERTY_MAPPINGS()
    {
        return [
            // The beamsteerer housing colour is determined by ilkState.
            { svgClass: "beamsteerer", ilkState: ilkStates.Ok,       property: "--beamsteerer-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Ilk,      property: "--beamsteerer-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Bridged,  property: "--beamsteerer-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Rebooted, property: "--beamsteerer-housing-color", propertyValue: this.#rebootColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Offline,  property: "--beamsteerer-housing-color", propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not configured
            { svgClass: "beamsteerer", cfgState: cfgStates.Inconsistent, property: "--beamsteerer-housing-color", propertyValue: this.#inconsistentColor },

            // The beamsplitter pathD colour is determined by ilkState...
            { svgClass: "beamsteerer", ilkState: ilkStates.Ok,       property: "--beamsteerer-pathD-color",   propertyValue: this.#okColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Ilk,      property: "--beamsteerer-pathD-color",   propertyValue: this.#interlockedColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Bridged,  property: "--beamsteerer-pathD-color",   propertyValue: this.#bridgedColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Rebooted, property: "--beamsteerer-pathD-color",   propertyValue: this.#rebootColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Offline,  property: "--beamsteerer-pathD-color",   propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not active.
            { svgClass: "beamsteerer", cfgState: cfgStates.Middle,   property: "--beamsteerer-pathD-color",   propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Right,    property: "--beamsteerer-pathD-color",   propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Disabled, property: "--beamsteerer-pathD-color",   propertyValue: this.#invisibleColor },

            // The beamsplitter pathC colour is determined by ilkState...
            { svgClass: "beamsteerer", ilkState: ilkStates.Ok,       property: "--beamsteerer-pathC-color",   propertyValue: this.#okColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Ilk,      property: "--beamsteerer-pathC-color",   propertyValue: this.#interlockedColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Bridged,  property: "--beamsteerer-pathC-color",   propertyValue: this.#bridgedColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Rebooted, property: "--beamsteerer-pathC-color",   propertyValue: this.#rebootColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Offline,  property: "--beamsteerer-pathC-color",   propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not active.
            { svgClass: "beamsteerer", cfgState: cfgStates.Left,     property: "--beamsteerer-pathC-color",   propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Right,    property: "--beamsteerer-pathC-color",   propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Disabled, property: "--beamsteerer-pathC-color",   propertyValue: this.#invisibleColor },

            // The beamsplitter pathB colour is determined by ilkState...
            { svgClass: "beamsteerer", ilkState: ilkStates.Ok,       property: "--beamsteerer-pathB-color",   propertyValue: this.#okColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Ilk,      property: "--beamsteerer-pathB-color",   propertyValue: this.#interlockedColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Bridged,  property: "--beamsteerer-pathB-color",   propertyValue: this.#bridgedColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Rebooted, property: "--beamsteerer-pathB-color",   propertyValue: this.#rebootColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Offline,  property: "--beamsteerer-pathB-color",   propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not active.
            { svgClass: "beamsteerer", cfgState: cfgStates.Left,     property: "--beamsteerer-pathB-color",   propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Middle,   property: "--beamsteerer-pathB-color",   propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Disabled, property: "--beamsteerer-pathB-color",   propertyValue: this.#invisibleColor },

            // The beamsteerer opacity is determined by svcState.
            { svgClass: "beamsteerer", svcState: svcStates.Off, property: "--beamsteerer-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamsteerer", svcState: svcStates.On,  property: "--beamsteerer-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * COUPLING_POINT_PROPERTY_MAPPINGS
     *
     * The 'coupling_point' svgClass is used on the 'E' End Section - "Endabschnitt mit Dump oder Target". eg "$GNT2, $OPT, $GNT3, PIF, $GNT1"
     *
     */
    #COUPLING_POINT_PROPERTY_MAPPINGS()
    {
        return [
            // The coupling_point colour is determined by ilkState...
            { svgClass: "coupling_point", ilkState: ilkStates.Ok,       property: "--coupling-point-color",   propertyValue: this.#okColor },
            { svgClass: "coupling_point", ilkState: ilkStates.Ilk,      property: "--coupling-point-color",   propertyValue: this.#interlockedColor },
            { svgClass: "coupling_point", ilkState: ilkStates.Bridged,  property: "--coupling-point-color",   propertyValue: this.#bridgedColor },
            { svgClass: "coupling_point", ilkState: ilkStates.Rebooted, property: "--coupling-point-color",   propertyValue: this.#rebootColor },
            { svgClass: "coupling_point", ilkState: ilkStates.Offline,  property: "--coupling-point-color",   propertyValue: this.#offlineColor },

            // The coupling_point opacity is determined by svcState.
            { svgClass: "coupling_point", svcState: svcStates.Off, property: "--coupling-point-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "coupling_point", svcState: svcStates.On,  property: "--coupling-point-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * Returns an array of all property mappings defined in this class.
     *
     * @return {*[]}
     */
    #getComponentPropertyMappings()
    {
        return [
            ...this.#IONSOURCE_PROPERTY_MAPPINGS(),
            ...this.#COMET_PROPERTY_MAPPINGS(),
            ...this.#BEAMLINE_PROPERTY_MAPPINGS(),
            ...this.#BEAMSTOPPER_PROPERTY_MAPPINGS(),
            ...this.#KICKER_PROPERTY_MAPPINGS(),
            ...this.#BEAMSTEERER_PROPERTY_MAPPINGS(),
            ...this.#BEAMSPLITTER_PROPERTY_MAPPINGS(),
            ...this.#INFOSECTION_PROPERTY_MAPPINGS(),
            ...this.#EXTRASECTION_PROPERTY_MAPPINGS(),
            ...this.#LABEL_PROPERTY_MAPPINGS(),
            ...this.#COUPLING_POINT_PROPERTY_MAPPINGS()
        ];
    }

    /**
     * Returns an array of property mappings that are applicable for the specified state variable and target state.
     *
     * @param targetStateVariable
     * @param targetState
     * @return {*}
     */
    getTargetStatePropertySettings( targetStateVariable, targetState )
    {
        const mappings = this.#getComponentPropertyMappings();
        return mappings.filter( entry => entry[ targetStateVariable ] === targetState )
            .map( x => {
                return { "svgClass" : x.svgClass, "property" : x.property, "propertyValue" : x.propertyValue };
            } );
    }
}