import {cfgStates, svcStates, stopperStates} from "./svg-state-constants";

export class IlktestConfigurationViewPropertyManager
{
    #hiddenColor;
    #offlineColor;
    #rebootColor;
    #goColor;
    #stopColor;
    #disabledColor;
    #inconsistentColor;
    #housingColor;
    #invisibleColor;
    #inServiceOpacity;
    #outOfServiceOpacity;

    #openColor;
    #closedColor;
    #inBetweenColor;
    #errorColor;

    constructor( goColor, stopColor, disabledColor, inconsistentColor, rebootColor, offlineColor, hiddenColor, housingColor, inServiceOpacity, outOfServiceOpacity )
    {
        this.#goColor = goColor;
        this.#stopColor = stopColor;
        this.#disabledColor = disabledColor;
        this.#inconsistentColor = inconsistentColor;
        this.#rebootColor = rebootColor;
        this.#offlineColor = offlineColor;
        this.#hiddenColor = hiddenColor;
        this.#housingColor = housingColor;
        this.#invisibleColor = "rgba(0,0,0,0)";
        this.#inServiceOpacity = inServiceOpacity;
        this.#outOfServiceOpacity = outOfServiceOpacity;

        this.#openColor = "rgba(0,0,0,0)";
        this.#closedColor = "dimgray";
        this.#inBetweenColor ="cornflowerblue";
        this.#errorColor = "red";
    }

    /**
     * IONSOURCE Property Mappings.
     *
     * The 'ionsource' svgClass is used  on the 'BW2' Section.
     */
    #IONSOURCE_PROPERTY_MAPPINGS()
    {
        return [
            // The ionsource colour is determined by cfgState.
            { svgClass: "ionsource", cfgState: cfgStates.Propagate,    property: "--ionsource-color", propertyValue: this.#goColor },
            { svgClass: "ionsource", cfgState: cfgStates.Stop,         property: "--ionsource-color", propertyValue: this.#stopColor },
            { svgClass: "ionsource", cfgState: cfgStates.Disabled,     property: "--ionsource-color", propertyValue: this.#disabledColor },
            { svgClass: "ionsource", cfgState: cfgStates.Inconsistent, property: "--ionsource-color", propertyValue: this.#inconsistentColor },
            { svgClass: "ionsource", cfgState: cfgStates.Rebooted,     property: "--ionsource-color", propertyValue: this.#rebootColor },
            { svgClass: "ionsource", cfgState: cfgStates.Offline,      property: "--ionsource-color", propertyValue: this.#offlineColor },

            // The ionsource opacity is determined by svcState.
            { svgClass: "ionsource", svcState: svcStates.Off,       property: "--ionsource-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "ionsource", svcState: svcStates.On,        property: "--ionsource-opacity", propertyValue: this.#inServiceOpacity }
        ];
    }

    /**
     * BEAMSTOPPER Property Mappings.
     *
     * The 'beamstopper' svgClass is used on sections "BW2" and BN5.
     */
    #BEAMSTOPPER_PROPERTY_MAPPINGS()
    {
        return [
            // The beamstopper housing colour is determined by cfgState.
            { svgClass: "beamstopper", cfgState: cfgStates.Left,    property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Right,   property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Split,   property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Test,    property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Middle,  property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Protect, property: "--beamstopper-housing-color", propertyValue: this.#housingColor },

            { svgClass: "beamstopper", cfgState: cfgStates.Propagate,    property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Protect,      property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Stop,         property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Disabled,     property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Inconsistent, property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Rebooted,     property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Offline,      property: "--beamstopper-housing-color", propertyValue: this.#offlineColor },

            // The beamstopper path colour is determined by cfgState.
            { svgClass: "beamstopper", cfgState: cfgStates.Left,    property: "--beamstopper-path-color", propertyValue: this.#goColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Right,   property: "--beamstopper-path-color", propertyValue: this.#goColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Split,   property: "--beamstopper-path-color", propertyValue: this.#goColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Test,    property: "--beamstopper-path-color", propertyValue: this.#goColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Middle,  property: "--beamstopper-path-color", propertyValue: this.#goColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Protect, property: "--beamstopper-path-color", propertyValue: this.#goColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Propagate,    property: "--beamstopper-path-color", propertyValue: this.#goColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Stop,         property: "--beamstopper-path-color", propertyValue: this.#stopColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Disabled,     property: "--beamstopper-path-color", propertyValue: this.#disabledColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Inconsistent, property: "--beamstopper-path-color", propertyValue: this.#inconsistentColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Rebooted,     property: "--beamstopper-path-color", propertyValue: this.#rebootColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Offline,      property: "--beamstopper-path-color", propertyValue: this.#offlineColor },

            // The beamstopper stopper colour is determined by stopperState.
            { svgClass: "beamstopper", stopperState: stopperStates.Open,      property: "--beamstopper-stopper-color", propertyValue: this.#openColor },
            { svgClass: "beamstopper", stopperState: stopperStates.Closed,    property: "--beamstopper-stopper-color", propertyValue: this.#closedColor },
            { svgClass: "beamstopper", stopperState: stopperStates.InBetween, property: "--beamstopper-stopper-color", propertyValue: this.#inBetweenColor },
            { svgClass: "beamstopper", stopperState: stopperStates.Error,     property: "--beamstopper-stopper-color", propertyValue: this.#errorColor },
            { svgClass: "beamstopper", stopperState: cfgStates.Offline,       property: "--beamstopper-stopper-color", propertyValue: this.#offlineColor },

            // The beamstopper opacity is determined by svcState.
            { svgClass: "beamstopper", svcState: svcStates.Off, property: "--beamstopper-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamstopper", svcState: svcStates.On,  property: "--beamstopper-opacity", propertyValue: this.#inServiceOpacity }
        ];
    }

    /**
     * BEAMLINE Property Mappings.
     *
     * The 'beamline' svgClass is used on the 'BW2', 'BN5', 'AHK1' and 'VAR' Sections.
     */
    #BEAMLINE_PROPERTY_MAPPINGS()
    {
        return [
            // The beamline colour is determined by cfgState.
            { svgClass: "beamline", cfgState: cfgStates.Propagate,    property: "--beamline-color", propertyValue: this.#goColor },
            { svgClass: "beamline", cfgState: cfgStates.Protect,      property: "--beamline-color", propertyValue: this.#goColor },
            { svgClass: "beamline", cfgState: cfgStates.Left,         property: "--beamline-color", propertyValue: this.#goColor },
            { svgClass: "beamline", cfgState: cfgStates.Right,        property: "--beamline-color", propertyValue: this.#goColor },
            { svgClass: "beamline", cfgState: cfgStates.Split,        property: "--beamline-color", propertyValue: this.#goColor },
            { svgClass: "beamline", cfgState: cfgStates.Stop,         property: "--beamline-color", propertyValue: this.#stopColor },
            { svgClass: "beamline", cfgState: cfgStates.Disabled,     property: "--beamline-color", propertyValue: this.#disabledColor },
            { svgClass: "beamline", cfgState: cfgStates.Inconsistent, property: "--beamline-color", propertyValue: this.#inconsistentColor },
            { svgClass: "beamline", cfgState: cfgStates.Rebooted,     property: "--beamline-color", propertyValue: this.#rebootColor },
            { svgClass: "beamline", cfgState: cfgStates.Offline,      property: "--beamline-color", propertyValue: this.#offlineColor },

            // The beamline opacity is determined by svcState.
            { svgClass: "beamline", svcState: svcStates.Off, property: "--beamline-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamline", svcState: svcStates.On,  property: "--beamline-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * LABEL Property Mappings.
     */
    #LABEL_PROPERTY_MAPPINGS()
    {
        return [
            { svgClass: "label", svcState: svcStates.Off, property: "--label-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "label", svcState: svcStates.On,  property: "--label-opacity", propertyValue: this.#inServiceOpacity }
        ];
    }

    /**
     * COUPLING_POINT_PROPERTY_MAPPINGS.
     *
     * The 'coupling_point' svgClass is used on the 'AHK1' Section.
     */
    #COUPLING_POINT_PROPERTY_MAPPINGS()
    {
        return [
            // The coupling_point colour is determined by cfgState...
            { svgClass: "coupling_point", cfgState: cfgStates.Propagate,    property: "--coupling-point-color",   propertyValue: this.#goColor },
            { svgClass: "coupling_point", cfgState: cfgStates.Disabled,     property: "--coupling-point-color",   propertyValue: this.#disabledColor },
            { svgClass: "coupling_point", cfgState: cfgStates.Inconsistent, property: "--coupling-point-color",   propertyValue: this.#inconsistentColor },
            { svgClass: "coupling_point", cfgState: cfgStates.Rebooted,     property: "--coupling-point-color",   propertyValue: this.#rebootColor },
            { svgClass: "coupling_point", cfgState: cfgStates.Offline,      property: "--coupling-point-color",   propertyValue: this.#offlineColor },

            // The coupling_point opacity is determined by svcState.
            { svgClass: "coupling_point", svcState: svcStates.Off, property: "--coupling-point-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "coupling_point", svcState: svcStates.On,  property: "--coupling-point-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * Returns an array of all property mappings defined in this class.
     *
     * @return {*[]}
     */
    #getComponentPropertyMappings()
    {
        return [
            ...this.#IONSOURCE_PROPERTY_MAPPINGS(),
            ...this.#BEAMSTOPPER_PROPERTY_MAPPINGS(),
            ...this.#BEAMLINE_PROPERTY_MAPPINGS(),
            ...this.#LABEL_PROPERTY_MAPPINGS(),
            ...this.#COUPLING_POINT_PROPERTY_MAPPINGS(),
        ];
    }

    /**
     * Returns an array of property mappings that are applicable for the specified state variable and target state.
     *
     * @param targetStateVariable
     * @param targetState
     * @return {*}
     */
    getTargetStatePropertySettings( targetStateVariable, targetState )
    {
        const mappings = this.#getComponentPropertyMappings();
        return mappings.filter( entry => entry[ targetStateVariable ] === targetState )
            .map( x => {
                return { "svgClass" : x.svgClass, "property" : x.property, "propertyValue" : x.propertyValue };
            } );
    }
}