/**
 * Configuration states for a beamline section.
 *
 * @type {Readonly<{Inconsistent: symbol, Protect: symbol, Left: symbol, Rebooted: symbol, Stop: symbol,
 * Test: symbol, Right: symbol, Offline: symbol, Propagate: symbol, Disabled: symbol, Split: symbol, Middle: symbol}>}
 */
export const cfgStates = Object.freeze({
    Inconsistent:  Symbol ( "N/A" ),
    Offline:       Symbol ( "OFFLINE" ),
    Rebooted:      Symbol ( "PON" ),
    Disabled:      Symbol ( "O" ),
    Propagate:     Symbol ( "D" ),
    Stop:          Symbol ( "S" ),
    Left:          Symbol ( "L" ),
    Middle:        Symbol ( "M" ),
    Right:         Symbol ( "R" ),
    Split:         Symbol ( "T" ),
    Protect:       Symbol ( "P" ),
    Test:          Symbol ( "Z" )
} )

/**
 * Interlock states for a beamline section.
 *
 * @type {Readonly<{Bridged: symbol, Rebooted: symbol, Offline: symbol, Ilk: symbol, Ok: symbol}>}
 */
export const ilkStates = Object.freeze({
    Offline:   Symbol ( "OFFLINE" ),
    Rebooted:  Symbol ( "PON" ),
    Ok:        Symbol ( "OK" ),
    Ilk:       Symbol ( "ILK" ),
    Bridged:   Symbol ( "BRIDGED" )
} )

/**
 * Stopper states for a beamline section.
 *
 * @type {Readonly<{InBetween: symbol, Kicked: symbol, Closed: symbol, Error: symbol, Offline: symbol, Pulsed: symbol, Open: symbol}>}
 */
export const stopperStates = Object.freeze({
    Open:      Symbol ( "OPEN" ),
    Closed:    Symbol ( "CLOSED" ),
    InBetween: Symbol ( "IN-BETWEEN" ),
    Pulsed:    Symbol ( "PULSED" ),
    Kicked:    Symbol ( "KICKED" ),
    Error:     Symbol ( "ERROR" ),
    Offline:   Symbol ( "OFFLINE" )
} )

/**
 * States for a beamline section which may be in or out of service (operation).
 *
 * @type {Readonly<{Off: symbol, On: symbol}>}
 */
export const svcStates = Object.freeze({
    Off: Symbol ( "Off" ),
    On:  Symbol ( "On" ),
} )
