export const SESSION_SUPPORT = {
    "getCurrentSessionHelpText" : (currentUserSession) => getCurrentSessionHelpText( currentUserSession ),
    "getCurrentSessionIconColor" : (currentUserSession) => getCurrentSessionIconColor( currentUserSession ),
    "getCurrentUserPrivileges" : (currentUserSession) => getCurrentUserPrivileges( currentUserSession ),
    "getOtherUserSessionsHelpText" : (otherUserSessions) => getOtherUserSessionsHelpText( otherUserSessions ),
    "getOtherUserSessionsIconColor" : (otherUserSessions) => getOtherUserSessionsIconColor( otherUserSessions )
}

function getCurrentSessionHelpText( currentUserSession ) {
    const userName = currentUserSession[ "userName" ];
    const userPrivileges = currentUserSession[ "userPrivileges" ];
    const privilegesMsg = userPrivileges.length === 0 ? "none" : userPrivileges;
    return "You are logged in as user: '" + userName + "', with privileges: '" + privilegesMsg + "'";
}

function getCurrentSessionIconColor( currentUserSession )
{
    const userRole = currentUserSession[ "userRole" ];
    switch ( userRole )
    {
        default:
            return "darkgray";

        case "OBSERVER":
            return "white";

        case "OPERATOR":
            return "limegreen";

        case "PRIVILEGED":
            return "orange";
    }
}

function getCurrentUserPrivileges( currentUserSession )
{
    return currentUserSession[ "userPrivileges" ].split( ", " );
}

function getOtherUserSessionsHelpText( otherUserSessions )
{
    const userCountMap = createUserCountMap( otherUserSessions );
    let otherUsers = "";
    let index = 1;
    userCountMap.forEach((value, key) => {
        const separatorToken = index === userCountMap.size ? "" : ", ";
        const countToken = value === 1 ? "" : " (x" + value + ")"
        otherUsers = otherUsers + "'" + key + "'" + countToken + separatorToken;
        index++;
    });
    return (otherUsers.length === 0) ? "There are no other users" : "Other users: " + otherUsers;
}

function getOtherUserSessionsIconColor( otherUserSessions )
{
    let otherUsersMaxRole = ""
    otherUserSessions.forEach( ele  => {
        const userRole = ele[ "userRole" ];
        otherUsersMaxRole = maxUserRole_( otherUsersMaxRole, userRole );
    } );

    switch ( otherUsersMaxRole )
    {
        default:
            return "darkgray";

        case "OBSERVER":
            return "white";

        case "OPERATOR":
            return "limegreen";

        case "PRIVILEGED":
            return "orange";
    }
}

function maxUserRole_( roleA, roleB )
{
    switch ( roleA )
    {
        case "OBSERVER":
            return (roleB === "OPERATOR" ) || ( roleB === "PRIVILEGED" ) ? roleB : roleA;
        case "OPERATOR":
            return ( roleB === "PRIVILEGED" ) ? roleB : roleA;
        case "PRIVILEGED":
            return roleA;
        default:
            return roleB;
    }
}

function createUserCountMap( otherUserSessions )
{
    const userCountMap = new Map();
    otherUserSessions.forEach( (ele)  => {
        const userName = ele[ "userName" ];
        if ( userCountMap.has( userName ) )
        {
            const updatedCount = userCountMap.get( userName ) + 1
            userCountMap.set( userName, updatedCount );
        }
        else
        {
            userCountMap.set( userName, 1 );
        }
    } );
    return userCountMap;
}
