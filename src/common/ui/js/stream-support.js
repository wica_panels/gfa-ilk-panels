export const STREAM_SUPPORT = {
    "start_stream" : (probeUrl,subscribeUrl,ackUrl,sseEventType,streamStateChangedCb,streamDataUpdateCb) => start_stream( probeUrl, subscribeUrl, ackUrl, sseEventType, streamStateChangedCb, streamDataUpdateCb )
}

const STREAM_RECONNECT_INTERVAL_IN_SECONDS = 20;
let countdownInSeconds = 0;
let eventSource = undefined;

/**
 * Starts the stream manager.
 */
function start_stream( probeUrl, subscribeUrl, ackUrl, sseEventType, streamStateChangedCb, streamDataUpdateCb )
{
    const ONE_SECOND_IN_TIMER_UNITS = 1000;
    setInterval( () => {
        if ( countdownInSeconds === 0 ) {
            close_event_source();
            console.log( "Event source 'stream': creating new...");
            // Set up an asynchronous chain of events that will probe the
            // backend server and where successful subscribe to it to start
            // a stream. If the stream heartbeat signal is not seen the process
            // will repeat itself after the heartbeat interval timeout.
            // If the probe fails with a redirect code (implying an intermediate
            // web proxy session may have timed out) then the current page is reloaded.
            // If the probe fails for any other reason then further probes will
            // be made periodically until success is achieved.
            console.log( "Event source: 'stream' - Probing Server...");
            probe_server_then_call( probeUrl,
                // Handle probe Ok response => subscribe to stream
               () => {
                    console.log( "Event source: 'stream' - Server Probe OK. Subscribing to server stream...");
                    subscribe_facility_info( subscribeUrl, ackUrl, sseEventType, streamStateChangedCb, streamDataUpdateCb);
                },
                // Handle probe detects redirect response => inform handler and keep retrying.
                ( msg ) => {
                    console.log( "Event source: 'stream' - Server Probe Redirect. Message is: ", msg );
                    streamStateChangedCb( "REDIRECT" );
                    const URL_MESSAGE_OFFSET = 14;
                    // noinspection UnnecessaryLocalVariableJS
                    const relocateUrl = msg.substring( URL_MESSAGE_OFFSET );
                    window.location.href= relocateUrl;
                },
                // Handle probe error response => inform handler and keep retrying.
                ( statusMsg ) => {
                    console.log( "Event source: 'stream' - Server Probe Failed with status message: '" + statusMsg + "'" );
                    streamStateChangedCb( "ERROR" );
                }
            );
            countdownInSeconds = STREAM_RECONNECT_INTERVAL_IN_SECONDS;
        }
        countdownInSeconds--;
    }, ONE_SECOND_IN_TIMER_UNITS );
}

function close_event_source()
{
    if ( eventSource !== undefined )
    {
        console.log( "Event source: 'stream' - OK: closing existing EventSource..." );
        eventSource.close();
        console.log( "Event source: 'stream' - OK: EventSource closed." );
    }
}

// Facility Stream Subscription
function subscribe_facility_info( subscribeUrl, ackUrl, sseEventType, streamStateChangedCb, streamDataUpdateCb )
{
    console.log( "Subscribing to stream at url: '" + subscribeUrl + "' ..." );
    eventSource = new EventSource( subscribeUrl, { withCredentials: true } );
    streamStateChangedCb( "CREATED" );

    const SSE_HEARTBEAT_EVENT_TYPE = 'ev-ilk-heartbeat';
    eventSource.addEventListener( SSE_HEARTBEAT_EVENT_TYPE, () => {
        // const origin = ev.origin;
        // const lastEventId = ev.lastEventId;
        // console.log( "Origin = '" + origin + "'" );
        // console.log( "LastEventId = '" + lastEventId + "'" );
        // console.log( "Event source: 'ilk stream' - heartbeat event on stream with data: " + ev.currentTarget.url );
        countdownInSeconds = STREAM_RECONNECT_INTERVAL_IN_SECONDS;
        acknowledge_heartbeat( ackUrl );
    }, false) ;

    eventSource.addEventListener( sseEventType, ev => {
        // The following is currently (2021-01-17) disabled because it puts too much information in the browser log
        // console.trace( "Event source: 'ilk stream' - update event on stream with data: " + ev.currentTarget.url );
        // const origin = ev.origin;
        // const lastEventId = ev.lastEventId;
        // console.log( "Origin = '" + origin + "'" );
        // console.log( "LastEventId = '" + lastEventId + "'" );
        // console.log( "Event source: 'ilk stream' - information event on stream with data: " + ev.currentTarget.url );
        streamDataUpdateCb( ev );
    }, false );

    eventSource.onopen = (ev) => {
        // Note: the origin and lastEventId fields do not seem to be present when onopen is called
        // const origin = ev.origin;
        // const lastEventId = ev.lastEventId;
        // console.log( "Origin = '" + origin + "'" );
        // console.log( "LastEventId = '" + lastEventId + "'" );
        console.log( "Event source: 'ilk stream' - open event on stream with data: " + ev.currentTarget.url );
        streamStateChangedCb( "OPENED" );
    }

    eventSource.onerror = (ev) => {
        const origin = ev.origin;
        const lastEventId = ev.lastEventId;
        console.log( "Origin = '" + origin + "'" );
        console.log( "LastEventId = '" + lastEventId+ "'" );
        console.log( "Event source: 'ilk stream' - error event on stream with data: " + ev.currentTarget.url );
        streamStateChangedCb( "ERROR" );
        eventSource.close();
    }
}

function probe_server_then_call( probeUrl, cbOk, cbRedirect, cbOtherError )
{
    fetch( probeUrl, {credentials: 'include'} )
        .then( escalateHttpResponseErrors_ )
        .then( resp => cbOk( resp ) )
        .catch( error => {
            const errorMessage = error[ "message" ]
            // Possible causes of failure:
            // - the network connection was temporarily interrupted (eg PSI corp WLAN went down, train went through tunnel)
            // - the external VPN connection to PSI was closed.
            // - the F5 session ended for some reason
            // - the Spring security session timed out and the user needs to login again.
            if ( errorMessage.includes( "Redirected" ) ) {
                cbRedirect( errorMessage );
            }
            else {
                cbOtherError( "Error Message= " + errorMessage );
            }
        } )
}

function acknowledge_heartbeat( probeUrl )
{
    fetch( probeUrl, {credentials: 'include'} )
        .then( escalateHttpResponseErrors_ )
        .then( () => console.log( "heartbeat acknowledged ok" ) )
        .catch( error => {
            const errorMessage = error[ "message" ]
            console.log( "heartbeat acknowledged failed because: " + errorMessage );
        } );
}

function escalateHttpResponseErrors_( response )
{
    if ( ! response.ok )
    {
        console.log( response );
        const statusMessage = lookupStatusMessage( response.status );
        const statusText = statusMessage === "" ?  "" : " (" + statusMessage + ")";
        const errorMessage = response.headers.has( "X-ILK" ) ? response.headers.get( "X-ILK" ) : "";
        const extraText = errorMessage === "" ? "" : "\n\n" + errorMessage;
        const combinedMessage = "The HTTP server rejected the request with status code " + response.status + statusText + extraText;
        throw new Error( combinedMessage );
    }

    if ( response.redirected )
    {
        throw new Error( "Redirected to:" + response.url );
    }
    return response;
}

function lookupStatusMessage( statusCode )
{
    switch( statusCode )
    {
        case 400:
            return "Bad Request";
        case 401:
            return "Unauthorized";
        case 402:
            return "Bad Request";
        case 403:
            return "Forbidden";
        case 404:
            return "Not Found";
        case 500:
            return "Internal Server Error";
        case 502:
            return "Bad Gateway";
        case 503:
            return "Service Unavailable";
        default:
            return "";
    }
}