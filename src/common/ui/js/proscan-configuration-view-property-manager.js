import {cfgStates, svcStates, stopperStates} from "./svg-state-constants";

export class ProscanConfigurationViewPropertyManager
{
    #hiddenColor;
    #offlineColor;
    #rebootColor;
    #goColor;
    #stopColor;
    #disabledColor;
    #inconsistentColor;
    #housingColor;
    #invisibleColor;
    #inServiceOpacity;
    #outOfServiceOpacity;

    // Currently the colors used for stopper visualisation are fixed.
    #openColor;
    #closedColor;
    #inBetweenColor;
    #errorColor;

    #infoColor;

    constructor( goColor, stopColor, disabledColor, inconsistentColor, rebootColor, offlineColor, hiddenColor, housingColor, inServiceOpacity, outOfServiceOpacity )
    {
        this.#goColor = goColor;
        this.#stopColor = stopColor;
        this.#disabledColor = disabledColor;
        this.#inconsistentColor = inconsistentColor;
        this.#rebootColor = rebootColor;
        this.#offlineColor = offlineColor;
        this.#hiddenColor = hiddenColor;
        this.#housingColor = housingColor;
        this.#invisibleColor = "rgba(0,0,0,0)";
        this.#inServiceOpacity = inServiceOpacity;
        this.#outOfServiceOpacity = outOfServiceOpacity;

        // Currently the colors used for stopper visualisation are fixed.
        this.#openColor = "rgba(0,0,0,0)";
        this.#closedColor = "dimgray";
        this.#inBetweenColor ="cornflowerblue";
        this.#infoColor ="cornflowerblue";
        this.#errorColor = "red";
    }

    /**
     * IONSOURCE Property Mappings.
     *
     * The 'ionsource' svgClass is used only on the IQCOM 'Q' Quelle Section.
     */
    #IONSOURCE_PROPERTY_MAPPINGS()
    {
        return [
            // The ionsource colour is determined by cfgState.
            { svgClass: "ionsource", cfgState: cfgStates.Propagate,    property: "--ionsource-color", propertyValue: this.#goColor },
            { svgClass: "ionsource", cfgState: cfgStates.Test,         property: "--ionsource-color", propertyValue: this.#goColor },
            { svgClass: "ionsource", cfgState: cfgStates.Stop,         property: "--ionsource-color", propertyValue: this.#stopColor },

            { svgClass: "ionsource", cfgState: cfgStates.Disabled,     property: "--ionsource-color", propertyValue: this.#disabledColor },
            { svgClass: "ionsource", cfgState: cfgStates.Inconsistent, property: "--ionsource-color", propertyValue: this.#inconsistentColor },
            { svgClass: "ionsource", cfgState: cfgStates.Rebooted,     property: "--ionsource-color", propertyValue: this.#rebootColor },
            { svgClass: "ionsource", cfgState: cfgStates.Offline,      property: "--ionsource-color", propertyValue: this.#offlineColor },

            // The ionsource opacity is determined by svcState.
            { svgClass: "ionsource", svcState: svcStates.Off, property: "--ionsource-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "ionsource", svcState: svcStates.On,  property: "--ionsource-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * COMET Property Mappings.
     *
     * The 'comet' svgClass is used only on the COMET Section.
     */
    #COMET_PROPERTY_MAPPINGS()
    {
        return [
            // The comet shape colour is determined by cfgState.
            { svgClass: "comet", cfgState: cfgStates.Propagate,    property: "--comet-color", propertyValue: this.#goColor },
            { svgClass: "comet", cfgState: cfgStates.Stop,         property: "--comet-color", propertyValue: this.#stopColor },
            { svgClass: "comet", cfgState: cfgStates.Disabled,     property: "--comet-color", propertyValue: this.#disabledColor },
            { svgClass: "comet", cfgState: cfgStates.Inconsistent, property: "--comet-color", propertyValue: this.#inconsistentColor },
            { svgClass: "comet", cfgState: cfgStates.Rebooted,     property: "--comet-color", propertyValue: this.#rebootColor },
            { svgClass: "comet", cfgState: cfgStates.Offline,      property: "--comet-color", propertyValue: this.#offlineColor },

            // The comet opacity is determined by svcState.
            { svgClass: "comet", svcState: svcStates.Off, property: "--comet-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "comet", svcState: svcStates.On,  property: "--comet-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * INFOSECTION Property Mappings.
     *
     * At PROSCAN the 'infosection' svgClass is used in various places eg &$BME1, &#AMA3, &$GNT1, &$GNT2, &$GNT3, &$OPT, &#AMD2.
     */
    #INFOSECTION_PROPERTY_MAPPINGS()
    {
        return [
            // The infosection shape colour is determined by cfgState.
            { svgClass: "infosection", cfgState: cfgStates.Propagate,    property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", cfgState: cfgStates.Left,         property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", cfgState: cfgStates.Right,        property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", cfgState: cfgStates.Split,        property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", cfgState: cfgStates.Stop,         property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", cfgState: cfgStates.Disabled,     property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", cfgState: cfgStates.Inconsistent, property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", cfgState: cfgStates.Rebooted,     property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", cfgState: cfgStates.Offline,      property: "--infopoint-shape-color",   propertyValue: this.#infoColor },

            // The infosection opacity is determined by svcState.
            { svgClass: "infosection", svcState: svcStates.Off, property: "--infopoint-opacity",       propertyValue: this.#outOfServiceOpacity },
            { svgClass: "infosection", svcState: svcStates.On,  property: "--infopoint-opacity",       propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * EXTRASECTION Property Mappings.
     *
     * At PROSCAN the 'infosection' svgClass is used in various places eg &$BMA1, &#AMA1, &$BMD1, &$BMC1, &$BMB1, &$GNT2.
     */
    #EXTRASECTION_PROPERTY_MAPPINGS()
    {
        return [
            // The extrasection shape colour is determined by cfgState.
            { svgClass: "extrasection", cfgState: cfgStates.Propagate,    property: "--extrapoint-shape-color",   propertyValue: this.#goColor },
            { svgClass: "extrasection", cfgState: cfgStates.Left,         property: "--infopoint-shape-color",   propertyValue: this.#goColor },
            { svgClass: "extrasection", cfgState: cfgStates.Right,        property: "--extrapoint-shape-color",   propertyValue: this.#goColor },
            { svgClass: "extrasection", cfgState: cfgStates.Split,        property: "--extrapoint-shape-color",   propertyValue: this.#goColor },
            { svgClass: "extrasection", cfgState: cfgStates.Stop,         property: "--extrapoint-shape-color",   propertyValue: this.#stopColor },
            { svgClass: "extrasection", cfgState: cfgStates.Disabled,     property: "--extrapoint-shape-color",   propertyValue: this.#disabledColor },
            { svgClass: "extrasection", cfgState: cfgStates.Inconsistent, property: "--extrapoint-shape-color",   propertyValue: this.#inconsistentColor },
            { svgClass: "extrasection", cfgState: cfgStates.Rebooted,     property: "--extrapoint-shape-color",   propertyValue: this.#rebootColor },
            { svgClass: "extrasection", cfgState: cfgStates.Offline,      property: "--extrapoint-shape-color",   propertyValue: this.#offlineColor },

            // The extrasection opacity is determined by svcState.
            { svgClass: "extrasection", svcState: svcStates.Off, property: "--extrapoint-opacity",       propertyValue: this.#outOfServiceOpacity },
            { svgClass: "extrasection", svcState: svcStates.On,  property: "--extrapoint-opacity",       propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * BEAMLINE Property Mappings.
     *
     * The 'beamline' svgClass is used on the  'S', 'E' and 'N' Sections.
     */
    #BEAMLINE_PROPERTY_MAPPINGS()
    {
        return [
            // The beamline colour is determined by cfgState.
            { svgClass: "beamline",    cfgState: cfgStates.Propagate,    property: "--beamline-color", propertyValue: this.#goColor },
            { svgClass: "beamline",    cfgState: cfgStates.Stop,         property: "--beamline-color", propertyValue: this.#stopColor },
            { svgClass: "beamline",    cfgState: cfgStates.Disabled,     property: "--beamline-color", propertyValue: this.#disabledColor },
            { svgClass: "beamline",    cfgState: cfgStates.Inconsistent, property: "--beamline-color", propertyValue: this.#inconsistentColor },
            { svgClass: "beamline",    cfgState: cfgStates.Rebooted,     property: "--beamline-color", propertyValue: this.#rebootColor },
            { svgClass: "beamline",    cfgState: cfgStates.Offline,      property: "--beamline-color", propertyValue: this.#offlineColor },

            // The beamline opacity is determined by svcState.
            { svgClass: "beamline",    svcState: svcStates.Off, property: "--beamline-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamline",    svcState: svcStates.On,  property: "--beamline-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * BEAMSTOPPER Property Mappings.
     *
     * The 'beamstopper' svgClass is used on the 'S' Stopper Sections - "Abschnitt mit Strahlfaenger" and sometimes
     *  the 'E" End Sections. eg "COMET, $BMA1, $BMB1, $BMC1, $BMD1, $BMD2, $BME1".
     */
    #BEAMSTOPPER_PROPERTY_MAPPINGS()
    {
        return [
            { svgClass: "beamstopper", cfgState: cfgStates.Propagate,    property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Stop,         property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Disabled,     property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Inconsistent, property: "--beamstopper-housing-color", propertyValue: this.#inconsistentColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Rebooted,     property: "--beamstopper-housing-color", propertyValue: this.#rebootColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Offline,      property: "--beamstopper-housing-color", propertyValue: this.#offlineColor },

            // The beamstopper path colour is determined by cfgState.
            { svgClass: "beamstopper", cfgState: cfgStates.Propagate,    property: "--beamstopper-path-color",    propertyValue: this.#goColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Stop,         property: "--beamstopper-path-color",    propertyValue: this.#stopColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Disabled,     property: "--beamstopper-path-color",    propertyValue: this.#disabledColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Inconsistent, property: "--beamstopper-path-color",    propertyValue: this.#inconsistentColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Rebooted,     property: "--beamstopper-path-color",    propertyValue: this.#rebootColor },
            { svgClass: "beamstopper", cfgState: cfgStates.Offline,      property: "--beamstopper-path-color",    propertyValue: this.#offlineColor },

            // The beamstopper stopper colour is determined by stopperState.
            { svgClass: "beamstopper", stopperState: stopperStates.Open,      property: "--beamstopper-stopper-color", propertyValue: this.#openColor },
            { svgClass: "beamstopper", stopperState: stopperStates.Closed,    property: "--beamstopper-stopper-color", propertyValue: this.#closedColor },
            { svgClass: "beamstopper", stopperState: stopperStates.InBetween, property: "--beamstopper-stopper-color", propertyValue: this.#inBetweenColor },
            { svgClass: "beamstopper", stopperState: stopperStates.Error,     property: "--beamstopper-stopper-color", propertyValue: this.#errorColor },
            { svgClass: "beamstopper", stopperState: stopperStates.Offline,   property: "--beamstopper-stopper-color", propertyValue: this.#offlineColor },

            // The beamstopper opacity is determined by svcState.
            { svgClass: "beamstopper", svcState: svcStates.Off, property: "--beamstopper-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamstopper", svcState: svcStates.On,  property: "--beamstopper-opacity", propertyValue: this.#inServiceOpacity }
        ];
    }

    /**
     * KICKER Property Mappings
     *
     * At HIPA the 'kicker' svgClass is used on the 'S' Sections. eg &$BV4N (S)
     */
    #KICKER_PROPERTY_MAPPINGS()
    {
        return [
            // The beamstopper housing colour is determined by cfgState.
            { svgClass: "kicker", cfgState: cfgStates.Left,         property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", cfgState: cfgStates.Right,        property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", cfgState: cfgStates.Split,        property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", cfgState: cfgStates.Test,         property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", cfgState: cfgStates.Middle,       property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", cfgState: cfgStates.Protect,      property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", cfgState: cfgStates.Propagate,    property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", cfgState: cfgStates.Stop,         property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", cfgState: cfgStates.Disabled,     property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", cfgState: cfgStates.Inconsistent, property: "--kicker-housing-color", propertyValue: this.#inconsistentColor },
            { svgClass: "kicker", cfgState: cfgStates.Rebooted,     property: "--kicker-housing-color", propertyValue: this.#rebootColor },
            { svgClass: "kicker", cfgState: cfgStates.Offline,      property: "--kicker-housing-color", propertyValue: this.#offlineColor },
            
            // The kicker pathKicked colour is determined by cfgState.
            { svgClass: "kicker", cfgState: cfgStates.Left,         property: "--kicker-pathKicked-color", propertyValue: this.#goColor },
            { svgClass: "kicker", cfgState: cfgStates.Right,        property: "--kicker-pathKicked-color", propertyValue: this.#goColor },
            { svgClass: "kicker", cfgState: cfgStates.Split,        property: "--kicker-pathKicked-color", propertyValue: this.#goColor },
            { svgClass: "kicker", cfgState: cfgStates.Test,         property: "--kicker-pathKicked-color", propertyValue: this.#goColor },
            { svgClass: "kicker", cfgState: cfgStates.Middle,       property: "--kicker-pathKicked-color", propertyValue: this.#goColor },
            { svgClass: "kicker", cfgState: cfgStates.Protect,      property: "--kicker-pathKicked-color", propertyValue: this.#goColor },
            { svgClass: "kicker", cfgState: cfgStates.Propagate,    property: "--kicker-pathKicked-color", propertyValue: this.#goColor },
            { svgClass: "kicker", cfgState: cfgStates.Stop,         property: "--kicker-pathKicked-color", propertyValue: this.#stopColor },
            { svgClass: "kicker", cfgState: cfgStates.Disabled,     property: "--kicker-pathKicked-color", propertyValue: this.#disabledColor },
            { svgClass: "kicker", cfgState: cfgStates.Inconsistent, property: "--kicker-pathKicked-color", propertyValue: this.#inconsistentColor },
            { svgClass: "kicker", cfgState: cfgStates.Rebooted,     property: "--kicker-pathKicked-color", propertyValue: this.#rebootColor },
            { svgClass: "kicker", cfgState: cfgStates.Offline,      property: "--kicker-pathKicked-color", propertyValue: this.#offlineColor },

            // ...except where the stopperState says the path is not active.
            { svgClass: "kicker", stopperState: stopperStates.Open,    property: "--kicker-pathKicked-color", propertyValue: this.#invisibleColor },
            { svgClass: "kicker", stopperState: stopperStates.Offline, property: "--kicker-pathKicked-color", propertyValue: this.#offlineColor },

            // The kicker pathStraight colour is determined by cfgState.
            { svgClass: "kicker", cfgState: cfgStates.Propagate,     property: "--kicker-pathStraight-color", propertyValue: this.#goColor },
            { svgClass: "kicker", cfgState: cfgStates.Stop,          property: "--kicker-pathStraight-color", propertyValue: this.#stopColor },
            { svgClass: "kicker", cfgState: cfgStates.Disabled,      property: "--kicker-pathStraight-color", propertyValue: this.#disabledColor },
            { svgClass: "kicker", cfgState: cfgStates.Inconsistent,  property: "--kicker-pathStraight-color", propertyValue: this.#inconsistentColor },
            { svgClass: "kicker", cfgState: cfgStates.Rebooted,      property: "--kicker-pathStraight-color", propertyValue: this.#rebootColor },
            { svgClass: "kicker", cfgState: cfgStates.Offline,       property: "--kicker-pathStraight-color", propertyValue: this.#offlineColor },

            // ...except where the stopperState says the path is not active
            { svgClass: "kicker", stopperState: stopperStates.Closed,  property: "--kicker-pathStraight-color", propertyValue: this.#invisibleColor },
            { svgClass: "kicker", stopperState: stopperStates.Offline, property: "--kicker-pathStraight-color", propertyValue: this.#offlineColor },

            { svgClass: "kicker", svcState: svcStates.Off, property: "--kicker-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "kicker", svcState: svcStates.On,  property: "--kicker-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * BEAMSPLITTER Property Mappings.
     *
     * The 'beamsplitter' svgClass is used on the 'V' Verteiler Sections - "Verteilmagnet fuer zwei Aeste". eg "AMA1, AMD2"
     */
    #BEAMSPLITTER_PROPERTY_MAPPINGS()
    {
        return [
            // The beamsplitter housing colour is determined  by cfgState.
            { svgClass: "beamsplitter", cfgState: cfgStates.Left,         property: "--beamsplitter-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Right,        property: "--beamsplitter-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Disabled,     property: "--beamsplitter-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Inconsistent, property: "--beamsplitter-housing-color", propertyValue: this.#inconsistentColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Rebooted,     property: "--beamsplitter-housing-color", propertyValue: this.#rebootColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Offline,      property: "--beamsplitter-housing-color", propertyValue: this.#offlineColor },

            // The beamsplitter pathLeft colour is determined by cfgState...
            { svgClass: "beamsplitter", cfgState: cfgStates.Left,         property: "--beamsplitter-pathLeft-color", propertyValue: this.#goColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Right,        property: "--beamsplitter-pathLeft-color", propertyValue: this.#invisibleColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Disabled,     property: "--beamsplitter-pathLeft-color", propertyValue: this.#disabledColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Inconsistent, property: "--beamsplitter-pathLeft-color", propertyValue: this.#inconsistentColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Rebooted,     property: "--beamsplitter-pathLeft-color", propertyValue: this.#rebootColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Offline,      property: "--beamsplitter-pathLeft-color", propertyValue: this.#offlineColor },

            // The beamsplitter pathRight colour is determined by cfgState...
            { svgClass: "beamsplitter", cfgState: cfgStates.Left,         property: "--beamsplitter-pathRight-color", propertyValue: this.#invisibleColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Right,        property: "--beamsplitter-pathRight-color", propertyValue: this.#goColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Disabled,     property: "--beamsplitter-pathRight-color", propertyValue: this.#disabledColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Inconsistent, property: "--beamsplitter-pathRight-color", propertyValue: this.#inconsistentColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Rebooted,     property: "--beamsplitter-pathRight-color", propertyValue: this.#rebootColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Offline,      property: "--beamsplitter-pathRight-color", propertyValue: this.#offlineColor },

            // The beamsplitter opacity is determined by svcState.
            { svgClass: "beamsplitter", svcState: svcStates.Off, property: "--beamsplitter-opacity", propertyValue: 0.3 },
            { svgClass: "beamsplitter", svcState: svcStates.On,  property: "--beamsplitter-opacity", propertyValue: 1.0 },
        ];
    }

   /**
     * BEAMSTEERER Property Mappings
     *
     * The 'beamsplitter' svgClass is used on the 'G' Giesskanne Section - "Verteilmagnet fuer zwei Aeste".
     */
    #BEAMSTEERER_PROPERTY_MAPPINGS()
    {
        return [
            // The beamsteerer housing colour is determined by cfgState.
            { svgClass: "beamsteerer", cfgState: cfgStates.Left,         property: "--beamsteerer-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Middle,       property: "--beamsteerer-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Right,        property: "--beamsteerer-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Disabled,     property: "--beamsteerer-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Inconsistent, property: "--beamsteerer-housing-color", propertyValue: this.#inconsistentColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Rebooted,     property: "--beamsteerer-housing-color", propertyValue: this.#rebootColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Offline,      property: "--beamsteerer-housing-color", propertyValue: this.#offlineColor },

            // The beamsplitter pathD colour is determined by cfgState...
            { svgClass: "beamsteerer", cfgState: cfgStates.Left,         property: "--beamsteerer-pathD-color", propertyValue: this.#goColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Middle,       property: "--beamsteerer-pathD-color", propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Right,        property: "--beamsteerer-pathD-color", propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Disabled,     property: "--beamsteerer-pathD-color", propertyValue: this.#housingColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Inconsistent, property: "--beamsteerer-pathD-color", propertyValue: this.#inconsistentColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Rebooted,     property: "--beamsteerer-pathD-color", propertyValue: this.#rebootColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Offline,      property: "--beamsteerer-pathD-color", propertyValue: this.#offlineColor },

            // The beamsplitter pathC colour is determined by cfgState...
            { svgClass: "beamsteerer", cfgState: cfgStates.Left,         property: "--beamsteerer-pathC-color", propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Middle,       property: "--beamsteerer-pathC-color", propertyValue: this.#goColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Right,        property: "--beamsteerer-pathC-color", propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Disabled,     property: "--beamsteerer-pathC-color", propertyValue: this.#housingColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Inconsistent, property: "--beamsteerer-pathC-color", propertyValue: this.#inconsistentColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Rebooted,     property: "--beamsteerer-pathC-color", propertyValue: this.#rebootColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Offline,      property: "--beamsteerer-pathC-color", propertyValue: this.#offlineColor },

            // The beamsplitter pathB colour is determined by cfgState...
            { svgClass: "beamsteerer", cfgState: cfgStates.Left,         property: "--beamsteerer-pathB-color", propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Middle,       property: "--beamsteerer-pathB-color", propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Right,        property: "--beamsteerer-pathB-color", propertyValue: this.#goColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Disabled,     property: "--beamsteerer-pathB-color", propertyValue: this.#housingColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Inconsistent, property: "--beamsteerer-pathB-color", propertyValue: this.#inconsistentColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Rebooted,     property: "--beamsteerer-pathB-color", propertyValue: this.#rebootColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Offline,      property: "--beamsteerer-pathB-color", propertyValue: this.#offlineColor },

            // The beamsteerer opacity is determined by svcState.
            { svgClass: "beamsteerer", svcState: svcStates.Off, property: "--beamsteerer-opacity",       propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamsteerer", svcState: svcStates.On,  property: "--beamsteerer-opacity",       propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * COUPLING_POINT_PROPERTY_MAPPINGS
     *
     * The 'coupling_point' svgClass is used on the 'E' End Section - "Endabschnitt mit Dump oder Target". eg "$GNT2, $OPT, $GNT3, PIF, $GNT1"
     *     eg "$GNT2, $OPT, $GNT3, PIF, $GNT1".
     */
    #COUPLING_POINT_PROPERTY_MAPPINGS()
    {
        return [
            // The coupling_point colour is determined by cfgState...
            { svgClass: "coupling_point", cfgState: cfgStates.Propagate,    property: "--coupling-point-color", propertyValue: this.#goColor },
            { svgClass: "coupling_point", cfgState: cfgStates.Disabled,     property: "--coupling-point-color", propertyValue: this.#disabledColor },
            { svgClass: "coupling_point", cfgState: cfgStates.Inconsistent, property: "--coupling-point-color", propertyValue: this.#inconsistentColor },
            { svgClass: "coupling_point", cfgState: cfgStates.Rebooted,     property: "--coupling-point-color", propertyValue: this.#rebootColor },
            { svgClass: "coupling_point", cfgState: cfgStates.Offline,      property: "--coupling-point-color", propertyValue: this.#offlineColor },

            // The coupling_point opacity is determined by svcState.
            { svgClass: "coupling_point", svcState: svcStates.Off, property: "--coupling-point-opacity", propertyValue: 0.3 },
            { svgClass: "coupling_point", svcState: svcStates.On,  property: "--coupling-point-opacity", propertyValue: 1.0 },
        ];
    }

    /**
     * LABEL Property Mappings
     */
    #LABEL_PROPERTY_MAPPINGS()
    {
        return [
            { svgClass: "label", svcState: svcStates.Off, property: "--label-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "label", svcState: svcStates.On,  property: "--label-opacity", propertyValue: this.#inServiceOpacity }
        ];
    }

    /**
     * Returns an array of all property mappings defined in this class.
     *
     * @return {*[]}
     */
    #getComponentPropertyMappings()
    {
        return [
            ...this.#IONSOURCE_PROPERTY_MAPPINGS(),
            ...this.#COMET_PROPERTY_MAPPINGS(),
            ...this.#BEAMLINE_PROPERTY_MAPPINGS(),
            ...this.#BEAMSTOPPER_PROPERTY_MAPPINGS(),
            ...this.#KICKER_PROPERTY_MAPPINGS(),
            ...this.#BEAMSTEERER_PROPERTY_MAPPINGS(),
            ...this.#BEAMSPLITTER_PROPERTY_MAPPINGS(),
            ...this.#INFOSECTION_PROPERTY_MAPPINGS(),
            ...this.#EXTRASECTION_PROPERTY_MAPPINGS(),
            ...this.#LABEL_PROPERTY_MAPPINGS(),
            ...this.#COUPLING_POINT_PROPERTY_MAPPINGS()
        ];
    }

    /**
     * Returns an array of property mappings that are applicable for the specified state variable and target state.
     *
     * @param targetStateVariable
     * @param targetState
     * @return {*}
     */
    getTargetStatePropertySettings( targetStateVariable, targetState )
    {
        const mappings = this.#getComponentPropertyMappings();
        return mappings.filter( entry => entry[ targetStateVariable ] === targetState )
            .map( x => {
                return { "svgClass" : x.svgClass, "property" : x.property, "propertyValue" : x.propertyValue };
            } );
    }
}