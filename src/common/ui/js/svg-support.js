export const SVG_SUPPORT = {
    "svgInitDetector" : (shadowDomHelper) => svgInitDetector( shadowDomHelper )
}

async function svgInitDetector( shadowDomHelper)
{
    const svgFacilityElement = shadowDomHelper.getElementById( "svgFacilityElement" );
    const svgLegendElement = shadowDomHelper.getElementById( "svgLegendElement" );

    const event1 = new Promise(resolve => {

        // There are two possibilities here:
        // 1. The SVG is already loaded, in which case it is too late to listen for load events.
        // 2. The SVG is NOT already loaded, in which case we wait for the load event.
        // The test below is able to distinguish between these two situations. The null check seems to be required
        // on Firefox platforms, otherwise an error is thrown.
        if ( ( svgFacilityElement.contentDocument != null ) && ( svgFacilityElement.contentDocument.toString() !== "[object HTMLDocument]" ) )
        {
            const contentDocumentType = svgFacilityElement.contentDocument;
            console.log( "Promise 1 resolved - svgFacilityElement is already loaded. type is: " + contentDocumentType  );
            resolve();
        }
        else {
            svgFacilityElement.addEventListener( "load", () => {
                console.log( "Promise 1 resolved - svgFacilityElement has been loaded." );
                resolve();
            });
        }
    });

    const event2 = new Promise(resolve => {
        if ( ( svgLegendElement.contentDocument != null ) && ( svgLegendElement.contentDocument.toString() !== "[object HTMLDocument]" ) )
        {
            const contentDocumentType = svgLegendElement.contentDocument;
            console.log( "Promise 1 resolved - svgLegendElement is already loaded. type is: " + contentDocumentType  );
            resolve();
        }
        else {
            svgLegendElement.addEventListener("load", () => {
                console.log( "Promise 2 resolved - svgLegendElement has been loaded." );
                resolve();
            });
        }
    });
    return Promise.all([ event1, event2 ] );

}