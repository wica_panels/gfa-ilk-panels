const announcements = {
    SOUND_ENABLED : Symbol(),
    SOUND_DISABLED : Symbol(),
    VOICE_ENABLED : Symbol(),
    VOICE_DISABLED : Symbol(),
    MINOR_INTERLOCK : Symbol(),
    MAJOR_INTERLOCK : Symbol(),
    STANDARD_INTERLOCK : Symbol(),
    INTERLOCK_CLEARED : Symbol(),
    NONE : Symbol()
}

export const SOUND_SUPPORT = {
    "initialise": (facilityId,messageEle) => initialise( facilityId, messageEle ),
    "announce": (announcementType,enableSound,enableVoice ) => announce( announcementType, enableSound, enableVoice ),
    "announcements" : announcements
}

let soundEnabledAudio_;
let soundDisabledAudio_;
let voiceEnabledAudio_;
let voiceDisabledAudio_;
let minorInterlockAudio_;
let majorInterlockAudio_;
let standardInterlockAudio_;
let interlockClearedAudio_;
let up4noteAudio_;
let yeowAudio_;
let alertAudio_;
let messageEle_;

function initialise( facilityId, messageEle )
{
    switch( facilityId )
    {
        case "hipa":
            standardInterlockAudio_ = new Audio( "new_hipa_interlock.mp3" );
            break;

        case "proscan":
            standardInterlockAudio_ = new Audio( "new_proscan_interlock.mp3" );
            break;

        case "ists":
        case "ilktest":
            standardInterlockAudio_ = new Audio( "new_interlock.mp3" );
            break;

        default:
            console.warn( "The facilityId was not recognised. The standard sounds will be used." );
            standardInterlockAudio_ = new Audio( "new_interlock.mp3" );
            break;
    }

    up4noteAudio_ = new Audio( "up4note.mp3" );
    yeowAudio_ = new Audio( "yeow.mp3" );
    alertAudio_ = new Audio( "alert.mp3" );
    soundEnabledAudio_ = new Audio( "sound_enabled.mp3" );
    soundDisabledAudio_ = new Audio( "sound_disabled.mp3" );
    voiceEnabledAudio_ = new Audio( "voice_enabled.mp3" );
    voiceDisabledAudio_ = new Audio( "voice_disabled.mp3" );
    interlockClearedAudio_ = new Audio( "interlock_cleared.mp3" );
    majorInterlockAudio_ = new Audio( "ion_source_interlock.mp3" );
    minorInterlockAudio_ = new Audio( "ip_branch_interlock.mp3" );
    messageEle_ = messageEle;
}

function announce( announcementType, enableSound, enableVoice )
{
    switch (announcementType)
    {
        case announcements.NONE:
            break;

        case announcements.SOUND_ENABLED:
            playAudio( soundEnabledAudio_ );
            break;

        case announcements.SOUND_DISABLED:
            playAudio( soundDisabledAudio_ );
            break;

        case announcements.VOICE_ENABLED:
            playAudio( voiceEnabledAudio_ );
            break;

        case announcements.VOICE_DISABLED:
            playAudio( voiceDisabledAudio_ );
            break;

        case announcements.MINOR_INTERLOCK:
            if (enableSound && enableVoice)
            {
                alertAudio_.onended = () => playAudio( minorInterlockAudio_ );
                playAudio( alertAudio_ );
            }
            else if (enableSound)
            {
                playAudio( alertAudio_ );
            }
            else if (enableVoice)
            {
                playAudio( minorInterlockAudio_ );
            }
            break;

        case announcements.MAJOR_INTERLOCK:
            if (enableSound && enableVoice)
            {
                yeowAudio_.onended = () => playAudio( majorInterlockAudio_ );
                playAudio( yeowAudio_ );
            }
            else if ( enableSound )
            {
                playAudio( yeowAudio_ );
            }
            else if (enableVoice)
            {
                playAudio( majorInterlockAudio_ );
            }
            break;

        case announcements.STANDARD_INTERLOCK:
            if (enableSound && enableVoice)
            {
                up4noteAudio_.onended = () => playAudio( standardInterlockAudio_ );
                playAudio( up4noteAudio_ );
            }
            else if (enableSound)
            {
                playAudio( up4noteAudio_ );
            }
            else if (enableVoice)
            {
                playAudio( standardInterlockAudio_ );
            }
            break;

        case announcements.INTERLOCK_CLEARED:
            if (enableVoice)
            {
                playAudio( interlockClearedAudio_ );
            }
            break;

        default:
            console.warn("The announcement type: '" + announcementType + "' was not recognised.");
    }
}


function playAudio( audio )
{
    const promise = audio.play();

    if ( promise === undefined )
    {
        console.warn("audio play returned an undefined result." );
        return;
    }
    promise.then( () => {
        // Autoplay started!
        console.log( "audio play promise was started successfully." );
    } )
    .catch( error => {
        // Autoplay was prevented by the browser policy
        console.info("audio play resulted in the following error: '" + error + "'. Will request user permission !" ) ;
        messageEle_.addEventListener( 'ev-ilk-popup-message-close', function() {
            console.log( "popup was closed" );
            audio.play();
        });
        messageEle_.closeable = true;
        messageEle_.titleText = "Permission to Play Audio";
        messageEle_.messageText = "The application needs your permission to play audio. Please close this window to confirm you are ok with this. " +
            "To avoid seeing this message again please consider whitelisting this site in your browser's security settings.";
    } );
}
