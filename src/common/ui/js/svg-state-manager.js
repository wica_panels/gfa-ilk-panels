import {cfgStates, ilkStates, svcStates, stopperStates} from "./svg-state-constants";

/**
 * Manages the state of an SVG document.
 */
export class SvgStateManager
{
    #propertyManager;
    #svgDocument;

    static #BEAMLINE_SECTION_CLASS_IDENTIFIER = "BEAMLINE_SECTION";

    constructor( svgDocument, propertyManager )
    {
        this.#svgDocument = svgDocument;
        this.#propertyManager = propertyManager;
    }

    /**
     * Retrieves an array of beamline section elements from the SVG document.
     *
     * @returns {Array<SVGElement>} An array of SVG elements representing beamline sections.
     */
    getBeamlineSectionElements()
    {
        // Get an array of all svg elements in the SVG document whose classname is 'BEAMLINE SECTION'
        const beamlineSectionParentElements =  this.#svgDocument.getElementsByClassName( SvgStateManager.#BEAMLINE_SECTION_CLASS_IDENTIFIER );
        return Array.from( beamlineSectionParentElements );
    }

    /**
     * Sets the specified beamline section to visually reflect the section's interlock state.
     *
     * @param {string} sectionId the section id.
     * @param {string} sectionIlkState the section's interlock state.
     */
    setBeamlineSectionInterlockState( sectionId, sectionIlkState )
    {
        const targetStateVariable = "ilkState";
        const translations = {
            "UNKNOWN": ilkStates.Offline,
            "PON":     ilkStates.Rebooted,
            "BRIDGED": ilkStates.Bridged,
            "OK":      ilkStates.Ok,
            "ILK":     ilkStates.Ilk
        }
        if ( Object.hasOwnProperty.call( translations, sectionIlkState ) )
        {
            const targetedState = translations[ sectionIlkState ];
            this.#setBeamlineSectionState( sectionId, targetStateVariable, targetedState );
        }
        else {
            console.warn( "Unexpected sectionIlkState !") ;
        }
    }

    /**
     * Sets the specified beamline section to visually reflect the section's configuration state.
     *
     * @param {string}  sectionId the section id.
     * @param {string} sectionCfgState the section's configuration state.
     */
    setBeamlineSectionConfigurationState( sectionId, sectionCfgState )
    {
        const targetStateVariable = "cfgState";
        const translations = {
            "UNKNOWN":      cfgStates.Offline,
            "PON":          cfgStates.Rebooted,
            "N/A":          cfgStates.Inconsistent,
            "L (Left)":     cfgStates.Left,
            "R (Right)":    cfgStates.Right,
            "M (Middle)":   cfgStates.Middle,
            "P (Protect)":  cfgStates.Protect,
            "D (Durch)":    cfgStates.Propagate,
            "S (Stop)":     cfgStates.Stop,
            "T (Split)":    cfgStates.Split,
            "Z (Test)":     cfgStates.Test,
            "O (Inactive)": cfgStates.Disabled
        }

        if ( Object.hasOwnProperty.call( translations, sectionCfgState ) )
        {
            const targetedState = translations[ sectionCfgState ];
            this.#setBeamlineSectionState( sectionId, targetStateVariable, targetedState );
        }
        else {
            console.warn( "Unexpected sectionCfgState !") ;
        }
    }

    /**
     * Sets the specified beamline section to visually reflect the state of the stopper
     * (if any) with which it is associated.
     *
     * @param {string} sectionId the section id.
     * @param {string} sectionStopperState  the section's stopper state whose allowed values are
     * "OPEN", "CLOSED", "IN-BETWEEN", "ERROR" or "UNKNOWN"
     */
    setBeamlineSectionStopperState( sectionId, sectionStopperState )
    {
        const targetStateVariable = "stopperState";
        const translations = {
            "OPEN":       stopperStates.Open,
            "CLOSED":     stopperStates.Closed,
            "IN-BETWEEN": stopperStates.InBetween,
            "PULSED":     stopperStates.Pulsed,
            "KICKED":     stopperStates.Kicked,
            "ERROR":      stopperStates.Error,
            "UNKNOWN":    stopperStates.Offline,
        }
        if ( Object.hasOwnProperty.call( translations, sectionStopperState ) )
        {
            const targetedState = translations[ sectionStopperState ];
            this.#setBeamlineSectionState( sectionId, targetStateVariable, targetedState );
        }
        // BUG FIX: 2023-04-26 Currently the interlock system sets the sectionStopperState to
        // BUG FIX: an empty string when the section is Offline. This should be corrected on
        // BUG FIX: the ILKCS server but to avoid flooding the log for the moment these messages
        // will be suppressed.
        // else {
        //     console.trace( "Unexpected sectionStopperState !") ;
        // }
    }

    /**
     * Sets the specified beamline section to visually whether the section is in active service or not.
     *
     * @param {string} sectionId the section id.
     * @param {boolean} sectionInService flag set true when the section is in active operation.
     */
    setBeamlineSectionServiceState( sectionId, sectionInService )
    {
        const targetStateVariable = "svcState";
        const translations = {
            false: svcStates.Off,
            true:  svcStates.On
        }
        if ( Object.hasOwnProperty.call( translations, sectionInService ) )
        {
            const targetedState = translations[ sectionInService ];
            this.#setBeamlineSectionState( sectionId, targetStateVariable, targetedState );
        }
        else
        {
            console.warn( "Unexpected sectionInService !") ;
        }
    }

    /**
     * Sets the specified beamline section to visually whether the section is in active service or not.
     *
     * @param {string} sectionId the section id.
     * @param {string} targetStateVariable the state variable.
     * @param {any} targetState the state.
     */
    #setBeamlineSectionState( sectionId, targetStateVariable, targetState )
    {
        // Identify the beamline section (if any) whose id matches the specified sectionId
        const beamlineSections = this.getBeamlineSectionElements();
        const svgParentEle = beamlineSections.find( ele => ele.id === sectionId );
        if ( svgParentEle !== undefined )
        {
            const propertySettings = this.#propertyManager.getTargetStatePropertySettings( targetStateVariable, targetState );
            this.#setBeamlineSectionProperties( svgParentEle, propertySettings );
        }
    }

    /**
     * For all SVG children of the parent element apply any property settings that are relevant
     * for the element's class.
     *
     * @param {SVGElement} svgParentEle the parent element
     * @param {Array<Object>} propertySettings array of property mappings
     */
    #setBeamlineSectionProperties( svgParentEle, propertySettings )
    {
        // Iterate through each propertySetting object
        propertySettings.forEach( propSetting =>
        {
            // Get all SVG children of the SVG parent element with class names that match the class in the property object.
            const targetEles = svgParentEle.getElementsByClassName( propSetting.svgClass );

            // Set the property to the specified value
            Array.from( targetEles ).forEach( (ele) => ele.style.setProperty( propSetting.property, propSetting.propertyValue ) );
        } );
    }
}
