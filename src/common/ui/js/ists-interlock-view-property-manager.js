import {cfgStates, ilkStates, svcStates, stopperStates} from "./svg-state-constants";

export class IstsInterlockViewPropertyManager
{
    #hiddenColor;
    #offlineColor;
    #rebootColor;
    #okColor;
    #interlockedColor;
    #bridgedColor;
    #housingColor;
    #invisibleColor;
    #inServiceOpacity;
    #outOfServiceOpacity;

    #openColor;
    #closedColor;
    #inBetweenColor;
    #errorColor;
    #infoColor;

    #inconsistentColor;

    constructor( okColor, interlockedColor, bridgedColor, rebootColor, offlineColor, hiddenColor, housingColor, inServiceOpacity, outOfServiceOpacity )
    {
        this.#okColor = okColor;
        this.#interlockedColor = interlockedColor;
        this.#bridgedColor = bridgedColor;
        this.#rebootColor = rebootColor;
        this.#offlineColor = offlineColor;
        this.#hiddenColor = hiddenColor;
        this.#housingColor = housingColor;
        this.#invisibleColor = "rgba(0,0,0,0)";
        this.#inServiceOpacity = inServiceOpacity;
        this.#outOfServiceOpacity = outOfServiceOpacity;

        this.#openColor = "rgba(0,0,0,0)";
        this.#closedColor = "dimgray";
        this.#inBetweenColor ="cornflowerblue";
        this.#errorColor = "red";
        this.#inconsistentColor = "lightsalmon";

        this.#infoColor ="cornflowerblue";
    }

    /**
     * IONSOURCE Property Mappings.
     *
     * The 'ionsource' svgClass is used only on the IQIN2 'Q' Quelle Section.
     */
    #IONSOURCE_PROPERTY_MAPPINGS()
    {
        return [
            // The ionsource colour is determined by ilkState.
            { svgClass: "ionsource", ilkState: ilkStates.Ok,       property: "--ionsource-color", propertyValue: this.#okColor },
            { svgClass: "ionsource", ilkState: ilkStates.Ilk,      property: "--ionsource-color", propertyValue: this.#interlockedColor },
            { svgClass: "ionsource", ilkState: ilkStates.Bridged,  property: "--ionsource-color", propertyValue: this.#bridgedColor },
            { svgClass: "ionsource", ilkState: ilkStates.Rebooted, property: "--ionsource-color", propertyValue: this.#rebootColor },
            { svgClass: "ionsource", ilkState: ilkStates.Offline,  property: "--ionsource-color", propertyValue: this.#offlineColor },

            // The ionsource opacity is determined by svcState.
            { svgClass: "ionsource", svcState: "OFF", property: "--ionsource-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "ionsource", svcState: "ON", property: "--ionsource-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * TARGET Property Mappings.
     *
     * The 'target' svgClass is used on the $TGW ('S'), TGM ('N') and TGE ('N') Sections
     */
    #TARGET_PROPERTY_MAPPINGS()
    {
        return [
            // The target housing colour is determined by ilkState.
            { svgClass: "target", ilkState: ilkStates.Ok,        property: "--target-housing-color", propertyValue: this.#housingColor },
            { svgClass: "target", ilkState: ilkStates.Ilk,       property: "--target-housing-color", propertyValue: this.#housingColor },
            { svgClass: "target", ilkState: ilkStates.Bridged,   property: "--target-housing-color", propertyValue: this.#housingColor },
            { svgClass: "target", ilkState: ilkStates.Rebooted,  property: "--target-housing-color", propertyValue: this.#housingColor },
            { svgClass: "target", ilkState: ilkStates.Offline,   property: "--target-housing-color", propertyValue: this.#offlineColor },

            // The target path colour is determined by ilkState.
            { svgClass: "target", ilkState: ilkStates.Ok,        property: "--target-path-color",    propertyValue: this.#okColor },
            { svgClass: "target", ilkState: ilkStates.Ilk,       property: "--target-path-color",    propertyValue: this.#interlockedColor },
            { svgClass: "target", ilkState: ilkStates.Bridged,   property: "--target-path-color",    propertyValue: this.#bridgedColor },
            { svgClass: "target", ilkState: ilkStates.Rebooted,  property: "--target-path-color",    propertyValue: this.#rebootColor },
            { svgClass: "target", ilkState: ilkStates.Offline,   property: "--target-path-color",    propertyValue: this.#offlineColor },

            // The target opacity is determined by svcState.
            { svgClass: "target", svcState: svcStates.Off, property: "--target-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "target", svcState: svcStates.On,  property: "--target-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * INFOSECTION Property Mappings.
     *
     * At ISTS the 'infosection' svgClass is used on the 'S' Stopper Sections. eg &$BW2, &$BX1, &$BN5.
     */
    #INFOSECTION_PROPERTY_MAPPINGS()
    {
        return [
            // The infosection shape colour is determined by ilkState.
            { svgClass: "infosection", ilkState: ilkStates.Ok,        property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", ilkState: ilkStates.Ilk,       property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", ilkState: ilkStates.Bridged,   property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", ilkState: ilkStates.Rebooted,  property: "--infopoint-shape-color",   propertyValue: this.#infoColor },
            { svgClass: "infosection", ilkState: ilkStates.Offline,   property: "--infopoint-shape-color",   propertyValue: this.#infoColor },

            // The infosection opacity is determined by svcState.
            { svgClass: "infosection", svcState: svcStates.Off, property: "--infopoint-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "infosection", svcState: svcStates.On,  property: "--infopoint-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * EXTRASECTION Property Mappings.
     *
     * At ISTS the 'extrasection' svgClass is used on the 'S', 'T' and 'N' Sections. eg &$BW2N (S), &EXTA (T), &$BRN (N), &EHTA (T)
     */
    #EXTRASECTION_PROPERTY_MAPPINGS()
    {
        return [
            // The extrasection shape colour is determined by ilkState.
            { svgClass: "extrasection", ilkState: ilkStates.Ok,        property: "--extrapoint-shape-color", propertyValue: this.#okColor },
            { svgClass: "extrasection", ilkState: ilkStates.Ilk,       property: "--extrapoint-shape-color", propertyValue: this.#interlockedColor },
            { svgClass: "extrasection", ilkState: ilkStates.Bridged,   property: "--extrapoint-shape-color", propertyValue: this.#bridgedColor },
            { svgClass: "extrasection", ilkState: ilkStates.Rebooted,  property: "--extrapoint-shape-color", propertyValue: this.#rebootColor },
            { svgClass: "extrasection", ilkState: ilkStates.Offline,   property: "--extrapoint-shape-color", propertyValue: this.#offlineColor },

            // The extrasection opacity is determined by svcState.
            { svgClass: "extrasection", svcState: svcStates.Off, property: "--extrapoint-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "extrasection", svcState: svcStates.On,  property: "--extrapoint-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * BEAMLINE Property Mappings.
     *
     * The 'beamline' svgClass is used on the  'S', 'E' and 'N' Sections.
     */
    #BEAMLINE_PROPERTY_MAPPINGS()
    {
        return [
            // The beamline colour is determined by ilkState.
            { svgClass: "beamline",    ilkState: ilkStates.Ok,       property: "--beamline-color", propertyValue: this.#okColor },
            { svgClass: "beamline",    ilkState: ilkStates.Ilk,      property: "--beamline-color", propertyValue: this.#interlockedColor },
            { svgClass: "beamline",    ilkState: ilkStates.Bridged,  property: "--beamline-color", propertyValue: this.#bridgedColor },
            { svgClass: "beamline",    ilkState: ilkStates.Rebooted, property: "--beamline-color", propertyValue: this.#rebootColor },
            { svgClass: "beamline",    ilkState: ilkStates.Offline,  property: "--beamline-color", propertyValue: this.#offlineColor },

            // The beamline opacity is determined by svcState.
            { svgClass: "beamline",    svcState: svcStates.Off, property: "--beamline-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamline",    svcState: svcStates.On,  property: "--beamline-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * BEAMSTOPPER Property Mappings.
     *
     * The 'beamstopper' svgClass is used on the 'S' Stopper Sections - "Abschnitt mit Strahlfaenger".
     */
    #BEAMSTOPPER_PROPERTY_MAPPINGS()
    {
        return [
            // The beamstopper housing colour is determined by ilkState.
            { svgClass: "beamstopper", ilkState: ilkStates.Ok,       property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Ilk,      property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Bridged,  property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Rebooted, property: "--beamstopper-housing-color", propertyValue: this.#rebootColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Offline,  property: "--beamstopper-housing-color", propertyValue: this.#offlineColor },

            // The beamstopper path colour is determined by ilkState.
            { svgClass: "beamstopper", ilkState: ilkStates.Ok,       property: "--beamstopper-path-color", propertyValue: this.#okColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Ilk,      property: "--beamstopper-path-color", propertyValue: this.#interlockedColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Bridged,  property: "--beamstopper-path-color", propertyValue: this.#bridgedColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Rebooted, property: "--beamstopper-path-color", propertyValue: this.#rebootColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Offline,  property: "--beamstopper-path-color", propertyValue: this.#offlineColor },

            // The beamstopper stopper colour is determined by stopperState.
            { svgClass: "beamstopper", stopperState: stopperStates.Open,      property: "--beamstopper-stopper-color", propertyValue: this.#openColor },
            { svgClass: "beamstopper", stopperState: stopperStates.Closed,    property: "--beamstopper-stopper-color", propertyValue: this.#closedColor },
            { svgClass: "beamstopper", stopperState: stopperStates.InBetween, property: "--beamstopper-stopper-color", propertyValue: this.#inBetweenColor },
            { svgClass: "beamstopper", stopperState: stopperStates.Error,     property: "--beamstopper-stopper-color", propertyValue: this.#errorColor },
            { svgClass: "beamstopper", stopperState: stopperStates.Offline,   property: "--beamstopper-stopper-color", propertyValue: this.#offlineColor },

            // The beamstopper opacity is determined by svcState.
            { svgClass: "beamstopper", svcState: svcStates.Off, property: "--beamstopper-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamstopper", svcState: svcStates.On,  property: "--beamstopper-opacity", propertyValue: this.#inServiceOpacity }
        ];
    }

    /**
     * KICKER Property Mappings
     *
     * At ISTS the 'kicker' svgClass is used on the 'S' Sections. eg &$BV4N (S)
     */
    #KICKER_PROPERTY_MAPPINGS()
    {
        return [
            // The kicker housing colour is determined by ilkState.
            { svgClass: "kicker", ilkState: ilkStates.Ok,        property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", ilkState: ilkStates.Ilk,       property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", ilkState: ilkStates.Bridged,   property: "--kicker-housing-color", propertyValue: this.#housingColor },
            { svgClass: "kicker", ilkState: ilkStates.Rebooted,  property: "--kicker-housing-color", propertyValue: this.#rebootColor },
            { svgClass: "kicker", ilkState: ilkStates.Offline,   property: "--kicker-housing-color", propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not configured.
            { svgClass: "kicker", cfgState: cfgStates.Inconsistent, property: "--kicker-housing-color",  propertyValue: this.#inconsistentColor },

            // The kicker pathKicked colour is determined by ilkState...
            { svgClass: "kicker", ilkState: ilkStates.Ok,        property: "--kicker-pathKicked-color", propertyValue: this.#okColor },
            { svgClass: "kicker", ilkState: ilkStates.Ilk,       property: "--kicker-pathKicked-color", propertyValue: this.#interlockedColor },
            { svgClass: "kicker", ilkState: ilkStates.Bridged,   property: "--kicker-pathKicked-color", propertyValue: this.#bridgedColor },
            { svgClass: "kicker", ilkState: ilkStates.Rebooted,  property: "--kicker-pathKicked-color", propertyValue: this.#rebootColor },
            { svgClass: "kicker", ilkState: ilkStates.Offline,   property: "--kicker-pathKicked-color", propertyValue: this.#offlineColor },

            // ...except where the stopperState says the path is not active.
            { svgClass: "kicker", stopperState: stopperStates.Open,    property: "--kicker-pathKicked-color", propertyValue: this.#invisibleColor },
            { svgClass: "kicker", stopperState: stopperStates.Pulsed,  property: "--kicker-pathKicked-color", propertyValue: this.#invisibleColor },
            { svgClass: "kicker", stopperState: stopperStates.Offline, property: "--kicker-pathKicked-color", propertyValue: this.#offlineColor },

            // The kicker pathStraight colour is determined by ilkState...
            { svgClass: "kicker", ilkState: ilkStates.Ok,       property: "--kicker-pathStraight-color", propertyValue: this.#okColor },
            { svgClass: "kicker", ilkState: ilkStates.Ilk,      property: "--kicker-pathStraight-color", propertyValue: this.#interlockedColor },
            { svgClass: "kicker", ilkState: ilkStates.Bridged,  property: "--kicker-pathStraight-color", propertyValue: this.#bridgedColor },
            { svgClass: "kicker", ilkState: ilkStates.Rebooted, property: "--kicker-pathStraight-color", propertyValue: this.#rebootColor },
            { svgClass: "kicker", ilkState: ilkStates.Offline,  property: "--kicker-pathStraight-color", propertyValue: this.#offlineColor },

            // ...except where the stopperState says the path is not active
            { svgClass: "kicker", stopperState: stopperStates.Kicked, property: "--kicker-pathStraight-color", propertyValue: this.#invisibleColor },
            { svgClass: "kicker", stopperState: stopperStates.Pulsed, property: "--kicker-pathStraight-color", propertyValue: this.#invisibleColor },
            { svgClass: "kicker", stopperState: ilkStates.Offline,    property: "--kicker-pathStraight-color", propertyValue: this.#offlineColor },

            // The kicker pathPulsed colour is determined by ilkState...
            { svgClass: "kicker", ilkState: ilkStates.Ok,       property: "--kicker-pathPulsed-color", propertyValue: this.#okColor },
            { svgClass: "kicker", ilkState: ilkStates.Ilk,      property: "--kicker-pathPulsed-color", propertyValue: this.#interlockedColor },
            { svgClass: "kicker", ilkState: ilkStates.Bridged,  property: "--kicker-pathPulsed-color", propertyValue: this.#bridgedColor },
            { svgClass: "kicker", ilkState: ilkStates.Rebooted, property: "--kicker-pathPulsed-color", propertyValue: this.#rebootColor },
            { svgClass: "kicker", ilkState: ilkStates.Offline,  property: "--kicker-pathPulsed-color", propertyValue: this.#offlineColor },

            // ...except where the stopperState says the path is not active
            { svgClass: "kicker", stopperState: stopperStates.Kicked,  property: "--kicker-pathPulsed-color", propertyValue: this.#invisibleColor },
            { svgClass: "kicker", stopperState: stopperStates.Open,    property: "--kicker-pathPulsed-color", propertyValue: this.#invisibleColor },
            { svgClass: "kicker", stopperState: stopperStates.Offline, property: "--kicker-pathPulsed-color", propertyValue: this.#offlineColor },

            { svgClass: "kicker", svcState: svcStates.Off, property: "--kicker-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "kicker", svcState: svcStates.On,  property: "--kicker-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * BEAMSTEERER Property Mappings
     *
     * The 'beamsteerer' svgClass is used on the 'V' Verteiler Sections - "Verteilmagnet fuer zwei Aeste". eg "AXC", "AHL", "ABK2"
     */
    #BEAMSTEERER_PROPERTY_MAPPINGS()
    {
        return [
            // The beamsteerer housing colour is determined by ilkState.
            { svgClass: "beamsteerer", ilkState: ilkStates.Ok,       property: "--beamsteerer-housing-color",   propertyValue: this.#housingColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Ilk,      property: "--beamsteerer-housing-color",   propertyValue: this.#housingColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Bridged,  property: "--beamsteerer-housing-color",   propertyValue: this.#housingColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Rebooted, property: "--beamsteerer-housing-color",   propertyValue: this.#rebootColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Offline,  property: "--beamsteerer-housing-color",   propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not configured
            { svgClass: "beamsteerer", cfgState: cfgStates.Inconsistent, property: "--beamsteerer-housing-color",  propertyValue: this.#inconsistentColor },

            // The beamsteerer pathLeft colour is determined by ilkState...
            { svgClass: "beamsteerer", ilkState: ilkStates.Ok,       property: "--beamsteerer-pathLeft-color",  propertyValue: this.#okColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Ilk,      property: "--beamsteerer-pathLeft-color",  propertyValue: this.#interlockedColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Bridged,  property: "--beamsteerer-pathLeft-color",  propertyValue: this.#bridgedColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Rebooted, property: "--beamsteerer-pathLeft-color",  propertyValue: this.#rebootColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Offline,  property: "--beamsteerer-pathLeft-color",  propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not active.
            { svgClass: "beamsteerer", cfgState: cfgStates.Right,    property: "--beamsteerer-pathLeft-color",  propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Disabled, property: "--beamsteerer-pathLeft-color",  propertyValue: this.#invisibleColor },

            // The beamsteerer pathRight colour is determined by ilkState...
            { svgClass: "beamsteerer", ilkState: ilkStates.Ok,       property: "--beamsteerer-pathRight-color", propertyValue: this.#okColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Ilk,      property: "--beamsteerer-pathRight-color", propertyValue: this.#interlockedColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Bridged,  property: "--beamsteerer-pathRight-color", propertyValue: this.#bridgedColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Rebooted, property: "--beamsteerer-pathRight-color", propertyValue: this.#rebootColor },
            { svgClass: "beamsteerer", ilkState: ilkStates.Offline,  property: "--beamsteerer-pathRight-color", propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not active
            { svgClass: "beamsteerer", cfgState: cfgStates.Left,     property: "--beamsteerer-pathRight-color", propertyValue: this.#invisibleColor },
            { svgClass: "beamsteerer", cfgState: cfgStates.Disabled, property: "--beamsteerer-pathRight-color", propertyValue: this.#invisibleColor },

            { svgClass: "beamsteerer", svcState: svcStates.Off, property: "--beamsteerer-opacity",         propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamsteerer", svcState: svcStates.On,  property: "--beamsteerer-opacity",         propertyValue: this.#inServiceOpacity },
        ];
    }


    /**
     * BEAMSPLITTER Property Mappings
     *
     * The 'beamsplitter' svgClass is used on the 'T' Teiler Sections - "Verteilmagnet fuer zwei Aeste".
     */
    #BEAMSPLITTER_PROPERTY_MAPPINGS()
    {
        return [
            // The beamsplitter housing colour is determined by ilkState.
            { svgClass: "beamsplitter", ilkState: ilkStates.Ok,       property: "--beamsplitter-housing-color",   propertyValue: this.#housingColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Ilk,      property: "--beamsplitter-housing-color",   propertyValue: this.#housingColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Bridged,  property: "--beamsplitter-housing-color",   propertyValue: this.#housingColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Rebooted, property: "--beamsplitter-housing-color",   propertyValue: this.#rebootColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Offline,  property: "--beamsplitter-housing-color",   propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not configured.
            { svgClass: "beamsplitter", cfgState: cfgStates.Inconsistent, property: "--beamsplitter-housing-color",  propertyValue: this.#inconsistentColor },

            // The beamsplitter pathLeft colour is determined by ilkState...
            { svgClass: "beamsplitter", ilkState: ilkStates.Ok,       property: "--beamsplitter-pathLeft-color",  propertyValue: this.#okColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Ilk,      property: "--beamsplitter-pathLeft-color",  propertyValue: this.#interlockedColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Bridged,  property: "--beamsplitter-pathLeft-color",  propertyValue: this.#bridgedColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Rebooted, property: "--beamsplitter-pathLeft-color",  propertyValue: this.#rebootColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Offline,  property: "--beamsplitter-pathLeft-color",  propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not active.
            { svgClass: "beamsplitter", cfgState: cfgStates.Right,    property: "--beamsplitter-pathLeft-color",  propertyValue: this.#invisibleColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Disabled, property: "--beamsplitter-pathLeft-color",  propertyValue: this.#invisibleColor },

            // The beamsplitter pathRight colour is determined by ilkState...
            { svgClass: "beamsplitter", ilkState: ilkStates.Ok,       property: "--beamsplitter-pathRight-color", propertyValue: this.#okColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Ilk,      property: "--beamsplitter-pathRight-color", propertyValue: this.#interlockedColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Bridged,  property: "--beamsplitter-pathRight-color", propertyValue: this.#bridgedColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Rebooted, property: "--beamsplitter-pathRight-color", propertyValue: this.#rebootColor },
            { svgClass: "beamsplitter", ilkState: ilkStates.Offline,  property: "--beamsplitter-pathRight-color", propertyValue: this.#offlineColor },

            // ...except where the cfgState says the path is not active.
            { svgClass: "beamsplitter", cfgState: cfgStates.Left,     property: "--beamsplitter-pathRight-color", propertyValue: this.#invisibleColor },
            { svgClass: "beamsplitter", cfgState: cfgStates.Disabled, property: "--beamsplitter-pathRight-color", propertyValue: this.#invisibleColor },

            // The beamsplitter opacity is determined by svcState.
            { svgClass: "beamsplitter", svcState: svcStates.Off, property: "--beamsplitter-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamsplitter", svcState: svcStates.On,  property: "--beamsplitter-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * LABEL Property Mappings
     */
    #LABEL_PROPERTY_MAPPINGS()
    {
        return [
            { svgClass: "label", svcState: svcStates.Off, property: "--label-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "label", svcState: svcStates.On,  property: "--label-opacity", propertyValue: this.#inServiceOpacity }
        ];
    }

    /**
     * Returns an array of all property mappings defined in this class.
     *
     * @return {*[]}
     */
    #getComponentPropertyMappings()
    {
        return [
            ...this.#IONSOURCE_PROPERTY_MAPPINGS(),
            ...this.#TARGET_PROPERTY_MAPPINGS(),
            ...this.#BEAMLINE_PROPERTY_MAPPINGS(),
            ...this.#BEAMSTOPPER_PROPERTY_MAPPINGS(),
            ...this.#BEAMSTEERER_PROPERTY_MAPPINGS(),
            ...this.#KICKER_PROPERTY_MAPPINGS(),
            ...this.#BEAMSPLITTER_PROPERTY_MAPPINGS(),
            ...this.#INFOSECTION_PROPERTY_MAPPINGS(),
            ...this.#EXTRASECTION_PROPERTY_MAPPINGS(),
            ...this.#LABEL_PROPERTY_MAPPINGS(),
        ];
    }

    /**
     * Returns an array of property mappings that are applicable for the specified state variable and target state.
     *
     * @param targetStateVariable
     * @param targetState
     * @return {*}
     */
    getTargetStatePropertySettings( targetStateVariable, targetState )
    {
        const mappings = this.#getComponentPropertyMappings();
        return mappings.filter( entry => entry[ targetStateVariable ] === targetState )
            .map( x => {
                return { "svgClass" : x.svgClass, "property" : x.property, "propertyValue" : x.propertyValue };
            } );
    }
}