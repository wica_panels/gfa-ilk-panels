export class StateManager
{
    #stateMap

    /**
     * Constructs a new StateManager object.
     */
    constructor()
    {
        this.#stateMap = new Map();
    }

    /**
     * Checks if an object with the specified name has been saved.
     *
     * @param {string} objectName - The name of the object to check.
     * @returns {boolean} Returns true if the object exists, otherwise false.
     */
    hasSavedObject( objectName )
    {
        return this.#stateMap.has( objectName )
    }

    /**
     * Saves an object with the specified name.
     *
     * @param {string} objectName - The name of the object to save.
     * @param {any} updateObject - The object to save.
     */
    saveObject( objectName, updateObject )
    {
        this.#stateMap.set( objectName, updateObject )
    }

    /**
     * Retrieves the object with the specified name.
     *
     * @param {string} objectName - The name of the object to retrieve.
     * @returns {any | undefined} The saved object, or undefined if it doesn't exist.
     */
    getSavedObject( objectName )
    {
        return this.#stateMap.get( objectName );
    }
}
