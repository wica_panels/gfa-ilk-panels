export const INTERLOCK_VIEW_STATE_MANAGER = {
    "saveObject" : (objectName, streamUpdateObject) => stateMap.set( objectName, streamUpdateObject ),
    "getSavedObject"  : (objectName) => stateMap.get( objectName ),
}

const stateMap = new Map();
