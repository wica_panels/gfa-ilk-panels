export const SECTION_SUPPORT = {
    "containsSectionId" : (moduleId, sectionInfosArray ) => containsSectionId( moduleId, sectionInfosArray ),
    "containsModuleId" : (moduleId, sectionInfosArray ) => containsModuleId( moduleId, sectionInfosArray )
}

function containsSectionId( sectionId, sectionInfosArray )
{
    return sectionInfosArray.filter( (i) => i.sectionId === sectionId ).length > 0;
}

function containsModuleId( moduleId, sectionInfosArray )
{
    return sectionInfosArray.filter( (i) => i.moduleIds.includes( moduleId ) ).length > 0;
}
