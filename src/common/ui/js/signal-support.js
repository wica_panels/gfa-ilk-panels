// Run the global code associated with the 'ilk-signal' module to register the 'ilk-signal" web
// component with the browser.
import "@psi/ilk-wc-factory/gen/ilk-signal.js";

// Run the global code associated with the 'ilk-signal-group-container' module to register the
// 'ilk-signal-group-container" web component with the browser.
import "@psi/ilk-wc-factory/gen/ilk-signal-group-container.js";

export const SIGNAL_SUPPORT = {
    "updateTargetSectionSignals" : (ilkSignalGroupContainerEle, sectionStatesArray, targetSection) => updateTargetSectionSignalGroupContainer( ilkSignalGroupContainerEle, sectionStatesArray, targetSection ),
    "updateBridgeSignals" : (ilkSignalGroupEle, sectionStatesArray) => updateBridgeSignalGroup( ilkSignalGroupEle, sectionStatesArray ),
    "updateFaultSignals" : (ilkSignalGroupEle, sectionStatesArray) => updateFaultSignalGroup( ilkSignalGroupEle, sectionStatesArray ),
    "updateInputInterlockSignals" : (ilkSignalGroupEle, sectionStatesArray) => updateInputInterlockSignalGroup( ilkSignalGroupEle, sectionStatesArray ),
    "updatePrimaryInputSignals" : (ilkSignalGroupEle, sectionStatesArray) => updatePrimaryInputSignalGroup( ilkSignalGroupEle, sectionStatesArray ),
    "updateTraceableOutputSignals" : (ilkSignalGroupEle, sectionStatesArray) => updateTraceableOutputSignalGroup( ilkSignalGroupEle, sectionStatesArray ),
    "updateImportantOutputSignals" : (ilkSignalGroupEle, sectionStatesArray) => updateImportantOutputSignalGroup( ilkSignalGroupEle, sectionStatesArray ),
    "updateInterlockHistorySignals" : (ilkSignalGroupEle, interlockArray, hoursAgo) => updateInterlockHistorySignalGroup( ilkSignalGroupEle, interlockArray, hoursAgo ),
    "calculateInterlockStatistics" : (interlockHistoryArray,hoursAgo) => calculateInterlockStatistics( interlockHistoryArray, hoursAgo ),
    "updateInterlockStatistics" : (ilkStatisticsEle, interlockHistoryArray,hoursAgo) => updateInterlockStatistics( ilkStatisticsEle, interlockHistoryArray, hoursAgo ),
    "setSignalUserPreferences" : (searchStartEle, expertUser, reversedSignalDirection, showSignalRegisterBits, showLabels) => setSignalUserPreferences( searchStartEle, expertUser, reversedSignalDirection, showSignalRegisterBits, showLabels ),
    "setSignalUserPrivileges" : (searchStartEle, userPrivileges) => setSignalUserPrivileges( searchStartEle, userPrivileges )
}

/**
 * A reminder of the data structure of an OUTPUT SIGNAL... In this example the signal is in the
 * interlock state and the potential "causes" of the interlock have been traced. Each "cause" is
 * a leaf on the interlock signal tree that is also in the interlocked state.
 *
 * {
 *     "sectionId": "BY2",
 *     "moduleId": "0005002",
 *     "portId": "1",
 *     "type": "output",
 *     "activeName": "",
 *     "okName": "BY1 INTERLOCK",
 *     "nokName": "",
 *     "tstrigger": "2022-05-25T16:58:07.752543941",
 *     "tsclear": "",
 *     "port-fault": "OK",
 *     "port-signal": "NOK",
 *     "logic-signal": "NOK",
 *     "bridge": "USI2_CLR",
 *     "interlock": "ILK",
 *     "causes": [
 *         { "sectionId": "BY2", "moduleId": "0003001", "okName": "MYC1"   },
 *         { "sectionId": "BY2", "moduleId": "0003001", "okName": "MYC2"   },
 *         { "sectionId": "BY2", "moduleId": "0005002", "okName": "KY5-8I" }
 *     ],
 *     "view": "DEFAULT",
 *     "bridgePolicy": "SW",
 *     "traceable": true,
 *     "important": true,
 *     "comment": ""
 * },
 *
 * An INPUT SIGNAL is somewhat  simpler:
 * {
 *     "sectionId": "$MVP1",
 *     "moduleId": "031002",
 *     "portId": "1",
 *     "type": "input",
 *     "activeName": "EVEX EIN",
 *     "okName": "EVEX AUS",
 *     "nokName": "EVEX EIN",
 *     "epicsInfoChannel": "",
 *     "tstrigger": "",
 *     "tsclear": "2022-05-25T18:21:03.946121486",
 *     "port-fault": "OK",
 *     "port-signal": "NOK",
 *     "logic-signal": "NOK",
 *     "bridge": "RPM_CLR",
 *     "interlock": "OK",
 *     "view": "EXTRACTION",
 *     "bridgePolicy": "SW",
 *     "source": "",
 *     "comment": "Statusinterlock des Speisegeraets. ACHTUNG: NOK(ROT) bedeutet hier EINGESCHALTET."
 * },
 */

function setSignalUserPreferences( searchStartEle, expertUser, reversedSignalDirection, showSignalRegisterBits, showLabels )
{
    // noinspection CssInvalidHtmlTagReference
    const signalGroupEles = searchStartEle.querySelectorAll( "ilk-signal-group" );
    signalGroupEles.forEach( ( ele) => {
        ele.showExpertView = expertUser;
        ele.reversed = reversedSignalDirection;
        ele.showRegisterBits = showSignalRegisterBits;
        ele.showLabels = showLabels;
    } );

    const signalContainerEles = searchStartEle.querySelectorAll( "ilk-signal-group-container" );
    signalContainerEles.forEach( ( ele) => {
        ele.showExpertView = expertUser;
        ele.reversed = reversedSignalDirection;
        ele.showRegisterBits = showSignalRegisterBits;
        ele.showLabels = showLabels;
    } );
}

function setSignalUserPrivileges( searchStartEle, userPrivileges )
{
    // noinspection CssInvalidHtmlTagReference
    const signalGroupEles = searchStartEle.querySelectorAll( "ilk-signal-group" );
    signalGroupEles.forEach( ( ele) => {
        ele.userPrivileges = userPrivileges;
    } );

    const signalContainerEles = searchStartEle.querySelectorAll( "ilk-signal-group-container" );
    signalContainerEles.forEach( ( ele) => {
        ele.userPrivileges = userPrivileges;
    } );
}

function updateTargetSectionSignalGroupContainer( ilkSignalGroupContainerEle, sectionStatesArray, targetSection )
{
    const targetedSection = (section) => section === targetSection;
    const viewElements = (ele) => ele[ "view" ] !== "DEFAULT";
    const inputSignals = filterSectionInputSignals( sectionStatesArray, targetedSection, viewElements );
    const outputSignals = filterSectionOutputSignals_( sectionStatesArray, targetedSection, viewElements );
    ilkSignalGroupContainerEle.signalDescriptors = [ ...outputSignals, ...inputSignals ];
}


function updateBridgeSignalGroup( ilkSignalGroupEle, sectionStatesArray )
{
    const allSections = () => true;
    // Note: when a module signal is in the state UNKNOWN it will not be detected as bridged.
    const bridgeElements = (ele) =>  ( ele[ "bridge" ] === "RPM_SET" ) || ( ele[ "bridge" ] === "USI2_OK" ) || ( ele[ "bridge" ] === "USI2_NOK" );
    const inputSignals = filterSectionInputSignals( sectionStatesArray, allSections, bridgeElements );
    const outputSignals = filterSectionOutputSignals_( sectionStatesArray, allSections, bridgeElements );
    ilkSignalGroupEle.signalDescriptors = [ ...inputSignals, ...outputSignals ];
}

function updateFaultSignalGroup( ilkSignalGroupEle, sectionStatesArray )
{
    const allSections = () => true;
    // Note: when a module signal is in the state UNKNOWN it will not be detected as having a port fault.
    const allInputFaultElements = (ele) => ( ele[ "port-fault" ] === "ERR" ) || ( ele[ "connectionFault" ] === true ) || ( ele[ "epicsInfoChannelValue" ] === "UNKNOWN" );
    const inputSignals = filterSectionInputSignals( sectionStatesArray, allSections, allInputFaultElements );
    const allOutputFaultElements = (ele) => ( ele[ "port-fault" ] === "ERR" ) || ( ele[ "connectionFault" ] === true );
    const outputSignals = filterSectionOutputSignals_( sectionStatesArray, allSections, allOutputFaultElements );
    ilkSignalGroupEle.signalDescriptors = [ ...inputSignals, ...outputSignals ];
}

function updateInputInterlockSignalGroup( ilkSignalGroupEle, sectionStatesArray )
{
    const allSections = () => true;
    // Note: when a module signal is in the state UNKNOWN it will not be detected as having an interlock.
    // Note: when a module signal is included in the DEFAULT view it will not be detected as having an interlock.
    const interlockElements = (ele) => ( ele[ "interlock" ] === "ILK" ) && ( ele[ "view" ] !== "DEFAULT" );
    ilkSignalGroupEle.signalDescriptors  = filterSectionInputSignals( sectionStatesArray, allSections, interlockElements );
}

function updatePrimaryInputSignalGroup( ilkSignalGroupEle, sectionStatesArray )
{
    const allSections = () => true;
    const interlockElements = (ele) => ele[ "source" ] === "";
    ilkSignalGroupEle.signalDescriptors  = filterSectionInputSignals( sectionStatesArray, allSections, interlockElements );
}

function updateTraceableOutputSignalGroup( ilkSignalGroupEle, sectionStatesArray )
{
    const allSections = () => true;
    const traceableSignalElements = (ele) =>  ele[ "traceable" ];
    ilkSignalGroupEle.signalDescriptors = filterSectionOutputSignals_( sectionStatesArray, allSections, traceableSignalElements );
}

function updateImportantOutputSignalGroup( ilkSignalGroupEle, sectionStatesArray )
{
    const allSections = () => true;
    const importantSignalElements = (ele) =>  ele[ "important" ];
    ilkSignalGroupEle.signalDescriptors = filterSectionOutputSignals_( sectionStatesArray, allSections, importantSignalElements );
}

function updateInterlockHistorySignalGroup( ilkSignalGroupEle, interlockHistoryArray, hoursAgo )
{
    const allSections = () => true;
    const viewElements = (ele) => {
        if ( Object.prototype.hasOwnProperty.call( ele, "tstrigger" ) ) {
            const interlockTriggerTime = new Date( ele[ "tstrigger" ] );
            return isLaterThan_( interlockTriggerTime, hoursAgo );
        }
        else {
            console.warn( "Interlock signal had no trigger time !")
            return true;
        }
    }
    ilkSignalGroupEle.showTimestamps = true;
    ilkSignalGroupEle.showInterlockCauses = true;
    ilkSignalGroupEle.signalDescriptors = filterInterlockHistorySignals( interlockHistoryArray, allSections, viewElements );
}

function calculateInterlockStatistics( interlockHistoryArray, hoursAgo )
{
    const viewElements = (ele) => isLaterThan_( new Date( ele[ "tstrigger" ] ), hoursAgo);
    const allSections = () => true;
    const recentInterlocks = filterInterlockHistorySignals( interlockHistoryArray, allSections, viewElements);
    const totalInterlocks = recentInterlocks.length;

    const ilkFreqMap = new Map();
    recentInterlocks.forEach(item => {
        const interlockSignalName = item["okName"];
        const total = ilkFreqMap.has(interlockSignalName) ? ilkFreqMap.get(interlockSignalName) + 1 : 1;
        ilkFreqMap.set( interlockSignalName, total );
    })
    const ilkFreqMapSortedDescending = new Map( [...ilkFreqMap.entries() ].sort((a, b) => b[1] - a[1]));

    const ilkCauseMap = new Map();
    recentInterlocks.forEach(item => {
        if (Object.hasOwnProperty.call(item, "causes")) {
            const causes = item[ "causes" ];
            causes.forEach(cause => {
                const causeSignalName = cause[ "okName" ];
                const total = ilkCauseMap.has( causeSignalName ) ? ilkCauseMap.get( causeSignalName ) + 1 : 1;
                ilkCauseMap.set( causeSignalName, total );
            })
        }
    })
    const ilkCauseMapSortedDescending = new Map( [...ilkCauseMap.entries() ].sort((a, b) => b[1] - a[1]));

    // noinspection UnnecessaryLocalVariableJS
    const statistics = { "totalInterlocks": totalInterlocks, "signalFrequencyMap": ilkFreqMapSortedDescending, "causeFrequencyMap": ilkCauseMapSortedDescending };
    return statistics;
}

function updateInterlockStatistics( ilkStatisticsEle, interlockHistoryArray, hoursAgo )
{
    const statistics = calculateInterlockStatistics( interlockHistoryArray, hoursAgo );

    const ilkCauseMap = statistics[ "causeFrequencyMap" ];
    const truncatedIlKCauseMap = new Map([...ilkCauseMap.entries()].slice( 0, 10 ) );

    let ilkCauses = "<p><em>";
    truncatedIlKCauseMap.forEach( (value,key) => {
        ilkCauses = ilkCauses + key + ": " + value + "<p>";
    } );
    ilkCauses = ilkCauses + "</em>"

    let ilkFreqs = "<p><em>";
    const ilkSignalMap = statistics[ "signalFrequencyMap" ];
    ilkSignalMap.forEach( (value,key) => {
        ilkFreqs = ilkFreqs + key + ": " + value + "<p>";
    } );
    ilkFreqs = ilkFreqs + "</em>"

    const totalInterlocks =  statistics[ "totalInterlocks" ];
    const msg1 = hoursAgo > 24 ?
        "<h1>There have been <em>" + totalInterlocks + "</em> interlocks since the system was started</h1>" :
        "<h1>There have been <em>" + totalInterlocks + "</em> interlocks in the last " + hoursAgo + " hours</h1>"
    const msg2 = "<h1>Top 10 Interlock Causes</h1>"
    const msg3 = "<h1>Interlock Signal Frequencies</h1>"
    ilkStatisticsEle.innerHTML = msg1 + msg2 + ilkCauses + msg3 + ilkFreqs;
}

function filterSectionInputSignals( sectionStateArray, sectionIdSelector, signalPropertySelector )
{
    const signalSorter = (siga,sigb) => ( siga[ "view" ] > sigb[ "view" ] ) ? 1 :
                                        ( sigb[ "view" ] > siga[ "view" ] ) ? -1 : 0;

    const signals = [];
    sectionStateArray.forEach( (sectionStateObj) => {
        const allSectionInputSignals = sectionStateObj["sectionInputSignals" ];
        allSectionInputSignals.forEach( sectionInputSignalObj => {
            const sectionId = sectionInputSignalObj[ "sectionId" ];
            const sectionInputs = sectionInputSignalObj[ "inputs" ];
            sectionInputs.forEach((sig) => {
                if (sectionIdSelector( sectionId ) && signalPropertySelector( sig ) )
                {
                    signals.push( sig );
                }
            });
        } );
    } );
    return signals.sort( (a,b) => signalSorter( a, b) );
}

function filterInterlockHistorySignals( interlockHistoryArray, sectionIdSelector, signalPropertySelector )
{
    const signals = [];
    interlockHistoryArray.forEach( (sig) => {
        const sectionId = sig[ "sectionId" ];
        if ( sectionIdSelector( sectionId ) && signalPropertySelector( sig ) )
        {
            signals.push( sig );
        }
    } );
    return signals;
}


function filterSectionOutputSignals_( sectionStateArray, sectionIdSelector, signalPropertySelector )
{
    const signalSorter = (siga,sigb) => ( siga[ "view" ] > sigb[ "view" ] ) ? 1 :
                                        ( sigb[ "view" ] > siga[ "view" ] ) ? -1 : 0;

    const signals = [];
    sectionStateArray.forEach( (sectionStateObj) => {
        const allSectionOutputSignals = sectionStateObj[ "sectionOutputSignals" ];
        allSectionOutputSignals.forEach( sectionOutputSignalObj => {
            const sectionId = sectionOutputSignalObj[ "sectionId" ];
            const sectionOutputs = sectionOutputSignalObj[ "outputs" ];
            sectionOutputs.forEach( (sig) => {
                if ( sectionIdSelector( sectionId ) && signalPropertySelector( sig ) )
                {
                    signals.push( sig );
                }
            } );
        } );
    } );
    return signals.sort( (a,b) => signalSorter( a,b ) );
}

function isLaterThan_( date1, hoursAgo )
{
    const today = new Date();
    const date2 = new Date(today.getTime() - (1000*60*60*hoursAgo) );

    // Build JS Date objects based on the supplied timestamp which may
    // typically looks like something like this: 2022-05-15T03:46:24.981360032
    return (date1 - date2 > 0);
}