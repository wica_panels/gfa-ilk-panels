import {ilkStates, svcStates, stopperStates} from "./svg-state-constants";

export class IlktestInterlockViewPropertyManager
{
    #hiddenColor;
    #offlineColor;
    #rebootColor;
    #okColor;
    #interlockedColor;
    #bridgedColor;
    #housingColor;
    #invisibleColor;
    #inServiceOpacity;
    #outOfServiceOpacity;

    #openColor;
    #closedColor;
    #inBetweenColor;
    #errorColor;

    constructor( okColor, interlockedColor, bridgedColor, rebootColor, offlineColor, hiddenColor, housingColor, inServiceOpacity, outOfServiceOpacity )
    {
        this.#okColor = okColor;
        this.#interlockedColor = interlockedColor;
        this.#bridgedColor = bridgedColor;
        this.#rebootColor = rebootColor;
        this.#offlineColor = offlineColor;
        this.#hiddenColor = hiddenColor;
        this.#housingColor = housingColor;
        this.#invisibleColor = "rgba(0,0,0,0)";
        this.#inServiceOpacity = inServiceOpacity;
        this.#outOfServiceOpacity = outOfServiceOpacity;

        this.#openColor = "rgba(0,0,0,0)";
        this.#closedColor = "dimgray";
        this.#inBetweenColor ="cornflowerblue";
        this.#errorColor = "red";
    }

    /**
     * IONSOURCE Property Mappings.
     *
     * The 'ionsource' svgClass is used  on the 'BW2' Section.
     *
     * Allowed ilkState Values: [ "OK", "ILK", ilkStates.Bridged, "PON, ilkStates.Offline ].
     * Allowed svcState Values: [ "OFF", "ON" ].
     */
    #IONSOURCE_PROPERTY_MAPPINGS()
    {
        return [
            // The ionsource colour is determined by ilkState.
            { svgClass: "ionsource", ilkState: ilkStates.Ok,        property: "--ionsource-color", propertyValue: this.#okColor },
            { svgClass: "ionsource", ilkState: ilkStates.Ilk,       property: "--ionsource-color", propertyValue: this.#interlockedColor },
            { svgClass: "ionsource", ilkState: ilkStates.Bridged,   property: "--ionsource-color", propertyValue: this.#bridgedColor },
            { svgClass: "ionsource", ilkState: ilkStates.Rebooted,  property: "--ionsource-color", propertyValue: this.#rebootColor },
            { svgClass: "ionsource", ilkState: ilkStates.Offline,   property: "--ionsource-color", propertyValue: this.#offlineColor },

            // The ionsource opacity is determined by svcState.
            { svgClass: "ionsource", svcState: svcStates.Off, property: "--ionsource-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "ionsource", svcState: svcStates.On,  property: "--ionsource-opacity", propertyValue: this.#inServiceOpacity }
        ];
    }

    /**
     * BEAMSTOPPER Property Mappings.
     *
     * The 'beamstopper' svgClass is used on sections "BW2" and BN5.
     *
     * Allowed ilkState Values: [ "OK", ilkStates.Ilk, ilkStates.Bridged, "PON, ilkStates.Offline ].
     * Allowed svcState Values: [ "OFF", "ON" ].
     * Allowed stopperState Values: [ "OPEN", "CLOSED", "IN-BETWEEN", "ERROR",ilkStates.Offline ].
     */
    #BEAMSTOPPER_PROPERTY_MAPPINGS()
    {
        return [
            // The beamstopper housing colour is determined by ilkState.
            { svgClass: "beamstopper", ilkState: ilkStates.Ok,       property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Ilk,      property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Bridged,  property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Rebooted, property: "--beamstopper-housing-color", propertyValue: this.#housingColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Offline,  property: "--beamstopper-housing-color", propertyValue: this.#offlineColor },

            // The beamstopper path colour is determined by ilkState.
            { svgClass: "beamstopper", ilkState: ilkStates.Ok,       property: "--beamstopper-path-color", propertyValue: this.#okColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Ilk,      property: "--beamstopper-path-color", propertyValue: this.#interlockedColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Bridged,  property: "--beamstopper-path-color", propertyValue: this.#bridgedColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Rebooted, property: "--beamstopper-path-color", propertyValue: this.#rebootColor },
            { svgClass: "beamstopper", ilkState: ilkStates.Offline,  property: "--beamstopper-path-color", propertyValue: this.#offlineColor },

            // The beamstopper stopper colour is determined by stopperState.
            { svgClass: "beamstopper", stopperState: stopperStates.Open,      property: "--beamstopper-stopper-color", propertyValue: this.#openColor },
            { svgClass: "beamstopper", stopperState: stopperStates.Closed,    property: "--beamstopper-stopper-color", propertyValue: this.#closedColor },
            { svgClass: "beamstopper", stopperState: stopperStates.InBetween, property: "--beamstopper-stopper-color", propertyValue: this.#inBetweenColor },
            { svgClass: "beamstopper", stopperState: stopperStates.Error,     property: "--beamstopper-stopper-color", propertyValue: this.#errorColor },
            { svgClass: "beamstopper", stopperState: ilkStates.Offline,       property: "--beamstopper-stopper-color", propertyValue: this.#offlineColor },

            // The beamstopper opacity is determined by svcState.
            { svgClass: "beamstopper", svcState: svcStates.Off, property: "--beamstopper-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamstopper", svcState: svcStates.On,  property: "--beamstopper-opacity", propertyValue: this.#inServiceOpacity }
        ];
    }

    /**
     * BEAMLINE Property Mappings.
     *
     * The 'beamline' svgClass is used on the 'BW2', 'BN5', 'AHK1' and 'VAR' Sections.
     *
     *   Allowed ilkState Values: [ "OK", ilkStates.Ilk, ilkStates.Bridged, "PON, ilkStates.Offline ].
     *   Allowed svcState Values: [ "OFF", "ON" ].
     */
    #BEAMLINE_PROPERTY_MAPPINGS()
    {
        return [
            // The beamline colour is determined by ilkState.
            { svgClass: "beamline", ilkState: ilkStates.Ok,       property: "--beamline-color", propertyValue: this.#okColor },
            { svgClass: "beamline", ilkState: ilkStates.Ilk,      property: "--beamline-color", propertyValue: this.#interlockedColor },
            { svgClass: "beamline", ilkState: ilkStates.Bridged,  property: "--beamline-color", propertyValue: this.#bridgedColor },
            { svgClass: "beamline", ilkState: ilkStates.Rebooted, property: "--beamline-color", propertyValue: this.#rebootColor },
            { svgClass: "beamline", ilkState: ilkStates.Offline,  property: "--beamline-color", propertyValue: this.#offlineColor },

            // The beamline opacity is determined by svcState.
            { svgClass: "beamline", svcState: svcStates.Off, property: "--beamline-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "beamline", svcState: svcStates.On,  property: "--beamline-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * LABEL Property Mappings.
     */
    #LABEL_PROPERTY_MAPPINGS()
    {
        return [
            { svgClass: "label", svcState: svcStates.Off, property: "--label-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "label", svcState: svcStates.On,  property: "--label-opacity", propertyValue: this.#inServiceOpacity }
        ];
    }

    /**
     * COUPLING_POINT_PROPERTY_MAPPINGS.
     *
     * The 'coupling_point' svgClass is used on the 'AHK1' Section.
     *
     * Allowed ilkState Values:  [ "OK", ilkStates.Ilk, ilkStates.Bridged, ilkStates.Rebooted, ilkStates.Offline ].
     * Allowed svcState Values: [ "OFF", "ON" ].
     */
    #COUPLING_POINT_PROPERTY_MAPPINGS()
    {
        return [
            // The coupling_point colour is determined by ilkState...
            { svgClass: "coupling_point", ilkState: ilkStates.Ok,       property: "--coupling-point-color",   propertyValue: this.#okColor },
            { svgClass: "coupling_point", ilkState: ilkStates.Ilk,      property: "--coupling-point-color",   propertyValue: this.#interlockedColor },
            { svgClass: "coupling_point", ilkState: ilkStates.Bridged,  property: "--coupling-point-color",   propertyValue: this.#bridgedColor },
            { svgClass: "coupling_point", ilkState: ilkStates.Rebooted, property: "--coupling-point-color",   propertyValue: this.#rebootColor },
            { svgClass: "coupling_point", ilkState: ilkStates.Offline,  property: "--coupling-point-color",   propertyValue: this.#offlineColor },

            // The coupling_point opacity is determined by svcState.
            { svgClass: "coupling_point", svcState: svcStates.Off, property: "--coupling-point-opacity", propertyValue: this.#outOfServiceOpacity },
            { svgClass: "coupling_point", svcState: svcStates.On,  property: "--coupling-point-opacity", propertyValue: this.#inServiceOpacity },
        ];
    }

    /**
     * Returns an array of all property mappings defined in this class.
     *
     * @return {*[]}
     */
    #getComponentPropertyMappings()
    {
        return [
            ...this.#IONSOURCE_PROPERTY_MAPPINGS(),
            ...this.#BEAMSTOPPER_PROPERTY_MAPPINGS(),
            ...this.#BEAMLINE_PROPERTY_MAPPINGS(),
            ...this.#LABEL_PROPERTY_MAPPINGS(),
            ...this.#COUPLING_POINT_PROPERTY_MAPPINGS(),
        ];
    }

    /**
     * Returns an array of property mappings that are applicable for the specified state variable and target state.
     *
     * @param targetStateVariable
     * @param targetState
     * @return {*}
     */
    getTargetStatePropertySettings( targetStateVariable, targetState )
    {
        const mappings = this.#getComponentPropertyMappings();
        return mappings.filter( entry => entry[ targetStateVariable ] === targetState )
            .map( x => {
                return { "svgClass" : x.svgClass, "property" : x.property, "propertyValue" : x.propertyValue };
            } );
    }
}