// Run the global code associated with the 'ilk-module' module to register the 'ilk-module" web
// component with the browser.
import "@psi/ilk-wc-factory/gen/ilk-module.js";

// Run the global code associated with the 'ilk-module-container' module to register the
// 'ilk-module-container" web component with the browser.
import "@psi/ilk-wc-factory/gen/ilk-module-container.js";

// Run the global code associated with the 'wc-table-viewer' module to register the
// 'wc-table-viewer" web component with the browser.
import "@psi/ilk-wc-factory/wc/wc-table-viewer.js";

export const MODULE_SUPPORT = {
    "containsSectionId" : (moduleId, sectionInfosArray ) => containsSectionId( moduleId, sectionInfosArray ),
    "containsModuleId" : (moduleId, sectionInfosArray ) => containsModuleId( moduleId, sectionInfosArray ),
    "updateTargetSection" : (ilkModuleContainerEle, sectionStatesArray, sectionInfosArray, targetSection ) => updateTargetSection( ilkModuleContainerEle, sectionStatesArray, sectionInfosArray, targetSection ),
    "updateTargetSectionHighlighting" : (ilkModuleContainerEle, targetModule ) => updateTargetSectionHighlighting( ilkModuleContainerEle, targetModule ),
    "updateTargetModule" : (ilkModuleContainerEle, sectionStatesArray, sectionInfosArray, targetModule ) => updateTargetModule( ilkModuleContainerEle, sectionStatesArray, sectionInfosArray, targetModule ),
    "setSignalUserPreferences" : (searchStartEle, expertUser, reversedSignalMode, showSignalRegisterBits ) => setSignalUserPreferences( searchStartEle, expertUser, reversedSignalMode, showSignalRegisterBits ),
    "setSignalUserPrivileges" : (searchStartEle, userPrivileges ) => setSignalUserPrivileges( searchStartEle, userPrivileges )
}

function containsSectionId( sectionId, sectionInfosArray )
{
    return sectionInfosArray.filter( (i) => i.sectionId === sectionId ).length > 0;
}

function containsModuleId( moduleId, sectionInfosArray )
{
    return sectionInfosArray.filter( (i) => i.moduleIds.includes( moduleId ) ).length > 0;
}


function setSignalUserPreferences( searchStartEle, expertUser, reversedSignalDirection, showSignalRegisterBits )
{
    // noinspection CssInvalidHtmlTagReference
    const signalEles = searchStartEle.querySelectorAll( "ilk-module-container" );
    signalEles.forEach( ( ele) => {
        ele.showExpertView = expertUser;
        ele.reversed = reversedSignalDirection;
        ele.showRegisterBits = showSignalRegisterBits;
    } );
}

function setSignalUserPrivileges( searchStartEle, userPrivileges )
{
    // noinspection CssInvalidHtmlTagReference
    const signalEles = searchStartEle.querySelectorAll( "ilk-module-container" );
    signalEles.forEach( ( ele) => {
        ele.userPrivileges = userPrivileges;
    } );
}

function updateTargetSection( ilkModuleContainerEle, sectionStatesArray, sectionInfosArray, targetSection )
{
    const targetedSection = (section) => section === targetSection;
    const allModuleElements = () => true;
    ilkModuleContainerEle.moduleDescriptors = buildModuleDescriptors( sectionStatesArray, sectionInfosArray, targetedSection, allModuleElements );
    ilkModuleContainerEle.connected = true;
}

function updateTargetSectionHighlighting( ilkModuleContainerEle, targetModule )
{
    ilkModuleContainerEle.modulesToHighlight = [ targetModule ];
}

function updateTargetModule( ilkModuleContainerEle, sectionStatesArray, sectionInfosArray, targetModule )
{
    const allSectionElements =  () => true;
    const targetedModule = (module) => module === targetModule;
    ilkModuleContainerEle.moduleDescriptors = buildModuleDescriptors( sectionStatesArray, sectionInfosArray, allSectionElements, targetedModule );
    ilkModuleContainerEle.connected = true;
}

function buildModuleDescriptors( sectionStatesArray, sectionInfosArray, sectionIdSelector, moduleIdSelector )
{
    const moduleDescriptors = [];

    // Iterate through all sections present in the stream
    sectionStatesArray.forEach( (sectionStateObj) => {
        const sectionId =  sectionStateObj[ "sectionId" ];
        const allSectionModuleModeStates = sectionStateObj[ "sectionModuleModeStates" ];
        allSectionModuleModeStates.forEach( sectionModuleModeStateObj => {
            const moduleId = sectionModuleModeStateObj[ "moduleId" ];
            if ( sectionIdSelector( sectionId ) && moduleIdSelector( moduleId ) )
            {
                const moduleInfo = getSectionModuleInfo( sectionInfosArray, sectionId, moduleId );
                const modeDescriptions = sectionModuleModeStateObj[ "moduleModeDescriptions" ];
                const simplifiedModeDescription = createSimplifiedModeString( modeDescriptions );
                const moduleInputSignals = getModuleInputSignals( sectionStatesArray, moduleId );
                const moduleOutputSignals = getModuleOutputSignals( sectionStatesArray, moduleId );
                const moduleSpecialFunctionRegisters = getModuleSpecialFunctionRegisters( sectionStatesArray, moduleId );
                const moduleDescriptor = assembleModuleDescriptor( moduleInfo, simplifiedModeDescription, moduleSpecialFunctionRegisters, moduleInputSignals, moduleOutputSignals );
                moduleDescriptors.push( moduleDescriptor );
            }
        } );
    } );

    // Filter the returned module descriptors array to remove any duplicate module entries that may
    // result from a module's signals being split across multiple sections.
    const seen = new Set();
    return moduleDescriptors.filter( desc => {
        const duplicate = seen.has( desc.moduleId );
        seen.add( desc.moduleId );
        return !duplicate
    } );
}

function getModuleInputSignals( sectionStatesArray, targetModuleId )
{
    const moduleInputSignals = [];
    sectionStatesArray.forEach( (sectionStateObj) => {
        const sectionInputSignalsArray = sectionStateObj[ "sectionInputSignals" ];
        sectionInputSignalsArray.forEach( (sectionInputSignalObj) => {
                const inputSignalsArray = sectionInputSignalObj[ "inputs" ];
                const myInputSignals = inputSignalsArray.filter( inp => ( inp.moduleId === targetModuleId ) );
                moduleInputSignals.push( ...myInputSignals );
            } );
        } );
    return moduleInputSignals;
}

function getModuleOutputSignals( sectionStatesArray, targetModuleId )
{
    const moduleOutputSignals = [];
    sectionStatesArray.forEach( (sectionStateObj) => {
        const sectionOutputSignalsArray = sectionStateObj[ "sectionOutputSignals" ];
        sectionOutputSignalsArray.forEach( (sectionOutputSignalObj) => {
            const inputSignalsArray = sectionOutputSignalObj[ "outputs" ];
            const myOutputSignals = inputSignalsArray.filter( op => ( op.moduleId === targetModuleId ) );
            moduleOutputSignals.push( ...myOutputSignals );
        } );
    } );
    return moduleOutputSignals;
}

function getModuleSpecialFunctionRegisters( sectionStatesArray, targetModuleId )
{
    const results = [];
    sectionStatesArray.forEach( (sectionStateObj) => {
        const sectionModuleSpecialFunctionRegistersArray = sectionStateObj[ "sectionModuleSpecialFunctionRegisters" ];
        sectionModuleSpecialFunctionRegistersArray.forEach( (sectionModuleSpecialFunctionRegisterObj) => {
            const moduleId = sectionModuleSpecialFunctionRegisterObj[ "moduleId" ];
            const sfrArray = sectionModuleSpecialFunctionRegisterObj[ "sfrs" ];
            if ( moduleId === targetModuleId ) {
                results.push(...sfrArray );
            }
        } );
    } );
    return results;
}

function getSectionModuleInfo( sectionInfosArray, sectionId, moduleId )
{
    const allSectionModuleInfoData = sectionInfosArray.filter( x => ( x.sectionId === sectionId ) )[ 0 ][ "moduleInfos" ];
    const x2 =  allSectionModuleInfoData.filter( x => ( x.moduleId === moduleId ) );
    return x2[ 0 ];
}

function createSimplifiedModeString( modeDescriptionsObj )
{
    // if ( modeDescriptions === "UNKNOWN" ) {
    //     return "UNKNOWN";
    // }

    const { primary:modeSetting1, secondary:modeSetting2, optional:modeOptions}= modeDescriptionsObj
    const [ modeProperty1, modeValue1] = modeSetting1 === undefined ? ["", ""] : modeSetting1.split( ":" );
    const [ modeProperty2, modeValue2] = modeSetting2 === undefined ? ["", ""] : modeSetting2.split( ":" );

    const [ modeOption1, modeOption2, modeOption3] = modeOptions === undefined ? [ "", "", "" ] : modeOptions.split( ";" );

    const [ modeOptionProperty1] = modeOption1 === undefined ? ["", ""] : modeOption1.split( ":" );
    const [ modeOptionProperty2] = modeOption2 === undefined ? ["", ""] : modeOption2.split( ":" );
    const [ modeOptionProperty3] = modeOption3 === undefined ? ["", ""] : modeOption3.split( ":" );

    const token1 = modeValue1 ?? modeProperty1;
    const token2 = modeValue2 ?? modeProperty2;
    return [ token1,token2,modeOptionProperty1,modeOptionProperty2,modeOptionProperty3 ].join( " " );
}

function assembleModuleDescriptor( moduleInfo, moduleModeDescription, moduleSpecialFunctionRegisters, moduleInputSignals, moduleOutputSignals )
{
    let moduleDescriptor = {};
    moduleDescriptor.moduleId = moduleInfo[ "moduleId" ];
    moduleDescriptor.sectionIds = moduleInfo[ "sectionIds" ];
    moduleDescriptor.iocInfo = moduleInfo[ "iocInfo" ];
    moduleDescriptor.vmeInfo = moduleInfo[ "vmeInfo" ];
    moduleDescriptor.hardwareType = "H/W: " + moduleInfo[ "hardwareType" ];
    moduleDescriptor.logicalType = moduleInfo[ "logicalType" ];
    moduleDescriptor.interlockLevel = "Level: " + moduleInfo[ "interlockLevel" ];

    const tsRegisterArray = moduleInfo[ "typeSpecificRegisters" ];

    let tsConfigString = "";
    tsRegisterArray.forEach( (ele,idx) => {
       const prefix = idx === 0 ? "" : ", ";
       const name = ele[ "name" ];
       const value = ele[ "value" ];
       tsConfigString += prefix + name + "=" + value;
    })
    moduleDescriptor.configuration = tsConfigString.length === 0 ? "No TS Registers Defined" : tsConfigString;

    const sfRegisterArray =  moduleSpecialFunctionRegisters;

    let sfConfigString = "";
    sfRegisterArray.forEach( (ele,idx) => {
        const prefix = idx === 0 ? "" : ", ";
        const name = ele[ "name" ];
        const value = ele[ "value" ];
        sfConfigString += prefix + name + "=" + value;
    })
    moduleDescriptor.measurement = sfConfigString.length === 0 ? "No SF Registers Defined" : sfConfigString;

    moduleDescriptor.moduleMode = moduleModeDescription;
    moduleDescriptor.inputSignals = moduleInputSignals;
    moduleDescriptor.outputSignals = moduleOutputSignals;

    // Add initial support for USI2 interlock timestrip.
    let timestripDescriptors = [];
    moduleInputSignals.forEach( sig => {
        const interlockTransitionData  = sig[ "transitions" ];
        let interlockData = [];
        let timingData = [];
        interlockTransitionData.forEach( (ev) => {
            interlockData.push( ev[ "ilk" ] );
            timingData.push( ev[ "time" ] );
        });
        if ( interlockData.length > 0 ) {
            timestripDescriptors.push( {"interlockData": interlockData, "timingData": timingData});
        }
    } );
    moduleDescriptor.timestripDescriptors = timestripDescriptors;
    return moduleDescriptor;
}