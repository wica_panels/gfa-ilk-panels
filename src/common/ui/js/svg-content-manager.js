/**
 * Manages the content of an SVG document.
 */
export class SvgContentManager
{
    #svgDocument;

    /**
     * Constructs a new SvgContentManager object.
     *
     * @param {Document} svgDocument - The SVG document to manage.
     */
    constructor( svgDocument )
    {
        this.#svgDocument = svgDocument;
    }

    /**
     * Updates the text content of an SVG element with the specified ID.
     *
     * @param {string} svgEleId - The ID of the SVG element.
     * @param {string} text - The text content to set.
     */
    update( svgEleId, text)
    {
        const targetElement = this.#svgDocument.getElementById (svgEleId );
        if (targetElement == null )
        {
            return;
        }
        targetElement.textContent = text;
    }
}
