# The sound samples in this directory were created by going to an online speech synthesiser
# app using the Brave browser (chrome variant), then downloading the generated MP3 files.
#
# At the moment this site seems good enough:
# https://voicegenerator.io/
#
# From the above site use the "Download Google TTS Audio" option to play your text.
#
# The following text was used:
#
# - "Sound enabled."
# - "Sound disabled."
# - "Voice enabled."
# - "Voice disabled."
# - "New Inter lock."
# - "New HIPA Interlock."
# - "New PROSCAN Interlock."
# - "Interlock cleared."
# - "ion source interlock"
# - "ˈaˈvˈkeyˈ interlock"
# - "IP branch interlock."
#
