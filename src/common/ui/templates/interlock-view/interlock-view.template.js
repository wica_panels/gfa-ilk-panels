console.log( "interlock-view.js is running...")

    /*-----------------------------------------------------------------------*/
    /* 1.0 MODULE IMPORT DEFINITIONS                                         */
    /*-----------------------------------------------------------------------*/

    // noinspection NpmUsedModulesInstalled
    import {ShadowDomHelper} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {StateManager} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {InterlockViewPropertyManager} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SvgStateManager} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SvgContentManager} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {MODULE_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SIGNAL_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SESSION_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {HTML_ELEMENT_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {HTTP_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SOUND_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SVG_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {Formatter} from "__JS_SUPPORT_FILE__";

    /*-----------------------------------------------------------------------*/
    /* 2.0 VARIABLE DEFINITIONS                                              */
    /*-----------------------------------------------------------------------*/

    const facilityId = "__FACILITY__".toLowerCase();
    const serverUrl = window.location.origin + "__ILKCS_SERVER_PATH_PREFIX__";

    const shadowDomHelper = new ShadowDomHelper( "interlock-panel" );
    const shadowRoot = shadowDomHelper.getShadowRoot();
    const stateManager = new StateManager();

    const facilityPrimaryModeSelectorEle = shadowDomHelper.getElementById( 'facility-primary-mode-select' );
    const facilitySecondaryModeSelectorEle = shadowDomHelper.getElementById( 'facility-secondary-mode-select' );
    const facilityTertiaryModeSelectorEle = shadowDomHelper.getElementById( 'facility-tertiary-mode-select' );
    const facilityModeOptionSelectorEle = shadowDomHelper.getElementById( 'facility-mode-option-select' );

    const expertModeEle = shadowDomHelper.getElementById( 'expert-mode-widget' );
    const showSignalRegisterBitsEle = shadowDomHelper.getElementById( 'show-signal-register-bits-widget' );
    const selectOnHoverEle = shadowDomHelper.getElementById( 'select-on-hover-widget' );
    const reversedSignalDirectionEle = shadowDomHelper.getElementById( 'reversed-signal-direction-widget' );
    const signalNavigationLabelsEle = shadowDomHelper.getElementById( 'signal-navigation-labels-widget' );
    const showCommandLogEle = shadowDomHelper.getElementById( 'show-command-log-widget' );

    const downloadContentAsJsonEle = shadowDomHelper.getElementById( 'download-content-as-json-widget' );
    const enableSoundEle = shadowDomHelper.getElementById( 'enable-sound-widget' );
    const enableVoiceEle = shadowDomHelper.getElementById( 'enable-voice-widget' );

    const requestEle = shadowDomHelper.getElementById( 'http-request' );
    const responseEle = shadowDomHelper.getElementById( 'http-response' );
    const errorEle = shadowDomHelper.getElementById( 'http-error' );
    const messageEle = shadowDomHelper.getElementById( 'message' );


    /*-----------------------------------------------------------------------*/
    /* 3.0 PREFERENCES PANEL SUPPORT FUNCTIONS                               */
    /*-----------------------------------------------------------------------*/

    // Set User Profile
    function setUserSignalPreferences()
    {
        const expertMode = expertModeEle.checked;
        const reversedSignalDirection = reversedSignalDirectionEle.checked;
        const showSignalRegisterBits = showSignalRegisterBitsEle.checked;
        const showLabels = signalNavigationLabelsEle.checked;
        const searchStartEle = shadowDomHelper.getShadowRoot();
        SIGNAL_SUPPORT.setSignalUserPreferences( searchStartEle, expertMode, reversedSignalDirection, showSignalRegisterBits, showLabels );
    }

    function setUserOtherPreferences()
    {
        const showCommandLog = showCommandLogEle.checked;
        const gridContainerEle = shadowDomHelper.getElementByClassName( "grid-container" );
        if ( showCommandLog )
        {
            gridContainerEle.classList.add( "cmdlog" );
        }
        else
        {
            gridContainerEle.classList.remove( "cmdlog" );
        }
    }

    function setUserSoundPreferences()
    {
        const announcementType = enableSoundEle.checked ? SOUND_SUPPORT.announcements.SOUND_ENABLED : SOUND_SUPPORT.announcements.SOUND_DISABLED;
        const soundOff = false;
        const voiceOn= true;
        SOUND_SUPPORT.announce( announcementType, soundOff, voiceOn );
    }

    function setUserVoicePreferences()
    {
        const announcementType = enableVoiceEle.checked ? SOUND_SUPPORT.announcements.VOICE_ENABLED : SOUND_SUPPORT.announcements.VOICE_DISABLED;
        const soundOff = false;
        const voiceOn= true;
        SOUND_SUPPORT.announce( announcementType, soundOff, voiceOn );
    }

    /*-----------------------------------------------------------------------*/
    /* 4.0 ILKCS SERVER GET / PUT DATA SUPPORT FUNCTIONS                     */
    /*-----------------------------------------------------------------------*/

    async function getFacilityData( facility, path )
    {
        // Note: the call to doGetRequests below does not pass any HTML elements, consequently
        // failed requests will not result in popups, but they will get logged.
        const requestUrl = serverUrl + "/v3/facilities/" + facility + path;
        return HTTP_SUPPORT.doGetRequest( requestUrl );
    }

    async function putFacilityMode()
    {
        function joinWithSeparator( separator, ...args)
        {
            // filter out empty strings
            const nonEmptyArgs = args.filter(arg => arg !== "");
            return nonEmptyArgs.join( separator );
        }

        const selectedPrimaryModeIndex = facilityPrimaryModeSelectorEle.selectedIndex;
        const selectedPrimaryMode = facilityPrimaryModeSelectorEle.options[selectedPrimaryModeIndex];
        const settingRequest1 = (selectedPrimaryModeIndex > 0) ? selectedPrimaryMode.parentNode.id + ":" + selectedPrimaryMode.value : "";

        const selectedSecondaryModeIndex = facilitySecondaryModeSelectorEle.selectedIndex;
        const selectedSecondaryMode = facilitySecondaryModeSelectorEle.options[selectedSecondaryModeIndex];
        const settingRequest2 = (selectedSecondaryModeIndex > 0) ? selectedSecondaryMode.parentNode.id + ":" + selectedSecondaryMode.value : "";

        const selectedTertiaryModeIndex = facilityTertiaryModeSelectorEle.selectedIndex;
        const selectedTertiaryMode = facilityTertiaryModeSelectorEle.options[selectedTertiaryModeIndex];
        const settingRequest3 = (selectedTertiaryModeIndex > 0) ? selectedTertiaryMode.parentNode.id + ":" + selectedTertiaryMode.value : "";

        const selectedOptionIndex = facilityModeOptionSelectorEle.selectedIndex;
        const selectedOption = facilityModeOptionSelectorEle.options[selectedOptionIndex];
        const settingRequest4 = (selectedOptionIndex > 0) ? selectedOption.parentNode.id + ":" + selectedOption.value : "";

        const requestedMode = joinWithSeparator( ";", settingRequest1, settingRequest2, settingRequest3, settingRequest4 );
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/mode";
        return HTTP_SUPPORT.doPutRequest( requestUrl, requestedMode, requestEle, responseEle, errorEle );
    }

    /*-----------------------------------------------------------------------*/
    /* 5.0 ILKCS SERVER GET/PUT EVENT SUPPORT FUNCTIONS                      */
    /*-----------------------------------------------------------------------*/

    async function putFacility( event )
    {
        const putValue = event.target.dataset.putValue;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + event.target.dataset.path;
        return HTTP_SUPPORT.doPutRequest( requestUrl, putValue, requestEle, responseEle, errorEle );
    }


    /*-----------------------------------------------------------------------*/
    /* 5.0 STREAM HANDLING SUPPORT FUNCTIONS                                 */
    /*-----------------------------------------------------------------------*/

    function handleStreamConnected()
    {
        updateSvgConnectionState( true );
        updateSignalContainerConnectionState( true );
    }

    function handleStreamDisconnected()
    {
        updateSvgConnectionState( false );
        updateSignalContainerConnectionState( false );
        const UNKNOWN_FACILITY_STATE = {};
        stateManager.saveObject( "FacilityState", UNKNOWN_FACILITY_STATE );
    }

    function updateSvgConnectionState( connected )
    {
        const svgStateManager = stateManager.getSavedObject( "SvgStateManager" );
        const svgFacilityElement = shadowDomHelper.getElementById("svgFacilityElement");
        const svgLegendElement = shadowDomHelper.getElementById( "svgLegendElement" );

        const opacity = connected ? "1.0": "0.3";
        svgFacilityElement.style.opacity = opacity;
        svgLegendElement.style.opacity = opacity;
        if (! connected )
        {
            // Set all beamline sections to indicate that they are offline.
            // Set the sectionInService flag to indicate the section is in active service.
            // This means that the section will be displayed as Offline not Hidden.
           svgStateManager.getBeamlineSectionElements().forEach( ele => {
               const sectionId = ele.id;
               svgStateManager.setBeamlineSectionInterlockState( sectionId, "UNKNOWN" );
               svgStateManager.setBeamlineSectionConfigurationState( sectionId, "UNKNOWN" );
               svgStateManager.setBeamlineSectionStopperState( sectionId, "UNKNOWN" );
               svgStateManager.setBeamlineSectionServiceState( sectionId, false );
           });
       }
    }

    function updateSignalContainerConnectionState( connected )
    {
        const signalGroupContainerEles = shadowDomHelper.querySelectorAll( "ilk-signal-group-container" );
        signalGroupContainerEles.forEach(  (cont) => {
            console.log( "setting signal group container with ID: '" + cont.id + "' to connection state: " + connected + "'." );
            cont.connected = connected;
        } );

        const signalGroupEles = shadowDomHelper.querySelectorAll( "ilk-signal-group" );
        signalGroupEles.forEach(  (cont) => {
            console.log( "setting signal group with ID: '" + cont.id + "' to connection state: " + connected + "'." );
            cont.connected = connected;
        } );
    }

    /*-----------------------------------------------------------------------*/
    /* 6.0 CONFIGURE FACILITY PANEL SUPPORT FUNCTIONS                        */
    /*-----------------------------------------------------------------------*/

    async function loadFacilityInformation( facilityId )
    {
        await Promise.all( [
            addGroupedSelectFacilityModeMenuChoices( facilityPrimaryModeSelectorEle, facilityId, "/mode?detail=choices&filter=primary" ),
            addGroupedSelectFacilityModeMenuChoices( facilitySecondaryModeSelectorEle, facilityId, "/mode?detail=choices&filter=secondary" ),
            addGroupedSelectFacilityModeMenuChoices( facilityTertiaryModeSelectorEle, facilityId, "/mode?detail=choices&filter=tertiary" ),
            addGroupedSelectFacilityModeOptionMenuChoices( facilityModeOptionSelectorEle, facilityId, "/mode?detail=choices&filter=optional" ),
        ] );

        await synchronizeFacilityModeSelectWidgets( facilityPrimaryModeSelectorEle, facilitySecondaryModeSelectorEle, facilityTertiaryModeSelectorEle, facilityModeOptionSelectorEle );

        try {
            const facilityData = await getFacilityData(facilityId, "/sections?detail=info");
            stateManager.saveObject("SectionInfos", facilityData );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function synchronizeFacilityModeSelectWidgets( primaryConfigEle, secondaryConfigEle, tertiaryConfigEle, optionConfigEle )
    {
        try {
            const facilityState = await getFacilityData( facilityId, "?detail=state" )
            const modeDescriptions = facilityState[ "facilityMode" ]?.[ "facilityModeDescriptions" ];
            // Example Data:
            // "facilityModeDescriptions": {
            //    "primary": "Main-Path:BW2",
            //    "secondary": "AHK1-Path:Not-in-use",
            //    "tertiary": "VAR-Path:Beam Split",
            //    "optional": "DIA Mode:DIA-On;Intensity:High;MCH Mode:MCH-On"
            // },

            // Synchronize Facility Mode
            const primary = modeDescriptions[ "primary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            const secondary = modeDescriptions[ "secondary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            const tertiary = modeDescriptions[ "tertiary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            HTML_ELEMENT_SUPPORT.setSelectWidget( primaryConfigEle, primary );
            HTML_ELEMENT_SUPPORT.setSelectWidget( secondaryConfigEle, secondary );
            HTML_ELEMENT_SUPPORT.setSelectWidget( tertiaryConfigEle, tertiary );

            // Synchronize Facility Mode Option
            // Note: there may be more than one option to synchronize on. The algorithm here simply picks the first.
            const option = modeDescriptions[ "optional" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            HTML_ELEMENT_SUPPORT.setSelectWidget( optionConfigEle, option );

            // By dispatching the change event on one of the selectors this ensures that the button reflector will be
            // triggered to set the correct state of button enablement.
            primaryConfigEle.dispatchEvent( new Event( "change" ) );
        }
        catch( error ) {
            console.log( error );
        }
    }

    function handleFacilityStateChanged( facilityState, currentUserSession )
    {
        function updateCurrentUserPrivileges( currentUserSession )
        {
            // Update signal bridge setting privileges
            const userPrivileges = SESSION_SUPPORT.getCurrentUserPrivileges( currentUserSession );
            const searchStartEle = shadowDomHelper.getShadowRoot();
            SIGNAL_SUPPORT.setSignalUserPrivileges( searchStartEle, userPrivileges );
        }

        function getInterlockSoundAnnouncementType( currentInterlocksArray )
        {
            function getSoundAnnouncementType( currentInterlocksArray, minorInterlockNames, standardInterlockNames, majorInterlockNames )
            {
                const hasMajorInterlock =  currentInterlocksArray.filter( entry => {
                    const entrySignalName = entry[ "okName" ];
                    return majorInterlockNames.includes( entrySignalName );
                } ).length > 0;

                const hasStandardInterlock =  currentInterlocksArray.filter( entry => {
                    const entrySignalName = entry[ "okName" ];
                    return standardInterlockNames.includes( entrySignalName );
                } ).length > 0;

                const hasMinorInterlock =  currentInterlocksArray.filter( entry => {
                    const entrySignalName = entry[ "okName" ];
                    return minorInterlockNames.includes( entrySignalName );
                } ).length > 0;

                return hasMajorInterlock ? SOUND_SUPPORT.announcements.MAJOR_INTERLOCK :
                        hasStandardInterlock ? SOUND_SUPPORT.announcements.STANDARD_INTERLOCK :
                        hasMinorInterlock ? SOUND_SUPPORT.announcements.MINOR_INTERLOCK :
                        SOUND_SUPPORT.announcements.NONE;
            }

            function getHipaSoundAnnouncementType( currentInterlocksArray )
            {
                // 2022-12-03: Discussion with Antonio. Added 'SAIREM <50'W and 'IVWC ILK' to list of HIPA major interlocks.
                // 2023-05-23: Discussion with Patric. This list still looks correct. The 'SAIREM <50W' signal comes
                // 2023-05-23: from one input. The 'IVWC ILK' comes from four.
                const majorInterlockNames = [ "IQ BEW. EIN", "SAIREM <50W","IVWC ILK" ];
                const standardInterlockNames = [ "AVKI_STOP" ];
                const minorInterlockNames = [ "BY1 INTERLOCK", "BY2 INTERLOCK" ];
                return getSoundAnnouncementType( currentInterlocksArray, minorInterlockNames, standardInterlockNames, majorInterlockNames );
            }

            function getIstsSoundAnnouncementType( currentInterlocksArray )
            {
                // 2023-07-21: Needs discussion with ISTS stakeholders
                const majorInterlockNames = [ "" ];
                const standardInterlockNames = [ "ITVW ILK" ];
                const minorInterlockNames = [ "IQ FREIGABE",];
                return getSoundAnnouncementType( currentInterlocksArray, minorInterlockNames, standardInterlockNames, majorInterlockNames );
            }

            function getProscanSoundAnnouncementType( currentInterlocksArray )
            {
                const majorInterlockNames = [ "IQ FREI" ];
                const standardInterlockNames = [ "HF FREI", "HALLSONDE WRN", "BMA1 INTERLOCK" ];
                const minorInterlockNames = [ "" ];
                return getSoundAnnouncementType( currentInterlocksArray, minorInterlockNames, standardInterlockNames, majorInterlockNames );
            }

            function getIlktestSoundAnnouncementType( currentInterlocksArray )
            {
                const majorInterlockNames = [ "IonSourceOff" ];
                const standardInterlockNames = [ "BN5 ILK1" ];
                const minorInterlockNames = [ "AHK1 ILK1" ];
                return getSoundAnnouncementType( currentInterlocksArray, minorInterlockNames, standardInterlockNames, majorInterlockNames );
            }

            switch ( facilityId )
            {
                case "ilktest":
                    return getIlktestSoundAnnouncementType( currentInterlocksArray );
                case "hipa":
                    return getHipaSoundAnnouncementType( currentInterlocksArray );
                case "ists":
                    return getIstsSoundAnnouncementType( currentInterlocksArray )
                case "proscan":
                    return getProscanSoundAnnouncementType( currentInterlocksArray )
            }
        }

        function announceNewInterlocks( currentInterlocksArray )
        {
            function createNewEntriesArray( array1, array2 )
            {
                function isEqualObjects( obj1, obj2)
                {
                    return obj1.sectionId === obj2.sectionId;
                }
                return array2.filter(entry2 => !array1.some( entry1 => isEqualObjects( entry1, entry2 ) ) );
            }

            const savedCurrentInterlocksArray = stateManager.hasSavedObject( "savedCurrentInterlocks" ) ? stateManager.getSavedObject( "savedCurrentInterlocks" ) : [];
            const newEntriesArray = createNewEntriesArray( savedCurrentInterlocksArray, currentInterlocksArray  )
            const announcementType = getInterlockSoundAnnouncementType( newEntriesArray );
            if ( announcementType !== SOUND_SUPPORT.announcements.NONE )
            {
                const enableSound = enableSoundEle.checked;
                const enableVoice = enableVoiceEle.checked;
                SOUND_SUPPORT.announce(announcementType, enableSound, enableVoice);
            }
            stateManager.saveObject( "savedCurrentInterlocks", currentInterlocksArray );
        }

        function updateInterlockHistory( interlockHistoryArray )
        {
            const containerEle = shadowDomHelper.getElementById( "interlock-history-signals" );
            const hoursAgo = stateManager.getSavedObject( "EarliestInterlockOfInterest" );
            SIGNAL_SUPPORT.updateInterlockHistorySignals( containerEle, interlockHistoryArray, hoursAgo );
        }

        function updateImportantOutputSignals( sectionStatesArray )
        {
            const containerEle = shadowDomHelper.getElementById("important-output-signals");
            SIGNAL_SUPPORT.updateImportantOutputSignals( containerEle, sectionStatesArray );
        }

        function updateBridgeSignals( sectionStatesArray )
        {
            const containerEle = shadowDomHelper.getElementById( "signals-with-bridges" );
            SIGNAL_SUPPORT.updateBridgeSignals( containerEle, sectionStatesArray );
        }

        function updateFaultSignals( sectionStatesArray )
        {
            const containerEle = shadowDomHelper.getElementById("signals-with-faults");
            SIGNAL_SUPPORT.updateFaultSignals( containerEle, sectionStatesArray );
        }

        function updateTargetSectionSignals( sectionStatesArray )
        {
            const containerEle = shadowDomHelper.getElementById( "target-section-signals");
            const targetSectionId = stateManager.getSavedObject( "TargetSection" );
            SIGNAL_SUPPORT.updateTargetSectionSignals( containerEle, sectionStatesArray, targetSectionId );
        }

        function updateConfigureFacilityElements( facilityModeDescriptions )
        {
            function decodeOptions( optionsString )
            {
                let options = [];
                let optionsArrayObj = optionsString.split( ";" );
                optionsArrayObj.forEach( opt =>
                {
                    const [,value] = opt.split( ":" );
                    options.push( value );
                } );
                return options.join( " " );
            }

            const primary = facilityModeDescriptions[ "primary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            const secondary = facilityModeDescriptions[ "secondary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            const tertiary = facilityModeDescriptions[ "tertiary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            const optional = facilityModeDescriptions[ "optional" ];
            const decodedOptions = decodeOptions( optional );

            const facilityPrimaryModeEle = shadowDomHelper.getElementById( 'facility-primary-mode-label' );
            facilityPrimaryModeEle.textContent = primary;

            const facilitySecondaryModeEle = shadowDomHelper.getElementById( 'facility-secondary-mode-label' );
            facilitySecondaryModeEle.textContent = secondary;

            const facilityTertiaryModeEle = shadowDomHelper.getElementById( 'facility-tertiary-mode-label' );
            facilityTertiaryModeEle.textContent = tertiary;

            const facilityModeOptionEle = shadowDomHelper.getElementById( 'facility-mode-option-label' );
            facilityModeOptionEle.textContent = decodedOptions;
        }

        function updateSvgInformationBox( value )
        {
            // Note: Currently (2023-03-29) this feature is not fully supported. In the future it could
            // be used to update the SVG Graphic with live control system values.
            stateManager.getSavedObject( "SvgContentManager" ).update( "info-item", value );
        }

        // Update Facility State
        stateManager.saveObject( "FacilityState", facilityState );

        // Update Facility Mode Indicators
        const facilityModeDescriptions = facilityState[ "facilityMode" ]?.[ "facilityModeDescriptions" ];
        updateConfigureFacilityElements( facilityModeDescriptions );

        // Update SVG Graphic
        const svgStateManager = stateManager.getSavedObject( "SvgStateManager");
        const sectionStatesArrayObject = facilityState[ "sectionStates" ];
        sectionStatesArrayObject.forEach( (sectionStateObject) => updateSvgState( sectionStateObject, svgStateManager ) );

        // Note: Currently (2023-03-29) this feature is not yet fully supported.
        updateSvgInformationBox( "under devl" );

        // Update Signal Privileges
        updateCurrentUserPrivileges( currentUserSession );

        // Announce New Interlocks
        const currentInterlocksArray = facilityState[ "currentInterlocks" ];
        announceNewInterlocks( currentInterlocksArray );

        // Update Interlock History Display
        const interlockHistoryArray = facilityState[ "interlockHistory" ];
        const combinedInterlockHistoryArray = [ ...currentInterlocksArray, ...interlockHistoryArray ];
        combinedInterlockHistoryArray.sort( (sigA,sigB) => Date.parse( sigB[ "tstrigger" ] ) - Date.parse( sigA[ "tstrigger" ] ) );
        updateInterlockHistory( combinedInterlockHistoryArray );

        // Update Interlock Signals
        updateImportantOutputSignals( sectionStatesArrayObject) ;
        updateBridgeSignals( sectionStatesArrayObject );
        updateFaultSignals( sectionStatesArrayObject );

        // Update Target Section Signals
        updateTargetSectionSignals( sectionStatesArrayObject );
    }

    /*-----------------------------------------------------------------------*/
    /* 6.0 INTERLOCK HISTORY PANEL SUPPORT FUNCTIONS                         */
    /*-----------------------------------------------------------------------*/

    function downloadInterlockHistory()
    {
        if ( downloadContentAsJsonEle.checked )
        {
            downloadInterlockHistoryAsJson()
        }
        else {
            downloadInterlockHistoryAsCsv();
        }
    }

    function downloadInterlockHistoryAsCsv()
    {
        function getFormattedTimestamp( fullTimestamp )
        {
            // Define function to add leading zeros to time/date tokens when the token is only
            // 1 character in length
            function get2Digits(inp)
            {
                return ("0" + inp).slice(-2);
            }

            // Build a JS Date object based on the supplied timestamp which typically looks like something
            // like this: 2022-05-15T03:46:24.981360032
            const d = new Date( fullTimestamp) ;

            // Create and return the timestamp string in the format we want it.
            const formattedDate = d.toDateString();
            const formattedTime = get2Digits(d.getHours()) + ":" + get2Digits(d.getMinutes()) + ":" + get2Digits(d.getSeconds()) + "." + d.getMilliseconds();
            return formattedDate + " " + formattedTime;
        }

        function serializeInterlockHistoryAsCsv( )
        {
            const facilityState = stateManager.getSavedObject( "FacilityState" );
            const interlockHistoryArray = facilityState[ "interlockHistory" ];
            const maxInterlockCauses = interlockHistoryArray.reduce( (acc,item) => {
                // eslint-disable-next-line no-prototype-builtins
            return item.hasOwnProperty( "causes" ) && item.causes.length > acc ? item.causes.length : acc;
        }, 0 );

        let headerString= "Trigger Time,Output Signal Name";
        for ( let i = 1; i <= maxInterlockCauses; i++ )
        {
            headerString = headerString + "," + "Interlock Cause Candidate " +  i;
        }

        const compactHistoryString = interlockHistoryArray.reduce( (acc,item) => {
            // noinspection JSUnresolvedVariable
            const formattedTriggerTime = getFormattedTimestamp( item.tstrigger );

            // eslint-disable-next-line no-prototype-builtins
            if ( ! item.hasOwnProperty( "causes" ) ) {
                return acc + formattedTriggerTime + "," + item.okName;
            }

            const formattedCauses = item.causes.reduce( (acc,cause) => {
                const extraInfo = cause.extraInfo === "" ? "" : " (" + cause.extraInfo + ")";
                return acc + "," + cause.okName + extraInfo;
            }, "" );
                return acc +  formattedTriggerTime + "," + item.okName  + formattedCauses + "\n";
            }, "" );

            return headerString + "\n" + compactHistoryString;
        }

        const history = serializeInterlockHistoryAsCsv();

        // Credit: https://thewebdev.info/2022/05/02/how-to-save-json-to-local-text-file-with-javascript/
        const a = document.createElement("a");
        const file = new Blob( [ history ], { type: "text/csv;charset=utf-8",  endings: "native"  });
        a.href = URL.createObjectURL(file);
        a.download = "interlock-history.csv";
        a.click();
    }

    function downloadInterlockHistoryAsJson()
    {
        function serializeInterlockHistoryAsJson( )
        {
            const facilityState = stateManager.getSavedObject( "FacilityState" );
            const interlockHistoryArray = facilityState.interlockHistory;
            const compactHistory = interlockHistoryArray.map( (item) => {
                return { "time" : item.tstrigger, "okName" : item.okName, "causes" : item.causes };
            } );

            const formatter = new Formatter();
            formatter.maxInlineLength = 160;
            return formatter.serialize( compactHistory );
        }

        const history = serializeInterlockHistoryAsJson();

        // Credit: https://thewebdev.info/2022/05/02/how-to-save-json-to-local-text-file-with-javascript/
        const a = document.createElement("a");
        const file = new Blob([history], { type: "application/json" });
        a.href = URL.createObjectURL(file);
        a.download = "interlock-history.json";
        a.click();
    }

    function showInterlocksSince( hoursAgo=24+365+50 )
    {
        stateManager.saveObject( "EarliestInterlockOfInterest", hoursAgo );
        const interlockHistoryInfoHeaderEle = shadowDomHelper.getElementById( "interlock-history-header-text" );
        if ( hoursAgo === 24+365+50 ) {
            interlockHistoryInfoHeaderEle.textContent = "Interlock History - All";
        }
        else {
            interlockHistoryInfoHeaderEle.textContent = "Interlock History - last " + hoursAgo + " hours";
        }

        const containerEle = shadowDomHelper.getElementById( "interlock-history-signals" );
        const facilityState = stateManager.getSavedObject( "FacilityState" );
        const interlockHistoryArray = facilityState[ "interlockHistory" ];
        SIGNAL_SUPPORT.updateInterlockHistorySignals( containerEle, interlockHistoryArray, hoursAgo );
    }

    /*-----------------------------------------------------------------------*/
    /* 9.0 SIGNAL HANDLING SUPPORT FUNCTIONS                                 */
    /*-----------------------------------------------------------------------*/

    async function handleIlKSignalClicked( event )
    {
        switch ( event.type )
        {
            case "ev-ilk-signal-bridge-clicked":
                return handleIlkSignalBridgeCellClicked( event );
            case "ev-ilk-signal-interlock-clicked":
                return handleIlkSignalInterlockCellClicked( event );
            case "ev-ilk-signal-link-clicked":
                return handleIlkSignalLinkCellClicked( event );
            case "ev-ilk-signal-label-clicked":
                return handleIlkSignalLabelCellClicked( event );
            case "ev-ilk-signal-interlock-cause-clicked":
                return handleIlkSignalCauseClicked( event );
        }
    }

    async function handleIlkSignalBridgeCellClicked( event )
    {
        const moduleId = event.detail.moduleId;
        const portId = event.detail.portId;
        const ioType = event.detail.ioType;
        const currentBridgeState = event.detail.bridgeState;
        const facilityId = "__FACILITY__".toLowerCase();
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + "/" +  ioType + "s/" + portId + "/bridges";
        switch ( currentBridgeState )
        {
            case "rpm_clr":
                return HTTP_SUPPORT.doPutRequest( requestUrl, "SET_BRIDGE", requestEle, responseEle, errorEle );

            case "rpm_set":
                return HTTP_SUPPORT.doPutRequest( requestUrl, "CLEAR_BRIDGE", requestEle, responseEle, errorEle );

            case "usi2_clr":
                return HTTP_SUPPORT.doPutRequest( requestUrl, "SET_OK_BRIDGE", requestEle, responseEle, errorEle );

            case "usi2_ok":
                // Note: 2023-04-12 USI2 NOK bridging has been disabled from the user interface until it is
                // provided via a context-sensitive menu.
                // HTTP_SUPPORT.doPutRequest( requestUrl, "SET_NOK_BRIDGE", requestEle, responseEle, errorEle );
                console.warn( "USI2 NOK bridging has been disabled => USI2 OK bridge will be cleared." );
                return HTTP_SUPPORT.doPutRequest( requestUrl, "CLEAR_BRIDGE", requestEle, responseEle, errorEle );

            case "usi2_nok":
                return HTTP_SUPPORT.doPutRequest( requestUrl, "CLEAR_BRIDGE", requestEle, responseEle, errorEle );

            default:
                console.error( "Unexpected bridge state" );
                break;
        }
    }

    async function handleIlkSignalInterlockCellClicked( event )
    {
        const moduleId = event.detail.moduleId;
        const facilityId = "__FACILITY__".toLowerCase();
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + "/interlocks";
        return HTTP_SUPPORT.doPutRequest( requestUrl, "CLEAR", requestEle, responseEle, errorEle );
    }

    function handleIlkSignalLabelCellClicked( event )
    {
        const sectionId = event.detail[ "sectionId" ];
        const moduleId = event.detail[ "moduleId" ];
        const portId = event.detail[ "portId" ];
        console.log( "Label cell clicked on section: '" + sectionId + "', moduleId: '" + moduleId + "', portId:" + portId + "'" );
        document.dispatchEvent( new CustomEvent( 'ev-doc-select-panel', { bubbles: true, composed: true, detail: { "panelName" : "configuration" } } ) );
        document.dispatchEvent( new CustomEvent( 'ev-doc-target-section-selected', { bubbles: true, composed: true, detail: { "sectionId": sectionId } } ) );
        document.dispatchEvent( new CustomEvent( 'ev-doc-target-module-selected',  { bubbles: true, composed: true, detail: { "moduleId": moduleId } } ) );
        document.dispatchEvent( new CustomEvent( 'ev-doc-expert-module-selected', { bubbles: true, composed: true, detail: { "moduleId": moduleId } } ) );
    }

    function handleIlkSignalLinkCellClicked( event )
    {
        const sectionId = event.detail.connectionInfo[ "sectionId" ];
        const moduleId = event.detail.connectionInfo[ "moduleId" ];
        const portId = event.detail.connectionInfo[ "portId" ];
        console.log( "Link cell clicked on section: '" + sectionId + "', moduleId: '" + moduleId + "', portId:" + portId + "'" );
        document.dispatchEvent( new CustomEvent( 'ev-doc-select-panel', { bubbles: true, composed: true, detail: { "panelName" : "configuration" } } ) );
        document.dispatchEvent( new CustomEvent( 'ev-doc-target-section-selected', { bubbles: true, composed: true, detail: { "sectionId": sectionId } } ) );
        document.dispatchEvent( new CustomEvent( 'ev-doc-target-module-selected',  { bubbles: true, composed: true, detail: { "moduleId": moduleId } } ) );
        document.dispatchEvent( new CustomEvent( 'ev-doc-expert-module-selected', { bubbles: true, composed: true, detail: { "moduleId": moduleId } } ) );
    }

    function handleIlkSignalCauseClicked( event )
    {
        const sectionId = event.detail.causeInfo[ "sectionId" ];
        const moduleId = event.detail.causeInfo[ "moduleId" ];
        const portId = event.detail.causeInfo[ "portId" ];
        console.log( "Interlock cause clicked on sectionId: '" + sectionId + "', moduleId: '" + moduleId + "', portId: '" + portId + "'" );
        document.dispatchEvent( new CustomEvent( 'ev-doc-select-panel', { bubbles: true, composed: true, detail: { "panelName" : "configuration" } } ) );
        document.dispatchEvent( new CustomEvent( 'ev-doc-target-section-selected', { bubbles: true, composed: true, detail: { "sectionId": sectionId } } ) );
        document.dispatchEvent( new CustomEvent( 'ev-doc-target-module-selected',  { bubbles: true, composed: true, detail: { "moduleId": moduleId } } ) );
        document.dispatchEvent( new CustomEvent( 'ev-doc-expert-module-selected', { bubbles: true, composed: true, detail: { "moduleId": moduleId } } ) );
    }

    /*-----------------------------------------------------------------------*/
    /* 8.0 SVG SUPPORT FUNCTIONS                                             */
    /*-----------------------------------------------------------------------*/

    function svgInit()
    {
        const svgFacilityElement = shadowDomHelper.getElementById( "svgFacilityElement" );
        const svgLegendElement = shadowDomHelper.getElementById( "svgLegendElement" );
        const svgFacilityDoc = svgFacilityElement.contentDocument;
        const svgLegendDoc = svgLegendElement.contentDocument;

        registerSvgSectionClickHandlers( svgFacilityDoc );

        const propertyManager = createSvgPropertyManager( svgLegendDoc );
        stateManager.saveObject( "SvgStateManager", new SvgStateManager( svgFacilityDoc, propertyManager ) );
        stateManager.saveObject( "SvgContentManager", new SvgContentManager( svgFacilityDoc ) );
    }

    function updateSvgState( sectionStateObject, svgStateManager )
    {
        // Attempt to find an SVG element whose ID corresponds to the name of the section to update.
        const sectionId = sectionStateObject[ "sectionId" ];

        // Get all the data from the transfer object
        const sectionInterlockState = sectionStateObject[ "sectionInterlockState" ];
        const sectionConfigurationState = sectionStateObject?.[ "sectionMode" ]?.[ "sectionModeDescriptions" ]?.[ "primary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
        const sectionInService = sectionStateObject[ "sectionInService" ];
        const sectionBeamStopperState = sectionStateObject[ "sectionBeamStopperState" ];

        // Update the graphic
        svgStateManager.setBeamlineSectionInterlockState( sectionId, sectionInterlockState );
        svgStateManager.setBeamlineSectionConfigurationState( sectionId, sectionConfigurationState );
        svgStateManager.setBeamlineSectionStopperState( sectionId, sectionBeamStopperState );
        svgStateManager.setBeamlineSectionServiceState( sectionId, sectionInService );
    }

    function registerSvgSectionClickHandlers( svgDocument )
    {
        const BEAMLINE_SECTION_CLASS_IDENTIFIER = "BEAMLINE_SECTION";
        const sectionParentElements =  svgDocument.getElementsByClassName( BEAMLINE_SECTION_CLASS_IDENTIFIER );
        Array.from( sectionParentElements ).forEach( (section) => {
            section.addEventListener( "mouseover", (ev) => handleTargetSectionMouseSelect( ev, section ) );
            section.addEventListener( "click", (ev) => handleTargetSectionMouseSelect( ev, section ) );
        });
    }

    // Handles a change of section when a section is selected by means of a received event
    async function handleTargetSectionEventSelect( event )
    {
        const sectionId = event.detail.sectionId;
        const sectionInfosArray = stateManager.getSavedObject( "SectionInfos" );
        if ( ! MODULE_SUPPORT.containsSectionId( sectionId, sectionInfosArray ) ) {
            console.warn( "The sectionId: '" + sectionId + "' was invalid.");
            return;
        }

        // Update the model with the ID of the section that has been selected
        stateManager.saveObject( "TargetSection", sectionId );
        const targetSectionModuleInfoHeaderEle = shadowDomHelper.getElementById( "target-section-signal-info");
        targetSectionModuleInfoHeaderEle.textContent = "Section " + sectionId;
        updateWatermarks();
    }

    // Handles updates when a section is clicked on in the SVG Graphic window
    function handleTargetSectionMouseSelect( event, section )
    {
        if ( ( ( event.type === "mouseover" ) && ( selectOnHoverEle.checked ) ) ||
        ( ( event.type === "click" ) && ( ! selectOnHoverEle.checked ) ) )
        {
            // Update the model with the ID of the section that has been selected
            const sectionId = section.id;
            stateManager.saveObject( "TargetSection", sectionId );
            const targetSectionModuleInfoHeaderEle = shadowDomHelper.getElementById( "target-section-signal-info");
            targetSectionModuleInfoHeaderEle.textContent = "Section " + sectionId;
            updateTargetSection();
            updateWatermarks();
        }
    }

    // Handles a change of section when a section is manually selected from the Select Section widget
    function updateTargetSection()
    {
        // The following comment applies when using the PATCH stream...
        // We need to refresh the view immediately using the previous state information until
        // there is some change on the sectionState that really affects the targeted section.
        const containerEle = shadowDomHelper.getElementById( "target-section-signals" );
        const previousFacilityState = stateManager.getSavedObject( "FacilityState" );
        const sectionStatesArray = previousFacilityState[ "sectionStates" ];
        const targetSectionId = stateManager.getSavedObject( "TargetSection" );
        SIGNAL_SUPPORT.updateTargetSectionSignals( containerEle, sectionStatesArray, targetSectionId );
    }

    function createSvgPropertyManager( svgLegendDoc )
    {
        // Get the colors for the different possible states of the beamline elements directly from the
        // relevant properties on the legend
        const okColor = HTML_ELEMENT_SUPPORT.getSvgElementProperty( svgLegendDoc, "legend-graphic-ok", "fill" );
        const interlockedColor = HTML_ELEMENT_SUPPORT.getSvgElementProperty( svgLegendDoc,  "legend-graphic-interlocked", "fill" );
        const bridgedColor = HTML_ELEMENT_SUPPORT.getSvgElementProperty( svgLegendDoc, "legend-graphic-bridged", "fill" );
        const rebootColor = HTML_ELEMENT_SUPPORT.getSvgElementProperty( svgLegendDoc, "legend-graphic-reboot", "fill" );
        const offlineColor = HTML_ELEMENT_SUPPORT.getSvgElementProperty( svgLegendDoc, "legend-graphic-offline", "fill" );
        const hiddenColor = HTML_ELEMENT_SUPPORT.getSvgElementProperty( svgLegendDoc, "legend-graphic-hidden", "fill" );
        const housingColor = "darkgray";
        const inServiceOpacity = 1.0;
        const outOfServiceOpacity = 0.15;
        return new InterlockViewPropertyManager( okColor, interlockedColor, bridgedColor, rebootColor, offlineColor, hiddenColor, housingColor, inServiceOpacity, outOfServiceOpacity );
    }

    function updateWatermarks()
    {
        const targetSectionDefined = stateManager.hasSavedObject( "TargetSection" );
        const watermarkEle = shadowDomHelper.getElementById( "target-section-watermark" );
        watermarkEle.style.display = targetSectionDefined ? "none" : "block";
    }


    /*-----------------------------------------------------------------------*/
    /* 9.0 HTML ELEMENT EVENT LISTENER SUPPORT                               */
    /*-----------------------------------------------------------------------*/

    const handlerMap = new Map();

    function registerHtmlEventHandlers()
    {
        handlerMap.set( "handleIlKSignalClicked", (event) => handleIlKSignalClicked( event ) );
        handlerMap.set( "setUserSignalPreferences", (event) => setUserSignalPreferences( event ) );
        handlerMap.set( "setUserSoundPreferences", (event) => setUserSoundPreferences( event ) );
        handlerMap.set( "setUserVoicePreferences", (event) => setUserVoicePreferences( event ) );
        handlerMap.set( "setUserOtherPreferences", (event) => setUserOtherPreferences( event ) );
        handlerMap.set( "downloadInterlockHistory", (event) => downloadInterlockHistory( event ) );
        handlerMap.set( "showAllInterlocks", () => showInterlocksSince() );
        handlerMap.set( "showLast24hInterlocks", () => showInterlocksSince( 24 ) );
        handlerMap.set( "showLast8hInterlocks", () => showInterlocksSince( 8 ) );
        handlerMap.set( "showLast1hInterlocks", () => showInterlocksSince( 1 ) );
        handlerMap.set( "putFacility", (event) => putFacility( event ) );
        handlerMap.set( "putFacilityMode", (event) => putFacilityMode( event ) );
    }

    function addHtmlElementEventListeners( elementClass, eventName )
    {
        // Find all elements of the specified class
        const allElements = shadowDomHelper.querySelectorAll( "." + elementClass );

        for ( const ele of allElements )
        {
            // If the data-func attribute is present attempt to find a function with
            // the corresponding name and register it as an event handler
            if (  ele.dataset[ "func" ] !== undefined )
            {
                const funcName = ele.dataset[ "func" ];

                // If a function of the required name has been registered in the
                // handler map attach the handler function as an event listener
                if ( handlerMap.has( funcName ) )
                {
                    const func = handlerMap.get( funcName );
                    ele.addEventListener( eventName, func );
                }
                else
                {
                    console.error( "The function named '" + funcName + "' is not defined !" );
                }
            }
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 12.0 HTML SELECT ELEMENT MENU CHOICES SUPPORT                         */
    /*-----------------------------------------------------------------------*/

    async function addGroupedSelectFacilityModeMenuChoices( selectWidget, facility, path )
    {
        // Example returned data:
        //    {"Main-Path":["BW2","BN5"]}
        //    {"AHK1-Path":["Target","Inactive","Not-in-use"]}
        //    {"VAR-Path":["Beam Allow","Beam Stop","Beam Left","Beam Right","Beam Split","Inactive"]}
        try {
            const facilityData = await getFacilityData( facility, path )
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionArrayObj, selectWidget, "Please select"  );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectFacilityModeOptionMenuChoices( selectWidget, facility, path )
    {
        // Example returned data:
        //    {"DIA Mode":["DIA-Off","DIA-On"],"Intensity":["Normal","High"],"MCH Mode":["MCH-Off","MCH-On"]}
        try {
            const facilityData = await getFacilityData( facility, path )
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionArrayObj, selectWidget, "Please select"  );
        }
        catch( error ) {
            console.log( error );
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 13.0 PAGE INITIALISATION                                              */
    /*-----------------------------------------------------------------------*/

    function startStreamListener()
    {
        console.log( "Starting stream listener" );
        document.addEventListener( "ev-stream-facility-state-changed", ev => {
            const facilityState =  ev.detail.facilityState;
            const currentUserSession =  ev.detail.currentUserSession;
            handleFacilityStateChanged( facilityState, currentUserSession );
        } )
        document.addEventListener( "ev-stream-state-connected", () => handleStreamConnected() );
        document.addEventListener( "ev-stream-state-disconnected", () => handleStreamDisconnected() );
    }

    async function init()
    {
        function initialiseStateManager()
        {
            const UNKNOWN_FACILITY_STATE = {};
            stateManager.saveObject( "FacilityState", UNKNOWN_FACILITY_STATE );
            const longAgoInHours = 24+365+50;
            stateManager.saveObject( "EarliestInterlockOfInterest", longAgoInHours );
        }

        function initialiseButtonHandling()
        {
            // Disable all buttons
            HTML_ELEMENT_SUPPORT.setButtonEnableState(shadowRoot, ".request-button", false);

            // Set up the QUAD button reflector to enable the targeted button if any of the select widgets has an active selection.
            const anySelectedPredicate = (a, b, c, d) => (a || b || c || d)
            HTML_ELEMENT_SUPPORT.registerHtmlSelectElementQuadButtonStateReflector(shadowRoot, facilityPrimaryModeSelectorEle, facilitySecondaryModeSelectorEle,
                facilityTertiaryModeSelectorEle, facilityModeOptionSelectorEle, ".facility-config.request-button", anySelectedPredicate);

            // Enable facility command buttons
            HTML_ELEMENT_SUPPORT.setButtonEnableState(shadowRoot, ".facility-command", true);
        }

        function initialiseSound()
        {
            function showSoundPopup()
            {
                // Show the sound popup only in situations where there are not any URL parameters
                const urlParams = new URLSearchParams( window.location.search );
                return [ ...urlParams ].length === 0;
            }

            // Initialise the sound support module
            SOUND_SUPPORT.initialise( facilityId, messageEle );

            if ( showSoundPopup() )
            {
                // Popup message box reminding users to enable the sound features that they require.
                messageEle.closeable = true;
                messageEle.titleText = "Sound Configuration and Test";
                messageEle.messageText = "Please select your required sound options from the Preferences Menu";
            }
        }

        initialiseStateManager()
        initialiseButtonHandling();
        registerHtmlEventHandlers();

        // Bind events that occur on specific HTML classes to their respective handler
        addHtmlElementEventListeners( "select", "change" );
        addHtmlElementEventListeners( "request-button", "click" );
        addHtmlElementEventListeners( "checkbox-setting", "change" );
        addHtmlElementEventListeners( "ilk-signal-handler", "ev-ilk-signal-bridge-clicked" );
        addHtmlElementEventListeners( "ilk-signal-handler", "ev-ilk-signal-interlock-clicked" );
        addHtmlElementEventListeners( "ilk-signal-handler", "ev-ilk-signal-link-clicked" );
        addHtmlElementEventListeners( "ilk-signal-handler", "ev-ilk-signal-label-clicked" );
        addHtmlElementEventListeners( "ilk-signal-handler", "ev-ilk-signal-interlock-cause-clicked" );

        // Initialise the SVG Graphic
        // Note: This needs to come before listening to the stream.
        console.log( "interlock-view.js: Waiting for SVG initialise..." )
        await SVG_SUPPORT.svgInitDetector( shadowDomHelper );
        svgInit();
        console.log( "interlock-view.js: SVG initialised" )

        // Start listening to the stream from the ILKCS server
        startStreamListener();

        // Load Facility configuration information from the remote server and use it set up this page
        console.log( "Waiting for load facility information..." )
        await loadFacilityInformation( facilityId );
        console.log( "Facility information loaded." )

        // Initialise Watermarks
        updateWatermarks();

        // Initialise Sound Support
        initialiseSound();

        // Listen and respond to application wide events that set the target section
        document.addEventListener( "ev-doc-target-section-selected", (ev) => handleTargetSectionEventSelect( ev ) );
    }

    await init();

console.log( "interlock-view.js completed." )


