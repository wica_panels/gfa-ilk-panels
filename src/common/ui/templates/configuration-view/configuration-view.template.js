console.log( "configuration-view.js is running...")

    /*-----------------------------------------------------------------------*/
    /* 1.0 MODULE IMPORT DEFINITIONS                                         */
    /*-----------------------------------------------------------------------*/

    // noinspection NpmUsedModulesInstalled
    import {ShadowDomHelper} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {StateManager} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {ConfigurationViewPropertyManager} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SvgStateManager} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SvgContentManager} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SIGNAL_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SESSION_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {HTML_ELEMENT_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {HTTP_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {MODULE_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SVG_SUPPORT} from "__JS_SUPPORT_FILE__";

    /*-----------------------------------------------------------------------*/
    /* 2.0 VARIABLE DEFINITIONS                                              */
    /*-----------------------------------------------------------------------*/

    const facilityId = "__FACILITY__".toLowerCase();
    const serverUrl = window.location.origin + "__ILKCS_SERVER_PATH_PREFIX__";

    const shadowDomHelper = new ShadowDomHelper( "configuration-panel" );
    const shadowRoot = shadowDomHelper.getShadowRoot();
    const stateManager = new StateManager();

    const facilityPrimaryModeSelectorEle = shadowDomHelper.getElementById( 'facility-primary-mode-select' );
    const facilitySecondaryModeSelectorEle = shadowDomHelper.getElementById( 'facility-secondary-mode-select' );
    const facilityTertiaryModeSelectorEle = shadowDomHelper.getElementById( 'facility-tertiary-mode-select' );
    const facilityModeOptionSelectorEle = shadowDomHelper.getElementById( 'facility-mode-option-select' );

    const expertModeEle = shadowDomHelper.getElementById( 'expert-mode-widget' );
    const showSignalRegisterBitsEle = shadowDomHelper.getElementById( 'show-signal-register-bits-widget' );
    const selectOnHoverEle = shadowDomHelper.getElementById( 'select-on-hover-widget' );
    const reversedSignalDirectionEle = shadowDomHelper.getElementById( 'reversed-signal-direction-widget' );
    const showCommandLogEle = shadowDomHelper.getElementById( 'show-command-log-widget' );

    const sectionIdSelectorEle = shadowDomHelper.getElementById( 'section-id-select' );
    const sectionModeSelectorEle = shadowDomHelper.getElementById( 'section-mode-select' );
    const sectionModeOptionSelectorEle = shadowDomHelper.getElementById( 'section-mode-option-select' );

    const moduleIdSelectorEle = shadowDomHelper.getElementById( 'module-select' );
    const moduleModeSelectorEle = shadowDomHelper.getElementById( 'module-mode-select' );
    const moduleModeOptionSelectorEle = shadowDomHelper.getElementById( 'module-mode-option-select' );

    const requestEle = shadowDomHelper.getElementById( 'http-request' );
    const responseEle = shadowDomHelper.getElementById( 'http-response' );
    const errorEle = shadowDomHelper.getElementById( 'http-error' );


    /*-----------------------------------------------------------------------*/
    /* 3.0 PREFERENCES PANEL SUPPORT FUNCTIONS                               */
    /*-----------------------------------------------------------------------*/

    // Set User Profile
    function setUserSignalPreferences()
    {
        const expertMode = expertModeEle.checked;
        const reversedSignalDirection = reversedSignalDirectionEle.checked;
        const showSignalRegisterBits = showSignalRegisterBitsEle.checked;
        const searchStartEle = shadowDomHelper.getShadowRoot();
        SIGNAL_SUPPORT.setSignalUserPreferences( searchStartEle, expertMode, reversedSignalDirection, showSignalRegisterBits );
        MODULE_SUPPORT.setSignalUserPreferences( searchStartEle, expertMode, reversedSignalDirection, showSignalRegisterBits );
    }

    function setUserOtherPreferences()
    {
        const showCommandLog = showCommandLogEle.checked;
        const gridContainerEle = shadowDomHelper.getElementByClassName( "grid-container" );
        if ( showCommandLog )
        {
            gridContainerEle.classList.add( "cmdlog" );
        }
        else
        {
            gridContainerEle.classList.remove( "cmdlog" );
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 4.0 ILKCS SERVER GET/PUT SUPPORT FUNCTIONS                            */
    /*-----------------------------------------------------------------------*/

    async function getFacilityData( facility, path )
    {
        // Note: the call to doGetRequests below does not pass any HTML elements, consequently
        // failed requests will not result in popups, but they will get logged.
        const requestUrl = serverUrl + "/v3/facilities/" + facility + path;
        return HTTP_SUPPORT.doGetRequest( requestUrl );
    }

    async function putFacilityMode()
    {
        function joinWithSeparator( separator, ...args)
        {
            // filter out empty strings
            const nonEmptyArgs = args.filter(arg => arg !== "");
            return nonEmptyArgs.join( separator );
        }

        const selectedPrimaryModeIndex = facilityPrimaryModeSelectorEle.selectedIndex;
        const selectedPrimaryMode = facilityPrimaryModeSelectorEle.options[selectedPrimaryModeIndex];
        const settingRequest1 = (selectedPrimaryModeIndex > 0) ? selectedPrimaryMode.parentNode.id + ":" + selectedPrimaryMode.value : "";

        const selectedSecondaryModeIndex = facilitySecondaryModeSelectorEle.selectedIndex;
        const selectedSecondaryMode = facilitySecondaryModeSelectorEle.options[selectedSecondaryModeIndex];
        const settingRequest2 = (selectedSecondaryModeIndex > 0) ? selectedSecondaryMode.parentNode.id + ":" + selectedSecondaryMode.value : "";

        const selectedTertiaryModeIndex = facilityTertiaryModeSelectorEle.selectedIndex;
        const selectedTertiaryMode = facilityTertiaryModeSelectorEle.options[selectedTertiaryModeIndex];
        const settingRequest3 = (selectedTertiaryModeIndex > 0) ? selectedTertiaryMode.parentNode.id + ":" + selectedTertiaryMode.value : "";

        const selectedOptionIndex = facilityModeOptionSelectorEle.selectedIndex;
        const selectedOption = facilityModeOptionSelectorEle.options[selectedOptionIndex];
        const settingRequest4 = (selectedOptionIndex > 0) ? selectedOption.parentNode.id + ":" + selectedOption.value : "";

        const requestedMode = joinWithSeparator( ";", settingRequest1, settingRequest2, settingRequest3, settingRequest4 );
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/mode";
        return HTTP_SUPPORT.doPutRequest( requestUrl, requestedMode, requestEle, responseEle, errorEle );
    }

    async function putFacilitySectionMode()
    {
        const sectionId = stateManager.getSavedObject("TargetSection");
        const selectedModeIndex = sectionModeSelectorEle.selectedIndex;
        const selectedMode = sectionModeSelectorEle.options[selectedModeIndex];
        const settingRequest1 = (selectedModeIndex > 0) ? selectedMode.parentNode.id + ":" + selectedMode.value : "";

        const selectedOptionIndex = sectionModeOptionSelectorEle.selectedIndex;
        const selectedOption = sectionModeOptionSelectorEle.options[selectedOptionIndex];
        const settingRequest2 = (selectedOptionIndex > 0) ? selectedOption.parentNode.id + ":" + selectedOption.value : "";

        const separator = ( settingRequest1 === "" ) || ( settingRequest2 === "" ) ? "" : ";";
        const requestedMode = settingRequest1 + separator + settingRequest2;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/sections/" + encodeURIComponent( sectionId ) + "/mode";
        return HTTP_SUPPORT.doPutRequest( requestUrl, requestedMode, requestEle, responseEle, errorEle );
    }

    async function putFacilityModuleMode()
    {
        const moduleId = stateManager.getSavedObject( "TargetModule" );
        const selectedIndex1 = moduleModeSelectorEle.selectedIndex;
        const selectedOption1 = moduleModeSelectorEle.options[ selectedIndex1 ];
        const settingRequest1 =  ( selectedIndex1 > 0 ) ? selectedOption1.parentNode.id + ":" + selectedOption1.value : "";

        const selectedIndex2 = moduleModeOptionSelectorEle.selectedIndex;
        const selectedOption2 = moduleModeOptionSelectorEle.options[ selectedIndex2 ];
        const settingRequest2 =  ( selectedIndex2 > 0 ) ?  selectedOption2.parentNode.id + ":" + selectedOption2.value : "";

        const separator = ( settingRequest1 === "" ) || ( settingRequest2 === "" ) ? "" : ";";
        const requestedMode = settingRequest1 + separator + settingRequest2;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + "/mode";
        return HTTP_SUPPORT.doPutRequest( requestUrl, requestedMode, requestEle, responseEle, errorEle );
    }

    /*-----------------------------------------------------------------------*/
    /* 5.0 ILKCS SERVER GET/PUT EVENT SUPPORT FUNCTIONS                      */
    /*-----------------------------------------------------------------------*/

    async function putFacility( event )
    {
        // Note: Failed PUT requests will result in popups
        const putValue = event.target.dataset.putValue;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + event.target.dataset.path;
        return HTTP_SUPPORT.doPutRequest( requestUrl, putValue, requestEle, responseEle, errorEle );
    }

    async function putFacilitySection( event )
    {
        const sectionId = stateManager.getSavedObject( "TargetSection" );
        const putValue = event.target.dataset.putValue;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/sections/" + encodeURIComponent( sectionId ) + event.target.dataset.path;
        return HTTP_SUPPORT.doPutRequest( requestUrl, putValue, requestEle, responseEle, errorEle );
    }

    async function putFacilityModule( event )
    {
        const moduleId = stateManager.getSavedObject( "TargetModule" );
        const putValue = event.target.dataset.putValue;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + event.target.dataset.path;
        return HTTP_SUPPORT.doPutRequest( requestUrl, putValue, requestEle, responseEle, errorEle );
    }

    /*-----------------------------------------------------------------------*/
    /* 6.0 STREAM HANDLING SUPPORT FUNCTIONS                                 */
    /*-----------------------------------------------------------------------*/

    function handleStreamConnected()
    {
        updateSvgConnectionState( true );
        updateModuleContainerConnectionState( true );
    }

    function handleStreamDisconnected()
    {
        updateSvgConnectionState( false );
        updateModuleContainerConnectionState( false );
        const UNKNOWN_FACILITY_STATE = {};
        stateManager.saveObject( "FacilityState", UNKNOWN_FACILITY_STATE );
    }

    function updateSvgConnectionState( connected )
    {
        const svgStateManager = stateManager.getSavedObject( "SvgStateManager" );
        const svgFacilityElement = shadowDomHelper.getElementById("svgFacilityElement");
        const svgLegendElement = shadowDomHelper.getElementById( "svgLegendElement" );

        const opacity = connected ? "1.0": "0.3";
        svgFacilityElement.style.opacity = opacity;
        svgLegendElement.style.opacity = opacity;
        if (! connected )
        {
            // Set all beamline sections to indicate that they are offline.
            // Set the sectionInService flag to indicate the section is in active service.
            // This means that the section will be displayed as Offline not Hidden.
            svgStateManager.getBeamlineSectionElements().forEach( ele => {
                const sectionId = ele.id;
                svgStateManager.setBeamlineSectionConfigurationState( sectionId, "UNKNOWN" );
                svgStateManager.setBeamlineSectionStopperState( sectionId, "UNKNOWN" );
                svgStateManager.setBeamlineSectionServiceState( sectionId, false );
            });
        }
    }

    function updateModuleContainerConnectionState( connected )
    {
        const moduleContainerEles = shadowDomHelper.querySelectorAll( "ilk-module-container" );
        moduleContainerEles.forEach(  (cont) => {
            console.log( "setting module container with ID: '" + cont.id + "' to connection state: " + connected + "'." );
            cont.connected = connected;
        } );
    }

    /*-----------------------------------------------------------------------*/
    /* 7.0 CONFIGURE FACILITY PANEL SUPPORT FUNCTIONS                        */
    /*-----------------------------------------------------------------------*/

    async function loadFacilityInformation( facilityId )
    {
        await Promise.all( [
            addGroupedSelectFacilityModeMenuChoices( facilityPrimaryModeSelectorEle, facilityId, "/mode?detail=choices&filter=primary" ),
            addGroupedSelectFacilityModeMenuChoices( facilitySecondaryModeSelectorEle, facilityId, "/mode?detail=choices&filter=secondary" ),
            addGroupedSelectFacilityModeMenuChoices( facilityTertiaryModeSelectorEle, facilityId, "/mode?detail=choices&filter=tertiary" ),
            addGroupedSelectFacilityModeOptionMenuChoices( facilityModeOptionSelectorEle, facilityId, "/mode?detail=choices&filter=optional" ),
            addGroupedSelectSectionIdMenuChoices( sectionIdSelectorEle, facilityId, "?detail=sectionIdsByGroupId", ),
            addGroupedSelectModuleIdMenuChoices( moduleIdSelectorEle, facilityId, "?detail=moduleIds" )
        ] );

        await synchronizeFacilityModeSelectWidgets( facilityPrimaryModeSelectorEle, facilitySecondaryModeSelectorEle, facilityTertiaryModeSelectorEle, facilityModeOptionSelectorEle );

        try {
            const sectionInfos = await getFacilityData( facilityId, "/sections?detail=info")
            stateManager.saveObject( "SectionInfos", sectionInfos );
        }
        catch( error ) {
           console.log( error );
         }
    }

    async function synchronizeFacilityModeSelectWidgets( primaryConfigEle, secondaryConfigEle, tertiaryConfigEle, optionConfigEle )
    {
        try {
            const facilityState = await getFacilityData( facilityId, "?detail=state" )
            const modeDescriptions = facilityState[ "facilityMode" ]?.[ "facilityModeDescriptions" ];
            // Example Data:
            // "facilityModeDescriptions": {
            //    "primary": "Main-Path:BW2",
            //    "secondary": "AHK1-Path:Not-in-use",
            //    "tertiary": "VAR-Path:Beam Split",
            //    "optional": "DIA Mode:DIA-On;Intensity:High;MCH Mode:MCH-On"
            // },

            // Synchronize Facility Mode
            const primary = modeDescriptions[ "primary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            const secondary = modeDescriptions[ "secondary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            const tertiary = modeDescriptions[ "tertiary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            HTML_ELEMENT_SUPPORT.setSelectWidget( primaryConfigEle, primary );
            HTML_ELEMENT_SUPPORT.setSelectWidget( secondaryConfigEle, secondary );
            HTML_ELEMENT_SUPPORT.setSelectWidget( tertiaryConfigEle, tertiary );

            // Synchronize Facility Mode Option
            // Note: there may be more than one option to synchronize on. The algorithm here simply picks the first.
            const option = modeDescriptions[ "optional" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            HTML_ELEMENT_SUPPORT.setSelectWidget( optionConfigEle, option );

            // By dispatching the change event on one of the selectors this ensures that the button reflector will be
            // triggered to set the correct state of button enablement.
            primaryConfigEle.dispatchEvent( new Event( "change" ) );
        }
        catch( error ) {
            console.log( error );
        }
    }

    function handleFacilityStateChanged( facilityState, currentUserSession )
    {
        function updateCurrentUserPrivileges( currentUserSession )
        {
            // Update signal bridge setting privileges
            const userPrivileges = SESSION_SUPPORT.getCurrentUserPrivileges( currentUserSession );
            const searchStartEle = shadowDomHelper.getShadowRoot();
            SIGNAL_SUPPORT.setSignalUserPrivileges( searchStartEle, userPrivileges );
            MODULE_SUPPORT.setSignalUserPrivileges( searchStartEle, userPrivileges );
        }

        function updateConfigureFacilityElements( facilityModeDescriptions )
        {
            function decodeOptions( optionsString )
            {
                let options = [];
                let optionsArrayObj = optionsString.split( ";" );
                optionsArrayObj.forEach( opt =>
                {
                    const [,value] = opt.split( ":" );
                    options.push( value );
                } );
                return options.join( " " );
            }

            const primary = facilityModeDescriptions[ "primary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            const secondary = facilityModeDescriptions[ "secondary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            const tertiary = facilityModeDescriptions[ "tertiary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
            const optional = facilityModeDescriptions[ "optional" ];
            const decodedOptions = decodeOptions( optional );

            const facilityPrimaryModeEle = shadowDomHelper.getElementById( 'facility-primary-mode-label' );
            facilityPrimaryModeEle.textContent = primary;

            const facilitySecondaryModeEle = shadowDomHelper.getElementById( 'facility-secondary-mode-label' );
            facilitySecondaryModeEle.textContent = secondary;

            const facilityTertiaryModeEle = shadowDomHelper.getElementById( 'facility-tertiary-mode-label' );
            facilityTertiaryModeEle.textContent = tertiary;

            const facilityModeOptionEle = shadowDomHelper.getElementById( 'facility-mode-option-label' );
            facilityModeOptionEle.textContent = decodedOptions;
        }

        function updateSvgInformationBox( value )
        {
            // Note: Currently (2023-03-29) this feature is not fully supported. In the future it could
            // be used to update the SVG Graphic with live control system values.
            stateManager.getSavedObject( "SvgContentManager" ).update( "info-item", value );
        }

        // Update Facility State
        stateManager.saveObject( "FacilityState", facilityState );

        // Update Facility Mode Indicators
        const facilityModeDescriptions = facilityState[ "facilityMode" ]?.[ "facilityModeDescriptions" ];
        updateConfigureFacilityElements( facilityModeDescriptions );

        // Update SVG Graphic
        const svgStateManager = stateManager.getSavedObject( "SvgStateManager" );
        const sectionStatesArrayObject = facilityState[ "sectionStates" ];
        sectionStatesArrayObject.forEach( (sectionStateObject) => updateSvgState( sectionStateObject, svgStateManager ) );

        // Note: Currently (2023-03-29) this feature is not yet fully supported.
        updateSvgInformationBox( "under devl" );

        // Update Signal and Module Privileges
        updateCurrentUserPrivileges( currentUserSession );

        // Update Target Section if it has already been selected
        if ( stateManager.hasSavedObject( "TargetSection" ) ) {
            updateTargetSection();
        }

        // Update Target Module if it has already been selected
        if ( stateManager.hasSavedObject( "TargetModule" ) ) {
            updateTargetModule();
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 8.0 CONFIGURE SECTION PANEL SUPPORT FUNCTIONS                         */
    /*-----------------------------------------------------------------------*/

    async function loadSectionInformation( sectionId )
    {
        // Configure the section-dependant query buttons
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".config-section.request-button", false );

        // Enable the select elements for section-related queries
        await addGroupedSelectSectionModeMenuChoices( sectionModeSelectorEle, facilityId, "/sections/" + encodeURIComponent( sectionId ) + "/mode?detail=choices&filter=primary" );
        await addGroupedSelectSectionModeOptionMenuChoices( sectionModeOptionSelectorEle, facilityId, "/sections/" + encodeURIComponent( sectionId )  + "/mode?detail=choices&filter=optional" );
    }

    // Handles a change of section when a section is selected by means of a received event
    async function handleTargetSectionEventSelect( event )
    {
        const sectionId = event.detail.sectionId;
        const sectionInfosArray = stateManager.getSavedObject( "SectionInfos" );
        if ( ! MODULE_SUPPORT.containsSectionId( sectionId, sectionInfosArray ) ) {
            console.warn( "The sectionId: '" + sectionId + "' was invalid.");
            return;
        }

        stateManager.saveObject( "TargetSection", sectionId);
        await updateConfigureSectionElements();
        updateWatermarks();
    }

    // Handles a change of section when a section is clicked on in the SVG graphic
    async function handleTargetSectionMouseSelect( event, section )
    {
        // Only process events which correspond to the preferred display preference
        if ( ( ( event.type === "mouseover" ) && ( selectOnHoverEle.checked ) ) ||
             ( ( event.type === "click" ) && ( ! selectOnHoverEle.checked ) ) )
        {
            // Update the model with the ID of the section that has been selected
            const sectionId = section.id;
            stateManager.saveObject( "TargetSection", sectionId );
            await updateConfigureSectionElements();
            updateTargetSection();
            updateWatermarks();
        }
    }

    // Handles updates when a section is manually selected from the "Select Section" select widget.
    async function handleTargetSectionWidgetSelect( event )
    {
        // Clear the section-dependant select elements. This is required to clear the options if the
        // selection changes to select nothing.
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( sectionModeSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( sectionModeOptionSelectorEle );

        // Clear the section-dependant button elements
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".section-config.request-button", false );

        // If a new section is selected...
        if ( event.target.selectedIndex !== 0)
        {
            // Enable the select elements for section-related queries
            const sectionId = sectionIdSelectorEle.value;
            stateManager.saveObject( "TargetSection", sectionId );
            await updateConfigureSectionElements();
            updateTargetSection();
            updateWatermarks();
        }
    }

    // Handles a change of section when a section is manually selected from the Select Section widget
    async function updateConfigureSectionElements()
    {
        // Get the target section
        const targetSectionId = stateManager.getSavedObject( "TargetSection" );

        // Update the Configure Section information element to show the ID of the section that has been selected
        const configureSectionInfoEle = shadowDomHelper.getElementById( "configure-section-info" );
        configureSectionInfoEle.textContent = "Configure Section " + targetSectionId;

        // Update the Target Section information to show the ID of the section that has been selected
        const targetSectionInfoEle = shadowDomHelper.getElementById( "target-section-info");
        targetSectionInfoEle.textContent = "Section " + targetSectionId + " Modules";
        sectionIdSelectorEle.value = targetSectionId;

        // Fetch the section information from the backend server
        await loadSectionInformation( targetSectionId );

        // Enable the section command buttons, for example, Section Reset
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".section-command", true );
    }

    // Handles a change of section when a section is manually selected from the Select Section widget
    function updateTargetSection()
    {
        function decodeModeOptions( optionsString )
        {
            let options = [];
            let optionsArrayObj = optionsString.split( ";" );
            optionsArrayObj.forEach( opt =>
            {
                const [,value] = opt.split( ":" );
                options.push( value );
            } );
            return options.join( " " );
        }

        const sectionInfosArray = stateManager.getSavedObject( "SectionInfos" );
        const targetSectionId = stateManager.getSavedObject( "TargetSection" );
        const previousFacilityState = stateManager.getSavedObject( "FacilityState" );
        const sectionStatesArrayObject = previousFacilityState[ "sectionStates" ];
        const targetSectionContainerEle = shadowDomHelper.getElementById( "target-section-container");
        MODULE_SUPPORT.updateTargetSection( targetSectionContainerEle, sectionStatesArrayObject, sectionInfosArray, targetSectionId );

        const sectionNameEle = shadowDomHelper.getElementById( 'section-name-label' );
        sectionNameEle.textContent = targetSectionId;

        const sectionState = sectionStatesArrayObject.find( st => st[ "sectionId" ] === targetSectionId );
        const sectionMode = sectionState?.[ "sectionMode" ]?.[ "sectionModeDescriptions" ]?.[ "primary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
        const sectionModeEle = shadowDomHelper.getElementById( 'section-mode-label' );
        sectionModeEle.textContent = sectionMode;

        const sectionModeOptionEle = shadowDomHelper.getElementById( 'section-mode-option-label' );
        const sectionModeOptions = sectionState?.[ "sectionMode" ]?.[ "sectionModeDescriptions" ]?.[ "optional" ];
        sectionModeOptionEle.textContent = decodeModeOptions( sectionModeOptions );
    }

    function updateTargetSectionHighlighting()
    {
        if ( stateManager.hasSavedObject( "TargetModule" ) ) {
            const targetModuleId = stateManager.getSavedObject( "TargetModule" );
            const targetSectionContainerEle = shadowDomHelper.getElementById( "target-section-container" );
            MODULE_SUPPORT.updateTargetSectionHighlighting( targetSectionContainerEle, targetModuleId );
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 9.0 CONFIGURE MODULE SUPPORT FUNCTIONS                                */
    /*-----------------------------------------------------------------------*/

    async function loadModuleInformation( moduleId )
    {
        // Enable the select elements for module-related queries
        await addGroupedSelectModuleModeMenuChoices( moduleModeSelectorEle, facilityId, "/modules/" + moduleId + "/mode?detail=choices&filter=primary" );
        await addGroupedSelectModuleModeOptionMenuChoices( moduleModeOptionSelectorEle, facilityId, "/modules/" + moduleId + "/mode?detail=choices&filter=other" );
    }

    // Handles a change of module when a module is selected by means of a received event
    async function handleTargetModuleEventSelect( event )
    {
        const moduleId = event.detail.moduleId;
        const sectionInfosArray = stateManager.getSavedObject( "SectionInfos" );
        if ( ! MODULE_SUPPORT.containsModuleId( moduleId, sectionInfosArray ) ) {
            console.warn( "The moduleId: '" + moduleId + "' was invalid.");
            return;
        }

        stateManager.saveObject( "TargetModule", moduleId );
        await updateConfigureModuleElements();
        updateTargetSectionHighlighting();
        updateWatermarks();
    }

    // Handles a change of module when a module is clicked on in the "Target Module" area.
    async function handleExpertModuleMouseSelect( event )
    {
        const moduleId = event.detail[ "moduleId" ];
        console.log( "Click on moduleId: '" + moduleId + "'" );
        document.dispatchEvent( new CustomEvent( 'ev-doc-select-panel', { bubbles: true, composed: true, detail: { "panelName" : "module-expert" } } ) );
        document.dispatchEvent( new CustomEvent( 'ev-doc-expert-module-selected', { bubbles: true, composed: true, detail: { "moduleId": moduleId } } ) );
    }

// Handles a change of module when a module is clicked on in the "Target Section" area.
    async function handleTargetModuleMouseSelect( event )
    {
        // Only process events which correspond to the preferred display preference
        if ( ( ( event.type === "ev-ilk-module-mouseover" ) && ( selectOnHoverEle.checked ) ) ||
            ( ( event.type === "ev-ilk-module-click" ) && ( ! selectOnHoverEle.checked ) ) )
        {
            const moduleId = event.detail.moduleId;
            stateManager.saveObject( "TargetModule", moduleId );
            await updateConfigureModuleElements();
            updateTargetSectionHighlighting();
            updateTargetModule();
            updateWatermarks();
        }
    }

    // Handles a change of module when a module is manually selected from the Select Module widget
    async function handleTargetModuleWidgetSelect( event )
    {
        // Clear the section-dependant select elements. This is required to clear the options if the
        // selection changes to select nothing.
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleModeSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleModeOptionSelectorEle );

        // Clear the section-dependant button elements
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".module-config.request-button", false );

        // If a new module is selected...
        if ( event.target.selectedIndex !== 0)
        {
            // Enable the select elements for module-related queries
            const moduleId = moduleIdSelectorEle.value;
            stateManager.saveObject( "TargetModule", moduleId );
            await updateConfigureModuleElements();
            updateTargetSectionHighlighting();
            updateTargetModule();
            updateWatermarks();
        }
    }

    async function updateConfigureModuleElements()
    {
        // Get the target module
        const targetModuleId = stateManager.getSavedObject( "TargetModule" );

        // Update the Configure Module information element to show the ID of the module that has been selected
        const configureModuleHeaderInfoEle = shadowDomHelper.getElementById( "configure-module-info");
        configureModuleHeaderInfoEle.textContent = "Configure Module " + targetModuleId;

        // Update the Target Module information element to show the ID of the module that has been selected
        const targetModuleHeaderInfoEle = shadowDomHelper.getElementById( "target-module-info");
        targetModuleHeaderInfoEle.textContent = "Module " + targetModuleId;
        moduleIdSelectorEle.value = targetModuleId;

        // Fetch the module information from the backend server
        await loadModuleInformation( targetModuleId );

        // Enable the module command buttons, for example Module Reset
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".module-command", true );
    }

    function updateTargetModule()
    {
        const sectionInfosArray = stateManager.getSavedObject( "SectionInfos" );
        const targetModuleId = stateManager.getSavedObject( "TargetModule" );
        const previousFacilityState = stateManager.getSavedObject( "FacilityState" );
        const sectionStatesArrayObject = previousFacilityState[ "sectionStates" ];
        const targetModuleContainerEle = shadowDomHelper.getElementById( "target-module-container" );
        MODULE_SUPPORT.updateTargetModule( targetModuleContainerEle, sectionStatesArrayObject, sectionInfosArray, targetModuleId );
    }

    function updateWatermarks()
    {
        const targetSectionDefined = stateManager.hasSavedObject( "TargetSection" );
        const targetSectionWatermarkEle = shadowDomHelper.getElementById( "target-section-watermark" );
        targetSectionWatermarkEle.style.display = targetSectionDefined ? "none" : "block";
        const targetModuleDefined = stateManager.hasSavedObject( "TargetModule" );
        const targetModuleWatermarkEle1 = shadowDomHelper.getElementById( "target-module-watermark-1");
        targetModuleWatermarkEle1.style.display = ( targetModuleDefined || targetSectionDefined ) ? "none" : "block";
        const targetModuleWatermarkEle2 = shadowDomHelper.getElementById("target-module-watermark-2");
        targetModuleWatermarkEle2.style.display = ( targetSectionDefined && !targetModuleDefined ) ? "block" : "none";
    }

    /*-----------------------------------------------------------------------*/
    /* 10.0 SIGNAL HANDLING EVENT SUPPORT FUNCTIONS                          */
    /*-----------------------------------------------------------------------*/

    async function handleIlKSignalClicked( event )
    {
        switch ( event.type )
        {
            case "ev-ilk-signal-bridge-clicked":
                return handleIlkSignalBridgeCellClicked( event );
            case "ev-ilk-signal-interlock-clicked":
                return handleIlkSignalInterlockCellClicked( event );
            case "ev-ilk-signal-link-clicked":
                return handleIlkSignalLinkCellClicked( event );
        }
    }

    async function handleIlkSignalBridgeCellClicked( event )
    {
        const moduleId = event.detail.moduleId;
        const portId = event.detail.portId;
        const ioType = event.detail.ioType;
        const currentBridgeState = event.detail.bridgeState;
        const facilityId = "__FACILITY__".toLowerCase();
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + "/" +  ioType + "s/" + portId + "/bridges";
        switch ( currentBridgeState )
        {
            case "rpm_clr":
                return HTTP_SUPPORT.doPutRequest( requestUrl, "SET_BRIDGE", requestEle, responseEle, errorEle );

            case "rpm_set":
                return HTTP_SUPPORT.doPutRequest( requestUrl, "CLEAR_BRIDGE", requestEle, responseEle, errorEle );

            case "usi2_clr":
                return HTTP_SUPPORT.doPutRequest( requestUrl, "SET_OK_BRIDGE", requestEle, responseEle, errorEle );

            case "usi2_ok":
                // Note: 2023-04-12 USI2 NOK bridging has been disabled from the user interface until it is
                // provided via a context-sensitive menu.
                // HTTP_SUPPORT.doPutRequest( requestUrl, "SET_NOK_BRIDGE", requestEle, responseEle, errorEle );
                console.warn( "USI2 NOK bridging has been disabled => USI2 OK bridge will be cleared." );
                return HTTP_SUPPORT.doPutRequest( requestUrl, "CLEAR_BRIDGE", requestEle, responseEle, errorEle );

            case "usi2_nok":
                return HTTP_SUPPORT.doPutRequest( requestUrl, "CLEAR_BRIDGE", requestEle, responseEle, errorEle );

            default:
                console.error( "Unexpected bridge state" );
                break;
        }
    }

    async function handleIlkSignalInterlockCellClicked( event )
    {
        const moduleId = event.detail.moduleId;
        const facilityId = "__FACILITY__".toLowerCase();
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + "/interlocks";
        return HTTP_SUPPORT.doPutRequest( requestUrl, "CLEAR", requestEle, responseEle, errorEle );
    }

    async function handleIlkSignalLinkCellClicked( event )
    {
        const moduleId = event.detail.connectionInfo[ "moduleId" ];
        stateManager.saveObject( "TargetModule", moduleId );
        await updateConfigureModuleElements( moduleId );
        updateTargetModule();
    }

    /*-----------------------------------------------------------------------*/
    /* 11.0 SVG SUPPORT FUNCTIONS                                            */
    /*-----------------------------------------------------------------------*/

    function svgInit()
    {
        const svgFacilityElement = shadowDomHelper.getElementById( "svgFacilityElement" );
        const svgLegendElement = shadowDomHelper.getElementById( "svgLegendElement" );
        const svgFacilityDoc = svgFacilityElement.contentDocument;
        const svgLegendDoc = svgLegendElement.contentDocument;

        registerSvgSectionClickHandlers( svgFacilityDoc );

        const propertyManager = createSvgPropertyManager( svgLegendDoc );
        stateManager.saveObject( "SvgStateManager", new SvgStateManager( svgFacilityDoc, propertyManager ) );
        stateManager.saveObject( "SvgContentManager", new SvgContentManager( svgFacilityDoc ) );
}

    function updateSvgState( sectionStateObject, svgStateManager )
    {
        // Attempt to find an SVG element whose ID corresponds to the name of the section to update.
        const sectionId = sectionStateObject[ "sectionId" ];

        // Get all the data from the transfer object
        const sectionInterlockState = sectionStateObject[ "sectionInterlockState" ];
        const sectionConfigurationState = sectionStateObject?.[ "sectionMode" ]?.[ "sectionModeDescriptions" ]?.[ "primary" ]?.split( ":" )?.[ 1 ] ?? "UNKNOWN";
        const sectionInService = sectionStateObject[ "sectionInService" ];
        const sectionBeamStopperState = sectionStateObject[ "sectionBeamStopperState" ];

        // Update the graphic
        svgStateManager.setBeamlineSectionInterlockState( sectionId, sectionInterlockState );
        svgStateManager.setBeamlineSectionConfigurationState( sectionId, sectionConfigurationState );
        svgStateManager.setBeamlineSectionStopperState( sectionId, sectionBeamStopperState );
        svgStateManager.setBeamlineSectionServiceState( sectionId, sectionInService );
    }

    function registerSvgSectionClickHandlers( svgDocument )
    {
        const BEAMLINE_SECTION_CLASS_IDENTIFIER = "BEAMLINE_SECTION";
        const sectionParentElements = svgDocument.getElementsByClassName( BEAMLINE_SECTION_CLASS_IDENTIFIER );
        Array.from( sectionParentElements ).forEach( (section) => {
            section.addEventListener( "mouseover", (ev) => handleTargetSectionMouseSelect( ev, section ) );
            section.addEventListener( "click", (ev) => handleTargetSectionMouseSelect( ev, section ) );
        });
    }

    function createSvgPropertyManager( svgLegendDoc )
    {
        // Get the colors for the different possible states of the beamline elements directly from the
        // relevant properties on the legend
        const goColor = HTML_ELEMENT_SUPPORT.getSvgElementProperty( svgLegendDoc, "legend-graphic-go", "fill" );
        const stopColor = HTML_ELEMENT_SUPPORT.getSvgElementProperty( svgLegendDoc, "legend-graphic-stop", "fill" );
        const disabledColor = HTML_ELEMENT_SUPPORT.getSvgElementProperty( svgLegendDoc, "legend-graphic-disabled", "fill" );
        const inconsistentColor = HTML_ELEMENT_SUPPORT.getSvgElementProperty( svgLegendDoc, "legend-graphic-inconsistent", "fill" );
        const rebootColor = HTML_ELEMENT_SUPPORT.getSvgElementProperty( svgLegendDoc, "legend-graphic-reboot", "fill" );
        const offlineColor = HTML_ELEMENT_SUPPORT.getSvgElementProperty( svgLegendDoc, "legend-graphic-offline", "fill" );
        const hiddenColor = HTML_ELEMENT_SUPPORT.getSvgElementProperty( svgLegendDoc, "legend-graphic-hidden", "fill" );
        const housingColor = "darkgray";
        const inServiceOpacity = 1.0;
        const outOfServiceOpacity = 0.3;
        return new ConfigurationViewPropertyManager( goColor, stopColor, disabledColor, inconsistentColor, rebootColor, offlineColor, hiddenColor, housingColor, inServiceOpacity, outOfServiceOpacity );
    }


    /*-----------------------------------------------------------------------*/
    /* 12.0 HTML ELEMENT EVENT LISTENER SUPPORT                              */
    /*-----------------------------------------------------------------------*/

    const handlerMap = new Map();

    function registerHtmlEventHandlers()
    {
        handlerMap.set( "handleIlKSignalClicked", (event) => handleIlKSignalClicked( event ) );
        handlerMap.set( "handleTargetSectionWidgetSelect", (event) => handleTargetSectionWidgetSelect( event ) );
        handlerMap.set( "handleTargetModuleWidgetSelect", (event) => handleTargetModuleWidgetSelect( event ) );
        handlerMap.set( "handleTargetModuleMouseSelect", (event) => handleTargetModuleMouseSelect( event ) );
        handlerMap.set( "handleExpertModuleMouseSelect", (event) => handleExpertModuleMouseSelect( event ) );
        handlerMap.set( "setUserSignalPreferences", (event) => setUserSignalPreferences( event ) );
        handlerMap.set( "setUserOtherPreferences", (event) => setUserOtherPreferences( event ) );
        handlerMap.set( "putFacility", (event) => putFacility( event ) );
        handlerMap.set( "putFacilityMode", (event) => putFacilityMode( event ) );
        handlerMap.set( "putFacilitySection", (event) => putFacilitySection( event ) );
        handlerMap.set( "putFacilitySectionMode", (event) => putFacilitySectionMode( event ) );
        handlerMap.set( "putFacilityModule", (event) => putFacilityModule( event ) );
        handlerMap.set( "putFacilityModuleMode", (event) => putFacilityModuleMode( event ) );
    }

    function addHtmlElementEventListeners( elementClass, eventName )
    {
        // Find all elements of the specified class
        const allElements = shadowDomHelper.querySelectorAll( "." + elementClass );

        for ( const ele of allElements )
        {
            // If the data-func attribute is present attempt to find a function with
            // the corresponding name and register it as an event handler
            if (  ele.dataset[ "func" ] !== undefined )
            {
                const funcName = ele.dataset[ "func" ];

                // If a function of the required name has been registered in the
                // handler map attach the handler function as an event listener
                if ( handlerMap.has( funcName ) )
                {
                    const func = handlerMap.get( funcName );
                    ele.addEventListener( eventName, func );
                }
                else
                {
                    console.error( "The function named '" + funcName + "' is not defined !" );
                }
            }
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 13.0 HTML SELECT ELEMENT MENU CHOICES SUPPORT                         */
    /*-----------------------------------------------------------------------*/

    async function addGroupedSelectFacilityModeMenuChoices( selectWidget, facilityId, path )
    {
        try {
            const facilityData = await getFacilityData( facilityId, path );
                // Example returned data:
                //    {"Main-Path":["BW2","BN5"]}
                //    {"AHK1-Path":["Target","Inactive","Not-in-use"]}
                //    {"VAR-Path":["Beam Allow","Beam Stop","Beam Left","Beam Right","Beam Split","Inactive"]}
                const optionArrayObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
                HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionArrayObj, selectWidget, "Please select" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectFacilityModeOptionMenuChoices( selectWidget, facilityId, path )
    {
        try {
            const facilityData = await getFacilityData( facilityId, path );
            // Example returned data:
            //    {"DIA Mode":["DIA-Off","DIA-On"],"Intensity":["Normal","High"],"MCH Mode":["MCH-Off","MCH-On"]}
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionArrayObj, selectWidget, "Please select"  );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectSectionIdMenuChoices( selectWidget, facilityId, path )
    {
        try {
            const facilityData = await getFacilityData( facilityId, path );
            // Example returned data:
            //    {"BEAM":["BW2","BN5","AHK1"],"TEST":["VAR"]}
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromSectionId( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionArrayObj, selectWidget, "Please select", "Group" )
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectSectionModeMenuChoices( selectWidget, facilityId, path )
    {
        try {
            const facilityData = await getFacilityData( facilityId, path );
            // Example returned data:
            //    {"Mode":["O (Inactive)","D (Durch)"]}
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionArrayObj, selectWidget, "Please select" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectSectionModeOptionMenuChoices( selectWidget, facilityId, path )
    {
        try {
            const facilityData = await getFacilityData( facilityId, path );
            // Example returned data:
            //    {"DIA Mode":["DIA-Off","DIA-On"],"Intensity":["Normal","High"],"MCH Mode":["MCH-Off","MCH-On"]}
            const optionObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionObj, selectWidget, "Please select" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectModuleIdMenuChoices( selectWidget, facilityId, path )
    {
        try {
            const facilityData = await getFacilityData( facilityId, path );
            // Example returned data:
            //    ["002004","002006","002037","002097","3001001" ]
            const optionObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModuleId( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionObj, selectWidget, "Please select" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectModuleModeMenuChoices( selectWidget, facilityId, path )
    {
        try {
            const facilityData = await getFacilityData( facilityId, path );
            // Example returned data:
            //    {"MODE":["SF-Inactive","SF-Stop","SF-Unknown-Mode2","SF-Open"]}
            const optionObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionObj, selectWidget, "Please select" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectModuleModeOptionMenuChoices( selectWidget, facilityId, path )
    {
        try {
            const facilityData = await getFacilityData( facilityId, path );
            // Example returned data:
            //    {"BT":["Off","On"],"DIA":["Off","On"],"KNT":["Off","On"],"MCH":["Off","On"],"MR":["Off","On"]}
            const optionObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionObj, selectWidget,"Please select" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 14.0 PAGE INITIALISATION                                              */
    /*-----------------------------------------------------------------------*/

    function startStreamListener()
    {
        console.log( "Starting stream listener" );
        document.addEventListener( "ev-stream-facility-state-changed", ev => {
            const facilityState =  ev.detail.facilityState;
            const currentUserSession =  ev.detail.currentUserSession;
            handleFacilityStateChanged( facilityState, currentUserSession );
        } )
        document.addEventListener( "ev-stream-state-connected", () => handleStreamConnected() );
        document.addEventListener( "ev-stream-state-disconnected", () => handleStreamDisconnected() );
    }

    async function init()
    {
        function initialiseStateManager()
        {
            const UNKNOWN_FACILITY_STATE = {};
            stateManager.saveObject( "FacilityState", UNKNOWN_FACILITY_STATE );
        }

        function initialiseButtonHandling()
        {
            // Disable all buttons
            HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".request-button", false );

            // Set up the QUAD button reflector to enable the targeted button if any of the select widgets has an active selection.
            const anySelectedPredicate = (a,b,c,d)  => (a || b || c || d )
            HTML_ELEMENT_SUPPORT.registerHtmlSelectElementQuadButtonStateReflector( shadowRoot, facilityPrimaryModeSelectorEle, facilitySecondaryModeSelectorEle,
                facilityTertiaryModeSelectorEle, facilityModeOptionSelectorEle, ".facility-config.request-button", anySelectedPredicate );

            // Set up the PAIR button reflector to enable the targeted button if any of the select widgets has an active selection.
            const eitherSelectedPredicate = (a,b) => a || b;
            HTML_ELEMENT_SUPPORT.registerHtmlSelectElementPairButtonStateReflector( shadowRoot, sectionModeSelectorEle, sectionModeOptionSelectorEle, ".section-config.request-button", eitherSelectedPredicate );
            HTML_ELEMENT_SUPPORT.registerHtmlSelectElementPairButtonStateReflector( shadowRoot, moduleModeSelectorEle, moduleModeOptionSelectorEle, ".module-config.request-button", eitherSelectedPredicate );

            // Enable facility command buttons
            HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".facility-command", true );
        }

        initialiseStateManager()
        initialiseButtonHandling();
        registerHtmlEventHandlers();

        // Bind events that occur on specific HTML classes to their respective handler
        addHtmlElementEventListeners( "select", "change" );
        addHtmlElementEventListeners( "request-button", "click" );
        addHtmlElementEventListeners( "checkbox-setting", "change" );
        addHtmlElementEventListeners( "ilk-signal-handler", "ev-ilk-signal-bridge-clicked" );
        addHtmlElementEventListeners( "ilk-signal-handler", "ev-ilk-signal-interlock-clicked" );
        addHtmlElementEventListeners( "ilk-signal-handler", "ev-ilk-signal-link-clicked" );
        addHtmlElementEventListeners( "module-select-handler", "ev-ilk-module-mouseover" );
        addHtmlElementEventListeners( "module-select-handler", "ev-ilk-module-click" );
        addHtmlElementEventListeners( "expert-module-select-handler", "ev-ilk-module-click" );

        // Initialise the SVG Graphic.
        // Note: This needs to come before listening to the stream.
        console.log( "Waiting for SVG initialise..." )
        await SVG_SUPPORT.svgInitDetector( shadowDomHelper );
        svgInit();
        console.log( "SVG initialised" )

        // Start listening to the stream from the ILKCS server
        startStreamListener();

        // Load Facility configuration information from the remote server and use it set up this page
        console.log( "Waiting for load facility information..." )
        await loadFacilityInformation( facilityId );
        console.log( "Facility information loaded." )

        // Initialise Watermarks
        updateWatermarks();

        // Listen and respond to application wide events that set the target section or target module
        document.addEventListener( "ev-doc-target-section-selected", (ev) => handleTargetSectionEventSelect( ev ) );
        document.addEventListener( "ev-doc-target-module-selected", (ev) => handleTargetModuleEventSelect( ev ) );
    }

    await init();

console.log( "configuration-view.js completed." )


