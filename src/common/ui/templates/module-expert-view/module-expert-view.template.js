console.log( "module-expert-view.js is running...")

    /*-----------------------------------------------------------------------*/
    /* 1.0 MODULE IMPORT DEFINITIONS                                         */
    /*-----------------------------------------------------------------------*/

    // noinspection NpmUsedModulesInstalled
    import {ShadowDomHelper} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {StateManager} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SIGNAL_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SESSION_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {HTML_ELEMENT_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {HTTP_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {MODULE_SUPPORT} from "__JS_SUPPORT_FILE__";

    /*-----------------------------------------------------------------------*/
    /* 2.0 VARIABLE DEFINITIONS                                              */
    /*-----------------------------------------------------------------------*/

    const facilityId = "__FACILITY__".toLowerCase();
    const serverUrl = window.location.origin + "__ILKCS_SERVER_PATH_PREFIX__";

    const shadowDomHelper = new ShadowDomHelper( "module-expert-panel" );
    const shadowRoot = shadowDomHelper.getShadowRoot();
    const stateManager = new StateManager();

    const module1IdSelectorEle = shadowDomHelper.getElementById( 'module1-select' );
    const module1PrimaryModeSelectorEle = shadowDomHelper.getElementById( 'module1-mode-select-1' );
    const module1OtherModeSelectorEle = shadowDomHelper.getElementById( 'module1-mode-select-2' );

    const module2IdSelectorEle = shadowDomHelper.getElementById( 'module2-select' );
    const module2PrimaryModeSelectorEle = shadowDomHelper.getElementById( 'module2-mode-select-1' );
    const module2OtherModeSelectorEle = shadowDomHelper.getElementById( 'module2-mode-select-2' );

    const expertModeEle = shadowDomHelper.getElementById( 'expert-mode-widget' );
    const showSignalRegisterBitsEle = shadowDomHelper.getElementById( 'show-signal-register-bits-widget' );
    const reversedSignalDirectionEle = shadowDomHelper.getElementById( 'reversed-signal-direction-widget' );
    const showCommandLogEle = shadowDomHelper.getElementById( 'show-command-log-widget' );

    const requestEle = shadowDomHelper.getElementById( 'http-request' );
    const responseEle = shadowDomHelper.getElementById( 'http-response' );
    const errorEle = shadowDomHelper.getElementById( 'http-error' );


    /*-----------------------------------------------------------------------*/
    /* 3.0 PREFERENCES PANEL SUPPORT FUNCTIONS                               */
    /*-----------------------------------------------------------------------*/

    // Set User Profile
    function setUserSignalPreferences()
    {
        const expertMode = expertModeEle.checked;
        const reversedSignalDirection = reversedSignalDirectionEle.checked;
        const showSignalRegisterBits = showSignalRegisterBitsEle.checked;
        const searchStartEle = shadowDomHelper.getShadowRoot();
        SIGNAL_SUPPORT.setSignalUserPreferences( searchStartEle, expertMode, reversedSignalDirection, showSignalRegisterBits );
        MODULE_SUPPORT.setSignalUserPreferences( searchStartEle, expertMode, reversedSignalDirection, showSignalRegisterBits );
    }

    function setUserOtherPreferences()
    {
        const showCommandLog = showCommandLogEle.checked;
        const gridContainerEle = shadowDomHelper.getElementByClassName( "grid-container" );
        if ( showCommandLog )
        {
            gridContainerEle.classList.add( "cmdlog" );
        }
        else
        {
            gridContainerEle.classList.remove( "cmdlog" );
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 4.0 ILKCS SERVER GET / PUT DATA SUPPORT FUNCTIONS                     */
    /*-----------------------------------------------------------------------*/

    async function getFacilityData( facility, path )
    {
        const requestUrl = serverUrl + "/v3/facilities/" + facility + path;
        return HTTP_SUPPORT.doGetRequest( requestUrl );
    }

    async function putFacilityModule( event )
    {
        const buttonSelectorEle = event.target;
        const inst = buttonSelectorEle.id.match(/\d+/)[ 0 ];
        const moduleId = stateManager.getSavedObject( "TargetModule" + inst );
        const putValue = event.target.dataset.putValue;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + event.target.dataset.path;
        return HTTP_SUPPORT.doPutRequest( requestUrl, putValue, requestEle, responseEle, errorEle );
    }

    async function putFacilityModuleMode( event )
    {
        const configSelectorEle = event.target;
        const inst = configSelectorEle.id.match(/\d+/)[ 0 ];

        const moduleId = stateManager.getSavedObject( "TargetModule" + inst );
        const selectorEle1Id = "module" + inst + "-mode-select-1";
        const selectorEle1 = shadowDomHelper.getElementById( selectorEle1Id );
        const selectedIndex1 = selectorEle1.selectedIndex;
        const selectedOption1 = selectorEle1.options[ selectedIndex1 ];
        const settingRequest1 =  ( selectedIndex1 > 0 ) ? selectedOption1.parentNode.id + ":" + selectedOption1.value : "";

        const selectorEle2Id = "module" + inst + "-mode-select-2";
        const selectorEle2 = shadowDomHelper.getElementById( selectorEle2Id );
        const selectedIndex2 = selectorEle2.selectedIndex;
        const selectedOption2 = selectorEle2.options[ selectedIndex2 ];
        const settingRequest2 =  ( selectedIndex2 > 0 ) ?  selectedOption2.parentNode.id + ":" + selectedOption2.value : "";

        const separator = ( settingRequest1 === "" ) || ( settingRequest2 === "" ) ? "" : ";";
        const requestedMode = settingRequest1 + separator + settingRequest2;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + "/mode";
        return HTTP_SUPPORT.doPutRequest( requestUrl, requestedMode, requestEle, responseEle, errorEle );
    }

    /*-----------------------------------------------------------------------*/
    /* 5.0 STREAM HANDLING SUPPORT FUNCTIONS                                 */
    /*-----------------------------------------------------------------------*/

    function handleStreamConnected()
    {
        updateModuleContainerConnectionState( true );
    }

    function handleStreamDisconnected()
    {
        updateModuleContainerConnectionState( false );
        const UNKNOWN_FACILITY_STATE = {};
        stateManager.saveObject("FacilityState", UNKNOWN_FACILITY_STATE);
    }

    function updateModuleContainerConnectionState( connected )
    {
        const moduleContainerEles = shadowDomHelper.querySelectorAll( "ilk-module-container" );
        moduleContainerEles.forEach(  (cont) => {
            console.log( "setting module container with ID: '" + cont.id + "' to connection state: " + connected + "'." );
            cont.connected = connected;
        } );
    }

    /*-----------------------------------------------------------------------*/
    /* 6.0 CONFIGURE FACILITY PANEL SUPPORT FUNCTIONS                        */
    /*-----------------------------------------------------------------------*/

    async function loadFacilityInformation( facilityId )
    {
        await addGroupedSelectModuleIdChoices( module1IdSelectorEle, facilityId, "?detail=moduleIds" );
        await addGroupedSelectModuleIdChoices( module2IdSelectorEle, facilityId, "?detail=moduleIds" );

        try {
            const facilityData = await getFacilityData(facilityId, "/sections?detail=info")
            stateManager.saveObject("SectionInfos", facilityData);
        }
        catch( error ) {
            console.log( error );
        }
    }

    function handleFacilityStateChanged( facilityState, currentUserSession )
    {
        function updateCurrentUserPrivileges( currentUserSession )
        {
            // Update signal bridge setting privileges
            const userPrivileges = SESSION_SUPPORT.getCurrentUserPrivileges( currentUserSession );
            const searchStartEle = shadowDomHelper.getShadowRoot();
            SIGNAL_SUPPORT.setSignalUserPrivileges( searchStartEle, userPrivileges );
            MODULE_SUPPORT.setSignalUserPrivileges( searchStartEle, userPrivileges );
        }

        // Update Facility State
        stateManager.saveObject( "FacilityState", facilityState );

        // Update Target Module when it has already been selected
        if ( stateManager.hasSavedObject( "TargetModule1" ) ) {
            updateTargetModule( 1 );
        }

        // Update Target Module when it has already been selected
        if ( stateManager.hasSavedObject( "TargetModule2" ) ) {
            updateTargetModule( 2 );
        }

        // Update Signal and Module Privileges
        updateCurrentUserPrivileges( currentUserSession );
    }


    /*-----------------------------------------------------------------------*/
    /* 7.0 CONFIGURE MODULE SUPPORT FUNCTIONS                                */
    /*-----------------------------------------------------------------------*/

    async function loadModuleInformation( moduleId, modulePrimaryModeSelectorEle, moduleOtherModeSelectorEle )
    {
        // Enable the select elements for module-related queries
        await addGroupedSelectModuleModeChoices( modulePrimaryModeSelectorEle, facilityId, "/modules/" + moduleId + "/mode?detail=choices&filter=primary" );
        await addGroupedSelectModuleModeChoices( moduleOtherModeSelectorEle, facilityId, "/modules/" + moduleId + "/mode?detail=choices&filter=other" );
    }

    // Handles a change of module when a module is selected by means of a received event
    async function handleExpertModuleEventSelect( event )
    {
        const moduleId = event.detail.moduleId;
        const sectionInfosArray = stateManager.getSavedObject( "SectionInfos" );
        if ( ! MODULE_SUPPORT.containsModuleId( moduleId, sectionInfosArray ) ) {
            console.warn( "The moduleId: '" + moduleId + "' was invalid.");
            return;
        }

        if ( stateManager.hasSavedObject( "TargetModule" + 1 ) )
        {
            const moduleId = stateManager.getSavedObject( "TargetModule" + 1 );
            stateManager.saveObject( "TargetModule" + 2, moduleId );
            await updateTargetModuleIndicators( 2 );
            updateTargetModule( 2 );
        }

        stateManager.saveObject( "TargetModule" + 1, moduleId );
        await updateTargetModuleIndicators( 1 );
        updateTargetModule( 1 );
    }

    // Handles a change of module when a module is manually selected from the Select Module widget
    async function handleTargetModuleWidgetSelect( event )
    {
        const moduleIdSelectorEle = event.target;
        const moduleId = moduleIdSelectorEle.value;
        const moduleIdSelectorEleId = moduleIdSelectorEle.id;
        const inst = moduleIdSelectorEleId.match(/\d+/)[ 0 ];

        // Clear the section-dependant select elements. This is required to clear the options if the
        // selection changes to select nothing.
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( "module" + inst + "PrimaryModePropertySelectorEle" );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( "module" + inst + "OtherModePropertySelectorEle" );

        // Clear the section-dependant button elements
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowDomHelper.getShadowRoot(), ".module-config.request-button", false );

        // If a new module is selected...
        if ( event.target.selectedIndex !== 0)
        {
            // Enable the select elements for module-related queries
            stateManager.saveObject( "TargetModule" + inst, moduleId );
            await updateTargetModuleIndicators( inst );
            updateTargetModule( inst );
        }
    }

    async function updateTargetModuleIndicators( inst )
    {
        // Get the target module
        const targetModuleId = stateManager.getSavedObject( "TargetModule" + inst );

        // Update the Configure Module information element to show the ID of the module that has been selected
        const configureModuleHeaderInfoEle = shadowDomHelper.getElementById( "configure-module" + inst + "-info" );
        configureModuleHeaderInfoEle.textContent = "Configure Module " + targetModuleId;

        // Update the Target Module information element to show the ID of the module that has been selected
        const targetModuleHeaderInfoEle = shadowDomHelper.getElementById( "target-module" + inst + "-info");
        targetModuleHeaderInfoEle.textContent = "Module " + targetModuleId;
        const moduleIdSelectorEle = shadowDomHelper.getElementById( "module" + inst + "-select" );
        moduleIdSelectorEle.value = targetModuleId;

        // Fetch the module information from the backend server
        if ( inst === 1 ) {
            await loadModuleInformation( targetModuleId, module1PrimaryModeSelectorEle, module1OtherModeSelectorEle );
        }

        if ( inst === 2 ) {
            await loadModuleInformation( targetModuleId, module2PrimaryModeSelectorEle, module2OtherModeSelectorEle );
        }

        // Enable the module command button eg Clear Interlocks
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowDomHelper.getShadowRoot(), ".module-command", true );
    }

    function updateTargetModule( inst )
    {
        const sectionInfosArray = stateManager.getSavedObject( "SectionInfos" );
        const targetModuleId = stateManager.getSavedObject( "TargetModule" + inst );
        const previousFacilityState = stateManager.getSavedObject( "FacilityState" );
        const sectionStatesArrayObject = previousFacilityState[ "sectionStates" ];
        const targetModuleContainerEle = shadowDomHelper.getElementById( "target-module" + inst + "-container" );
        MODULE_SUPPORT.updateTargetModule( targetModuleContainerEle, sectionStatesArrayObject, sectionInfosArray, targetModuleId );
    }

    /*-----------------------------------------------------------------------*/
    /* 8.0 SIGNAL HANDLING SUPPORT FUNCTIONS                                 */
    /*-----------------------------------------------------------------------*/

    async function handleIlKSignalClicked( event, instance )
    {
        switch ( event.type )
        {
            case "ev-ilk-signal-bridge-clicked":
                return handleIlkSignalBridgeCellClicked( event );
            case "ev-ilk-signal-interlock-clicked":
                return handleIlkSignalInterlockCellClicked( event );
            case "ev-ilk-signal-link-clicked":
                return handleIlkSignalLinkCellClicked( event, instance );
        }
    }

    async function handleIlkSignalBridgeCellClicked( event )
    {
        const moduleId = event.detail.moduleId;
        const portId = event.detail.portId;
        const ioType = event.detail.ioType;
        const currentBridgeState = event.detail.bridgeState;
        const facilityId = "__FACILITY__".toLowerCase();
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + "/" +  ioType + "s/" + portId + "/bridges";
        switch ( currentBridgeState )
        {
            case "rpm_clr":
                return HTTP_SUPPORT.doPutRequest( requestUrl, "SET_BRIDGE", requestEle, responseEle, errorEle );
            case "rpm_set":
                return HTTP_SUPPORT.doPutRequest( requestUrl, "CLEAR_BRIDGE", requestEle, responseEle, errorEle );
            case "usi2_clr":
                return HTTP_SUPPORT.doPutRequest( requestUrl, "SET_OK_BRIDGE", requestEle, responseEle, errorEle );
            case "usi2_ok":
                // Note: 2023-04-12 USI2 NOK bridging has been disabled from the user interface until it is
                // provided via a context-sensitive menu.
                // HTTP_SUPPORT.doPutRequest( requestUrl, "SET_NOK_BRIDGE", requestEle, responseEle, errorEle );
                console.warn( "USI2 NOK bridging has been disabled => USI2 OK bridge will be cleared." );
                return HTTP_SUPPORT.doPutRequest( requestUrl, "CLEAR_BRIDGE", requestEle, responseEle, errorEle );
            case "usi2_nok":
                return HTTP_SUPPORT.doPutRequest( requestUrl, "CLEAR_BRIDGE", requestEle, responseEle, errorEle );
            default:
                console.error( "Unexpected bridge state" );
                break;
        }
    }

    async function handleIlkSignalInterlockCellClicked( event )
    {
        const moduleId = event.detail.moduleId;
        const facilityId = "__FACILITY__".toLowerCase();
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + "/interlocks";
        return HTTP_SUPPORT.doPutRequest( requestUrl, "CLEAR", requestEle, responseEle, errorEle );
    }

    async function handleIlkSignalLinkCellClicked( event,instance )
    {
        const sectionId = event.detail.connectionInfo[ "sectionId" ];
        const moduleId = event.detail.connectionInfo[ "moduleId" ];
        const portId = event.detail.connectionInfo[ "portId" ];
        const type = event.detail.connectionInfo[ "type" ];
        console.log( "Connection info is section: '" + sectionId + "', moduleId: '" + moduleId + "', portId: '" + portId + "', type:'" + type + "'" );

        if ( ( instance === 1 ) && (type === "from" ) ) {
            stateManager.saveObject( "TargetModule" + 2, moduleId );
            await updateTargetModuleIndicators( 2 );
            updateTargetModule( 2 );
        }

        if ( ( instance === 1 ) && (type === "to" ) ) {
            const oldModuleId = stateManager.getSavedObject( "TargetModule" + 1, moduleId );
            stateManager.saveObject( "TargetModule" + 2, oldModuleId );
            await updateTargetModuleIndicators( 2 );
            updateTargetModule( 2 );
            stateManager.saveObject( "TargetModule" + 1, moduleId );
            await updateTargetModuleIndicators( 1 );
            updateTargetModule( 1 );
        }

        if ( ( instance === 2 ) && (type === "to" ) ) {
            stateManager.saveObject( "TargetModule" + 1, moduleId );
            await updateTargetModuleIndicators( 1 );
            updateTargetModule( 1 );
        }

        if ( ( instance === 2 ) && (type === "from" ) ) {
            const oldModuleId = stateManager.getSavedObject( "TargetModule" + 2, moduleId );
            stateManager.saveObject( "TargetModule" + 1, oldModuleId );
            await updateTargetModuleIndicators( 1 );
            updateTargetModule( 1 );
            stateManager.saveObject( "TargetModule" + 2, moduleId );
            await updateTargetModuleIndicators( 2 );
            updateTargetModule( 2 );
        }
    }


    /*-----------------------------------------------------------------------*/
    /* 9.0 HTML ELEMENT EVENT LISTENER SUPPORT                               */
    /*-----------------------------------------------------------------------*/

    const handlerMap = new Map();

    function registerHtmlEventHandlers()
    {
        handlerMap.set( "handleIlKSignalClicked1", (event) => handleIlKSignalClicked( event, 1 ) );
        handlerMap.set( "handleIlKSignalClicked2", (event) => handleIlKSignalClicked( event, 2 ) );
        handlerMap.set( "handleTargetModuleWidgetSelect", (event) => handleTargetModuleWidgetSelect( event ) );
        handlerMap.set( "setUserSignalPreferences", (event) => setUserSignalPreferences( event ) );
        handlerMap.set( "setUserOtherPreferences", (event) => setUserOtherPreferences( event ) );
        handlerMap.set( "putFacilityModule", (event) => putFacilityModule( event ) );
        handlerMap.set( "putFacilityModuleMode", (event) => putFacilityModuleMode( event ) );
    }

    function addHtmlElementEventListeners( elementClass, eventName )
    {
        // Find all elements of the specified class
        const allElements = shadowDomHelper.querySelectorAll( "." + elementClass );

        for ( const ele of allElements )
        {
            // If the data-func attribute is present attempt to find a function with
            // the corresponding name and register it as an event handler
            if (  ele.dataset[ "func" ] !== undefined )
            {
                const funcName = ele.dataset[ "func" ];

                // If a function of the required name has been registered in the
                // handler map attach the handler function as an event listener
                if ( handlerMap.has( funcName ) )
                {
                    const func = handlerMap.get( funcName );
                    ele.addEventListener( eventName, func );
                }
                else
                {
                    console.error( "The function named '" + funcName + "' is not defined !" );
                }
            }
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 10.0 HTML SELECT ELEMENT MENU CHOICES SUPPORT                         */
    /*-----------------------------------------------------------------------*/

    async function addGroupedSelectModuleIdChoices( selectWidget, facilityId, path )
    {
        try {
            // Example returned data:
            //    ["002004","002006","002037","002097","3001001" ]
            const facilityData = await getFacilityData( facilityId, path );
            const optionObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModuleId( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionObj, selectWidget, "Please select" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectModuleModeChoices( selectWidget, facilityId, path )
    {
        try {
            // Example returned data:
            //    {"MODE":["SF-Inactive","SF-Stop","SF-Unknown-Mode2","SF-Open"]}
            const facilityData = await getFacilityData( facilityId, path );
            const optionObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionObj, selectWidget, "Please select" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 11.0 PAGE INITIALISATION                                              */
    /*-----------------------------------------------------------------------*/

    function startStreamListener()
    {
        console.log( "Starting stream listener" );
        document.addEventListener( "ev-stream-facility-state-changed", ev => {
            const facilityState =  ev.detail.facilityState;
            const currentUserSession =  ev.detail.currentUserSession;
            handleFacilityStateChanged( facilityState, currentUserSession );
        } )
        document.addEventListener( "ev-stream-state-connected", () => handleStreamConnected() );
        document.addEventListener( "ev-stream-state-disconnected", () => handleStreamDisconnected() );
    }

    async function init()
    {
        // Initialise the State Manager
        const UNKNOWN_FACILITY_STATE = { "interlockHistory" : [],"sectionStates"  : [] };
        stateManager.saveObject( "FacilityState", UNKNOWN_FACILITY_STATE );

        // Disable all buttons
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".request-button", false );

        // Set up the PAIR button reflector to enable the targeted button if any of the select widgets has an active selection.
        const eitherSelectedPredicate = (a,b) => a || b;
        HTML_ELEMENT_SUPPORT.registerHtmlSelectElementPairButtonStateReflector( shadowRoot, module1PrimaryModeSelectorEle,  module1OtherModeSelectorEle, ".module-config.request-button", eitherSelectedPredicate );
        HTML_ELEMENT_SUPPORT.registerHtmlSelectElementPairButtonStateReflector( shadowRoot, module2PrimaryModeSelectorEle,  module2OtherModeSelectorEle, ".module-config.request-button", eitherSelectedPredicate );

        // Create a map that allows event handler functions to be invoked by their name.
        registerHtmlEventHandlers();

        // Bind events that occur on specific HTML classes to their respective handler
        addHtmlElementEventListeners( "select", "change" );
        addHtmlElementEventListeners( "request-button", "click" );
        addHtmlElementEventListeners( "checkbox-setting", "change" );
        addHtmlElementEventListeners( "ilk-signal-handler", "ev-ilk-signal-bridge-clicked" );
        addHtmlElementEventListeners( "ilk-signal-handler", "ev-ilk-signal-interlock-clicked" );
        addHtmlElementEventListeners( "ilk-signal-handler", "ev-ilk-signal-link-clicked" );

        // Start listening to the stream from the ILKCS server
        startStreamListener();

        // Load Facility configuration information from the remote server and use it set up this page
        await loadFacilityInformation( facilityId );

        // Listen and respond to application wide events that set the target module
        document.addEventListener( "ev-doc-expert-module-selected", (ev) => handleExpertModuleEventSelect( ev ) );
    }

    await init();

console.log( "module-expert-view.js completed." )