console.log( "quick-view.js is running...")

    /*-----------------------------------------------------------------------*/
    /* 1.0 MODULE IMPORT DEFINITIONS                                         */
    /*-----------------------------------------------------------------------*/

    // noinspection NpmUsedModulesInstalled
    import {ShadowDomHelper} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {StateManager} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SIGNAL_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {Chart} from "__JS_SUPPORT_FILE__";

    /*-----------------------------------------------------------------------*/
    /* 2.0 VARIABLE DEFINITIONS                                              */
    /*-----------------------------------------------------------------------*/

    const shadowDomHelper = new ShadowDomHelper( "quick-view-panel" );
    const stateManager = new StateManager();

    /*-----------------------------------------------------------------------*/
    /* 3.0 STREAM HANDLING SUPPORT                                           */
    /*-----------------------------------------------------------------------*/

    function updateSignalContainerConnectionState( connected )
    {
        const signalGroupEles = shadowDomHelper.querySelectorAll( "ilk-signal-group" );
        signalGroupEles.forEach(  (cont) => {
            console.log( "setting signal group with ID: '" + cont.id + "' to connection state: " + connected + "'." );
            cont.connected = connected;
        } );
    }

    function handleStreamConnected()
    {
        updateSignalContainerConnectionState( true );
    }

    function handleStreamDisconnected()
    {
        updateSignalContainerConnectionState( false );
        const UNKNOWN_FACILITY_STATE = {};
        stateManager.saveObject( "FacilityState", UNKNOWN_FACILITY_STATE );
    }


    function getMappedTimestamp( fullTimestamp ) {

        function addLeadingZeros(num, totalLength) {
            return String(num).padStart(totalLength, '0');
        }

        // Build a JS Date object based on the supplied timestamp which typically looks like something
        // like this: 2022-05-15T03:46:24.981360032
        const d = new Date( fullTimestamp );

        // Create and return the timestamp string in the format we want it.
        const formattedDate = d.getFullYear() + "-" + addLeadingZeros(d.getMonth() + 1, 2) + "-" + addLeadingZeros(d.getDate(), 2);
        const formattedTime = addLeadingZeros(d.getHours(), 2) + ":" + addLeadingZeros(d.getMinutes(), 2) + ":" + addLeadingZeros(d.getSeconds(), 2);
        const formattedMillis = addLeadingZeros( d.getMilliseconds(), 3);
        return formattedDate + " " + formattedTime + "." + formattedMillis;
    }

    function getMappedInterlockCauses( interlockCauses )
    {
        let causes = "";
        interlockCauses.forEach((item, index) => {
            const cause = item[ "okName" ];
            const extraInfo = item[ "extraInfo" ];
            const causePrefix = index === 0 ? "" : ", ";
            if ( extraInfo === "" )
            {
                causes = causes + causePrefix + cause;
            }
            else
            {
                causes = causes + causePrefix + cause + " (" + extraInfo + ")";
            }
        });
        return causes;
    }

    function updateLastInterlock( interlockHistoryArray )
    {
        interlockHistoryArray.sort( (sigA,sigB) => Date.parse( sigB[ "tstrigger" ] ) - Date.parse( sigA[ "tstrigger" ] ) );
        const triggerTimeEle = shadowDomHelper.getElementById( "last-interlock-trigger-time" );
        const signalNameEle = shadowDomHelper.getElementById( "last-interlock-signal-name" );
        const causesEle = shadowDomHelper.getElementById( "last-interlock-causes" );

        if ( interlockHistoryArray.length === 0 )
        {
            triggerTimeEle.textContent = "N/A";
            signalNameEle.textContent = "N/A";
            causesEle.textContent = "N/A";
            return;
        }

        // noinspection UnnecessaryLocalVariableJS
        const signalName = interlockHistoryArray[ 0 ][ "okName" ];
        signalNameEle.textContent = signalName;

        const triggerTime = interlockHistoryArray[ 0 ][ "tstrigger" ];
        triggerTimeEle.textContent = getMappedTimestamp( triggerTime );

        if ( Object.prototype.hasOwnProperty.call( interlockHistoryArray[ 0 ], "causes" ) )
        {
            const interlockCauses = interlockHistoryArray[ 0 ][ "causes" ];
            causesEle.textContent = getMappedInterlockCauses( interlockCauses );
        }
        else {
            causesEle.textContent = "Not Available";
        }
    }

    function updateInterlockStatistics( statistics, since )
    {
        function mapSince( since )
        {
            switch ( since )
            {
                case "1": return "Last 1 hour";
                case "10000": return "Since server started";
                default: return  "Last " + since + " hours";
            }
        }
        const interlockCountEle = shadowDomHelper.getElementById( "interlock-count" );
        const interlockCount = statistics[ "totalInterlocks" ];
        interlockCountEle.textContent = "" + interlockCount + " Interlocks";
        const interlocksSinceEle = shadowDomHelper.getElementById( "interlocks-since" );
        interlocksSinceEle.textContent = mapSince( since );
    }

    let chartIlkSignals;
    let chartIlKCauses;

    function updatePlots( statistics, since )
    {
        function mapSince( since )
        {
            switch ( since )
            {
                case "1": return "Last 1 hour";
                case "10000": return "Since server started";
                default: return  "Last " + since + " hours";
            }
        }

        if ( chartIlkSignals !== undefined )
        {
            chartIlkSignals.destroy();
        }
        const plotIlkSignalsEle = shadowDomHelper.getElementById( "ilk-signal-plot" );
        const ilkSignalMap = statistics[ "signalFrequencyMap" ]
        const truncatedIlKSignalMap = new Map([...ilkSignalMap.entries()].slice( 0, 10 ) );
        const ilkSignals = Array.from( truncatedIlKSignalMap.keys() );
        const ilKSignalFrequencies = Array.from( truncatedIlKSignalMap.values() );
        const plotIlkSignalsTitle = "Top 10 Interlock Types - " + mapSince( since );
        const plotIlkSignalsXAxisTitle = "Total Number of Interlocks";
        const plotIlkSignalsYAxisTitle = "Interlock Type";

        if ( ilkSignals.length === 0)
        {
            const emptyLabels = Array( 10).fill( "" );
            const emptyData = Array( 10).fill( 0 )
            chartIlkSignals = createPlot( plotIlkSignalsEle, plotIlkSignalsTitle, plotIlkSignalsXAxisTitle, plotIlkSignalsYAxisTitle, emptyLabels, emptyData );
        }
        else
        {
            chartIlkSignals = createPlot( plotIlkSignalsEle, plotIlkSignalsTitle, plotIlkSignalsXAxisTitle, plotIlkSignalsYAxisTitle, ilkSignals, ilKSignalFrequencies );
        }

        if ( chartIlKCauses !== undefined )
        {
            chartIlKCauses.destroy();
        }
        const plotIlkCausesEle = shadowDomHelper.getElementById( "ilk-cause-plot" );
        const ilkCauseMap = statistics[ "causeFrequencyMap" ]
        const truncatedIlKCauseMap = new Map([...ilkCauseMap.entries()].slice( 0, 10 ) );
        const ilkCauses = Array.from( truncatedIlKCauseMap.keys() );
        const ilkCauseFrequencies = Array.from( truncatedIlKCauseMap.values() );
        const plotIlkCausesTitle = "Top 10 Interlock Causes - " +  mapSince( since );
        const plotIlkCausesAxisTitle = "Total Number of Interlocks";
        const plotIlkCausesYAxisTitle = "Interlock Cause";
        if ( ilkSignals.length === 0)
        {
            const emptyLabels = Array( 10).fill( "" );
            const emptyData = Array( 10).fill( 0 )
            chartIlKCauses = createPlot( plotIlkCausesEle, plotIlkCausesTitle, plotIlkCausesAxisTitle, plotIlkCausesYAxisTitle, emptyLabels, emptyData );
        }
        else
        {
            chartIlKCauses = createPlot( plotIlkCausesEle, plotIlkCausesTitle, plotIlkCausesAxisTitle, plotIlkCausesYAxisTitle, ilkCauses, ilkCauseFrequencies);
        }
    }

    function handleFacilityStateChanged( facilityState )
    {
        // Update Facility State
        stateManager.saveObject( "FacilityState", facilityState );

        // Update Interlock History Display
        const currentInterlocksArray = facilityState[ "currentInterlocks" ];
        const interlockHistoryArray = facilityState[ "interlockHistory" ];
        const combinedInterlockHistoryArray = [ ...currentInterlocksArray, ...interlockHistoryArray ];
        const since = stateManager.getSavedObject( "EarliestInterlockOfInterestInHoursAgo" );
        const statistics = SIGNAL_SUPPORT.calculateInterlockStatistics( combinedInterlockHistoryArray, since );

        updateLastInterlock( combinedInterlockHistoryArray );
        updateInterlockStatistics( statistics, since )
        updatePlots( statistics, since );
    }

    /*-----------------------------------------------------------------------*/
    /* 4.0 PLOT SUPPORT                                                      */
    /*-----------------------------------------------------------------------*/

    function createPlot( plotEle, plotTitle, plotXAxisTitle, plotYAxisTitle, labelsArray, valuesArray )
    {
        Chart.defaults.font.size = 20;
        Chart.defaults.plugins.tooltip.enabled = false;

        const data = {
            labels: labelsArray,
            datasets: [{
                data: valuesArray,
                backgroundColor: '#0075a7',
                minBarLength: 5,
            }]
        };

        const options = {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
                customCanvasBackgroundColor: {
                    color: 'lightGreen',
                },
                title: {
                    display: true,
                    text: plotTitle
                },
                legend: {
                    display: false
                }
            },
            animation: false,
            indexAxis: 'y',
            scales: {
                x: {
                    title: {
                        text: plotXAxisTitle,
                        display: true
                    },
                },
                y: {
                    title: {
                        text: plotYAxisTitle,
                        display: true
                    },
                }
            }
        };

        const config = {
            type: 'bar',
            data: data,
            options: options
        }
        return new Chart( plotEle, config );
    }


    /*-----------------------------------------------------------------------*/
    /* 5.0 PAGE INITIALISATION                                               */
    /*-----------------------------------------------------------------------*/

    async function init()
    {
        function startStreamListener()
        {
            document.addEventListener( "ev-stream-facility-state-changed", ev => {
                const facilityState =  ev.detail.facilityState;
                handleFacilityStateChanged( facilityState );
            } )
            document.addEventListener( "ev-stream-state-connected", () => handleStreamConnected() );
            document.addEventListener( "ev-stream-state-disconnected", () => handleStreamDisconnected() );
        }

        function startCheckboxListeners()
        {
            const showInterlockTypeEle = shadowDomHelper.getElementById( "show-interlock-type" );
            showInterlockTypeEle.addEventListener( "click", (ev) => {
                const checked = ev.target.checked;
                const showInterlockTypeEle = shadowDomHelper.getElementById( "interlock-type-container" );
                if ( checked ) {
                    showInterlockTypeEle.style.display = "flex";
                }
                else  {
                    showInterlockTypeEle.style.display = "none";
                }
            } );

            const showInterlockTypeStatisticsEle = shadowDomHelper.getElementById( "show-interlock-type-statistics" );
            showInterlockTypeStatisticsEle.addEventListener( "click", (ev) => {
                const checked = ev.target.checked;
                const ilkSignalBodyEle = shadowDomHelper.getElementById( "ilk-signal-body" );
                const ilkSignalCauseEle = shadowDomHelper.getElementById( "ilk-cause-body" );
                if ( checked ) {
                    ilkSignalBodyEle.style.display = "block";
                    ilkSignalCauseEle.style.width = "45vw";
                }
                else  {
                    ilkSignalBodyEle.style.display = "none";
                    ilkSignalCauseEle.style.width = "90vw";
                }
            } );
        }

        function startButtonListeners()
        {
            const buttonGroupEles = shadowDomHelper.querySelectorAll( ".statistics-interval-button" );
            buttonGroupEles.forEach( (eles) => {
                eles.addEventListener( "click", (ev) => {
                    const hoursAgo = ev.target.value;
                    stateManager.saveObject( "EarliestInterlockOfInterestInHoursAgo", hoursAgo );
                    console.log( "EarliestInterlockOfInterestInHoursAgo was set to:" + hoursAgo );
                } );
            } );
        }

        const longAgoInHours = "10000";
        stateManager.saveObject( "EarliestInterlockOfInterestInHoursAgo", longAgoInHours );

        // Start listening for stream events
        startStreamListener();

        // Start listening for button events
        startCheckboxListeners();
        startButtonListeners();
    }

    await init();

console.log( "quick-view.js completed." )