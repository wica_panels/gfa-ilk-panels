console.log( "main-view.js is running...")

    /*-----------------------------------------------------------------------*/
    /* 1.0 MODULE IMPORT DEFINITIONS                                         */
    /*-----------------------------------------------------------------------*/

    // noinspection NpmUsedModulesInstalled
    import {StateManager} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {SESSION_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {FAST_JSON_PATCH} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {STREAM_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import "./view-manager.js"

    /*-----------------------------------------------------------------------*/
    /* 2.0 VARIABLE DEFINITIONS                                              */
    /*-----------------------------------------------------------------------*/

    const stateManager = new StateManager();

    /*-----------------------------------------------------------------------*/
    /* 3.0 STREAM HANDLING SUPPORT FUNCTIONS                                 */
    /*-----------------------------------------------------------------------*/

    function handleStreamDataChanged( streamData )
    {
        function handleTimestampUpdate(timestamp) {
            const lastUpdateTimeEle = document.getElementById("last-update-time");
            const timeStartIndex = 10;
            lastUpdateTimeEle.textContent = timestamp.substring(timeStartIndex, timestamp.indexOf("."));
            lastUpdateTimeEle.dataset.connected = "true";
        }

        function handleFacilityCurrentSessionUpdate( currentUserSession )
        {
            // Update shoelace tooltip text
            const currentUserTextEle = document.getElementById("current-user-text");
            currentUserTextEle.content = SESSION_SUPPORT.getCurrentSessionHelpText(currentUserSession);

            // Update icon color
            const iconColor = SESSION_SUPPORT.getCurrentSessionIconColor(currentUserSession);
            const currentUserIconEle = document.getElementById("current-user-icon");
            currentUserIconEle.style.color = iconColor;
        }

        function handleFacilityOtherSessionUpdate( otherUserSessions ) {
            // Update shoelace tooltip text
            const otherUserSessionsHelpText = SESSION_SUPPORT.getOtherUserSessionsHelpText(otherUserSessions);
            const otherUsersTextEle = document.getElementById("other-users-text");
            otherUsersTextEle.content = otherUserSessionsHelpText;

            // Update icon color
            const iconColor = SESSION_SUPPORT.getOtherUserSessionsIconColor(otherUserSessions);
            const otherUsersIconEle = document.getElementById("other-users-icon");
            otherUsersIconEle.style.color = iconColor;
        }

        function patchFacilityState( previousFacilityState, facilityStatePatchObject)
        {
            const document = previousFacilityState;
            const patch = facilityStatePatchObject;
            const validateOperation = true;
            const mutateDocument = false;
            const banPrototypeModifications = true;
            try {
                const patchResult = FAST_JSON_PATCH.applyPatch(document, patch, validateOperation, mutateDocument, banPrototypeModifications);
                return patchResult["newDocument"];
            } catch (err) {
                console.warn("error patching object");
            }
        }

        const sseDataObject = JSON.parse(streamData.data);
        const currentUserSession = sseDataObject["currentUserSession"];
        const otherUserSessions = sseDataObject["otherUserSessions"];
        const timestamp = sseDataObject["timestamp"];
        const facilityStatePatchObject = sseDataObject["facilityStatePatch"];

        // Apply patch object to obtain facility state
        const previousFacilityState = stateManager.getSavedObject("FacilityState");
        const facilityState = patchFacilityState(previousFacilityState, facilityStatePatchObject);

        stateManager.saveObject("FacilityState", facilityState);
        handleTimestampUpdate( timestamp );
        handleFacilityCurrentSessionUpdate(currentUserSession);
        handleFacilityOtherSessionUpdate(otherUserSessions);

        const evStreamFacilityStateData = new CustomEvent( "ev-stream-facility-state-changed", {
            detail: {"facilityState": facilityState, "currentUserSession": currentUserSession}
        });
        document.dispatchEvent( evStreamFacilityStateData );
    }

    function handleStreamStateChanged(streamState)
    {
        const messageEle = document.getElementById("message");

        switch (streamState) {
            case "REDIRECT":
                console.log("Received page redirect from backend");
                break;

            case "CREATED":
                document.dispatchEvent(new CustomEvent("ev-stream-state-connecting"));
                break;

            case "ERROR":
                document.dispatchEvent(new CustomEvent("ev-stream-state-disconnected"));
                messageEle.closeable = false;
                messageEle.titleText = "Interlock Server Disconnected";
                messageEle.messageText = "Will periodically attempt to reconnect...";
                break;

            case "OPENED":
                document.dispatchEvent(new CustomEvent("ev-stream-state-connected"));
                messageEle.closeable = false;
                messageEle.titleText = "";
                messageEle.messageText = "";
                break;
        }
    }

    function startServerStream() {
        const probeUrlPath = "__ILKCS_PROBE_URL_PATH__";
        const streamUrlPath = "__ILKCS_STREAM_URL_PATH__";
        const ackUrlPath = "__ILKCS_ACK_URL_PATH__";

        const serverUrl = window.location.origin + "__ILKCS_SERVER_PATH_PREFIX__";
        const probeUrl = serverUrl + probeUrlPath;
        const subscribeUrl = serverUrl + streamUrlPath;
        const ackUrl = serverUrl + ackUrlPath;
        const streamName = "ev-ilk-facility-state";

        const UNKNOWN_FACILITY_STATE = {};
        stateManager.saveObject("FacilityState", UNKNOWN_FACILITY_STATE);

        STREAM_SUPPORT.start_stream(probeUrl, subscribeUrl, ackUrl, streamName,
            (streamState) => handleStreamStateChanged(streamState),
            (streamData) => handleStreamDataChanged(streamData)
        );
    }

    /*-----------------------------------------------------------------------*/
    /* 4.0 HTML ELEMENT EVENT LISTENER SUPPORT                               */
    /*-----------------------------------------------------------------------*/

    function sendPanelSelectedEvent( shortPanelName )
    {
        const views = {
            "ILK": "interlock",
            "MOD": "configuration",
            "QIK": "quick-view",
            "EXP": "module-expert",
            "CS": "command-tool",
            "DB": "database-tool",
            "IMG": "software-arch",
            "HLP": "user-guide"
        };

        const upperCaseShortPanelName = shortPanelName.toUpperCase();
        if ( ! Object.prototype.hasOwnProperty.call( views, upperCaseShortPanelName ) )
        {
            console.warn( "The panel with the short name: '" + shortPanelName + "' was not recognised" );
            return false;
        }
        const longPanelName = views[ upperCaseShortPanelName ] ;
        const selectPanelEvent = new CustomEvent( 'ev-doc-select-panel', {
            bubbles: true, composed: true, detail: {
                "panelName": longPanelName
            }
        });
        document.dispatchEvent( selectPanelEvent );
        return true;
    }

    function sendSectionSelectedEvent( sectionId )
    {

        const selectSectionEvent = new CustomEvent( 'ev-doc-target-section-selected', {
            bubbles: true, composed: true, detail: {
                "sectionId": sectionId
            }
        });
        document.dispatchEvent( selectSectionEvent );
    }

    function sendModuleSelectedEvent( moduleId )
    {

        const selectModuleEvent = new CustomEvent( 'ev-doc-target-module-selected', {
            bubbles: true, composed: true, detail: {
                "moduleId": moduleId
            }
        });
        document.dispatchEvent( selectModuleEvent);

        const selectExpertModuleEvent = new CustomEvent( 'ev-doc-expert-module-selected', {
            bubbles: true, composed: true, detail: {
                "moduleId": moduleId
            }
        });
        document.dispatchEvent( selectExpertModuleEvent );
    }

    function addPanelEventListeners()
    {
        // Listen and react to requests sent from the individual views to switch the active view
        document.addEventListener("ev-doc-select-panel", (ev) => {
            console.log( "Select panel event for panel : " + ev.detail.panelName )
            const panelName = ev.detail.panelName;
            // The event is processed in the same way as a click on the specified shoelace tab.
            const slTabSelector = 'sl-tab[panel="' + panelName + '"]';
            const slTabEle= document.querySelector( slTabSelector );
            slTabEle.click();
        });

        // Listen and react to show events generated by the tab group handler
        const tabGroup = document.querySelector("sl-tab-group");
        tabGroup.addEventListener("sl-tab-show", (ev) => {
            console.log( "Tab shown: " + ev.detail.name );
            const viewManagerSelector = "#" + ev.detail.name + "-panel";
            const viewManagerEle= document.querySelector( viewManagerSelector );
            viewManagerEle.show = true;
        });

        // Listen and react to hide events generated by the tab group handler
        tabGroup.addEventListener("sl-tab-hide", (ev) => {
            console.log( "Tab hidden: " + ev.detail.name );
            const viewManagerSelector = "#" + ev.detail.name + "-panel";
            const viewManagerEle = document.querySelector( viewManagerSelector );
            viewManagerEle.show = false;
        });
    }

    /*-----------------------------------------------------------------------*/
    /* 5.0 PAGE INITIALISATION                                               */
    /*-----------------------------------------------------------------------*/

    async function waitForViewManagerReadyEvents( targetCount )
    {
        let count = 0;

        function incrementCount() {
            count++;
        }

        async function awaitCount( value )
        {
            return new Promise( (resolve) => {
                const interval = setInterval( () => {
                    if ( count === value ) {
                        clearInterval( interval );
                        resolve();
                    }
                }, 100 );
            });
        }

        document.addEventListener( "ev-view-manager-view-ready", () => {
            console.log( "view manager ready event!")
            incrementCount();
        })

        return awaitCount( targetCount );
    }

    async function init()
    {
        addPanelEventListeners();

        const NUMBER_OF_VIEW_MANAGER_INSTANCES = 8;
        await waitForViewManagerReadyEvents( NUMBER_OF_VIEW_MANAGER_INSTANCES );
        console.log( "All view manager instances are ready." )

        const urlParams = new URLSearchParams( window.location.search );
        const shortPanelName = urlParams.get( 'panelId' );
        if ( ( shortPanelName == null ) || ( ! sendPanelSelectedEvent( shortPanelName ) ) )
        {
            const DEFAULT_PANEL_NAME = "ILK";
            sendPanelSelectedEvent( DEFAULT_PANEL_NAME );
        }

        const sectionId = urlParams.get( 'sectionId' );
        if ( sectionId !== null )
        {
            sendSectionSelectedEvent( sectionId );
        }

        const moduleId = urlParams.get( 'moduleId' );
        if ( moduleId  !== null )
        {
            sendModuleSelectedEvent( moduleId );
        }

        console.log( "Starting server stream..." );
        startServerStream();
    }

    await init();

console.log( "main-view.js completed." )