const template = document.createElement( "template" );

template.innerHTML= `
   <style id="vars-holder"></style>
   <style id="style-holder"></style>
   <div id="html-holder" style="display: block; height: 100%"></div>
   <script id="script-holder" type="module"></script>
`;

class ViewManager extends HTMLElement
{
    /**
     * Determines which attributes will cause attributesChangedCallback to fire.
     *
     * @returns {string[]} array containing the attributes to monitor.
     */
    static get observedAttributes()
    {
        return [ "html-file", "vars-file", "css-file", "script-file", "show" ];
    }

    /**
     * Constructs a new ViewManager element.
     *
     * The element will have its initial display property set to block mode
     * (which allows the shadow DOM to be completely populated during the initialisation
     * phase), but the component will be hidden.
     */
    constructor()
    {
        super();
        this.attachShadow( { mode: "open" } );
        this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        this.style.display = "block";
        this.style.height = "0";
        this.style.width = "0";
        this.style.visibility = "hidden";
    }

    /**
     * Handles an update to the html file attribute.
     * @param url the new html file url.
     */
    set htmlFile( url )
    {
        console.log( "view_manager fetching html from: '" + url + "'..." );
        fetch( url )
            .then( response => response.text())
            .then( html => {
                console.log( "view_manager html obtained." )
                const htmlHolder = this.shadowRoot.querySelector( "#html-holder" );
                htmlHolder.innerHTML = html;
                console.log( "view_manager html loaded." )
                if ( this._scriptFile && this._scriptFile.length > 0 ) {
                    console.log("view_manager importing script from: '" + this._scriptFile + "'...");
                    import( this._scriptFile ).then( () => {
                        console.log("view_manager script imported.");
                        console.log( "view_manager sending event: 'ev-view-manager-view-ready' (" + url + ")." );
                        this.dispatchEvent( new CustomEvent( 'ev-view-manager-view-ready', { bubbles: true, composed: true, detail: {} } ) );
                    } );
                }
                else {
                    console.log( "view_manager sending event: 'ev-view-manager-view-ready'(" + url + ")." );
                    this.dispatchEvent( new CustomEvent( 'ev-view-manager-view-ready', { bubbles: true, composed: true, detail: {} } ) );
                }
            } );
    }

    /**
     * Handles an update to the vars file attribute.
     * @param url the new vars file url.
     */
    set varsFile( url )
    {
        console.log( "view_manager fetching vars from: '" + url + "'..." );
        fetch( url )
            .then( response => response.text())
            .then(html => {
                console.log( "view_manager vars obtained." );
                const cssHolder = this.shadowRoot.querySelector( "#vars-holder" );
                cssHolder.innerHTML = html;
                console.log( "view_manager vars loaded." );
            });
    }

    /**
     * Handles an update to the css file attribute.
     * @param url the new css file url.
     */
    set cssFile( url )
    {
        console.log( "view_manager fetching styles from: '" + url + "'..." );
        fetch( url )
            .then( response => response.text())
            .then(html => {
                console.log( "view_manager styles obtained." );
                const cssHolder = this.shadowRoot.querySelector( "#style-holder" );
                cssHolder.innerHTML = html;
                console.log( "view_manager styles loaded." );
            });
    }

    /**
     * Handles an update to the script file attribute.
     * @param url the new script file url.
     */
    set scriptFile( url )
    {
        console.log( "view_manager setting script to: '" + url + "'..." );
        this._scriptFile = url;
    }

    /**
     * Shows or hides the web component.
     * @param value boolean property specifying the required state.
     */
    set show( value )
    {
        if ( value ) {
            this.style.height = "100%";
            this.style.width = "100%";
            this.style.visibility = "visible";
        }
        else {
            this.style.height = "0";
            this.style.width = "0";
            this.style.visibility = "hidden";
        }
    }

    attributeChangedCallback( attrName, oldValue, newValue )
    {
         switch( attrName )
         {
             case "html-file":
                 this.htmlFile = newValue;
                 break;

             case "vars-file":
                 this.varsFile = newValue;
                 break;

             case "css-file":
                 this.cssFile = newValue;
                 break;

             case "script-file":
                 this.scriptFile = newValue;
                 break;

             case "show":
                 this.show = true;
                 break;
         }
    }

    connectedCallback()
    {
        console.log( "view_manager connected !" );
    }

    disconnectedCallback()
    {
        console.log( "view_manager disconnected !" );
    }
}

customElements.define( "view-manager", ViewManager );