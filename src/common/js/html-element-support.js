/* eslint-disable no-unused-vars */
export const HTML_ELEMENT_SUPPORT = {

    "removeSelectWidgetOptions" : (selectWidget) => removeSelectWidgetOptions( selectWidget ),

    "addGroupedSelectWidgetOptions" : (optionGroupObj, selectWidget, initialOptionText, widgetGroupNamePrefix ) =>
        addGroupedSelectWidgetOptions( optionGroupObj, selectWidget, initialOptionText, widgetGroupNamePrefix ),

    "addSelectWidgetOptions" : (dataObj, selectWidget, initialOptionText ) => addSelectWidgetOptions( dataObj, selectWidget, initialOptionText ),

    "setSelectWidget" : (selectWidget,targetText) => setSelectWidget( selectWidget, targetText ),

    "setButtonEnableState" : (searchStartEle, buttonSelector, buttonEnabled) => setButtonEnableState( searchStartEle, buttonSelector, buttonEnabled ),

    "getSvgElementProperty" : (svgDocument, elementId, propertyName) => getSvgElementProperty( svgDocument, elementId, propertyName ),

    "registerHtmlSelectElementButtonStateReflector" : (searchStartEle, selectWidget, buttonSelector) => registerHtmlSelectElementButtonStateReflector( searchStartEle, selectWidget, buttonSelector ),
    "registerHtmlSelectElementPairButtonStateReflector" : ( searchStartEle, selectWidget1, selectWidget2, buttonSelector, predicate ) =>
        registerHtmlSelectElementPairButtonStateReflector( searchStartEle, selectWidget1, selectWidget2, buttonSelector, predicate ),

    "registerHtmlSelectElementQuadButtonStateReflector" : ( searchStartEle, selectWidget1, selectWidget2, selectWidget3, selectWidget4, buttonSelector, predicate ) =>
        registerHtmlSelectElementQuadButtonStateReflector( searchStartEle, selectWidget1, selectWidget2, selectWidget3, selectWidget4, buttonSelector, predicate  ),

    "buildGroupedSelectWidgetOptionsFromModeProperties" : (jsonDataObj) => buildGroupedSelectWidgetOptionsFromModeProperties( jsonDataObj ),
    "buildSelectWidgetOptionsFromGroupedChoices" : (jsonDataObj) => buildGroupedSelectWidgetOptionsFromModeProperties( jsonDataObj ),

    "buildGroupedSelectWidgetOptionsFromIocId" : (jsonDataObj) => buildGroupedSelectWidgetOptionsFromIocId( jsonDataObj ),
    "buildGroupedSelectWidgetOptionsFromSectionId" : (jsonDataObj) => buildGroupedSelectWidgetOptionsFromSectionId( jsonDataObj ),
    "buildGroupedSelectWidgetOptionsFromModuleId" : (jsonDataArray) => buildGroupedSelectWidgetOptionsFromModuleId( jsonDataArray ),

    "buildSelectWidgetOptionsFromSignalData" : (jsonDataArray) => buildSelectWidgetOptionsFromSignalData( jsonDataArray ),
    "buildSelectWidgetOptionsFromSignalNameData" : (jsonDataArray) => buildSelectWidgetOptionsFromSignalNameData( jsonDataArray )

}

/*-----------------------------------------------------------------------*/
/* 1.0 HTML SELECT ELEMENT SUPPORT                                       */
/*-----------------------------------------------------------------------*/

/**
 * Removes all options from an HTML select object.
 *
 * @param selectWidget reference to the HTML select element to be updated.
 */
function removeSelectWidgetOptions( selectWidget )
{
    while ( selectWidget.firstChild ) {
        selectWidget.removeChild( selectWidget.firstChild );
    }
}

/**
 * Updates an HTML select object so that the currently selected item matches the targeted text.
 *
 * If no match can be found the selected item is left unchanged.
 *
 * @param selectWidget reference to the HTML select element to be updated.
 * @param text the text to be matched.
 */
function setSelectWidget( selectWidget, text )
{
    const options = Array.from( selectWidget.options );
    let index = 0;
    options.forEach( opt => {
        if ( opt.label === text ) {
            selectWidget.selectedIndex = index;
        }
        index++;
    } );
}

/**
 * Adds information to an HTML select object which is organised in groups.
 *
 * The HTML select item entries will be populated from the supplied data object whose (String) keys
 * become the widget's OPTGROUP names and whose (String[]) values become the widget's individual OPTIONs.
 *
 * Example Input Data:
 *    [{"IQIN2":{"mode":"Q","description":"DURCH"}},{"IQIN2":{"mode":"O","description":"INACTIV"}}]
 *
 * @param optionGroupObj the object containing the data payload.
 * @param selectWidget reference to the HTML select element to be updated.
 * @param initialOptionText text which is shown when no option from the dropdown list is actively selected.
 * @param widgetGroupNamePrefix optional text which is prepended to the name of each group.
 */
function addGroupedSelectWidgetOptions( optionGroupObj, selectWidget, initialOptionText= "", widgetGroupNamePrefix= "" )
{
    // Remove all existing widget options
    removeSelectWidgetOptions( selectWidget );

    if ( Object.keys( optionGroupObj ).length > 0 )
    {
        const option = document.createElement("OPTION");
        option.text = initialOptionText;
        selectWidget.appendChild(option);

        Object.keys(optionGroupObj).forEach(key =>
        {
            const optGroup = document.createElement("OPTGROUP");
            optGroup.id = key;
            optGroup.label = widgetGroupNamePrefix === "" ? key : widgetGroupNamePrefix + " " + key;
            selectWidget.appendChild( optGroup );
            optionGroupObj[key].forEach( (item) =>
                {
                    const option = document.createElement("OPTION" );

                    option.value = item[ "value" ];
                    option.text =  item[ "text" ];
                    option.disabled = Object.prototype.hasOwnProperty.call( item, "disabled" ) && item[ "disabled" ];
                    optGroup.appendChild( option );
                }
            )
        });
    }
}

/**
 * Adds information to an HTML select object which is organised as a single list of options.
 *
 * The HTML select item entries will be populated from the supplied data array whose (String) members
 * become the widget's individual OPTIONs.
 *
 * Example Input Data:
 *    [ { "text": "abcText", "value": "abcValue", "disabled": false }, ...  ] }.
 *
 * @param optionArrayObj the object containing the data payload.
 * @param selectWidget reference to the HTML select element to be updated.
 * @param initialOptionText text which is shown when no option from the dropdown list is actively selected.
 */
function addSelectWidgetOptions( optionArrayObj, selectWidget, initialOptionText )
{
    // Remove all existing widget options
    removeSelectWidgetOptions( selectWidget );

    if ( Array.isArray( optionArrayObj ) && optionArrayObj.length )
    {
        // Add initial text as first option.
        const option = document.createElement( "OPTION" );
        option.text = initialOptionText;
        selectWidget.appendChild( option );

        // Populate entries with the supplied data structure which should be an array
        // of objects whose text and value fields correspond to the values to be
        // assigned to each HTML element OPTION. When provided the disabled field will
        // result in the option being disabled in the dropdown list.
        optionArrayObj.forEach( (item) => {
            const option = document.createElement( "OPTION" );
            option.value = item[ "value" ];
            option.text =  item[ "text" ];
            option.disabled = Object.prototype.hasOwnProperty.call( item, "disabled" ) && item[ "disabled" ];
            selectWidget.appendChild( option );
        } );
    }
}


/*-----------------------------------------------------------------------*/
/* 2.0 HTML BUTTON ELEMENT SUPPORT                                       */
/*-----------------------------------------------------------------------*/

function setButtonEnableState( searchStartEle, buttonSelector, buttonEnabled )
{
    const targetedButtons = searchStartEle.querySelectorAll( buttonSelector );
    for ( let item of targetedButtons ) {
        item.disabled = ! buttonEnabled;
    }
}

function registerHtmlSelectElementButtonStateReflector( searchStartEle, selectWidget, buttonSelector )
{
    selectWidget.addEventListener( "change", () => {
        const buttonEnabled = ( selectWidget.selectedIndex > 0 );
        HTML_ELEMENT_SUPPORT.setButtonEnableState( searchStartEle, buttonSelector, buttonEnabled );
    } );
}

function registerHtmlSelectElementPairButtonStateReflector( searchStartEle, selectWidget1, selectWidget2, buttonSelector, predicate = (a,b)  => (a && b ) )
{
    selectWidget1.addEventListener( "change", () => {
        const buttonEnabled = predicate( ( selectWidget1.selectedIndex > 0 ), ( selectWidget2.selectedIndex > 0 ) );
        HTML_ELEMENT_SUPPORT.setButtonEnableState( searchStartEle, buttonSelector, buttonEnabled );
    } );

    selectWidget2.addEventListener( "change", () => {
        const buttonEnabled = predicate( ( selectWidget1.selectedIndex > 0 ), ( selectWidget2.selectedIndex > 0 ) );
        HTML_ELEMENT_SUPPORT.setButtonEnableState( searchStartEle, buttonSelector, buttonEnabled );
    } );
}

function registerHtmlSelectElementQuadButtonStateReflector( searchStartEle, selectWidget1, selectWidget2, selectWidget3, selectWidget4, buttonSelector, predicate = (a,b,c,d)  => (a && b && c && d ) )
{
    selectWidget1.addEventListener( "change", () => {
        const buttonEnabled = predicate( ( selectWidget1.selectedIndex > 0 ), ( selectWidget2.selectedIndex > 0 ), ( selectWidget3.selectedIndex > 0 ), ( selectWidget4.selectedIndex > 0) );
        HTML_ELEMENT_SUPPORT.setButtonEnableState( searchStartEle, buttonSelector, buttonEnabled );
    } );

    selectWidget2.addEventListener( "change", () => {
        const buttonEnabled = predicate( ( selectWidget1.selectedIndex > 0 ), ( selectWidget2.selectedIndex > 0 ), ( selectWidget3.selectedIndex > 0 ), ( selectWidget4.selectedIndex > 0 ) );
        HTML_ELEMENT_SUPPORT.setButtonEnableState( searchStartEle, buttonSelector, buttonEnabled );
    } );

    selectWidget3.addEventListener( "change", () => {
        const buttonEnabled = predicate( ( selectWidget1.selectedIndex > 0 ), ( selectWidget2.selectedIndex > 0 ), ( selectWidget3.selectedIndex > 0 ), ( selectWidget4.selectedIndex > 0 ) );
        HTML_ELEMENT_SUPPORT.setButtonEnableState( searchStartEle, buttonSelector, buttonEnabled );
    } );

    selectWidget4.addEventListener( "change", () => {
        const buttonEnabled = predicate( ( selectWidget1.selectedIndex > 0 ), ( selectWidget2.selectedIndex > 0 ), ( selectWidget3.selectedIndex > 0 ), ( selectWidget4.selectedIndex > 0 ) );
        HTML_ELEMENT_SUPPORT.setButtonEnableState( searchStartEle, buttonSelector, buttonEnabled );
    } );
}


/*-----------------------------------------------------------------------*/
/* 3.0 HTML SVG ELEMENT SUPPORT                                          */
/*-----------------------------------------------------------------------*/

function getSvgElementProperty( svgDocument, elementId, propertyName )
{
    const element = svgDocument.getElementById( elementId );
    if ( element == null ) {
        return "white";
    }
    else {
        const style = window.getComputedStyle( element );
        return style.getPropertyValue( propertyName );
    }
}

/*-----------------------------------------------------------------------*/
/* 4.0 HTML SELECT ELEMENT JSON PAYLOAD DECODING SUPPORT                 */
/*-----------------------------------------------------------------------*/

/**
 * Returns a data structure suitable for passing to an HTML select widget using the information built
 * from a JSON payload returned as the result of a transaction to obtain the facility mode information
 * organised by mode property name.
 *
 * Example Input Data:
 *   { "Main-Path": [ "BW2", "BN5" ] }
 *   { "AHK1-Path": [ "Target", "Inactive", "Not-in-use" ] }
 *   { "VAR-Path": [ "Beam Allow", "Beam Stop", "Beam Left", "Beam Right", "Beam Split", "Inactive" ] }
 *   { "Mode": [ "O (Inactive)", "S (Stop)", "D (Durch)"] }
 *   { "MODE": [ "TL-Inactive", "TL-Left", "TL-Right", "TL-Split" ] }
 *
 * Example Output Data:
 * { grpName1: [ { "value": grpChoice1, "text": grpChoice1 }, { "value": grpChoice2, "text": grpChoice2 } ] ,  }
 *
 * @param jsonDataObj the JSON payload to process.
 * @returns {{[p: string]: any}} the result data structure, suitable for populating the HTML select widget.
 */
function buildGroupedSelectWidgetOptionsFromModeProperties( jsonDataObj )
{
    const resultMap = new Map();
    Object.keys( jsonDataObj ).forEach( (key) => {
        const jsonDataArray = jsonDataObj[ key ];
        jsonDataArray.forEach( (val) => {
            const groupName = key;
            if (!resultMap.has( groupName ) ) {
                resultMap.set( groupName, []);
            }
            const arr = resultMap.get( groupName );
            const option = {};
            option.text = val;
            option.value = val;
            option.disabled = false;
            arr.push( {value: val, text: val} );
            resultMap.set( groupName, arr );
        });
    });
    return Object.fromEntries( resultMap );
}

/**
 * Returns a data structure suitable for passing to an HTML select widget using the information built
 * from a JSON payload returned as the result of a transaction to obtain the facility IOC IDs,
 * organised by group.
 *
 * All menu options will be enabled.
 *
 * Example Input Data:
 *    { "AHK1" : [ "ZTEST-CVME-ILK01" ], "VAR" : [ "ZTEST-CVME-ILK01" ], "BN5" : [ "ZTEST-CVME-ILK01" ], "BW2" : [ "ZTEST-CVME-ILK01" ] }
 *
 * Example Output Data:
 *    { "AHK1": [ { "value": "ZTEST-CVME-ILK01", "text": "ZTEST-CVME-ILK01", "disabled": false } ],
 *       "VAR": [ { "value": "ZTEST-CVME-ILK01", "text": "ZTEST-CVME-ILK01", "disabled": false } ],
 *       "BN5": [ { "value": "ZTEST-CVME-ILK01", "text": "ZTEST-CVME-ILK01", "disabled": false } ],
 *       "BW2": [ { "value": "ZTEST-CVME-ILK01", "text": "ZTEST-CVME-ILK01", "disabled": false } ],
 *    }
 * @param jsonDataObj the JSON payload to process.
 * @returns {{[p: string]: any}} the result data structure, suitable for populating the HTML select widget.
 */
function buildGroupedSelectWidgetOptionsFromIocId( jsonDataObj )
{
    const resultMap = new Map();
    Object.keys( jsonDataObj ).forEach( (key) => {
        const grpId = key;
        const sectionEntries = jsonDataObj[ grpId ];
        sectionEntries.forEach( secId => {
            if ( !resultMap.has( grpId) ) {
                resultMap.set( grpId, [] );
            }
            const optionDataArray = resultMap.get( grpId );
            const option = {};
            option.text = secId;
            option.value = secId;
            option.disabled = false;
            optionDataArray.push( option );
            resultMap.set( grpId, optionDataArray );
        } );
    } );
    return Object.fromEntries( resultMap );
}

/**
 * Returns a data structure suitable for passing to an HTML select widget using the information built
 * from a JSON payload returned as the result of a transaction to obtain the facility section IDs,
 * organised by group.
 *
 * All menu options will be enabled.
 *
 * Example Input Data:
 *    { "BEAM": [ "BW2", "BN5", "AHK1"], "TEST": [ "VAR" ] }
 *
 * Example Output Data:
 *    { "BEAM": [ { "value": "BW2", "text": "BW2", "disabled": false }, { "value": "BN5", "text": "BN5", "disabled": false } ],
 *      "TEST": [ { "value": "VAR", "text": "VAR", "disabled": false } }
 *
 * @param jsonDataObj the JSON payload to process.
 * @returns {{[p: string]: any}} the result data structure, suitable for populating the HTML select widget.
 */
function buildGroupedSelectWidgetOptionsFromSectionId( jsonDataObj )
{
    const resultMap = new Map();
    Object.keys( jsonDataObj ).forEach( (key) => {
        const grpId = key;
        const sectionEntries = jsonDataObj[ grpId ];
        sectionEntries.forEach( secId => {
            if ( !resultMap.has( grpId) ) {
                resultMap.set( grpId, [] );
            }
            const optionDataArray = resultMap.get( grpId );
            const option = {};
            option.text = secId;
            option.value = secId;
            option.disabled = false;
            optionDataArray.push( option );
            resultMap.set( grpId, optionDataArray );
        } );
    } );
    return Object.fromEntries( resultMap );
}


/**
 * Returns a data structure suitable for passing to an HTML select widget using the information built
 * from a JSON payload returned as the result of a transaction to obtain the module IDs.
 *
 * The implementation creates its own groups based on classifying the module's id according to
 * the implied module type (RPM or USI2) and the numeric range in group's of 1000.
 *
 * All menu options will be enabled.
 *
 * Example Input Data:
 *    ["002004","002006","002037","002097", ...]
 *
 * Example Output Data
 *    { "USI2s: 0003000-0003999:" [ { "value": 0003000, "text": 0003000, "disabled": false },
 *                                  { "value": 0003001, "text": 0003001, "disabled": false }, ... ],
 *        "RPMs:  001000-001999:" [ { "value":  001000, "text":  001000, "disabled": false },
 *                                  { "value":  001001, "text":  001001, "disabled": false }, ... ],
 *    ... }
 *
 * @param jsonDataArray the JSON payload to process.
 * @returns {{[p: string]: any}} the result data structure, suitable for populating the HTML select widget.
 */
function buildGroupedSelectWidgetOptionsFromModuleId( jsonDataArray )
{
    function getClassifier( moduleId )
    {
        const numericId = parseInt( moduleId, 10 );
        if ( moduleId.length === 6 ) {
            const range = "" + Math.floor( numericId / 1000 );
            const moduleIdStart =  range.padStart( 3, "0" ) + "000";
            const moduleIdEnd =  range.padStart( 3, "0" ) + "999";
            return "RPMs: " + moduleIdStart + "-" + moduleIdEnd;
        }
        else if ( moduleId.length === 7 ) {
            const range = "" + Math.floor( numericId / 1000 );
            const moduleIdStart =  range.padStart( 4, "0" ) + "000";
            const moduleIdEnd =  range.padStart( 4, "0" ) + "999";
            return "USI2s: " + moduleIdStart + "-" + moduleIdEnd;
        }
    }

    const resultMap = new Map();
    jsonDataArray.forEach( (moduleId) => {
        const classifier = getClassifier( moduleId );
        if ( ! resultMap.has( classifier ) ) {
            resultMap.set( classifier, [] );
        }
        const arr = resultMap.get( classifier );
        const option = {};
        option.text = moduleId;
        option.value = moduleId;
        option.disabled = false;
        arr.push( option );
        resultMap.set( classifier, arr );
    } );
    return Object.fromEntries( resultMap );
}

/**
 * Returns a data structure suitable for passing to an HTML select widget using the information built
 * from a JSON payload returned as the result of a transaction with the ILKDB Server to obtain
 * signal name information.
 *
 * All menu options will be enabled.
 *
 * Example Input Data:
 *    [ { "sectionId" : "BN5", "moduleId" : "002004", "portId" : "1", "ok" : "BN5 ILK4", "nok" : "" } ]
 *
 * Example Output Data:
 *    [ { "text": "Port 1 (BN5 ILK4)", "value": "1", "disabled": false }, ...  ] }.
 *
 * @param jsonDataArray the JSON payload to process.
 * @returns {*[]} the result data structure, suitable for populating the HTML select widget.
 */
function buildSelectWidgetOptionsFromSignalNameData( jsonDataArray )
{
    const optionDataArray = [];
    jsonDataArray.forEach( (item) => {
        const option = {};
        option.text = "Port " + item.portId + " (" + item[ "ok" ] + ")";
        option.value = item.portId;
        option.disabled = false;
        optionDataArray.push( option );
    } );
    return optionDataArray;
}

/**
 * Returns a data structure suitable for passing to an HTML select widget using the information built
 * from a JSON payload returned as the result of a transaction with the ILKCS Server to obtain
 * complete signal information.
 *
 * The input data should include the bridgePolicy; where the user does not have the necessary
 * access rights the associated menu option will be disabled.
 *
 * Example Input Data:
 *
 *    [ {"sectionId":"VAR","moduleId":"002006","portId":"1","type":"input","activeName":"",
 *       "okName":"INPUT1","nokName":"","epicsInfoChannelName":"SOMEPV1","epicsInfoChannelValue":"UNKNOWN",
 *       "tstrigger":"","tsclear":"2023-04-14T13:48:55.982713187",
 *       "port-fault":"OK","port-signal":"NOK","logic-signal":"NOK","bridge":"RPM_CLR","interlock":"OK",
 *       "view":"DEFAULT","bridgePolicy":"SW","tracePolicy":"IGNORE","comment":""}, ...
 *    ]
 *
 * Example Output Data:
 *    [ { "text": "Port 1 (INPUT1)", "value": "1", "disabled": false }, ...  ] }.
 *
 * @param jsonDataArray the JSON payload to process.
 * @returns {*[]} the result data structure, suitable for populating the HTML select widget.
 */
function buildSelectWidgetOptionsFromSignalData( jsonDataArray )
{
    const optionDataArray = [];
    jsonDataArray.forEach( (item) => {
        const option = {};
        option.text = "Port " + item.portId + " (" + item.okName + ")";
        option.value = item.portId;

        // Currently (2023-11-27) we only disable options which are explicitly defined as unbridgeable.
        // Signals with any other bridge policy (eg "anybody", "privileged", "dynamic" ) are enabled.
        option.disabled =  [ "nobody", ].includes( item.bridgePolicy );
        optionDataArray.push( option );
    } );
    return optionDataArray;
}


