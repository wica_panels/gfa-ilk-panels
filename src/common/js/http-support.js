import {Formatter} from "fracturedjsonjs";

export const HTTP_SUPPORT = {
    "doGetRequest" : (requestUrl, requestEle, responseEle, popupEle  ) => doGetRequest( requestUrl, requestEle, responseEle, popupEle  ),
    "doPutRequest" : (requestUrl, putValue, requestEle, responseEle, popupEle ) => doPutRequest( requestUrl, putValue, requestEle, responseEle, popupEle )
}

// Create a formatter for use with this module.
document.formatter = new Formatter();
document.formatter.maxInlineLength = 160;

/**
 * Performs a GET request to the specified URL, handles the expected JSON response and/or
 * exceptional situations where other non-JSON responses are received.
 *
 * @param {string} requestUrl - The URL to send the GET request to.
 * @param {HTMLElement} requestEle - Optional. The element to log the request text, if provided.
 * @param {HTMLElement} responseEle - Optional. The element to log the response text, if provided.
 * @param {HTMLElement} popupEle - Optional. The element to display an error popup, if provided.
 * @returns {Promise<Object|string>} - A promise that resolves with the JSON response object when
 * successful, or rejects with a JS Error object when otherwise.
 */
async function doGetRequest( requestUrl, requestEle, responseEle, popupEle)
{
    const requestText = `GET="${requestUrl}"`;
    logRequest_( requestText, requestEle );

    try
    {
        const response = await fetch( requestUrl,{
            method: 'GET',
            credentials: 'include' }
        );

        const escalatedResponse = escalateHttpResponseErrors_( response );
        const text = await escalatedResponse.text();
        const responseObj = JSON.parse(text);
        const responseText = document.formatter.Serialize( responseObj );
        logResponse_( responseText, responseEle );
        return responseObj;
    }
    catch( error )
    {
        logResponse_(error, responseEle);
        const responseText = error.message;
        if (popupEle !== undefined) {
            popupEle.titleText = 'Interlock System Request Failure';
            popupEle.messageText = '🙁 Oh dear, the last request sent to the interlock system backend failed.';
            popupEle.requestText = requestText;
            popupEle.responseText = responseText;
        }
        throw error;
    }
}


/**
 * Performs a PUT request to the specified URL with the provided data, handles the expected
 * JSON response and/or exceptional situations where other non-JSON responses are received.
 *
 * @param {string} requestUrl - The URL to send the PUT request to.
 * @param {string} putValue - The data to send in the request body.
 * @param {HTMLElement} requestEle - Optional. The element to log the request text, if provided.
 * @param {HTMLElement} responseEle - Optional. The element to log the response text, if provided.
 * @param {HTMLElement} popupEle - Optional. The element to display an error popup, if provided.
 * @returns {Promise<boolean>} A promise that resolves with a boolean set true when the request
 * was successful, or rejects with a boolean set false when otherwise.
 */
async function doPutRequest( requestUrl, putValue, requestEle, responseEle, popupEle)
{
    // Since V7.1.0 we send a JSON command object here. Requires ILKCS version 7.3.0 or greater.
    const jsonPutObject = { "command": putValue  };
    const jsonPutValue = JSON.stringify( jsonPutObject );
    const requestText = `PUT="${requestUrl}", BODY="${jsonPutValue}"`;
    logRequest_( requestText, requestEle );

    try {
        const response = await fetch( requestUrl,{
            method: 'PUT',
            body: jsonPutValue,
            headers: { 'Content-Type': 'application/json' },
            credentials: 'include'
        });

        const escalatedResponse = escalateHttpResponseErrors_( response );
        const text = await escalatedResponse.text();

        const responseObj = JSON.parse( text );
        const responseText = document.formatter.Serialize( responseObj );
        logResponse_( responseText, responseEle );
        return true;
    }
    catch( error )
    {
        const responseText = error.message;
        logResponse_( responseText, responseEle );
        if (popupEle !== undefined) {
            popupEle.titleText = 'Interlock System Request Failure';
            popupEle.messageText = '🙁 Oh dear, the last request sent to the interlock system backend failed.';
            popupEle.requestText = requestText;
            popupEle.responseText = responseText;
        }
        return false;
    }
}

function escalateHttpResponseErrors_( response )
{
    if ( ! response.ok )
    {
        console.log( response );
        const statusMessage = lookupStatusMessage_( response.status );
        const statusText = statusMessage === "" ?  "" : " (" + statusMessage + ")";
        const errorMessage = response.headers.has( "X-ILK" ) ? response.headers.get( "X-ILK" ) : "";
        const extraText = errorMessage === "" ? "" : "\n\n" + errorMessage;
        const combinedMessage = "The HTTP server rejected the request with status code " + response.status + statusText + extraText;
        throw new Error( combinedMessage );
    }
    return response;
}

function lookupStatusMessage_( statusCode )
{
    switch( statusCode )
    {
        case 400:
            return "Bad Request";
        case 401:
            return "Unauthorized";
        case 402:
            return "Bad Request";
        case 403:
            return "Forbidden";
        case 404:
            return "Not Found";
        case 500:
            return "Internal Server Error";
        case 502:
            return "Bad Gateway";
        case 503:
            return "Service Unavailable";
        default:
            return "";
    }
}

function logRequest_( requestText, requestEle = undefined )
{
    console.log( requestText );
    if ( requestEle !== undefined )
    {
        requestEle.textContent = requestText;
    }
}

function logResponse_( responseText, responseEle = undefined )
{
    // 2023-06-22: Disable copious logging of response text (since all communication
    // with the backend server now goes through this module)
    const logResponseText = false;
    if ( logResponseText ) {
        console.log( responseText );
    }
    if ( responseEle !== undefined )
    {
        responseEle.textContent = responseText;
    }
}

