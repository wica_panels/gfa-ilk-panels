import {Formatter} from "fracturedjsonjs";

export const HTTP_STREAM_SUPPORT = {
    "doSubscribe" : (subscribeUrl, streamName, requestEle, responseEle ) => doSubscribe( subscribeUrl, streamName, requestEle, responseEle ),
    "doUnsubscribe" : ( responseEle) => doUnsubscribe( responseEle )
}

let eventSource = undefined;

// Subscribe Facility
function doSubscribe( subscribeUrl, streamName, requestEle, responseEle )
{
    requestEle.textContent = "GET=" + subscribeUrl;
    responseEle.textContent = "Subscribing to stream at url: '" + subscribeUrl + "' ...";

    // Assign a new event source
    eventSource = new EventSource( subscribeUrl, { withCredentials: true } );

    // Capture and log event stream heartbeat events
    eventSource.addEventListener( "ev-ilk-hearbeat", ev => console.log( ev.data ), false);

    // Capture and display events associated with the specified name
    eventSource.addEventListener( streamName, ev => {
        console.log( ev.data );
        const responseEntry = document.createElement( "DIV" );
        responseEntry.style.fontFamily = "monospace";
        responseEle.appendChild( responseEntry );
        const responseObj = JSON.parse( ev.data );
        responseEntry.textContent =  document.formatter.Serialize( responseObj );
        responseEle.scrollTop = responseEle.scrollHeight;
    }, false);

    // Capture and display stream open events
    eventSource.onopen= () => {
        responseEle.textContent = "Opening event stream at url: '" + subscribeUrl + "' ...";
    }

    // Capture and display stream error events
    eventSource.onerror = () => {
        responseEle.textContent = "The event stream had an error. Probably there was a connection failure.";
        eventSource.close();
        eventSource = undefined;
    }
}

// Unsubscribe Facility
function doUnsubscribe( responseEle )
{
    if ( eventSource !== undefined ) {
        const url = eventSource.url;
        console.log( "Closing " + url + " stream..." );
        eventSource.close();
        eventSource = undefined;
        const responseEntry = document.createElement( "DIV" );
        responseEntry.style.fontFamily = "monospace";
        responseEntry.textContent = "Stream closed.";
        responseEle.appendChild( responseEntry );
        responseEle.scrollTop = responseEle.scrollHeight;
    }
}

document.formatter = new Formatter();
document.formatter.maxInlineLength = 120;