
    /*-----------------------------------------------------------------------*/
    /* 1.0 MODULE IMPORT DEFINITIONS                                         */
    /*-----------------------------------------------------------------------*/

    // noinspection NpmUsedModulesInstalled
    import {ShadowDomHelper} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {HTML_ELEMENT_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {HTTP_SUPPORT} from "__JS_SUPPORT_FILE__";


    /*-----------------------------------------------------------------------*/
    /* 2.0 VARIABLE DEFINITIONS                                              */
    /*-----------------------------------------------------------------------*/

    const facilityId = "__FACILITY__".toLowerCase();
    const serverUrl = window.location.origin + "__ILKDB_SERVER_PATH_PREFIX__";

    const shadowDomHelper = new ShadowDomHelper( "database-tool-panel" );
    const shadowRoot = shadowDomHelper.getShadowRoot();

    const iocIdSelectorEle = shadowRoot.getElementById( 'ioc-select' );
    const sectionIdSelectorEle = shadowRoot.getElementById( 'section-select');
    const moduleIdSelectorEle = shadowRoot.getElementById( 'module-select' );
    const moduleInputIdSelectorEle = shadowRoot.getElementById( 'module-input-select');
    const moduleOutputIdSelectorEle = shadowRoot.getElementById( 'module-output-select');

    const requestEle = shadowRoot.getElementById( 'http-request' );
    const responseEle = shadowRoot.getElementById( 'http-response' );


    /*-----------------------------------------------------------------------*/
    /* 3.0 ILKDB SERVER GET DATA SUPPORT FUNCTIONS                           */
    /*-----------------------------------------------------------------------*/

    // Get Facility
    function getFacility( event )
    {
        const requestUrl = serverUrl + "/facilities/" + facilityId  + event.target.dataset.path;
        HTTP_SUPPORT.doGetRequest( requestUrl, requestEle, responseEle );
    }

    function getFacilityData( facility, path )
    {
        const requestUrl = serverUrl + "/facilities/" + facility + path;
        return fetch( requestUrl, { credentials: 'include'} ).then( resp => resp.json() );
    }

    // Get Facility Section
    function getFacilitySection( event )
    {
        // Note: unlike the other URI components the sectionId needs to be URI encoded since it may contain non-standard chars (eg '$', '&', '#' )
        const sectionId = sectionIdSelectorEle.value;
        const requestUrl = serverUrl + "/facilities/" + facilityId + "/sections/" + encodeURIComponent( sectionId ) + event.target.dataset.path;
        HTTP_SUPPORT.doGetRequest( requestUrl, requestEle, responseEle );
    }

    // Get Facility IOC
    function getFacilityIoc( event )
    {
        const iocId = iocIdSelectorEle.value;
        const requestUrl = serverUrl + "/facilities/" + facilityId + "/iocs/" + iocId + event.target.dataset.path;
        HTTP_SUPPORT.doGetRequest( requestUrl, requestEle, responseEle );
    }

    // Get Facility Module
    function getFacilityModule( event )
    {
        const moduleId = moduleIdSelectorEle.value;
        const requestUrl = serverUrl + "/facilities/" + facilityId + "/modules/" + moduleId + event.target.dataset.path;
        HTTP_SUPPORT.doGetRequest( requestUrl, requestEle, responseEle );
    }

    // Get Facility Module Input
    function getFacilityModuleInput( event )
    {
        const moduleId = moduleIdSelectorEle.value;
        const inputId = moduleInputIdSelectorEle.value;
        const requestUrl = serverUrl + "/facilities/" + facilityId + "/modules/" + moduleId + "/inputs/" + inputId + event.target.dataset.path;
        HTTP_SUPPORT.doGetRequest( requestUrl, requestEle, responseEle );
    }

    // Get Facility Module Output
    function getFacilityModuleOutput( event )
    {
        const moduleId = moduleIdSelectorEle.value;
        const outputId = moduleOutputIdSelectorEle.value;
        const requestUrl = serverUrl + "/facilities/" + facilityId + "/modules/" + moduleId + "/outputs/" + outputId + event.target.dataset.path;
        HTTP_SUPPORT.doGetRequest( requestUrl, requestEle, responseEle );
    }


    /*-----------------------------------------------------------------------*/
    /* 4.0 CHOOSE MODULE PANEL SUPPORT FUNCTIONS                             */
    /*-----------------------------------------------------------------------*/

    // Facility Module Change Handler
    async function fetchModuleInformation( event )
    {
        // Disable the module-dependant query buttons
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".module-input.request-button", false );
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".module-output.request-button", false );

        // Clear the module-dependant select elements
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleInputIdSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleOutputIdSelectorEle );

        // If a new module is selected...
        if ( event.target.selectedIndex !== 0 )
        {
            const moduleId = moduleIdSelectorEle.value;
            addSelectModuleInputMenuChoices( moduleInputIdSelectorEle, facilityId, "/modules/" + moduleId + "/inputs?detail=inputSignalNames" );
            addSelectModuleOutputMenuChoices( moduleOutputIdSelectorEle, facilityId, "/modules/" + moduleId + "/outputs?detail=outputSignalNames" );
            HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".module.request-button", true );
        }
    }


    /*-----------------------------------------------------------------------*/
    /* 5.0 HTML ELEMENT EVENT LISTENER SUPPORT                               */
    /*-----------------------------------------------------------------------*/

    const handlerMap = new Map();

    function registerHtmlEventHandlers()
    {
        handlerMap.set( "fetchModuleInformation", (event) => fetchModuleInformation( event ) );
        handlerMap.set( "getFacility", (event) => getFacility( event ) );
        handlerMap.set( "getFacilityIoc", (event) => getFacilityIoc( event ) );
        handlerMap.set( "getFacilitySection", (event) => getFacilitySection( event ) );
        handlerMap.set( "getFacilityModule", (event) => getFacilityModule( event ) );
        handlerMap.set( "getFacilityModuleInput", (event) => getFacilityModuleInput( event ) );
        handlerMap.set( "getFacilityModuleOutput", (event) => getFacilityModuleOutput( event ) );
    }

    function addHtmlEventListeners( elementClass, eventName )
    {
        // Find all elements of the specified class
        const allElements = shadowDomHelper.querySelectorAll( "." + elementClass );

        for ( const ele of allElements )
        {
            // If the data-func attribute is present attempt to find a function with
            // the corresponding name and register it as an event handler
            if (  ele.dataset[ "func" ] !== undefined )
            {
                const funcName = ele.dataset[ "func" ];

                // If a function of the required name has been registered in the
                // handler map attach the handler function as an event listener
                if ( handlerMap.has( funcName ) )
                {
                    const func = handlerMap.get( funcName );
                    ele.addEventListener( eventName, func );
                }
                else
                {
                    console.error( "The function named '" + funcName + "' is not defined !" );
                }
            }
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 6.0 HTML SELECT ELEMENT MENU CHOICES SUPPORT                          */
    /*-----------------------------------------------------------------------*/

    function addGroupedSelectIocMenuChoices( selectWidget, facilityId, path )
    {
        getFacilityData( facilityId, path ).then( json => {
            // Example returned data:
            //    { "AHK1" : [ "ZTEST-CVME-ILK01" ], "VAR" : [ "ZTEST-CVME-ILK01" ],
            //       "BN5" : [ "ZTEST-CVME-ILK01" ], "BW2" : [ "ZTEST-CVME-ILK01" ] }
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromIocId( json );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionArrayObj, selectWidget, "Please select IOC", "Section" )
        } );
    }

    function addGroupedSelectSectionIdMenuChoices( selectWidget, facilityId, path )
    {
        getFacilityData( facilityId, path ).then( json => {
            // Example returned data:
            //    { "BEAM" : [ "BN5", "BW2", "AHK1" ], "TEST" : [ "VAR" ] }
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromSectionId( json );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionArrayObj, selectWidget, "Please select Section", "Group" )
        } );
    }

    function addGroupedSelectModuleIdMenuChoices( selectWidget, facilityId, path )
    {
        getFacilityData( facilityId, path ).then( json => {
            // Example returned data:
            //   [ "002004", "002006",   "002037",  "002097", "3001001", "3001002", "3001003", "3002001",
            //     "3002002","3002003", "3002004", "3002005", "3003001", "3003002", "3003003"]
            const optionObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModuleId( json );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionObj, selectWidget, "Please select Module", "Section" );
        } );
    }

    function addSelectModuleInputMenuChoices( selectWidget, facilityId, path )
    {
        getFacilityData( facilityId, path ).then( json => {
            // Example returned data:
            //    [ { "sectionId" : "BN5","moduleId" : "002004","portId" : "1","ok" : "BN5 ILK4","nok" : "" } ]
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildSelectWidgetOptionsFromSignalNameData( json );
            HTML_ELEMENT_SUPPORT.addSelectWidgetOptions( optionArrayObj, selectWidget, "Please select Input", "Port" );
        } );
    }

    function addSelectModuleOutputMenuChoices( selectWidget, facilityId, path )
    {
        getFacilityData( facilityId, path ).then( json => {
            // Example returned data:
            //    [ { "sectionId" : "BN5","moduleId" : "002004","portId" : "1","ok" : "BN5 ILK4","nok" : "" } ]
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildSelectWidgetOptionsFromSignalNameData( json );
            HTML_ELEMENT_SUPPORT.addSelectWidgetOptions( optionArrayObj, selectWidget, "Please select Output", "Port" );
        } );
    }


    /*-----------------------------------------------------------------------*/
    /* 7.0 PAGE INITIALISATION                                               */
    /*-----------------------------------------------------------------------*/

    async function loadFacilityInformation( facilityId )
    {
        // Clear the facility-dependant select element (= all of them !)
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( iocIdSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( sectionIdSelectorEle);
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleIdSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleInputIdSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleOutputIdSelectorEle );

        // Clear the text areas which report on the outcomes of a previous transaction
        requestEle.textContent = "";
        responseEle.textContent = "";

        // Configure the select elements for facility, section and module related queries
        addGroupedSelectIocMenuChoices( iocIdSelectorEle, facilityId, "/iocs?arrangeBy=sectionName" );
        addGroupedSelectSectionIdMenuChoices( sectionIdSelectorEle, facilityId, "/groups/sections?arrangeBy=groupName" );
        addGroupedSelectModuleIdMenuChoices( moduleIdSelectorEle, facilityId, "/modules" );
    }

    function init()
    {
        // Disable all query buttons except Facility Specific ones
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".request-button", false );
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".facility.request-button", true );

        // Set up the SINGLE button reflectors to enable the targeted buttons when the select widget has an active selection.
        HTML_ELEMENT_SUPPORT.registerHtmlSelectElementButtonStateReflector( shadowRoot, sectionIdSelectorEle, ".section.request-button" );
        HTML_ELEMENT_SUPPORT.registerHtmlSelectElementButtonStateReflector( shadowRoot, iocIdSelectorEle, ".ioc.request-button" );
        HTML_ELEMENT_SUPPORT.registerHtmlSelectElementButtonStateReflector( shadowRoot, moduleIdSelectorEle, ".module.request-button" );
        HTML_ELEMENT_SUPPORT.registerHtmlSelectElementButtonStateReflector( shadowRoot, moduleInputIdSelectorEle, ".module-input.request-button"  );
        HTML_ELEMENT_SUPPORT.registerHtmlSelectElementButtonStateReflector( shadowRoot, moduleOutputIdSelectorEle, ".module-output.request-button" );

        // Add support for invoking the relevant function on button click or select widget change§
        registerHtmlEventHandlers();
        addHtmlEventListeners( "select", "change" );
        addHtmlEventListeners( "request-button", "click" );

        // Load Facility configuration information from the remote server and use it set up this page.
        loadFacilityInformation( facilityId );
    }

    init();
