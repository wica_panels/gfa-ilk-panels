console.log( "command-tool.template.js is running...")

    /*-----------------------------------------------------------------------*/
    /* 1.0 MODULE IMPORT DEFINITIONS                                         */
    /*-----------------------------------------------------------------------*/

    // noinspection NpmUsedModulesInstalled
    import {ShadowDomHelper} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {HTML_ELEMENT_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {HTTP_SUPPORT} from "__JS_SUPPORT_FILE__";
    // noinspection NpmUsedModulesInstalled
    import {HTTP_STREAM_SUPPORT} from "__JS_SUPPORT_FILE__";


    /*-----------------------------------------------------------------------*/
    /* 2.0 VARIABLE DEFINITIONS                                              */
    /*-----------------------------------------------------------------------*/

    const facilityId = "__FACILITY__".toLowerCase();
    const serverUrl = window.location.origin + "__ILKCS_SERVER_PATH_PREFIX__";

    const shadowDomHelper = new ShadowDomHelper( "command-tool-panel" );
    const shadowRoot = shadowDomHelper.getShadowRoot();

    // Facility-related definitions
    const facilityPrimaryModeSelectorEle = shadowRoot.getElementById( 'facility-primary-mode-select' );
    const facilitySecondaryModeSelectorEle = shadowRoot.getElementById( 'facility-secondary-mode-select' );
    const facilityTertiaryModeSelectorEle = shadowRoot.getElementById( 'facility-tertiary-mode-select' );
    const facilityModeOptionSelectorEle = shadowRoot.getElementById( 'facility-mode-option-select' );

    // Section-related definitions
    const sectionIdSelectorEle = shadowRoot.getElementById( 'section-select' );
    const sectionModeSelectorEle = shadowRoot.getElementById( 'section-mode-select' );
    const sectionModeOptionSelectorEle = shadowRoot.getElementById( 'section-mode-option-select' );

    // Module-related definitions
    const moduleIdSelectorEle = shadowRoot.getElementById( 'module-select' );
    const moduleInputIdSelectorEle = shadowRoot.getElementById( 'module-input-select' );
    const moduleOutputIdSelectorEle = shadowRoot.getElementById( 'module-output-select' );
    const moduleModeSelectorEle = shadowRoot.getElementById( 'module-mode-select-1' );
    const moduleModeOptionSelectorEle = shadowRoot.getElementById( 'module-mode-select-2' );

    const requestEle = shadowRoot.getElementById( 'http-request' );
    const simpleResponseEle = shadowRoot.getElementById( 'simple-http-response' );
    const streamingResponseEle = shadowRoot.getElementById( 'streaming-http-response' );


    /*-----------------------------------------------------------------------*/
    /* 3.0 BUTTON ENABLE/DISABLE STATE REFLECTION HANDLER SUPPORT            */
    /*-----------------------------------------------------------------------*/

    function registerModuleTypeSpecificButtonStateReflector( selectWidget, buttonSelector )
    {
        function getFacilityModuleHardwareType()
        {
            const module = moduleIdSelectorEle.value;
            const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + module + "?detail=hardwareType";
            return HTTP_SUPPORT.doGetRequest( requestUrl, requestEle, simpleResponseEle );
        }

        selectWidget.addEventListener( "change", () => {
            if(  selectWidget.selectedIndex === 0 )
            {
                HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, buttonSelector, false)
            }
            else {
                getFacilityModuleHardwareType().then( hardwareType => {
                    const buttonClassToDeselect = hardwareType.includes("USI2") ? "rpm" : "usi2";
                    HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, buttonSelector, true );
                    HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, buttonSelector + "." + buttonClassToDeselect, false);
                })
            }
        } );
    }

    /*-----------------------------------------------------------------------*/
    /* 4.0 ILKCS SERVER HTTP GET/PUT SUPPORT FUNCTIONS                       */
    /*-----------------------------------------------------------------------*/

    async function getFacilityData( facilityId, path )
    {
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + path;
        return HTTP_SUPPORT.doGetRequest( requestUrl, requestEle, simpleResponseEle );
    }

    async function putFacilityMode()
    {
        function joinWithSeparator( separator, ...args)
        {
            // filter out empty strings
            const nonEmptyArgs = args.filter(arg => arg !== "");
            return nonEmptyArgs.join( separator );
        }

        const selectedPrimaryModeIndex = facilityPrimaryModeSelectorEle.selectedIndex;
        const selectedPrimaryMode = facilityPrimaryModeSelectorEle.options[selectedPrimaryModeIndex];
        const settingRequest1 = (selectedPrimaryModeIndex > 0) ? selectedPrimaryMode.parentNode.id + ":" + selectedPrimaryMode.value : "";

        const selectedSecondaryModeIndex = facilitySecondaryModeSelectorEle.selectedIndex;
        const selectedSecondaryMode = facilitySecondaryModeSelectorEle.options[selectedSecondaryModeIndex];
        const settingRequest2 = (selectedSecondaryModeIndex > 0) ? selectedSecondaryMode.parentNode.id + ":" + selectedSecondaryMode.value : "";

        const selectedTertiaryModeIndex = facilityTertiaryModeSelectorEle.selectedIndex;
        const selectedTertiaryMode = facilityTertiaryModeSelectorEle.options[selectedTertiaryModeIndex];
        const settingRequest3 = (selectedTertiaryModeIndex > 0) ? selectedTertiaryMode.parentNode.id + ":" + selectedTertiaryMode.value : "";

        const selectedOptionIndex = facilityModeOptionSelectorEle.selectedIndex;
        const selectedOption = facilityModeOptionSelectorEle.options[selectedOptionIndex];
        const settingRequest4 = (selectedOptionIndex > 0) ? selectedOption.parentNode.id + ":" + selectedOption.value : "";

        const requestedMode = joinWithSeparator( ";", settingRequest1, settingRequest2, settingRequest3, settingRequest4 );
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/mode";
        return HTTP_SUPPORT.doPutRequest( requestUrl, requestedMode, requestEle, simpleResponseEle );
    }

    async function putFacilitySectionMode()
    {
        const sectionId = sectionIdSelectorEle.value;
        const selectedModeIndex = sectionModeSelectorEle.selectedIndex;
        const selectedMode = sectionModeSelectorEle.options[selectedModeIndex];
        const settingRequest1 = (selectedModeIndex > 0) ? selectedMode.parentNode.id + ":" + selectedMode.value : "";

        const selectedOptionIndex = sectionModeOptionSelectorEle.selectedIndex;
        const selectedOption = sectionModeOptionSelectorEle.options[selectedOptionIndex];
        const settingRequest2 = (selectedOptionIndex > 0) ? selectedOption.parentNode.id + ":" + selectedOption.value : "";

        const separator = ( settingRequest1 === "" ) || ( settingRequest2 === "" ) ? "" : ";";
        const requestedMode = settingRequest1 + separator + settingRequest2;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/sections/" + encodeURIComponent( sectionId ) + "/mode";
        return HTTP_SUPPORT.doPutRequest( requestUrl, requestedMode, requestEle, simpleResponseEle );
    }

    async function putFacilityModuleMode()
    {
        const moduleId = moduleIdSelectorEle.value;
        const selectedIndex1 = moduleModeSelectorEle.selectedIndex;
        const selectedOption1 = moduleModeSelectorEle.options[ selectedIndex1 ];
        const settingRequest1 =  ( selectedIndex1 > 0 ) ? selectedOption1.parentNode.id + ":" + selectedOption1.value : "";

        const selectedIndex2 = moduleModeOptionSelectorEle.selectedIndex;
        const selectedOption2 = moduleModeOptionSelectorEle.options[ selectedIndex2 ];
        const settingRequest2 =  ( selectedIndex2 > 0 ) ?  selectedOption2.parentNode.id + ":" + selectedOption2.value : "";

        const separator = ( settingRequest1 === "" ) || ( settingRequest2 === "" ) ? "" : ";";
        const requestedMode = settingRequest1 + separator + settingRequest2;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + "/mode";
        return HTTP_SUPPORT.doPutRequest( requestUrl, requestedMode, requestEle, simpleResponseEle );
    }

    function unsubscribeFacilityStream()
    {
        HTTP_STREAM_SUPPORT.doUnsubscribe( streamingResponseEle );
    }

    /*-----------------------------------------------------------------------*/
    /* 5.0 ILKCS SERVER HTTP GET/PUT EVENT SUPPORT FUNCTIONS                 */
    /*-----------------------------------------------------------------------*/

    async function getFacility( event )
    {
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId  + event.target.dataset.path;
        return HTTP_SUPPORT.doGetRequest( requestUrl, requestEle, simpleResponseEle );
    }

    async function getFacilitySection( event )
    {
        // Note: unlike the other URI components the sectionId needs to be URI encoded since it may contain non-standard chars (eg '$', '&', '#' )
        const sectionId = sectionIdSelectorEle.value;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/sections/" + encodeURIComponent( sectionId ) + event.target.dataset.path;
        requestEle.textContent = "GET=" + requestUrl;
        return HTTP_SUPPORT.doGetRequest( requestUrl, requestEle, simpleResponseEle ).then().catch();
    }

    async function getFacilityModule( event )
    {
        const moduleId = moduleIdSelectorEle.value;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + event.target.dataset.path;
        return HTTP_SUPPORT.doGetRequest( requestUrl, requestEle, simpleResponseEle ).then().catch();
    }

    async function putFacility( event )
    {
        const putValue = event.target.dataset.putValue;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + event.target.dataset.path;
        return HTTP_SUPPORT.doPutRequest( requestUrl, putValue, requestEle, simpleResponseEle );
    }

    async function putFacilitySection( event )
    {
        const sectionId = sectionIdSelectorEle.value;
        const putValue = event.target.dataset.putValue;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/sections/" + encodeURIComponent( sectionId ) + event.target.dataset.path;
        return HTTP_SUPPORT.doPutRequest( requestUrl, putValue, requestEle, simpleResponseEle );
    }

    async function putFacilityModule( event )
    {
        const moduleId = moduleIdSelectorEle.value;
        const putValue = event.target.dataset.putValue;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + event.target.dataset.path;
        return HTTP_SUPPORT.doPutRequest( requestUrl, putValue, requestEle, simpleResponseEle );
    }

    async function putFacilityModuleInput( event )
    {
        const moduleId = moduleIdSelectorEle.value;
        const inputId = moduleInputIdSelectorEle.value;
        const putValue = event.target.dataset.putValue;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + "/inputs/" + inputId + event.target.dataset.path;
        return HTTP_SUPPORT.doPutRequest( requestUrl, putValue, requestEle, simpleResponseEle );
    }

    async function putFacilityModuleOutput( event )
    {
        const moduleId = moduleIdSelectorEle.value;
        const outputId = moduleOutputIdSelectorEle.value;
        const putValue = event.target.dataset.putValue;
        const requestUrl = serverUrl + "/v3/facilities/" + facilityId + "/modules/" + moduleId + "/outputs/" + outputId + event.target.dataset.path;
        return HTTP_SUPPORT.doPutRequest( requestUrl, putValue, requestEle, simpleResponseEle );
    }

    function subscribeFacilityStream( event )
    {
        const subscribeUrl = serverUrl + "/v3/facilities/" + facilityId + event.target.dataset.path;
        const streamName = event.target.dataset.streamName;
        HTTP_STREAM_SUPPORT.doSubscribe( subscribeUrl, streamName, requestEle, streamingResponseEle );
    }

    /*-----------------------------------------------------------------------*/
    /* 6.0 SECTION REQUESTS PANEL SUPPORT FUNCTIONS                          */
    /*-----------------------------------------------------------------------*/

    // Section Change Handler
    async function handleSectionSelectionChanged( event )
    {
        // Clear the section-dependant select elements. This is required to clear the options if the
        // selection changes to select nothing.
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( sectionModeSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( sectionModeOptionSelectorEle );

        // Clear the section-dependant button elements
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot,".section-mode.request-button", false );

        // If a new section is selected...
        if ( event.target.selectedIndex !== 0)
        {
            // Enable the select elements for section-related queries
            const sectionId = sectionIdSelectorEle.value;
            await addGroupedSelectSectionModeMenuChoices( sectionModeSelectorEle, facilityId, "/sections/" + encodeURIComponent( sectionId ) + "/mode?detail=choices&filter=primary" );
            await addGroupedSelectSectionModeOptionMenuChoices( sectionModeOptionSelectorEle, facilityId, "/sections/" + encodeURIComponent( sectionId )  + "/mode?detail=choices&filter=optional" );
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 7.0 CHOOSE MODULE PANEL SUPPORT FUNCTIONS                             */
    /*-----------------------------------------------------------------------*/

    // Module Change Handler
    async function handleModuleSelectionChanged( event )
    {
        // Clear the module-dependant select elements This is required to clear the options if the
        // selection changes to select nothing.
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleInputIdSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleOutputIdSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleModeSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleModeOptionSelectorEle );

        // Clear the module dependant button elements
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".module-bridge.request-button", false );
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".module-input.request-button", false );
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".module-output.request-button", false );

        // If a new module is selected...
        if ( event.target.selectedIndex !== 0)
        {
            // Enable the select elements for module-related queries
            const moduleId = moduleIdSelectorEle.value;
            await addGroupedSelectModuleModeMenuChoices( moduleModeSelectorEle, facilityId, "/modules/" + moduleId + "/mode?detail=choices&filter=primary" );
            await addGroupedSelectModuleModeOptionMenuChoices( moduleModeOptionSelectorEle, facilityId, "/modules/" + moduleId + "/mode?detail=choices&filter=other" );
            await addSelectModuleInputMenuChoices( moduleInputIdSelectorEle, facilityId, "/modules/" + moduleId + "/inputs/signals" );
            await addSelectModuleOutputMenuChoices( moduleOutputIdSelectorEle, facilityId, "/modules/" + moduleId + "/outputs/signals" );
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 8.0 HTML ELEMENT EVENT LISTENER SUPPORT                               */
    /*-----------------------------------------------------------------------*/

    const handlerMap = new Map();

    function registerHtmlEventHandlers()
    {
        handlerMap.set( "handleSectionSelectionChanged", (event) => handleSectionSelectionChanged( event ) );
        handlerMap.set( "handleModuleSelectionChanged", (event) => handleModuleSelectionChanged( event ) );
        handlerMap.set( "getFacility", (event) => getFacility( event ) );
        handlerMap.set( "putFacility", (event) => putFacility( event ) );
        handlerMap.set( "putFacilityMode", (event) => putFacilityMode(event) );
        handlerMap.set( "subscribeFacility", (event) => subscribeFacilityStream( event ) );
        handlerMap.set( "unsubscribeFacility", (event) => unsubscribeFacilityStream( event ) );
        handlerMap.set( "getFacilitySection", (event) => getFacilitySection( event ) );
        handlerMap.set( "putFacilitySection", (event) => putFacilitySection( event ) );
        handlerMap.set( "putFacilitySectionMode", (event) => putFacilitySectionMode( event ) );
        handlerMap.set( "getFacilityModule", (event) => getFacilityModule( event ) );
        handlerMap.set( "putFacilityModule", (event) => putFacilityModule( event ) );
        handlerMap.set( "putFacilityModuleMode", (event) => putFacilityModuleMode( event ) );
        handlerMap.set( "putFacilityModuleInput", (event) => putFacilityModuleInput( event ) );
        handlerMap.set( "putFacilityModuleOutput", (event) => putFacilityModuleOutput( event ) );
    }

    function addHtmlEventListeners( elementClass, eventName )
    {
        // Find all elements of the specified class
        const allElements = shadowDomHelper.querySelectorAll( "." + elementClass );

        for ( const ele of allElements )
        {
            // If the data-func attribute is present attempt to find a function with
            // the corresponding name and register it as an event handler
            if (  ele.dataset[ "func" ] !== undefined )
            {
                const funcName = ele.dataset[ "func" ];

                // If a function of the required name has been registered in the
                // handler map attach the handler function as an event listener
                if ( handlerMap.has( funcName ) )
                {
                    const func = handlerMap.get( funcName );
                    ele.addEventListener( eventName, func );
                }
                else
                {
                    console.error( "The function named '" + funcName + "' is not defined !" );
                }
            }
        }
    }

    /*-----------------------------------------------------------------------*/
    /* 9.0 HTML SELECT ELEMENT MENU CHOICES SUPPORT                          */
    /*-----------------------------------------------------------------------*/

    async function addGroupedSelectFacilityModeMenuChoices( selectWidget, facilityId, path )
    {
        // Example returned data:
        //    {"Main-Path":["BW2","BN5"]}
        //    {"AHK1-Path":["Target","Inactive","Not-in-use"]}
        //    {"VAR-Path":["Beam Allow","Beam Stop","Beam Left","Beam Right","Beam Split","Inactive"]}
        try {
            const facilityData = await getFacilityData( facilityId, path );
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            const pathName = Object.keys( optionArrayObj ).length === 1 ? Object.keys( optionArrayObj )[ 0 ] : "Facility Mode";
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionArrayObj, selectWidget, "Please select " + pathName, "" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectFacilityModeOptionMenuChoices( selectWidget, facilityId, path )
    {
        // Example returned data:
        //    {"DIA Mode":["DIA-Off","DIA-On"],"Intensity":["Normal","High"],"MCH Mode":["MCH-Off","MCH-On"]}
        try {
            const facilityData = await getFacilityData( facilityId, path );
            const optionObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionObj, selectWidget, "Please select Mode Option" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectSectionIdMenuChoices( selectWidget, facilityId, path )
    {
        // Example returned data:
        //    { "BEAM" : [ "BN5", "BW2", "AHK1" ], "TEST" : [ "VAR" ] }
        try {
            const facilityData = await getFacilityData( facilityId, path );
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromSectionId( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionArrayObj, selectWidget, "Please select Section", "Group" )
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectSectionModeMenuChoices( selectWidget, facilityId, path )
    {
        // Example returned data:
        //    {"Mode":["O (Inactive)","D (Durch)"]}
        try {
            const facilityData = await getFacilityData( facilityId, path );
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionArrayObj, selectWidget, "Please select Section Mode" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectSectionModeOptionMenuChoices( selectWidget, facilityId, path )
    {
        // Example returned data:
        //    {"DIA Mode":["DIA-Off","DIA-On"],"Intensity":["Normal","High"],"MCH Mode":["MCH-Off","MCH-On"]}
        try {
            const facilityData = await getFacilityData( facilityId, path );
            const optionObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionObj, selectWidget, "Please select Section Mode Option", "", "" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectModuleIdMenuChoices( selectWidget, facilityId, path ) {
        // Example returned data:
        //    ["002004","002006","002037","002097","3001001" ]
        try {
            const facilityData = await getFacilityData( facilityId, path);
            const optionObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModuleId(facilityData);
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions(optionObj, selectWidget, "Please select Module", "", "");
        }
        catch (error) {
            console.log(error);
        }
    }

    async function addGroupedSelectModuleModeMenuChoices( selectWidget, facilityId, path )
    {
        // Example returned data:
        //    {"MODE":["SF-Inactive","SF-Stop","SF-Unknown-Mode2","SF-Open"]}
        try {
            const facilityData = await getFacilityData( facilityId, path );
            const optionObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionObj, selectWidget,"Please select Module Mode", "", "" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addGroupedSelectModuleModeOptionMenuChoices( selectWidget, facilityId, path )
    {
        // Example returned data:
        //    {"BT":["Off","On"],"DIA":["Off","On"],"KNT":["Off","On"],"MCH":["Off","On"],"MR":["Off","On"]}
        try {
            const facilityData = await getFacilityData( facilityId, path );
            const optionObj = HTML_ELEMENT_SUPPORT.buildGroupedSelectWidgetOptionsFromModeProperties( facilityData );
            HTML_ELEMENT_SUPPORT.addGroupedSelectWidgetOptions( optionObj, selectWidget,"Please select Module Mode Option", "", "" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addSelectModuleInputMenuChoices( selectWidget, facilityId, path )
    {
        // Example returned data:
        //    [{"sectionId":"VAR","moduleId":"002006","portId":"1","type":"input","activeName":"",
        //      "okName":"INPUT1","nokName":"","epicsInfoChannelName":"SOMEPV1","epicsInfoChannelValue":"UNKNOWN",
        //      "tstrigger":"","tsclear":"2023-04-14T13:48:55.982713187",
        //      "port-fault":"OK","port-signal":"NOK","logic-signal":"NOK","bridge":"RPM_CLR","interlock":"OK",
        //      "view":"DEFAULT","bridgePolicy":"SW","tracePolicy":"IGNORE","comment":""}, ...
        //    ]
        try {
            const facilityData = await getFacilityData( facilityId, path );
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildSelectWidgetOptionsFromSignalData( facilityData );
            HTML_ELEMENT_SUPPORT.addSelectWidgetOptions( optionArrayObj, selectWidget, "Please select Input" );
        }
        catch( error ) {
            console.log( error );
        }
    }

    async function addSelectModuleOutputMenuChoices( selectWidget, facility, path )
    {
        // Example returned data:
        //    [{"sectionId":"VAR","moduleId":"002006","portId":"1","type":"output","activeName":"",
        //    "okName":"OUTPUT1","nokName":"","tstrigger":"2023-04-14T13:48:55.983280478","tsclear":"",
        //    "port-fault":"OK","port-signal":"NOK","logic-signal":"NOK","bridge":"RPM_CLR","interlock":"ILK",
        //    "view":"DEFAULT","bridgePolicy":"SW","traceable":false,"important":false,"comment":""},
        //    ]
        try {
            const facilityData = await getFacilityData( facilityId, path );
            const optionArrayObj = HTML_ELEMENT_SUPPORT.buildSelectWidgetOptionsFromSignalData( facilityData );
            HTML_ELEMENT_SUPPORT.addSelectWidgetOptions( optionArrayObj, selectWidget, "Please select Output" );
        }
        catch( error ) {
            console.log( error );
        }
    }


    /*-----------------------------------------------------------------------*/
    /* 10.0 PAGE INITIALISATION                                              */
    /*-----------------------------------------------------------------------*/

    async function loadFacilityInformation( facilityId )
    {
        // Clear the facility-dependant select element (= all of them !)
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( sectionIdSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( sectionModeSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( sectionModeOptionSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleIdSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleInputIdSelectorEle);
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleOutputIdSelectorEle);
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleModeSelectorEle );
        HTML_ELEMENT_SUPPORT.removeSelectWidgetOptions( moduleModeOptionSelectorEle );

        // Clear the text areas which report on the outcomes of a previous transaction
        requestEle.textContent = "";
        simpleResponseEle.textContent = "";
        streamingResponseEle.textContent = "";

        // Configure the select elements for facility, section and module related queries
        await addGroupedSelectFacilityModeMenuChoices( facilityPrimaryModeSelectorEle, facilityId, "/mode?detail=choices&filter=primary" );
        await addGroupedSelectFacilityModeMenuChoices( facilitySecondaryModeSelectorEle, facilityId, "/mode?detail=choices&filter=secondary" );
        await addGroupedSelectFacilityModeMenuChoices( facilityTertiaryModeSelectorEle, facilityId, "/mode?detail=choices&filter=tertiary" );
        await addGroupedSelectFacilityModeOptionMenuChoices( facilityModeOptionSelectorEle, facilityId, "/mode?detail=choices&filter=optional" );
        await addGroupedSelectSectionIdMenuChoices( sectionIdSelectorEle, facilityId, "?detail=sectionIdsByGroupId", );
        await addGroupedSelectModuleIdMenuChoices( moduleIdSelectorEle, facilityId, "?detail=moduleIds" );
    }

    async function init()
    {
        // Disable all query buttons except Facility Specific ones
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".request-button", false );
        HTML_ELEMENT_SUPPORT.setButtonEnableState( shadowRoot, ".facility.request-button", true );

        // Set up the QUAD button reflector to enable the targeted button if any of the select widgets has an active selection.
        const anySelectedPredicate = (a,b,c,d)  => (a || b || c || d )
        HTML_ELEMENT_SUPPORT.registerHtmlSelectElementQuadButtonStateReflector( shadowRoot, facilityPrimaryModeSelectorEle, facilitySecondaryModeSelectorEle,
            facilityTertiaryModeSelectorEle, facilityModeOptionSelectorEle, ".facility-mode.request-button", anySelectedPredicate );

        // Set up the PAIR button reflector to enable the targeted button if any of the select widgets has an active selection.
        const eitherSelectedPredicate = (a,b) => a || b;
        HTML_ELEMENT_SUPPORT.registerHtmlSelectElementPairButtonStateReflector( shadowRoot, sectionModeSelectorEle,sectionModeOptionSelectorEle, ".section-mode.request-button", eitherSelectedPredicate );
        HTML_ELEMENT_SUPPORT.registerHtmlSelectElementPairButtonStateReflector( shadowRoot, moduleModeSelectorEle, moduleModeOptionSelectorEle, ".module-mode.request-button", eitherSelectedPredicate );

        // Set up the SINGLE button reflectors to enable the targeted buttons when the select widget has an active selection.
        HTML_ELEMENT_SUPPORT.registerHtmlSelectElementButtonStateReflector( shadowRoot, sectionIdSelectorEle, ".section.request-button" );
        HTML_ELEMENT_SUPPORT.registerHtmlSelectElementButtonStateReflector( shadowRoot, moduleIdSelectorEle, ".module.request-button" );

        // Register exotic (local) handlers to reflect select widget state to buttons
        registerModuleTypeSpecificButtonStateReflector( moduleIdSelectorEle, ".module-bridge.request-button" );
        registerModuleTypeSpecificButtonStateReflector( moduleInputIdSelectorEle, ".module-input.request-button" );
        registerModuleTypeSpecificButtonStateReflector( moduleOutputIdSelectorEle, ".module-output.request-button" );

        // Add support for invoking the relevant function on button click or select widget change
        registerHtmlEventHandlers();
        addHtmlEventListeners( "select", "change" );
        addHtmlEventListeners( "request-button", "click" );

        // Load Facility configuration information from the remote server and use it set up this page.
        await loadFacilityInformation( facilityId );
    }

    await init();
