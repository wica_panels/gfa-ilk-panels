console.debug( "Executing script in wc-support.js module...");

import {WicaDataElement} from "@psi/ilk-wc-factory/wica/wica-data-element.js";
import {WicaDataGroup} from "@psi/ilk-wc-factory/wica/wica-data-group.js";
import {WicaChannelWriter} from "@psi/ilk-wc-factory/wica/wica-channel-writer.js";

import {WebComponentEventViewer} from "@psi/ilk-wc-factory/wc/wc-event-viewer.js";
import {WebComponentTableViewer} from "@psi/ilk-wc-factory/wc/wc-table-viewer.js";

import {IlkModuleStateViewer} from "@psi/ilk-wc-factory/gen/ilk-module-state-viewer.js";
import {IlkRegisterDecoder} from "@psi/ilk-wc-factory/gen/ilk-register-decoder.js";
import {IlkModuleDecoder} from "@psi/ilk-wc-factory/gen/ilk-module-decoder.js";

import {SiioModule} from "@psi/ilk-wc-factory/siio/siio-module.js";
import {SiioModuleRegisterView} from "@psi/ilk-wc-factory/siio/siio-module-register-view.js";
import {SiioModuleStateView} from "@psi/ilk-wc-factory/siio/siio-module-state-view.js";

import {RpmipModule} from "@psi/ilk-wc-factory/rpmip/rpmip-module.js";
import {RpmipModuleRegisterView} from "@psi/ilk-wc-factory/rpmip/rpmip-module-register-view.js";
import {RpmipModuleStateView} from "@psi/ilk-wc-factory/rpmip/rpmip-module-state-view.js";

import {Usi2Module} from "@psi/ilk-wc-factory/usi2/usi2-module.js";
import {Usi2Common} from "@psi/ilk-wc-factory/usi2/usi2-common.js";
import {Usi2System} from "@psi/ilk-wc-factory/usi2/usi2-system.js";
import {Usi2Board} from "@psi/ilk-wc-factory/usi2/usi2-board.js";
import {Usi2ModuleStateView} from "@psi/ilk-wc-factory/usi2/usi2-module-state-view.js";
import {Usi2SystemRegisterView} from "@psi/ilk-wc-factory/usi2/usi2-system-register-view.js";
import {Usi2CommonRegisterView} from "@psi/ilk-wc-factory/usi2/usi2-common-register-view.js";
import {Usi2ModuleRegisterView} from "@psi/ilk-wc-factory/usi2/usi2-module-register-view.js";
import {IlkSignalView} from "@psi/ilk-wc-factory/gen/ilk-signal.js";

export {
    WicaDataElement,
    WicaDataGroup,
    WicaChannelWriter,
    SiioModule,
    RpmipModule,
    Usi2Module,
    Usi2Common,
    Usi2System,
    Usi2Board,
    WebComponentEventViewer,
    WebComponentTableViewer,
    IlkModuleStateViewer,
    IlkRegisterDecoder,
    IlkModuleDecoder,
    SiioModuleRegisterView,
    SiioModuleStateView,
    RpmipModuleRegisterView,
    RpmipModuleStateView,
    Usi2SystemRegisterView,
    Usi2CommonRegisterView,
    Usi2ModuleRegisterView,
    Usi2ModuleStateView,
    IlkSignalView
}
