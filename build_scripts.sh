#!/bin/bash

# Script starts here...
# Exit immediately if anything unexpected happens.
set -e

#-------------------------------------------------------------------------------
# PRIVATE FUNCTIONS
#-------------------------------------------------------------------------------

# Sets up a local worktree named 'dist_xxx' to track the remote branch named 'dist_xxx'.
function __dist_worktree_create() {
  if [[ "$1" == "dist_ilktest_dev" || "$1" == "dist_ilktest_prod" || "$1" == "dist_hipa_prod"  || "$1" == "dist_ists_prod" || "$1" == "dist_proscan_prod" ]]; then
     git worktree add -f "$1" "$1"
  else
     echo "Sets up a local worktree named 'dist_xxx' to track the remote branch named 'dist_xxx'."
  fi
}

# Pulls the latest information from the remote branch named 'dist_xxx' to update the local worktree named 'dist_xxx'.
# Currently (2021-06-05) Not Used.
function __dist_pull() {
  if [[ "$1" == "dist_ilktest_dev" || "$1" == "dist_ilktest_prod" || "$1" == "dist_hipa_prod" || "$1" == "dist_ists_prod" || "$1" == "dist_proscan_prod" ]]; then
     cd "$1"
     git pull
  else
     echo "Pulls the latest information from the remote branch named 'dist_xxx' to update the local worktree named 'dist_xxx'."
  fi
}

# Assembles the data to be published for the local worktree named 'dist_xxx'.
function __dist_assemble() {
  if [[ "$1" == "dist_ilktest_dev" ]]; then
     cd "$1"
     rm -fr ./*
     cp -R ../build/ilktest_dev/* .
     cp -R ../src/resources/docs .
     cd ..
  elif [[ "$1" == "dist_ilktest_prod" ]]; then
     cd "$1"
     rm -fr ./*
     cp -R ../build/ilktest_prod/* .
     cp -R ../src/resources/docs .
     cd ..
  elif [[ "$1" == "dist_hipa_prod" ]]; then
     cd "$1"
     rm -fr ./*
     cp -R ../build/hipa_prod/* .
     cp -R ../src/resources/docs .
     cd ..
  elif [[ "$1" == "dist_ists_prod" ]]; then
     cd "$1"
     rm -fr ./*
     cp -R ../build/ists_prod/* .
     cp -R ../src/resources/docs .
     cd ..
  elif [[ "$1" == "dist_proscan_prod" ]]; then
     cd "$1"
     rm -fr ./*
     cp -R ../build/proscan_prod/* .
     cp -R ../src/resources/docs .
     cd ..
  else
     echo "Copies the current state of the local build to the local branch named 'dist_xxx'."
  fi
}

# Commits the current state of the local worktree named 'dist_xxx' to the local branch named 'dist_xxx'.
function __dist_commit() {
  if [[ "$1" == "dist_ilktest_dev" || "$1" == "dist_ilktest_prod" || "$1" == "dist_hipa_prod" || "$1" == "dist_ists_prod" || "$1" == "dist_proscan_prod" ]]; then
     cd "$1"
     git add .
     git diff-index --quiet HEAD || git commit -m "updates" .
     cd ..
  else
     echo "Commits the current state of the local worktree named 'dist_xxx' to the local branch named 'dist_xxx'."
  fi
}

# Pushes the latest information from the local worktree named 'dist_xxx' to update the remote branch named 'dist_xxx'.
function __dist_push() {
  if [[ "$1" == "dist_ilktest_dev" || "$1" == "dist_ilktest_prod" || "$1" == "dist_hipa_prod" || "$1" == "dist_ists_prod" || "$1" == "dist_proscan_prod" ]]; then
     cd "$1"
     git push
     cd ..
  else
     echo "Pushes the latest information from the local worktree named 'dist_xxx' to update the remote branch named 'dist_xxx'."
  fi
}

# Trigger autodeployment on the autodeploy server associated with the supplied dist area."
# Note: the following definitions are based on the assumption of access to the underlying server through an nginx frontend proxy
function __dist_deploy() {
  contentType="Content-Type: application/x-www-form-urlencoded";
  deployCommand="deploy?gitUrl=git@gitlab.psi.ch:wica_panels/gfa-ilk-panels.git";
  devServer="https://gfa-ilk-dev.psi.ch/autodeploy"
  prodServer="https://gfa-ilk-prod.psi.ch/autodeploy"
  hipaProxyServer="https://hipa-ilk-ui.psi.ch/autodeploy"
  istsProxyServer="https://ists-ilk-ui.psi.ch/autodeploy"
  proscanProxyServer="https://proscan-ilk-ui.psi.ch/autodeploy"
  if [[ "$1" == "dist_ilktest_dev" ]]; then
     curl -k -H "${contentType}" -X POST "${devServer}/${deployCommand}";
  elif [[ "$1" == "dist_ilktest_prod" ]]; then
     curl -k -H "${contentType}" -X POST "${prodServer}/${deployCommand}";
  elif [[ "$1" == "dist_hipa_prod" ]]; then
     echo "Note: Automatic deployment is not possible to the HIPA machine network !"
     echo "Note: Manual deployment is possible by navigating your browser to the proxy server here: $hipaProxyServer/service-configuration/html"
  elif [[ "$1" == "dist_ists_prod" ]]; then
     echo "Note: Automatic deployment is not possible to the ISTS machine network !"
     echo "Note: Manual deployment is possible by navigating your browser to the proxy server here: $istsProxyServer/service-configuration/html"
  elif [[ "$1" == "dist_proscan_prod" ]]; then
     echo "Note: Automatic deployment is not possible to the PROSCAN machine network !"
     echo "Note: manual deployment is possible by navigating your browser to the proxy server here: $proscanProxyServer/service-configuration/html"
  else
     echo "Trigger deployment by the autodeploy server associated with the supplied dist area."
  fi
}


# Assembles and commits the current state of the local worktree named 'dist_xxx', pushes it to the remote branch
# named 'dist_xxx' and triggers deployment to the remote filesystem by the relevant autodeploy server.
function __dist_publish() {
  if [[ "$1" == "dist_ilktest_dev" || "$1" == "dist_ilktest_prod" || "$1" == "dist_hipa_prod" || "$1" == "dist_ists_prod" || "$1" == "dist_proscan_prod" ]]; then
     __dist_assemble "$1"
     __dist_commit "$1"
     __dist_push "$1"
     __dist_deploy "$1"
  else
     echo "Commits the current state of the local worktree named 'dist_xxx' and pushes it to the remote branch named 'dist_xxx'."
  fi
}

# Deletes and removes the local worktree and branch named 'dist_xxx'.
function __dist_worktree_clean() {
  if [[ "$1" == "dist_ilktest_dev" || "$1" == "dist_ilktest_prod" || "$1" == "dist_hipa_prod" || "$1" == "dist_ists_prod" || "$1" == "dist_proscan_prod" ]]; then
     rm -fr "$1"
     git worktree remove "$1"
     git branch -D "$1"
  else
     echo "Deletes and removes the local worktree and branch named 'dist_xxx'."
  fi
}

# Assembles the data to publish for the local worktree ('dist_ilktest_dev')
function __dist_assemble_dev() {
  __dist_assemble "dist_ilktest_dev"
}

# Assembles the data to publish for the local worktree ('dist_ilktest_prod')
function __dist_assemble_prod() {
  __dist_assemble "dist_ilktest_prod"
}

# Assembles the data to publish for the local worktree  ('dist_hipa_prod')
function __dist_assemble_hipa() {
  __dist_assemble "dist_hipa_prod"
}

# Assembles the data to publish for the local worktree  ('dist_ists_prod')
function __dist_assemble_ists() {
  __dist_assemble "dist_ists_prod"
}

# Assembles the data to publish for the local worktree  ('dist_proscan_prod')
function __dist_assemble_proscan() {
  __dist_assemble "dist_proscan_prod"
}

#-------------------------------------------------------------------------------
# PUBLIC TARGETS for use by NPM package.json' scripts
#-------------------------------------------------------------------------------

# Publishes the local worktree ('dist_ilktest_dev')
function dist_publish_ilktest_dev() {
  __dist_publish "dist_ilktest_dev"
}

# Publishes the local worktree ('dist_ilktest_prod')
function dist_publish_ilktest_prod() {
  __dist_publish "dist_ilktest_prod"
}

# Publishes the local worktree ('dist_hipa_prod')
function dist_publish_hipa_prod() {
  __dist_publish "dist_hipa_prod"
}

# Publishes the local worktree ('dist_ists_prod')
function dist_publish_ists_prod() {
  __dist_publish "dist_ists_prod"
}

# Publishes the local worktree ('dist_proscan_prod')
function dist_publish_proscan_prod() {
  __dist_publish "dist_proscan_prod"
}

#  Creates the local worktrees ('dist_ilktest_dev', 'dist_ilktest_prod', 'dist_hipa_prod' and 'dist_proscan_prod' ).
function dist_worktrees_create_all() {
  __dist_worktree_create "dist_ilktest_dev"
  __dist_worktree_create "dist_ilktest_prod"
  __dist_worktree_create "dist_hipa_prod"
  __dist_worktree_create "dist_ists_prod"
  __dist_worktree_create "dist_proscan_prod"
}

#  Deletes and removes the local worktrees ('dist_ilktest_dev', 'dist_ilktest_prod', 'dist_hipa_prod' and 'dist_proscan_prod' ).
function dist_worktrees_clean_all() {
  __dist_worktree_clean "dist_ilktest_dev"
  __dist_worktree_clean "dist_ilktest_prod"
  __dist_worktree_clean "dist_hipa_prod"
  __dist_worktree_clean "dist_ists_prod"
  __dist_worktree_clean "dist_proscan_prod"
}
