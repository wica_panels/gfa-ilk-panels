# Overview

This project has been archived as it is now OBSOLETE.

The project now has a new home at the following location: https://git.psi.ch/controls_ilk_application/ilk-panels

This is the **gfa-ilk-panels**  repository which provides the user interface for the machine protection
systems in HIPA, PROSCAN and at the Interlock Test Stand ('ILKTEST'). 
 
The project is an NPM project which uses the ```rollup.js``` package to create the site installation
files needed by the following servers:
* ```proscan-ilk``` - the PROSCAN Production Server.
* ```hipa-ilk``` - the HIPA Production Server.
* ```gfa-ilk-prod``` - the ILKTEST Production Server (which includes read-only access to HIPA and PROSCAN).
* ```gfa-ilk-dev``` - the ILKTEST Development Server (used for software development). 

The project structure is organised by feature, and currently (2021-05-28) supports the functionality
required by the machine operators ('ui'), the tools needed by the interlock technical experts ('tools') 
and the functionality needed for integration testing ('test').

The current implementation uses the 'transform' and 'rename' features provided by the ```rollup-copy-plugin```
to customise the files held in the project templates areas for each installation site.

# How to build

The ```*:*:build``` scripts populate the build area with the installation files needed for the different sites:
```
  npm run <facility>:<target>:build
  
  Where:
     <facility> = 'hipa', 'proscan' or 'ilktest' 
     <target> = 'prod' (all facilities) or 'dev' (only 'ilktest').  
```

# How to publish and deploy

The ```*:*:publish``` script pushes the installation files created in the build area to the appropriate
deployment branches on the GitLab Server from where they are deployed on the relevant servers.
```
  npm run <facility>:<target>:publish
  
  Where:
     <facility> = 'hipa', 'proscan' or 'ilktest' 
     <target> = 'prod' (all facilities) or 'dev' (only 'ilktest').
```

# Project Changes and Tagged Releases

* See the [CHANGELOG.md](CHANGELOG.md) file for further information.
