# Overview

This log describes the functionality of tagged versions within the repository.

# Tags  
* [1.0.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.0.0)
  Initial release created from old wica_panels project.

* [1.1.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.1.0)
  Restored basic functionality and added database server query tool.

* [1.2.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.2.0)
  Provides first production release of the following tools:
  * Facility Overview Tool
  * ILKCS Live Data Query Tool
  * ILKDB Config Query Tool.

  Dependencies:G
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) [V2.1.0](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory/-/tags/2.1.0)
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V1.2.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V1.2.0.

  Known Bugs:
  Not yet assessed.

  Release Date: 2020-11-05
  
* [1.3.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.3.0)

  Changed banner to say "Machine Protection System".
  Added support for connected modules report. Added support for enabling/disabling modules and 
  for clearing all interlocks.
  Add support for leveraging new REST possibilities from ILKCS.
  Add improved register write support (hover color, change of pointer). 
  Add support for colouring USI bridges orange.
  
  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) [V2.2.0](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory/-/tags/2.1.0)
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V0.3.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V1.4.0.

  Known Bugs:
  Not yet assessed.

  Release Date: 2020-12-23   

* [1.4.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.4.0)

  Changed banner to say "Machine Protection System".
  Added support for connected modules report. Added support for enabling/disabling modules and
  for clearing all interlocks.
  Add support for leveraging new REST possibilities from ILKCS.
  Add improved register write support (hover color, change of pointer).
  Add support for colouring USI bridges orange.

  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) [V2.2.0](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory/-/tags/2.1.0)
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V0.3.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V1.6.0.

  Known Bugs:
  Not yet assessed.

  Release Date: 2020-01-19

* [2.0.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/2.0.0)

  Create new release with all debug tools working.
  
  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) [V2.4.1](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory/-/tags/2.4.1)
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V1.0.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V2.0.1.

  Known Bugs: 
  * Not yet assessed.
  
  Release Date: 2021-03-04

* [2.2.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/2.2.0)

  Release used for first round of HIPA testing on 2021-04-20.

  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) [V2.4.1](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory/-/tags/2.4.1)
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V1.1.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V2.1.1.

  Known Bugs:
  * See test report described in [INTLOCUPG-66](https://jira.psi.ch/browse/INTLOCUPG-66)

  Release Date: 2021-04-20

* [2.3.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/2.3.0)

  Release used for second round of HIPA testing on 2021-05-18.

  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) [V2.4.2](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory/-/tags/2.4.2)
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V1.2.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V2.1.3.

  Known Bugs:
  * To be assessed.
  
  Release Date: 2021-05-18

* [2.4.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/2.4.0)

  Project tree refactored based on templates and subs files.

  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) [V2.4.2](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory/-/tags/2.4.2)
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V1.2.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V2.1.3.

  Known Bugs:
  * To be assessed.

  Release Date: 2021-05-26

* [2.4.1](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/2.4.1)

  Upgraded rollup to latest.

  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) [V2.4.2](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory/-/tags/2.4.2)
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V1.2.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V2.1.3.

  Known Bugs:
  * To be assessed.

  Release Date: 2021-05-26

* [2.4.2](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/2.4.2)

  Completed overhaul of build system.

  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) [V2.4.2](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory/-/tags/2.4.2)
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V1.2.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V2.1.3.

  Known Bugs:
  * To be assessed.

  Release Date: 2021-05-26

* [2.4.3](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/2.4.3)

  Add first proper support for HIPA, PROSCAN and ILKTEST beam permission and interlock views.

  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) [V2.4.2](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory/-/tags/2.4.2)
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V1.5.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V2.3.0.

  Known Bugs:
  * To be assessed.

  Release Date: 2021-08-22

* [2.5.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/2.5.0)

  Changed SIIO to eliminate R34. Changed Mode Option to Mode Qualifier.

  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) [V2.5.0](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory/-/tags/2.4.2)
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V2.0.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.0.0.

  Known Bugs:
  * To be assessed.

  Release Date: 2021-09-03

* [3.0.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.0.0)
  * Changed beamline before stopper to show go color even when section mode is Stop.
  * BUG FIX: added missing sections $BMD2 and #GNT2.
  * BUG FIX: swapped $BR and #RING
  * Change section stop color to lightcoral for improved contrast.
  * Prepared for HIPA testing on Nov 4th 2021
  
  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) [V2.6.0](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory/-/tags/2.6.0)
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.0.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.1.0.

  Known Bugs:
  * To be assessed.

  Release Date: 2021-11-12

* [3.1.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.1.0)
  * Backup latest version used for HIPA/PROSCAN testing December 2021.

  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.8.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.3.0.
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.1.0
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.1.0

  Known Bugs:
  * To be assessed.

  Release Date: 2021-12-13

* [3.2.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.2.0)
  * 2021 Pre-christmas release now supports setting facility and individual beamline sections including mode modifiers.

  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.8.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.3.0.
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.2.0

  Known Bugs:
  * To be assessed.

  Release Date: 2021-12-23

* [3.3.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.3.0)
  * Add support for handling PATH updates
  * Add support for connection stream handling to signal display. 
  * Code structure improvements.
  * Lots of consistency cleaning up.
  * Retired babel from build chain as HIPA and PROSCAN CR browsers have now been updated to modern standards.
  * Add support for loading facility configurations from backend.
  * Control System Command Tool now loads relevant facility instance from URL rather than through user choice.
  
  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.9.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.4.0
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.3.0

  Known Bugs:
  * To be assessed.

  Release Date: 2022-01-07

* [3.4.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.4.0)
  * Add initial support for ilk-select
  * Refresh targeted Section View outside of signal updates.
  * Switch from patchStream to normal stream.
  * Improved support for bridge handling when module state is UNKNOWN.
  * Fixed bug in hipa command tool link.
  * Switched <Offline> to UNKNOWN.
  * TEILER section changed to support config options 'L','R' and 'T' (cf previously 'N','A','T')
  * Added support for reporting online and offline IOCs and modules.
  * Added section level support for clearing interlocks and showing or hiding beamline sections.
  * Added initial support (commented out) for initStore.
  
  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.9.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.4.0
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.4.0

  Known Bugs:
  * To be assessed.

  Release Date: 2022-01-17

* [3.5.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.5.0)
  * Add support for visualising IOC reboots (detected via RPM PON bit) in Section View.
  * Add support for command buttons on Section Configuration View and Section Interlock View.
  * Add support for showing Command Request Log and Command Response Log in Section Configuration Vew and Section Interlock View.
  * Fixed regression in stream timeout. Now restored to 20s as originally intended.
  * Reduced logging verbosity
  
  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.9.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.5.0
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.5.0

  Known Bugs:
  * To be assessed.

  Release Date: 2022-01-19

* [3.6.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.6.0)
  * Add support for disabling unused sections when setting facility mode.
  * Add support for writing mode setting request into and bridge setting requests into screen log (Expert User).
  * Add support for visualising sectionVisibility.
  * Add support for HIPA hover function.
  * Further improvements to SectionView.
  * Add support for reporting which IOCs would need to be rebooted to bring a section online.
  * Fixed bug in USI2 bridge detection.

  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.9.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.5.0
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.6.0

  Known Bugs:
  * To be assessed.

  Release Date: 2022-01-27

* [3.7.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.7.0)
  * Update ILKTEST view to support latest situation (XML from Patric's autogenerator).
  * Removed Facility Select from ILKDB Explorer.
  * Removed Facility Select from Facility Overview.
  * Add support for new MONITOR module.
  * Add support for reporting on failed connections.
  * Now supports NOBRIDGE="NO" bridging policy.
  * Now dims main view when stream connection offline.
  * PROSCAN: fixed bug in showing rebooted parts of the facility.
  * ILKTEST: add support for StopperProtection operating mode.
  * Create release 3.7.0

  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.10.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.6.0
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.7.0

  Known Bugs:
  * To be assessed.

  Release Date: 2022-02-11

* [3.7.1](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.7.1)
  * BUG_FIX: Now bundles [ilk-wc-factory] 2.10.1 with fix for broken bridging.
  * BUG_FIX: Interlock view now handles SVG initialisation like Config View (fixed issue with Firefox).
  * ENHANCEMENT: now supports separate target section views (all signals/ilk-relevant signals)
  * Create release 3.7.1

  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.10.1
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.6.0
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.7.2

  Known Bugs:
  * To be assessed.

  Release Date: 2022-02-14

* [3.8.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.8.0)

Release was made in preparation for first Operator Acceptance tests on March 15th.

  * BUG_FIX: Menu positioning (problem reported by Patric).
  * Fixed problem with broken selection widgets.
  * Fixed bug with port and connection fault filtering.
  * Renamed CSS variable file to show that the variables may not only be colors.
  * Moved facility overview colors into external variable file.
  * Create legend graphic showing colors for facility SVG files.
  * HIPA: updated SVG graphic for new USI2-based topography.
  * HIPA: preserve old-graphic without legend and without new UIS2-based topography. (In case we decide to rollback).
  * ILKTEST: add hover over beamline section support.
  * HIPA: Remove terser support as it was affecting the ability to debug using sourecemaps.
  * PROSCAN: Remove terser support as it was affecting the ability to debug using sourecemaps.
  * ILKTEST: Remove terser support as it was affecting the ability to debug using sourecemaps.
  * Moved facility overview colors into external variable file.
  * Add support for legend graphic.
  * Renamed CSS variable file to show that the variables may not only be colors.
  * Add support for InterlockLog window.
  * Add support for display preferences for reversing signal direction and for showing register bits.
  * Add support for more detailed module view.
  * Removed redundant function: addCheckboxWidgetOptions.
  * Added support for setting colors on SVG facility graphic according to colors on SVG legend graphic.
  * BUG FIX: Include credentials when performing HTTP GET operations. Fixes one of the problems when accessing the appplication through the AIT Web Application Firewall (WAF).
  * HIPA: adapt property manager to support new USI2-based facility graphic.
  * Moved facility overview colors into external variable file.
  * Backup latest UI files.
  * Upgrade dependencies. 
  * Capture latest dependencies.
  * Adapted for latest API using "traceable" and "ILK" keywords
  * Section view now shows module outputs first.
  * Changed signal background to transparent for improved contrast.
  * Add support for styling module input and output labels
  * Allow extra space for display preferences when in Expert Mode.
  * Add initial support for Operator View
  
  Dependencies:
  * Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.11.0
  * Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.8.0
  * Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.9.0

  Known Bugs:
  * To be assessed.

  Release Date: 2022-03-14

* [3.9.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.9.0)

Last tweaks in preparation for first Operator Acceptance tests on March 15th.

  * Improve naming: updateIlkRelevantSectionViewSignals -> updateTargetSectionViewSignals.
  * BUG FIX: fix incorrect name in ILKTEST for hipa operator view.
  * DEPENDENCY: use @psi/ilk-wc-factory 2.12.0 which supports showing view categories alongside signal names.
  * ENHANCEMENT: Add initial support for showing beam path on operator view.
  * Create release 3.9.0
  
Dependencies:
* Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.12.0
* Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.8.0
* Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.9.0

Known Bugs:
* To be assessed.

Release Date: 2022-03-14

* [3.9.1](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.9.1)

This release was to bring PROSCAN back to a functional and consistent state with HIPA following
the introduction of the HIPA Operator View.

PROSCAN now has an Operator View.

* Added slides for presentation on Tuesday's first User Acceptance Meeting.
* BUG FIX: PROSCAN: Fix regression bug with config view.
* BUG FIX: PROSCAN: Fix regression bug with interlock view.
* ENHANCEMENT: PROSCAN: Add initial support for operator view.
* BUG FIX: HIPA: Fix minor inconsistency with beamsplitter.
* Create release 3.9.1

Dependencies:
* Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.12.0
* Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.8.0
* Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.9.0

Known Bugs:
* This release and earlier ones makes use of JS private methods which were not supported in the
  Apple Safari browser before version 15.4.

Release Date: 2022-03-17


* [3.10.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.10.0)

This release was made in preparation for second operator meeting.

Main changes:
* Now supports two views: "Interlock View" (previously called "Operator's View") and "Configuration View".
* Support for Strong Colors Theme.
* Support for "info sections" and "extra sections". Patric can hopefully explain the difference ! 
* Now dims out sections that are not in use.
* Signals now show "ILK" text if they can be cleared by clicking.
* Modules now show which sections they are in.
* Increased size of graphics window. 
* Various bug fixes and Look and Feel improvements.

Dependencies:
* Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.12.2
* Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.8.2
* Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.10.0

Known Bugs:
* This release and earlier ones makes use of JS private methods which were not supported in the
  Apple Safari browser before version 15.4.
* There is a problem with the signal ILK clear functionality that Patric reported that has not yet been fixed.
* The infosections and extrasections do not go into an out-of-service state correctly.

Release Date: 2022-03-31

* [3.10.1](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.10.1)

Main changes:
* This release was made in preparation for third operator meeting. 
* Biggest functional change is to the PROSCAN views which now show the extra beamline sections. 
* Several other tweaks and improvements.

Changes from Git history:
* Clean up naming: change all instances of 'config' to 'configuration'.
* Change to application menu structure. Menu will now include all debug tools (except facility overview). 
  Operator View has been retired for the time being - will probably later be repurposed to hardware-view.
* Add support for PROSCAN extra sections.
* Cleanup SVG update implementation.
* Tweak SINQ label to make it properly visible.
* Retire operator view -> now replaced with interlock view.
* Restore JIRA FEEDBACK link.
* Add hyperlink to git CHANGELOG from version information.
* Add support for publishing plantUML architecture drawings.
* Update presentation for second operators meeting.
* Upgrade all dependencies to latest
* Create release 3.10.1

Dependencies:
* Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.12.2
* Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.8.2
* Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.11.0

Known Bugs:
* This release and earlier ones makes use of JS private methods which were not supported in the
  Apple Safari browser before version 15.4.
* There is a problem with the signal ILK clear functionality that Patric reported that has not yet been fixed.
* The infosections and extrasections do not go into an out-of-service state correctly.

Release Date: 2022-04-08


* [3.11.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.11.0)

Main changes:
* This release was made in preparation for third operator meeting. Then finalised during the evening afterwards.
* Biggest functional change is to the PROSCAN views which now show the extra beamline sections.
* Several other tweaks and improvements. For example...
* Facility configuration now works via three level menu system.
* Strong Colors are now the default.
* Separate check box for openeing command log (which isn't normally required by the operators).
* Better support for Interlock History
* Improved organisation of section signals within section signal group container.

Changes from Git history:
* Fixed incorrect Git link to usi2 firmware project.
* Add support for clicking; turned select-on-hover into a display preference.
* Turn strong colors theme into the default; add muted colors theme.
* BUG FIX: hover no longer changes color of text labels.
* ENHANCEMENT: add support for automatically selecting appropriate sections and modules when clicking.
* BUG FIX: added support for stopper protection ('P') mode.
* ENHANCEMENT: Add support dor clearing interlock history.
* BUG FIX: add await to async function call.
* BUG FIX: put opacity value in quotes.
* Create release 3.11.0

Dependencies:
* Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.13.2
* Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.8.2
* Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.12.0

Release Date: 2022-04-12


## [Version 3.12.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/3.12.0)

### Main changes:
-This release was made in preparation for fourth operator meeting on April 26th 2022.

### Changes from Git history:
- CHORE: Add state managers for UI views.
- ENHANCEMENT: added widget labels.
- ENHANCEMENT: added Select Section and Select Module widgets.
- ENHANCEMENT: add support for clearing module interlocks.
- BUG FIX: Don't offer Secondary Mode options where there are none available.
- CHORE: use new state manager.
- ENHANCEMENT: show server operating ports on architecture drawing.
- ENHANCEMENT: don't show 'Please select XXX' text on select widgets where there are no options available.
- BUG FIX: initialise ilk-signal-group web component. Today 21:49 rees_s b47863db
- CHORE: remove redundant operator view.
- BUG FIX: fix broken link.
- BUG FIX: change WBGB Machine Operator to GFA Machine Operator on architecture drawing.
- BUG FIX: fix regression bug when selecting muted colors theme.
- CHORE: update dependencies to latest.
- Create release 3.12.0

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.13.1
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V3.9.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V3.13.0

### Release Date
* 2022-04-24

## [Version 5.0.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.0.0)

### Description
- - First release in new series to handle HIPA USI2 topology. Functionally this release
    is the same as 3.12.0.

### Git Commit Information
- Create release 5.0.0

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.13.1
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.0.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.0.0

### Release Date
* 2022-04-28


## [Version 5.1.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.1.0)

### Description
- ENHANCEMENT: Add support for EPICS style colourisation when signal state is UNKNOWN.
- ENHANCEMENT: changed colourisation of port cell and logic cell to more neutral color (grey).
- CONFIG: HIPA: change hover effect to use CSS filter drop-shadow.
- CONFIG: ILKTEST: change hover effect to use CSS filter drop-shadow.
- CONFIG: PROSCAN: change hover effect to use CSS filter drop-shadow.
- CONFIG: PROSCAN: clean up beamline sections on SVG graphic.

### Git Commit Information
- Create release 5.1.0

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.14.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.0.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.0.0

### Release Date
* 2022-05-04


## [Version 5.1.1](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.1.1)

### Description
- ENHANCEMENT: revert colourisation of port cell and logic cell so that NOK status is as before (burlybrown).

### Git Commit Information
- Create release 5.1.1

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.14.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.0.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.2.0

### Release Date
* 2022-05-09


## [Version 5.2.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.2.0)

### Description
- ENHANCEMENT: revert colourisation of port cell and logic cell so that NOK status is as before (burlybrown).
- ENHANCEMENT: Add support for showing lastUpdateTime in header.
- ENHANCEMENT: Updated for compatibility with latest backend implementation of facility patch stream.
- ENHANCEMENT: add support for interlock history.

### Git Commit Information
- Create release 5.2.0
- ENHANCEMENT: Add support for showing lastUpdateTime in header.
- ENHANCEMENT: reduce width of interlock width cause cell to allow room for longer timestamp.
- ENHANCEMENT: add support for interlock history.
- ENHANCEMENT: updated for compatibility with latest backend SSE stream implementations.
- ENHANCEMENT: updated for compatibility with latest backend implementation of facility patch stream.
- ENHANCEMENT: Switch to using FacilityControllerPatchStream.
- CHORE: Removed redundant updateTargetSectionSignalsView and clearInterlockHistory. Improved doc comments.
- ENHANCEMENT: Provides support for new facility diff stream.
- CHORE: Simplify state managers to store data using user supplied string key.
- ENHANCEMENT: Update to later (2.15.0) version of ilk-wc-factory. (Improved interlock data decoding and now sperates inputs and outputs in module view)
- ENHANCEMENT: Revert colourisation of port cell and logic cell so that NOK status is as before (burlybrown).
- API: Change facility stream name to 'ev-ilk-facility-state'.

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.17.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.0.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.4.0

### Release Date
* 2022-05-29


## [Version 5.3.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.3.0)

### Description
- Implement usability enhancements following discussion with Patric. Merged in changes from V4 branch.

### Git Commit Information (the most important ones)
- Create release 5.3.0
- Commit Patric aesthetic changes.
- Allow more horiz space for SVG.
- Commit latest Patric improvements.
- Make Legend smaller.
- Make signals less wide.
- Add support for testing extraInfo channels.
- Simplify text on beam path setting.
- Make adaptions following discussions with Patric.
- BUG FIX: reinitialise state after disconnection event.
- Add initial support for watermarks.
- ENHANCEMENT: Increase line length from 120 to 160 in command and debug log windows.
- Add support for showing software arch via link on footer.
- ENHANCEMENT: Add Extra Info faults to interlock fault window.
- ENHANCEMENT: ILKTEST: Clean up beam config menu options.
- Add support for rendering interlock history from backend data array.

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.20.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.0.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.6.0


### Release Date
* 2022-07-21


## [Version 5.4.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.4.0)

### Description
- Merged in changes from V4 branch including better tooltips and beam stopper visualisation.

### Git Commit Information (the most important ones)
- Create release 5.4.0
- Update deps to latest.
- Removed old hipa graphic.
- CHORE: Change hover popup text to blue.
- ENHANCEMENT: Add support for beamstopper state visualisation.
- Eliminate use of part definition

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.22.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.1.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.8.0

### Release Date
* 2022-07-29


## [Version 5.5.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.5.0)

### Description
- ENHANCEMENT: Add support for showing the logged in user associated with the stream
- BUG FIX: Now provide credentials on all fetch operations.

### Git Commit Information (the most important ones)

- ENHANCEMENT: Add support for extracting and displaying user from stream.
- BUG FIX: Provide credentials when fetching.
- CHORE: Remove superfluous whitespace. 
- CHORE: Create release 5.5.0

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.22.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.1.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.8.0

### Release Date
* 2022-08-03


## [Version 5.6.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.6.0)

### Description
- ENHANCEMENT: Add support for downloading interlock history or copying it to the clipboard.

### Git Commit Information (the most important ones)
- ENHANCEMENT: Now uses fracturedjson smart formatter to format JSON associated with interlock history downloads or 
  copy-to-clipboard operations.
- ENHANCEMENT:Add initial support for downloading interlock history or copying it to the clipboard.
- CHORE: Create release 5.6.0

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.22.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.1.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.8.0

### Release Date
* 2022-08-03


## [Version 5.7.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.7.0)

### Description
- ENHANCEMENT: Add support for downloading interlock history or copying it to the clipboard.

### Git Commit Information (the most important ones)
- Pack checkbox preferences more closely so that more can be fitted in.
- Tweak size of download interlock history icon.
- Remove support for copy to clipboard.
- Change Display Preferences -> Preferences.
- Add 'Download content as JSON' preference.
- Add icon for 'Download content as JSON'.
- Add support for download interlock history as CSV.
- Add support for download interlock history as JSON.
- Change name: setUserProfile -> setSignalPreferences.
- Change name: setUserProfile -> setUserPreferences.
- Add support for download content as Json widget.
- Add support for two types of download.

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.22.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.1.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.8.0

### Release Date
* 2022-08-11


## [Version 5.8.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.8.0)

### Description
- ENHANCEMENT: Add support for downloading interlock history or copying it to the clipboard.

### Git Commit Information (the most important ones)
- Start using wc snapshots again.
- Fixed typo in comment.
- BUG FIX: Fix regression bug in Show New Interlocks.
- BUG FIX: Eliminated inappropriate use of plural form.
- CHORE: consistently extract data from JSON message structures using bracket rather than dot notation.
- BUG FIX: Address problem with interlock history header.
- ENHANCEMENT: Add support for user information popups.
- ENHANCEMENT: Add support for statistics view.
- CHORE: Create state-manager class which is flexible enough to be shared.
- ENHANCEMENT: Add support for statistics view.
- ENHANCEMENT: Add support for module expert view and statistics view.
- ENHANCEMENT: Add support for reporting type-specific registers.
- ENHANCEMENT: switch to later version of web components with improved expanded module support.
- CHORE: Add support for possibility of using nginx forward web proxy to create uniform URL addressing space.
- CHORE: Add icons for users to header.
- CHORE: Fix menu position.
- BUG FIX: Remove download icon.
- CHORE: Remove unused formatter.
- CHORE: Unpack JSON response using [ "xxx" ] notation.
- Remove indent.

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.23.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.2.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.11.0

### Release Date
* 2022-08-31


## [Version 5.9.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.9.0)

### Description
- Adds initial support for application sound.
- Now colorizes current user and other user icons according to the http session owners.
- Many other small tweaks and bug fixes.

### Git Commit Information (the most important ones)
- ENHANCEMENT: Add support for ILK favicon.
- ENHANCEMENT: Add support for application sound.
- ENHANCEMENT: Add support for clicking current user to login to ILKCS backend server.
- CHORE: Add support for decoding currentUSerSession and otherUSerSession information from facility data stream. AAlso, add support for application sound.
- BUG FIX: Fix broken endpoint spec so that unused facility sections are hidden.
- ENHANCEMENT: eliminate privileged user checkbox as this setting is now determined from the stream state.
- ENHANCEMENT: Add support for separate function to set signal privileged mode.
- ENHANCEMENT: change icon color scheme to eliminate red and to show logged-out users and observer users in different colors.
- CHORE: Improve log messages on stream open and stream error.
- ENHANCEMENT: Add mechanism for detecting and recovering from F5 proxy session timeout.
- CHORE: change JSON stream data extraction notation from dot to brackets for consistency.
- ENHANCEMENT: Upgrade user session reporting for consistency with main interlock page.
- ENHANCEMENT: Add support for get facility user sessions.
- CHORE: Update PROSCAN configuration for compatibility with upcoming nginx proxy.

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.23.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.2.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.13.0

### Release Date
* 2022-09-24

## [Version 5.10.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.10.0)

### Description
- Merge in changes from hipa_v5_new_beamline_topo. Includes the following:
- Add support for AVKI kicker visualisation.
- Add support for greying out user icons when disconnected.
- Stream heartbeat is now acknowledged which opens up path to reducing inactivity session timeout.
- Other cleanups.

### Git Commit Information (the most important ones)
- Merge in changes from hipa_v5_new_beamline_topo.

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.23.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.2.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.16.0

### Release Date
* 2022-10-23


## [Version 5.11.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.11.0)

### Description
- Main change is support of modal popup when command fails.

### Git Commit Information (the most important ones)
- ENHANCEMENT: Add support for popping up modal error message on command failure.
- BUG FIX: fixed broken links for software architecture and medical expert view.
- ENHANCEMENT: Add sound support to configuration view.
- ENHANCEMENT: improve layout of module expert view.
- CHORE: Change sound support: 'HIPA reset interlocks' ->'reset interlocks'.
- Upgrade package lock to latest.

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.24.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.2.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.17.0

### Release Date
* 2022-10-25


## [Version 5.12.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.12.0)

### Description
- Add support for reporting currentFacility interlocks. Interlock history now supports rolling time window.
  Interlock statistics time windows can be selected.

### Git Commit Information (the most important ones)
- ENHANCEMENT: Add support for reporting on current facility interlocks.
- CHORE: tidy up streamName -> sseEventType.
- ENHANCEMENT: updates to interlock signal group now supports rolling time window.
- ENHANCEMENT: updates to interlock statistics now supports rolling time window.
- ENHANCEMENT: add support for current interlocks panel.
- CHORE: clean up UNKNOWN_FACILITY_STATE so that it is an empty object which corresponds to the initial state on the backend.
- CHORE: created patchFacilityState function.
- CHORE: Changed function to not return anything.

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.24.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.2.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.18.0

### Release Date
* 2022-11-06


## [Version 5.13.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.13.0)

### Description
- Merge in changes from hipa_4_old_beamline_topo. Includes the following changes:
- CHORE: update autodeployment URL to go through nginx frontend proxy.
- ENHANCEMENT: update graphics to support showing facility configuration.
- ENHANCEMENT: add support for displaying facility configuration from backend server.
- ENHANCEMENT: restore position of interlock history buttons.
- CHORE: clean up user preferences.
- ENHANCEMENT: added user sound preferences option.
- ENHANCEMENT: eliminate sound support when resetting interlocks.
- ENHANCEMENT: add support for announcing changes to the number of current interlocks.
- ENHANCEMENT: clean up look, mostly fonts.
- ENHANCEMENT: add support for synchronizing facility configuration settings on page load.
- ENHANCEMENT: add support for disabling sound.
- ENHANCEMENT: eliminate primary and secondary selection options as these should be visible anyway (beamline sections that are unused will be dim).
- ENHANCEMENT: optimise panel sizes for display aspect ratios that one typically finds in the WBGB Control Room.
- ENHANCEMENT: add support for playing recorded sounds.
- ENHANCEMENT: clean up PROSCAN graphic (eg move to Helvetica font).
- ENHANCEMENT: clean up ILKTEST graphic (eg move to Helvetica font).
- CHORE: add missing semicolon.
- CHORE: cleanup ILKTEST view.
- CHORE: change name for consistency.
- BUG FIX: add support for synchronizing configuration settings.

### Git Commit Information (the most important ones)
- Merge in changes from hipa_v4_new_beamline_topo.
- Create release 5.13.0

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.24.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.3.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.19.0

### Release Date
* 2022-11-10

## [Version 5.14.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/5.14.0)

### Description
- This is the first release to support ongoing work during the current HIPA 2023 shutdown. 
- The release works with the new beamline topography that includes the USI2 modules.
- The release reimagines the disparate pages used previously into a single page application with switchable views.
- The release now supports an Operator Quick View feature that replaces the old statistics view.
- The USI2 features are now ready for evaluation and feedback from Patric Bucher.

### Git Commit Information (the most important ones)
- Merge in changes from hipa_4_old_beamline_topo. Includes the following changes...
- ENHANCEMENT: Add support for showing user guide from link on bottom of display.
- ENHANCEMENT: Upgrade user guide to contain links to content pages.
- BUG FIX: Fix bug with legend colours on interlock view.
- ENHANCEMENT: add new sounds.
- ENHANCEMENT: change NOK background color (burlywood) to darker shade to improve constant.
- ENHANCEMENT: change clearable interlock green background to make it consistent.
- ENHANCEMENT: extend sound support to include original tones.
- ENHANCEMENT: remove obsolete muted color theme from menu.
- ENHANCEMENT: change Reset Interlock Modules -> Master Reset.
- ENHANCEMENT: add support for disabling sounds and voices.
- ENHANCEMENT: change Current Interlocks to Active Interlocks.
- ENHANCEMENT: add new SVG symbol 'extrapoint' which is like 'infopoint' but which does not have an "i".
- ENHANCEMENT: add support for new target symbols.
- ENHANCEMENT: add support for extrasection(s).
- ENHANCEMENT: enhance graphic following feedback from operators and patric.
- ENHANCEMENT: add initial support for PROSCAN kicker.
- BUG FIX: Fix regression bug which prevent module expert view from working.
- BUG FIX: Change 'Current Interlocks' -> 'Active Interlocks' for consistency with main interlock page.
- ENHANCEMENT: Resize statistics view to eliminate blank columns.
- CHORE: improve function name.
- ENHANCEMENT: change ordering: put interlock causes on top.
- BUG FIX: fix PROSCAN AMAKI visualisation.
- ENHANCEMENT: increase font size on statistics page.
- ENHANCEMENT: add support for userPrivileges array.
- ENHANCEMENT: add support for bridge SVG icon.
- ENHANCEMENT: upgrade web components to 2.25.0

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.24.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V5.3.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V5.19.0

### Known Issues
- None

### Release Date
* 2023-01-23


## [Version 6.0.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/6.0.0)

### Description
- This is the first release ready for intensive testing before the HIPA 2023 return-to-service.
- The main new feature is automatic back-calculation of the section and facility modes. 
- The software is compatible with the new USI2 March 2023 Software Release.
- The software supports USI2 modules which have more than one output.
- Module view now has coloured borders to reflect interlock and bridge status.
- Bug fix for direction of output signals.
- The software has undergone significant changes and is now ready for testing !

### Git Commit Information (the most important ones)
- This v6 release is a "big-bang" release and there were many, many changes in comparison with the previous
  releases used in production in 2022. Consequently, it is probably not very useful to record all of them here.
  We will start tracking changes again now.

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.27.1
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V6.0.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V6.0.0

### Known Issues
- The ordering of the choices offered in the Configure Facility is not correct.
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currenlty offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-04-03


## [Version 6.1.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/6.1.0)

### Description
- Upgrades to web components to show the associated IOC when a module is offline.
- Add support for renamed HIPA sections. Disable USI2 NOK bridging.

### Git Commit Information (the most important ones)
- CHORE: Update HIPA graphic for compatibility with renamed HIPA sections.
- ENHANCEMENT: Upgrade web components to 2.28.0 (provides IocInfo in compact module view).
- BUG FIX: adjust SVG initial event handling to explicitly support Brave web browser.
- ENHANCEMENT: Disable USI2 NOK bridging.
- ENHANCEMENT: Create release 6.1.0

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.28.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V6.1.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V6.1.0

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-04-12


## [Version 6.2.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/6.2.0)

### Description
- This is mainly a code cleanup release. It fixes bugs with module mode configuration and issues on the command
  and database tools which were not working in all cases on the previous release. It also uses a new release of
  the web components that sorts the modules in increasing order of interlock level.

### Git Commit Information (the most important ones)
- CHORE: harden code by using svg-state constants to define allowed values for ilKStates, cfgStates and svcStates.
- CHORE: Clean up code to eliminate unnecessary align-content for flex containers where nowrap is asserted.
- CHORE: clean up id names for greater consistency.
- CHORE: eliminate error popup. THis utility does not use popups, errors are placed directly in the response window.
- CHORE: Eliminate obsolete code.
- CHORE: Improve JSDoc.
- CHORE: Improve code structure.
- CHORE: Add support for buildGroupedSelectWidgetOptionsFromIocId.
- CHORE: further cleanup for consistency.
- CHORE: eliminate obsolete code.
- CHORE: clean up for consistency with other views.
- ENHANCEMENT: eliminate obsolete buttons to set service state.
- ENHANCEMENT: Update interlock module container to sort modules in increasing order of interlock level.
- ENHANCEMENT: Create release 6.2.0
- BUG FIX: Fix module expert view configure mode feature so that it works on all module types.


### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.29.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V6.1.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V6.1.0

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-04-16


## [Version 6.3.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/6.3.0)

### Description
- Fixes a bug with the interlock history sorting, adds support for section interlock reset; a few other
  minor tweaks.

### Git Commit Information (the most important ones)
- CHORE: cleanup formatting.
- BUG FIX: Synchronize FacilityModeOption select widget.
- BUG FIX: Fix issue with incorrect sort on timestamps.
- ENHANCEMENT: now sorts current interlocks timestamp combined with interlock history timestamps.
- ENHANCEMENT: now enables section command button(s) when new section selected.
- CHORE: improve comment.
- ENHANCEMENT: Improve consistency of "Please select..." string when setting module mode options.
- ENHANCEMENT: Improve consistency of module mode options label.
- ENHANCEMENT: add support for clearing section interlocks.
- CHORE: format change only.
- BUG FIX: Change beemsteerer housing color when cfgState is inconsistent.
- ENHANCEMENT: Change component housing color when cfgState is inconsistent.
- CHORE: eliminate blank line.
- ENHANCEMENT: Create release 6.3.0
- ENHANCEMENT: Create release 6.3.0


### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.29.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V6.1.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V6.2.0

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-04-18


## [Version 6.4.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/6.4.0)

### Description
- ENHANCEMENT: Add initial support for bridge policy DYNAMIC (as used at PROSCAN) and Deutsch version of the user guide.

### Git Commit Information (the most important ones)
- ENHANCEMENT: Add initial support for bridge policy DYNAMIC (as used at PROSCAN).
- ENHANCEMENT: Install Deutsch version of the HIPA User Guide.
- ENHANCEMENT: Create release 6.4.0

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.30.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V6.1.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V6.2.0

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-04-22


## [Version 6.5.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/6.5.0)

### Description
- Add support for announcing minor interlocks with different sound (as used on IP-branch at HIPA). Fixed bug with 
  loading graphic on Windows Edge browsers. Changed color for sections which are in STOP mode, so that they look
  different for sections that are configured inconsistently or not configured.

### Git Commit Information (the most important ones)
- ENHANCEMENT: Add support for minor interlocks and associated sound.
- ENHANCEMENT: Add support for Windows edge browser.
- ENHANCEMENT: Adjust colors for increased contrast between sections that are in stop mode and sections which are in an inconsistent state ("not configured").
- CHORE: eliminate redundant lines.
- ENHANCEMENT: Create release 6.5.0

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.30.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V6.3.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V6.7.0 

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-05-23


## [Version 6.5.1](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/6.5.1)

### Description
- Bug Fix release.

### Git Commit Information (the most important ones)
- BUG FIX: Fix bug with all sounds being the same.
- ENHANCEMENT: Create release 6.5.1

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.30.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V6.3.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V6.7.0

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-05-23


## [Version 6.6.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/6.6.0)

### Description
- The database panel has been adapted to a vastly simplified ILKDB backend.
- Now handles situations where the browser prohibits autoplay by popping up an error message and
  forcing user to click.

### Git Commit Information (the most important ones)
- BUG FIX: Adjust ILKTEST interlock announcement types to reflect names of traced interlocks.
- CHORE: Eliminate obsolete support for signal Help table.
- CHORE: Eliminate obsolete support for BDAR table.
- CHORE: Eliminate obsolete support for facility operating modes.
- CHORE: Eliminate obsolete support for mode modifier mappings table.
- CHORE: Change name of WC: name 'ilk-popup' -> 'ilk-popup-http-error'.
- CHORE: Add support for message popup web component.
- ENHANCEMENT: Add support for popup message if audio play is unavailable.
- CHORE: improve name of show panel event.
- CHORE: Adapt for new ILKDB endpoint.
- CHORE: Retire unused WebComponentTableViewer.
- ENHANCEMENT: Update to web components 2.31.0 or later.
- CHORE: Fix broken path.
- CHORE: backup latest.

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.31.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V6.4.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V6.7.0

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-06-06


## [Version 6.7.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/6.7.0)

### Description
- This is mainly a code cleanup release, but there are several feature enhancements and one bug release.
- ENHANCEMENT: Interlock history now shows red-link icon where interlock trace does not identify cause of interlock.
- ENHANCEMENT: All views now pop-up modal error message if the stream from the backend server goes offline.
- ENHANCEMENT: Fault cell is now shown in interlock view. This allows to distinguish between current and cleared ILKs.
- ENHANCEMENT: Update Interlock Causes list to improve situation with respect to long lines.
- ENHANCEMENT: Add support for "Show Signal Navigation" checkbox.
- BUG FIX: watermark logic has changed so it no longer reappears if you switch target section.

### Git Commit Information (the most important ones)
- CHORE: Update node-plugin-resolve to 15.1.0
- CHORE: Update fracturedjsonjs to 3.0.2
- CHORE: Update npm to 9.7.1
- CHORE: Eliminate rollup-plugin-terser
- CHORE: Update eslint file ready for eslint version 9.
- CHORE: Explicitly state that this project creates an ES module.
- CHORE: Upgrade to rollup version 3.
- CHORE: Upgraded for compatibility with v3.x formatter
- CHORE: Improve Jsdoc
- CHORE: Add enhanced support for sound permission popup - now non-closeable.
- CHORE: Add enhanced support for sound stream error handling. Now supports X-ILK header, where available
- CHORE: Now uses HTTP_SUPPORT module.
- CHORE: Now uses and returns promises from HTTP_SUPPORT module.
- CHORE: Now catches and logs failed requests.
- CHORE: Add extra CSS variables for configuring font sizes on signal elements.
- ENHANCEMENT: Add support for signal trace policy icon.
- CHORE: Now explicitly defines projects as ES module.
- CHORE: Now explicitly builds for development.
- ENHANCEMENT: Now uses latest ILK web components (2.31.0)
- CHORE: Now uses latest rollup eslint.
- CHORE: Updated other dependencies.
- CHORE: Updated to latest.
- CHORE: Eliminate obsolete file.
- CHORE: Add extra CSS variables for configuring font sizes on signal elements.
- ENHANCEMENT: Add support for signal trace policy icon.
- ENHANCEMENT: Add support for message popup on stream failure.
- CHORE: made links relative.
- CHORE: Restore statistics view (as it will later turn into diagnostics page)
- CHORE: Eliminate warning suppression.
- CHORE: Now uses and returns promises from HTTP_SUPPORT module.
- ENHANCEMENT: Add jsdoc.
- CHORE: Improve and clarify contract for HTTP Get Requests.
- CHORE: Fix typo.
- CHORE: Further reformatting.
- CHORE: Restructured using async/await paradigm as recommended in Google style guidelines.
- BUG FIX: Fix watermark bugs.
- CHORE: Eliminate obsolete class.
- CHORE: Eliminate unused parameter and cleanup formatting.
- CHORE: Add jsdoc.
- ENHANCEMENT: Show interlock fault cell in interlock history.
- CHORE: made it clear that maxUserRole_ function is private.
- CHORE: Fix typos.
- CHORE: Switch output format to 'es' to more closely comply with rollup standards.
- CHORE: Clean up installation targets so that ILKTEST, HIPA and PROSCAN installations are handled separately.
- CHORE: Backup latest.
- CHORE: reordered for improved consistency.
- CHORE: turn off response logging.
- CHORE: Add ID to main view.
- BUG FIX: Eliminate regression bug: remove statistics page.
- BUG FIX: Expert view now shows full details for signals in interlock history.
- CHORE: Eliminate redundant comma.
- CHORE: Reordered for consistency.
- CHORE: Improve comments.
- CHORE: Improve consistency with respect to watermark handling and page initialisation.
- ENHANCEMENT: Switch to 2.32.0 web components.
- ENHANCEMENT: Simplify text: "Ilk Level" -> "Level".
- CHORE: fix comment capitalization.
- CHORE: Use new CSS var: 'module-content-cell-background-color'.
- CHORE: Reorder for consistency wth interlock-view-vars.css.
- CHORE: Reduce size of text in label cell.
- CHORE: Use new CSS var: 'signal-label-cell-background-color-hover'.
- CHORE: Improve name.
- ENHANCEMENT: Add support for "Show Signal Navigation" checkbox.
- CHORE: Remove obsolete 'hideLabels' attribute.
- CHORE: Use new CSS vars for signal tail and tip cells.
- CHORE: Add missing CSS var: '--module-content-cell-box-shadow-color-normal'.
- CHORE: Add other missing CSS vars.
- CHORE: Change signal name fontsize to 14px for consistency with elsewhere.
- CHORE: Change 'hideLabels' to 'showLabels'.
- CHORE: Upgrade to latest.
- ENHANCEMENT: Add support for setting target section and module on received event.
- ENHANCEMENT: Update Interlock Causes list to improve situation with respect to long lines.
- ENHANCEMENT: Create release 6.7.0

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.31.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V6.4.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V6.7.0

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-06-21


## [Version 6.7.1](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/6.7.1)

### Description

The need for this release was triggered by the information from the control room that disabling voices
also disabled sound. This bug has been fixed, more generally the sound concept has been improved so that
users need to opt into the sounds they require rather than disable them.
Considerable work was put into cleaning up the application startup.
Fixes a bug that was preventing bridges from being rendered on Safari.
Other significant changes are as follows:

- ENHANCEMENT: Add support for invoking startup page according to path variable.
- ENHANCEMENT: Now force users to explicitly enable the sound features that they require, rather than to disable the features that they don't need.
- ENHANCEMENT: Add support for defining initial page using 'page' query parameter.
- ENHANCEMENT: Now listens for target module selection events and selects module accordingly. (Module Expert View)
- BUG FIX: Disabling voice alerts should not disable sound alerts.
- BUG FIX: Switch to using iframe for PDF inclusion => works better on safari.

### Git Commit Information (the most important ones)
- ENHANCEMENT: Add support for invoking startup page according to path variable. 
- CHORE: Split off SVG Init detector into separate module.
- ENHANCEMENT: Now force users to explicitly enable the sound features that they require, rather than to disable the features that they don't need.
- CHORE: No longer display interlock panel by default.
- BUG FIX: Disabling voice alerts should not disable sound alerts.
- BUG FIX: Create release 6.7.1
- CHORE: Formatting change only.
  CHORE: Fix typo in name of variable.
- CHORE: Formatting improvement.
- CHORE: Made user guide the active view on startup.
- CHORE: Improve function name.
- CHORE: Log stream listener start.
- CHORE: Update to latest.
- CHORE: Eliminate view-manager styling as defaults work fine.
- CHORE: Moved svgInit.
- CHORE: Cleaned up page initialisation.
- CHORE: log script completed.
- CHORE: Comment improvement.
- CHORE: Set interlock as active tag and active page.
- CHORE: Reorder for consistency with tab order.
- CHORE: Delete unused statistics view.
- BUG FIX: Switch to using iframe for PDF inclusion => works better on safari.
- CHORE: Eliminate need for browser parser.
- CHORE: Improve name.
- ENHANCEMENT: Add support for defining initial page using 'page' query parameter.
- ENHANCEMENT: Add support for show property and for sending an event when view manager is ready.
- BUG FIX: Improve SVG init detection.
- ENHANCEMENT: Add sequence diagram showing page initialisation.
- CHORE: Cleanup structure slightly. 
- ENHANCEMENT: Now listens for target module selection events and selects module accordingly.
- CHORE: Cleanup await functionality.
- CHORE: Update shoelace to latest (2.5.2).
- ENHANCEMENT: Update software architecture diagram to include NGINX proxy server.
- BUG FIX: harden SVGInit detector for Firefox.

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.33.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V6.4.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V6.8.1

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-07-13


## [Version 6.8.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/6.8.0)

### Description
The Interlock History now provides links between interlock causes and the 
associated module. The Module Expert View now provides active links for 
basic navigation of the module tree.

### Git Commit Information (the most important ones)
- ENHANCEMENT: Fix #16: Provide active link from Interlock Causes to associated module.
- ENHANCEMENT: Fix #5: Add simple tree navigation to Module Expert View.
- ENHANCEMENT: Create release 6.8.0.
- CHORE: Upgrade to web components 2.34.0

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.33.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V6.4.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V6.8.1

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-07-15


## [Version 6.9.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/6.9.0)

### Description
Further improvements to linking.

### Git Commit Information (the most important ones)
- ENHANCEMENT: Improve link support.
- CHORE: Add support for new section-support module.
- CHORE: Eliminate section-support.js
- CHORE: Now send separate events for section and module linking.
- CHORE: Validate sectionId argument.
- CHORE: Add support for target section highlighting.
- ENHANCEMENT: Fix #17: Further improvements to user interface startup linking

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.35.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V6.4.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V6.8.1

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-07-18


## [Version 6.9.1](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/6.9.1)

### Description
Fix scroll bars in module view on linux platforms.

### Git Commit Information (the most important ones)
- BUG FIX: Switch to 2.35.1 web components.
- BUG FIX: Add missing vars.
- BUG FIX: Avoid module view scroll bars on linux platform.

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.35.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V6.4.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V6.8.1

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-07-18


## [Version 7.0.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/7.0.0)

### Description
- Provides first support for ISTS. Provides USI2 interlock timing strip.

### Git Commit Information (the most important ones)
- ENHANCEMENT: Add support for Ion Source Test Stand (ISTS).
- ENHANCEMENT: Add support for USI2 interlock timestrip.
- ENHANCEMENT: Now provide direct menu links to DB and CS tool pages.
- ENHANCEMENT: Eliminate redundant user guide.
- ENHANCEMENT: Switch to 2.36.0 web components with support for interlock timestrip.
- ENHANCEMENT: Create release 7.0.0
- ENHANCEMENT: Define colours for interlock timing strip.
- CHORE: Eliminate redundant InterlockHistory.
- BUG FIX: Fix missing line in build script.
- BUG FIX: Only show timestrips for modules that have at least one interlock transition event. (This suppresses display 
  of interlock timestrips for RPM and SIIO modules).

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.36.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V7.0.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V7.1.0

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-08-08


## [Version 7.1.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/7.1.0)

### Description
- This version adapts the HTTP PUT function to send command objects. This is required for
  compatibility with the ISTS Web Application Firewall (WAF) which detected situations
  where valid JSON was not being sent. Requires ILKCS V7.3.0 or later to correctly decode 
  this new command string format. Also fixes a bug on expert view when following links.

### Git Commit Information (the most important ones)
- CHORE: Improve HTTP to ensure it sends valid JSON.
- BUG FIX: Fix click on link doesn't update module selectors.

### Dependencies:
- Works with [ilk-wc-factory](https://git.psi.ch/controls_highlevel_applications/ilk-wc-factory) V2.36.0
- Works with [ilkdb](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkdb) V7.0.0
- Works with [ilkcs](https://git.psi.ch/controls_highlevel_applications/ch.psi.ilkcs) V7.1.0

### Known Issues
- It is impossible to use the Configure Facility feature when a single IOC is down. For example the upstream
  HIPA IOC ZPSAF31 is currently offline, but it is impossible to configure either the IP or UCN branches. (Individual
  sections can be configured via the Configure Section option).

### Release Date
* 2023-08-16


# Project Ideas Completed during HIPA SD 2023
* Combine Active Interlocks and Interlock History into one list sorted by interlock trigger time.
* Start adding TABS for additional views.
* User Guide should open new TAB or window.
* Hamburger Menu should behave as the firefox one just above it. hove=highlight, click=open menu. menu box should
  start below hamburger symbol. close on click symbol, click outside or any menu item. (Stefan)
* The Interlock Statistics view is not useful as currently used by the operators. The key for them is to be able to
  sit at the shift desks and see a display which shows the current interlock in VERY BIG LETTERS. I think this allows
  them to determine the importance of the audible announcements that they hear from time to time. Maybe we need an
  Operator Quick View.
* Eliminate need to maintain to separate versions for OLD and NEW HIPA.
* Change Intensity Options to High and Low, not HII/disabled.
* Add better support for USI2 as required.

# Project Ideas Backlog - relevant for HIPA SD 2024
* Turn command log into a feature that non-experts might use.
* Fix bug whereby UCN options are offered even when beam is stopped at BX1 or BN5.
* In certain cases (eg 'SAIREM < 50W', 'IVWC_ LK)' the ILK system was being used as an alarm annunciator. Since
  HIPA now has an EPICS Alarm Handler we should consider removing these signals from the list of traceable signals.
* The HIPA-ILK facility settings are not determined from the interlock module states but by the information saved
  in the ILKCS. This means the HIPA-ILK-VIEWER  does not show the Intensity and UCN branch setting correctly, nor
  which sections are in service.
* Try to avoid the situation where applying the facility configuration removes bridges (which then end up needing to be reapplied).
* Consider creating a bridge creation/removal log.
* The interlock history is lost after an ILKCS restart.
* Visualisation around the EHT should be enhanced to show additional elements.
* Add support for system periodic self-checking. (Probably need to create a separate GIT repo for ILK system scripting)
* Add support for PROSCAN RPS Test (see INTLOCUPG-77).
* Add persistence for interlock history list between server restarts.
* Move IP and UCN branch "Not in Use" to end of dropdown list.
* Config Button was grey but value in options were updated correctly. in configuration view. with reselect in
  dropdown of one option the button Config turned to blue/active.(Simon, Stefan)
* Add support for interlock module tracing (old facility hardware overview.)
* Add better support for USI2 as required.
* High Priority: Allow facility configuration when one IOC is down.
* High Priority: Fix Menu Ordering 
* High Priority: Ensure that the interlock history list does not grow infinitely and thereby degrade performance.


# Project Ideas Backlog - on hold: probably not implemented in HIPA SD 2023.
* Make signal BRIDGE functionality always available ?
* Idea from 4th Operator Meeting: thicker lines for sections to get overview from ~10m away.

# Backlog Ideas Implemented
* Add support for BOXNAMEs on Section Signal View.
* Implement facility setting based on 3 menus.
* Implemented some ideas from 4th Operator Meeting.
* Idea from 4th Operator Meeting: blue on hover misleading -> change to different way of feedback e.g. border. USED DROP SHADOW
* Idea from 4th Operator Meeting: OK background not blue -> grey'ish preferred.
* Idea from 4th Operator Meeting: general: no white background except for EPICS connection issue.
* Interlock History doesnt show all data.
* Create separate git branch for non USI2 based view.
* Create release to support HIPA old functionality.
* Always set IPH flag on modules that support it, and eliminate from menu.`
* Implement basic authentication for privileged mode with password that everyone knows.
* Add support for audio -> ask Hubert.
* Need to bring data to the statistics page. Or add new statistics page. Maybe add /statistics endpoint ?
* Explicitly show who is logged in
* Implement LDAP-based authentication for privileged mode.
* Menu options on Configure Facility should synchronize with existing situation.
* Idea from 4th Operator Meeting: bigger drawings for stoppers and magnets.

# Project Ideas Backlog
* Turn command log into a feature that non-experts might use.
* Make signal BRIDGE functionality always available ?
* Fix bug whereby UCN options are offered even when beam is stopped at BX1 or BN5.
* Idea from 4th Operator Meeting: thicker lines for sections to get overview from ~10m away.

